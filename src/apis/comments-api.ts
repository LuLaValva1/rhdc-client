import { HexId, Nullable, Uuid, Weak } from '../util/types';
import { RichUserRef } from './users-api';
import { HttpClient } from './http-client';
import { Page } from './pagination';

interface CommentDataBase {
	commentId: Uuid;
	hackId: HexId;
	author: RichUserRef;
	datePosted: string;
	dateEdited: Nullable<string>;
	message: string;
	actions: string[];
}

interface HackCommentData extends CommentDataBase {
	replyCount: number;
}

interface CommentReplyData extends CommentDataBase {
	parentId: Uuid;
}

export class Comment {

	public readonly commentId: Uuid;
	public readonly hackId: HexId;
	public readonly author: Readonly<RichUserRef>;
	public readonly datePosted: Readonly<Date>;
	public readonly dateEdited: Nullable<Readonly<Date>>;
	public readonly message: string;
	readonly #actions: string[];

	protected constructor( data: HackCommentData | CommentReplyData ) {
		this.commentId = data.commentId;
		this.hackId = data.hackId;
		this.author = Object.freeze( data.author );
		this.datePosted = Object.freeze( new Date( data.datePosted ) );
		this.dateEdited = data.dateEdited ? Object.freeze( new Date( data.dateEdited ) ) : null;
		this.message = data.message;
		this.#actions = data.actions;
	}

	public get canReply() : boolean {
		return this.#actions.includes( 'reply' );
	}

	public get canEdit() : boolean {
		return this.#actions.includes( 'edit' );
	}

	public get canDelete() : boolean {
		return this.#actions.includes( 'delete' );
	}

	public get canReport() : boolean {
		return this.#actions.includes( 'report' );
	}

}

export class HackComment extends Comment {

	public readonly replyCount: number;

	constructor( data: HackCommentData ) {
		super( data );
		this.replyCount = data.replyCount;
		Object.freeze( this );
	}

}

export class CommentReply extends Comment {

	public readonly parentId: Uuid;

	constructor( data: CommentReplyData ) {
		super( data );
		this.parentId = data.parentId;
		Object.freeze( this );
	}

}

export class CommentsApi {

	static getHackCommentsAsync( hackId: HexId, pageSize = 20 ) : Promise<Page<HackComment>> {
		return Page.fetchFirstAsync( `/v3/comments/hack/${hackId}?pageSize=${pageSize}`, (x: HackCommentData) => new HackComment( x ) );
	}

	static getRepliesAsync( commentId: Uuid, pageSize = 20 ) : Promise<Page<CommentReply>> {
		return Page.fetchFirstAsync( `/v3/comments/replies/${commentId}?pageSize=${pageSize}`, (x: CommentReplyData) => new CommentReply( x ) );
	}

	static async getCommentAsync( commentId: Uuid ) : Promise<HackComment | CommentReply> {
		const dto = await HttpClient.getAsync<HackCommentData & CommentReplyData>( `/v3/comments/comment/${commentId}` );
		return dto.parentId ? new CommentReply( dto ) : new HackComment( dto );
	}

	static async postHackCommentAsync( hackId: HexId, message: string ) : Promise<HackComment> {
		return new HackComment( await HttpClient.postTextAsync<HackCommentData>( `/v3/comments/hack/${hackId}`, message ) );
	}

	static async postCommentReplyAsync( commentId: Uuid, message: string ) : Promise<CommentReply> {
		return new CommentReply( await HttpClient.postTextAsync<CommentReplyData>( `/v3/comments/replies/${commentId}`, message ) );
	}

	static async editCommentAsync( commentId: Uuid, message: string ) : Promise<HackComment | CommentReply> {
		const dto = await HttpClient.putTextAsync<HackCommentData & CommentReplyData>( `/v3/comments/comment/${commentId}`, message );
		return dto.parentId ? new CommentReply( dto ) : new HackComment( dto );
	}

	static deleteCommentAsync( commentId: Uuid ) : Promise<void> {
		return HttpClient.deleteAsync<void>( `/v3/comments/comment/${commentId}` );
	}

	static reportCommentAsync( commentId: Uuid, reason?: Weak<string> ) {
		if( reason ) {
			return HttpClient.postTextAsync<void>( `/v3/comments/comment/${commentId}/report`, reason );
		} else {
			return HttpClient.postAsync<void>( `/v3/comments/comment/${commentId}/report` );
		}
	}

}
