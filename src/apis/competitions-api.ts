import { Hack, HackData } from './hacks-api';
import { HttpClient } from './http-client';
import { Optional, ConstArray, ConstRef, HexId, Uuid, FileLike } from '../util/types';
import { RichUserRef } from './users-api';
import { DownloadInfo } from './files';
import { freezeObjArray } from '../util/freeze';
import { Page } from './pagination';

export interface CompetitionSeriesCreate {
	name: string;
	description: string;
	canOfferPrizes: boolean;
	locked: boolean;
	hosts: ConstRef<string[]>;
}

export interface CompetitionCreate {
	name: string;
	description: string;
	startDate: ConstRef<Date>;
	endDate: ConstRef<Date>;
	hasCashPrize: boolean;
}

export interface CompetitionSeriesUpdate {
	name?: Optional<string>;
	description?: Optional<string>;
	canOfferPrizes?: Optional<boolean>;
	locked?: Optional<boolean>;
	hosts?: Optional<ConstRef<string[]>>;
}

export interface CompetitionUpdate {
	name?: string;
	description?: string;
	startDate?: ConstRef<Date>;
	endDate?: ConstRef<Date>;
}

interface CompetitionSeriesData {
	seriesId: Uuid;
	name: string;
	slug: string;
	description: string;
	canOfferPrizes: boolean;
	locked: boolean;
	dateCreated: string;
	public: boolean;
	hosts: RichUserRef[];
	logo: DownloadInfo;
	actions: string[];
}

interface CompetitionData {
	competitionId: Uuid;
	seriesId: Uuid;
	name: string;
	description: string;
	dateCreated: string;
	startDate: string;
	endDate: string;
	published: boolean;
	hasCashPrize: boolean;
	submissionCount?: Optional<number>;
	actions: string[];
}

export class CompetitionSeries {

	public readonly seriesId: Uuid;
	public readonly name: string;
	public readonly slug: string;
	public readonly description: string;
	public readonly canOfferPrizes: boolean;
	public readonly locked: boolean;
	public readonly dateCreated: Readonly<Date>;
	public readonly public: boolean;
	public readonly hosts: ConstArray<RichUserRef>;
	public readonly logo: Readonly<DownloadInfo>;
	#actions: string[];

	constructor( data: CompetitionSeriesData ) {
		this.seriesId = data.seriesId;
		this.name = data.name;
		this.slug = data.slug;
		this.description = data.description;
		this.canOfferPrizes = data.canOfferPrizes;
		this.locked = data.locked;
		this.dateCreated = Object.freeze( new Date( data.dateCreated ) );
		this.public = data.public;
		this.hosts = freezeObjArray( data.hosts );
		this.logo = Object.freeze( data.logo );
		this.#actions = data.actions;
		Object.freeze( this );
	}

	public get canEdit() : boolean {
		return this.#actions.includes( 'edit' );
	}

	public get canManage() : boolean {
		return this.#actions.includes( 'manage' );
	}

}

export class Competition {

	public readonly competitionId: Uuid;
	public readonly seriesId: Uuid;
	public readonly name: string;
	public readonly description: string;
	public readonly dateCreated: Readonly<Date>;
	public readonly startDate: Readonly<Date>;
	public readonly endDate: Readonly<Date>;
	public readonly published: boolean;
	public readonly hasCashPrize: boolean;
	public readonly submissionCount: Optional<number>;
	readonly #actions: string[];

	constructor( data: CompetitionData ) {
		this.competitionId = data.competitionId;
		this.seriesId = data.seriesId;
		this.name = data.name;
		this.description = data.description;
		this.dateCreated = Object.freeze( new Date( data.dateCreated ) );
		this.startDate = Object.freeze( new Date( data.startDate ) );
		this.endDate = Object.freeze( new Date( data.endDate ) );
		this.published = data.published;
		this.hasCashPrize = data.hasCashPrize;
		this.submissionCount = data.submissionCount;
		this.#actions = data.actions;
		Object.freeze( this );
	}

	public get canEdit() : boolean {
		return this.#actions.includes( 'edit' );
	}

	public get canPublish() : boolean {
		return this.#actions.includes( 'publish' );
	}

	public get canSubmit() : boolean {
		return this.#actions.includes( 'submit' );
	}

	public get canJudge() : boolean {
		return this.#actions.includes( 'judge' );
	}

}

export class CompetitionsApi {

	static async createCompetitionSeriesAsync( data: CompetitionSeriesCreate ) : Promise<CompetitionSeries> {
		return new CompetitionSeries( await HttpClient.postJsonAsync<CompetitionSeriesData>( '/v3/competitions/series', data ) );
	}

	static async createCompetitionAsync( seriesSlug: Uuid | string, data: CompetitionCreate ) : Promise<Competition> {
		return new Competition( await HttpClient.postJsonAsync<CompetitionData>( `/v3/competitions/series/${seriesSlug}/add`, {
			name: data.name,
			description: data.description,
			startDate: data.startDate.toISOString(),
			endDate: data.endDate.toISOString(),
			hasCashPrize: data.hasCashPrize
		}));
	}

	static async updateCompetitionSeriesAsync( seriesSlug: Uuid | string, data: CompetitionSeriesUpdate ) : Promise<CompetitionSeries> {
		return new CompetitionSeries( await HttpClient.patchJsonAsync<CompetitionSeriesData>( `/v3/competitions/series/${seriesSlug}`, data ) );
	}

	static validateCompetitionSeriesLogoAsync( logo: FileLike ) : Promise<void> {
		return HttpClient.postFileAsync<void>( '/v3/competitions/logo/test', logo );
	}

	static updateCompetitionSeriesLogoAsync( seriesSlug: Uuid | string, logo: FileLike ) : Promise<void> {
		return HttpClient.putFileAsync<void>( `/v3/competitions/series/${seriesSlug}/logo`, logo );
	}

	static async updateCompetitionAsync( competitionId: Uuid, data: CompetitionUpdate ) : Promise<Competition> {
		return new Competition( await HttpClient.putJsonAsync<CompetitionData>( `/v3/competitions/id/${competitionId}`, {
			name: data.name,
			description: data.description,
			startDate: data.startDate?.toISOString(),
			endDate: data.endDate?.toISOString()
		}));
	}

	static async getAllCompetitionSeriesAsync() : Promise<ConstArray<CompetitionSeries>> {
		const series = await HttpClient.getAsync<CompetitionSeriesData[]>( '/v3/competitions/series' );
		return Object.freeze( series.map( s => new CompetitionSeries( s ) ) );
	}

	static async getActiveCompetitionsAsync() : Promise<ConstArray<Competition>> {
		const competitions = await HttpClient.getAsync<CompetitionData[]>( '/v3/competitions/active' );
		return Object.freeze( competitions.map( c => new Competition( c ) ) );
	}

	static async getCompetitionSeriesAsync( seriesSlug: Uuid | string ) : Promise<CompetitionSeries> {
		return new CompetitionSeries( await HttpClient.getAsync<CompetitionSeriesData>( `/v3/competitions/series/${seriesSlug}` ) );
	}

	static async getCompetitionSeriesSlugAvailableAsync( seriesSlug: string ) : Promise<boolean> {
		try {
			await HttpClient.headAsync( `/v3/competitions/series/${seriesSlug}` );
			return false;
		} catch{
			return true;
		}
	}

	static getCompetitionsPagedAsync( seriesSlug: Uuid | string ) : Promise<Page<Competition>> {
		return Page.fetchFirstAsync(
			`/v3/competitions/series/${seriesSlug}/competitions`,
			(data: CompetitionData) => new Competition( data )
		);
	}

	static async getCompetitionAsync( competitionId: Uuid ) : Promise<Competition> {
		return new Competition( await HttpClient.getAsync<CompetitionData>( `/v3/competitions/id/${competitionId}` ) );
	}

	static async getCompetitionHacksAsync( competitionId: Uuid ) : Promise<ConstArray<Hack>> {
		const dtos = await HttpClient.getAsync<HackData[]>( `/v3/competitions/id/${competitionId}/hacks` );
		return Object.freeze( dtos.map( dto => new Hack( dto ) ) );
	}

	static deleteCompetitionSeriesAsync( seriesSlug: Uuid | string ) : Promise<void> {
		return HttpClient.deleteAsync<void>( `/v3/competitions/series/${seriesSlug}` );
	}

	static deleteCompetitionAsync( competitionId: Uuid ) : Promise<void> {
		return HttpClient.deleteAsync<void>( `/v3/competitions/id/${competitionId}` );
	}

	static updateCompetitionPlacementsAsync( competitionId: Uuid, hackIds: ConstRef<HexId[]> ) {
		return HttpClient.putJsonAsync<void>( `/v3/competitions/id/${competitionId}/placements`, hackIds );
	}

	static async publishCompetitionAsync( competitionId: Uuid ) : Promise<void> {
		await HttpClient.postAsync<unknown>( `/v3/competitions/id/${competitionId}/publish` );
	}

	static async judgeCompetitionAsync( competitionId: Uuid, hackIds: HexId[] ) : Promise<void> {
		await HttpClient.putJsonAsync<void>( `/v3/competitions/id/${competitionId}/placements`, hackIds );
	}

}
