import { HttpClient, UploadProgress } from './http-client';
import { ConstRef, FileLike, Nullable, Uuid } from '../util/types';
import { GfxPlugin, GfxPluginFlag, HackFlag, Category, ConsoleCompatibility, HackFileInfoData, HackFileInfo } from './hacks-api';
import { StarLayout } from '../util/star-layout';
import { deepFreeze } from '../util/freeze';

interface HackCompetitionInfoData {
	competitionId: Uuid;
	paypal: Nullable<string>;
}

export class HackCompetitionInfo {

	public readonly competitionId: Uuid;
	public readonly paypal: Nullable<string>;

	constructor( data: HackCompetitionInfoData ) {
		this.competitionId = data.competitionId;
		this.paypal = data.paypal;
		Object.freeze( this );
	}

}

interface HackMetaInfoData {
	title: string;
	description: string;
	category: Category;
	tags: string[];
	creationDate: string;
	authors: string[];
	stars: number;
	private: boolean;
}

export class HackMetaInfo {

	public readonly title: string;
	public readonly description: string;
	public readonly category: Category;
	public readonly tags: Readonly<string[]>;
	public readonly creationDate: Readonly<Date>;
	public readonly authors: Readonly<string[]>;
	public readonly stars: number;
	public readonly private: boolean;

	constructor( data: HackMetaInfoData ) {
		this.title = data.title;
		this.description = data.description;
		this.category = data.category;
		this.tags = Object.freeze( data.tags );
		this.creationDate = Object.freeze( new Date( data.creationDate ) );
		this.authors = Object.freeze( data.authors );
		this.stars = data.stars;
		this.private = data.private;
		Object.freeze( this );
	}

}

interface HackEmulationInfoData {
	plugin: GfxPlugin;
	pluginFlags: GfxPluginFlag[];
	hackFlags: HackFlag[];
	consoleCompatibility: Nullable<ConsoleCompatibility>;
}

export class HackEmulationInfo {

	public readonly plugin: GfxPlugin;
	public readonly pluginFlags: Readonly<GfxPluginFlag[]>;
	public readonly hackFlags: Readonly<HackFlag[]>;
	public readonly consoleCompatibility: Nullable<ConsoleCompatibility>;

	constructor( data: HackEmulationInfoData ) {
		this.plugin = data.plugin;
		this.pluginFlags = Object.freeze( data.pluginFlags );
		this.hackFlags = Object.freeze( data.hackFlags );
		this.consoleCompatibility = data.consoleCompatibility;
		Object.freeze( this );
	}

}

interface HackMediaInfoData {
	videos: string[];
	screenshots: string[];
}

export class HackMediaInfo {

	public readonly videos: Readonly<string[]>;
	public readonly screenshots: Readonly<string[]>;

	constructor( data: HackMediaInfoData ) {
		this.videos = Object.freeze( data.videos );
		this.screenshots = Object.freeze( data.screenshots );
		Object.freeze( this );
	}

	withVideos( videos: ConstRef<string[]> ) : HackMediaInfo {
		return new HackMediaInfo({
			videos: videos as string[],
			screenshots: this.screenshots as string[]
		});
	}

	withScreenshots( screenshots: ConstRef<string[]> ) : HackMediaInfo {
		return new HackMediaInfo({
			videos: this.videos as string[],
			screenshots: screenshots as string[]
		});
	}

}

interface HackStarLayoutInfoData {
	filename: string;
	stars: number;
	layout: StarLayout;
}

export class HackStarLayoutInfo {

	public readonly filename: string;
	public readonly stars: number;
	public readonly layout: Readonly<StarLayout>;

	constructor( data: HackStarLayoutInfoData ) {
		this.filename = data.filename;
		this.stars = data.stars;
		this.layout = deepFreeze( data.layout ) as Readonly<StarLayout>;
		Object.freeze( this );
	}

}

interface HackSubmissionSessionData {
	competition: Nullable<HackCompetitionInfoData>;
	hackFile: Nullable<HackFileInfoData>;
	metadata: Nullable<HackMetaInfoData>;
	emulation: Nullable<HackEmulationInfoData>;
	media: Nullable<HackMediaInfoData>;
	starLayout: Nullable<HackStarLayoutInfoData>;
}

export class HackSubmissionSession {

	public readonly competition: Nullable<HackCompetitionInfo>;
	public readonly hackFile: Nullable<HackFileInfo>;
	public readonly metadata: Nullable<HackMetaInfo>;
	public readonly emulation: Nullable<HackEmulationInfo>;
	public readonly media: Nullable<HackMediaInfo>;
	public readonly starLayout: Nullable<HackStarLayoutInfo>;

	constructor(
		_competition: Nullable<HackCompetitionInfo>,
		_hackFile: Nullable<HackFileInfo>,
		_metadata: Nullable<HackMetaInfo>,
		_emulation: Nullable<HackEmulationInfo>,
		_media: Nullable<HackMediaInfo>,
		_starLayout: Nullable<HackStarLayoutInfo>
	) {
		this.competition = _competition;
		this.hackFile = _hackFile;
		this.metadata = _metadata;
		this.emulation = _emulation;
		this.media = _media;
		this.starLayout = _starLayout;
		Object.freeze( this );
	}

	public get inProgess() : boolean {
		return !!(this.competition || this.hackFile || this.metadata || this.emulation || this.media || this.starLayout);
	}

	public withCompetition( _competition: Nullable<HackCompetitionInfo> ) {
		return new HackSubmissionSession(
			_competition,
			this.hackFile,
			this.metadata,
			this.emulation,
			this.media,
			this.starLayout
		);
	}

	public withHackFile( _hackFile: Nullable<HackFileInfo> ) {
		return new HackSubmissionSession(
			this.competition,
			_hackFile,
			this.metadata,
			this.emulation,
			this.media,
			this.starLayout
		);
	}

	public withMetadata( _metadata: Nullable<HackMetaInfo> ) {
		return new HackSubmissionSession(
			this.competition,
			this.hackFile,
			_metadata,
			this.emulation,
			this.media,
			this.starLayout
		);
	}

	public withEmulationInfo( _emulation: Nullable<HackEmulationInfo> ) {
		return new HackSubmissionSession(
			this.competition,
			this.hackFile,
			this.metadata,
			_emulation,
			this.media,
			this.starLayout
		);
	}

	public withMedia( _media: Nullable<HackMediaInfo> ) {
		return new HackSubmissionSession(
			this.competition,
			this.hackFile,
			this.metadata,
			this.emulation,
			_media,
			this.starLayout
		);
	}

	public withStarLayout( _starLayout: Nullable<HackStarLayoutInfo> ) {
		return new HackSubmissionSession(
			this.competition,
			this.hackFile,
			this.metadata,
			this.emulation,
			this.media,
			_starLayout
		);
	}

}

const _buildSession = function( data: HackSubmissionSessionData ) : HackSubmissionSession {
	return new HackSubmissionSession(
		data.competition ? new HackCompetitionInfo( data.competition ) : null,
		data.hackFile ? new HackFileInfo( data.hackFile ) : null,
		data.metadata ? new HackMetaInfo( data.metadata ) : null,
		data.emulation ? new HackEmulationInfo( data.emulation ) : null,
		data.media ? new HackMediaInfo( data.media ) : null,
		data.starLayout ? new HackStarLayoutInfo( data.starLayout ) : null
	);
}

export interface SubmittedHackLinks {
	urlSlug: string;
	apiHref: string;
	webHref: string;
}

export class HackSubmissionApi {

	static async getSessionAsync() : Promise<HackSubmissionSession> {
		return _buildSession( await HttpClient.getAsync<HackSubmissionSessionData>( '/v3/hacks/submit/session' ) );
	}

	static submitAsync() : Promise<SubmittedHackLinks> {
		return HttpClient.postAsync<SubmittedHackLinks>( '/v3/hacks/submit/commit' );
	}

	static async resetSessionAsync() : Promise<HackSubmissionSession> {
		await HttpClient.deleteAsync<void>( '/v3/hacks/submit/session' );
		return this.getSessionAsync();
	}

	static async updateCompetitionAsync( competitionId: Nullable<Uuid>, paypal: Nullable<string> ) : Promise<Nullable<HackCompetitionInfo>> {
		if( competitionId ) {
			await HttpClient.putJsonAsync<unknown>( '/v3/hacks/submit/competition', {
				competitionId: competitionId,
				paypal: paypal
			});
			
			return new HackCompetitionInfo({
				competitionId: competitionId,
				paypal: paypal
			});
		} else {
			await HttpClient.deleteAsync<void>( '/v3/hacks/submit/competition' );
			return null;
		}
	}

	static async uploadPatchAsync( file: File, progressCallback: (_:UploadProgress) => unknown ) : Promise<HackFileInfo> {
		return new HackFileInfo( await HttpClient.putFileAsync<HackFileInfoData>( '/v3/hacks/submit/patch', file, progressCallback ) );
	}

	static getPublicTagsAsync() : Promise<string[]> {
		return HttpClient.getAsync( '/v3/hacks/tags' );
	}

	static getAllUsernamesAsync() : Promise<string[]> {
		return HttpClient.getAsync( '/v3/users/usernames' );
	}

	static async updateMetadataAsync(
		isPublic: boolean,
		title: string,
		description: string,
		category: Category,
		authors: string[],
		tags: string[],
		creationDate: Date,
		stars: number
	) : Promise<HackMetaInfo> {
		const metadata = {
			title: title,
			description: description,
			category: category,
			tags: tags,
			creationDate: creationDate.toISOString(),
			authors: authors,
			stars: stars,
			private: !isPublic
		};

		await HttpClient.putJsonAsync<void>( '/v3/hacks/submit/metadata', metadata );
		return new HackMetaInfo( metadata );
	}

	static async updateEmulationInfoAsync(
		plugin: GfxPlugin,
		pluginFlags: GfxPluginFlag[],
		hackFlags: HackFlag[],
		consoleCompatibility: Nullable<ConsoleCompatibility>
	) : Promise<HackEmulationInfo> {
		const update = {
			plugin: plugin,
			pluginFlags: pluginFlags,
			hackFlags: hackFlags,
			consoleCompatibility: consoleCompatibility
		};

		await HttpClient.putJsonAsync<void>( '/v3/hacks/submit/emulation', update );
		return new HackEmulationInfo( update );
	}

	static fetchScreenshotAsync( index: number ) : Promise<Blob> {
		return HttpClient.getAsync<Blob>( `/v3/hacks/submit/screenshots/${index}` );
	}

	static moveScreenshotAsync( index: number, before: number ) : Promise<string[]> {
		return HttpClient.postAsync<string[]>( `/v3/hacks/submit/screenshots/move/${index}/${before}` );
	}

	static uploadScreenshotAsync( screenshot: File ) : Promise<string[]> {
		return HttpClient.postFileAsync<string[]>( '/v3/hacks/submit/screenshots', screenshot );
	}

	static removeScreenshotAsync( index: number ) : Promise<void> {
		return HttpClient.deleteAsync<void>( `/v3/hacks/submit/screenshots/${index}` );
	}

	static updateVideosAsync( videos: string[] ) : Promise<void> {
		return HttpClient.putJsonAsync<void>( '/v3/hacks/submit/videos', videos );
	}

	static async uploadStarLayoutAsync( layout: FileLike ) : Promise<HackStarLayoutInfo> {
		return new HackStarLayoutInfo( await HttpClient.putFileAsync<HackStarLayoutInfoData>( '/v3/hacks/submit/layout', layout ) );
	}

	static removeStarLayoutAsync() : Promise<void> {
		return HttpClient.deleteAsync<void>( '/v3/hacks/submit/layout' );
	}

}
