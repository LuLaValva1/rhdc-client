import { HttpClient } from './http-client';
import { Page } from './pagination';
import { UserRef, WeakRichUserRef } from './users-api';
import { DownloadInfo } from './files';
import { ConstArray, FileLike, HexId, Nullable, Optional, UserId, Uuid, Weak } from '../util/types';
import { freezeObjArray } from '../util/freeze';

export interface RandomHackInfo {
	hackId: HexId;
	title: string;
	slug: string;
}

export enum GfxPlugin {
	ParaLLEl = 'ParaLLEl',
	GLideN64 = 'GLideN64',
	OGRE = 'OGRE',
	Glide64 = 'Glide64',
	Angrylion = 'Angrylion',
	Rice = 'Rice'
}

export enum GfxPluginFlag {
	UpscaleTexrects = 'upscale-texrects',
	AllowHleFallback = 'allow-hle-fallback',
	EmulateFramebuffer = 'emulate-framebuffer',
	AccurateDepthCompare = 'accurate-depth-compare',
	Widescreen = 'widescreen'
}

export enum HackFlag {
	NoOverclock = 'no-overclock',
	DualAnalog = 'dual-analog',
	BigEEPROM = 'big-eeprom',
	RequiresMouse = 'requires-n64-mouse',
	RequiresGcc = 'requires-gamecube-controller'
}

export enum Category {
	Original = 'Original',
	Retexture = 'Retexture',
	Kaizo = 'Kaizo',
	Concept = 'Concept'
}

export enum HackSortField {
	DateCreated = 'created',
	Name = 'name',
	Popularity = 'downloads',
	LastModified = 'updated',
	DateUploaded = 'uploaded',
	Stars = 'stars',
	Difficulty = 'difficulty',
	Rating = 'rating'
}

export enum MatureFilter {
	Ignore = 'no',
	Include = 'allow',
	Exclusive = 'yes'
}

export enum ConsoleCompatibility {
	Excellent = 'Excellent',
	Good = 'Good',
	Playable = 'Playable',
	Unoptimized = 'Unoptimized',
	None = 'None'
}

export interface HackUpdateDto {
	title?: Optional<string>;
	description?: Optional<string>;
	stars?: Optional<number>;
	private?: Optional<boolean>;
	authors?: Optional<string[]>;
	tags?: Optional<string[]>;
}

export interface HackModUpdateDto {
	mature?: Optional<boolean>;
	completionBonus?: Optional<number>;
	category?: Optional<Category>;
}

export interface HackVersionUpdateDto {
	plugin?: Optional<GfxPlugin>;
	pluginFlags?: Optional<GfxPluginFlag[]>;
	hackFlags?: Optional<HackFlag[]>;
	consoleCompatibility?: Weak<ConsoleCompatibility>;
	archived?: Optional<boolean>;
}

export enum HackControllerSupport {
	Required = 'Required',
	Supported = 'Supported',
	Unused = 'Unused',
	Unknown = 'Unknown'
}

export enum HackSaveType {
	None = 'None',
	Eeprom4k = 'EEPROM4k',
	Eeprom16k = 'EEPROM16k',
	Sram = 'SRAM',
	FlashRam = 'FlashRAM'
}

export interface HackFileInfoData {
	patchFilename: string;
	hackSha1: string;
	hackFileSize: number;
	internalRomName: string;
	internalRomNameGood: boolean;
	pluginGuess: Nullable<GfxPlugin>;
	mouseSupport: HackControllerSupport;
	gamecubeControllerSupport: HackControllerSupport;
	saveType: Nullable<HackSaveType>;
}

export class HackFileInfo {

	public readonly patchFilename: string;
	public readonly hackSha1: string;
	public readonly hackFileSize: number;
	public readonly internalRomName: string;
	public readonly internalRomNameGood: boolean;
	public readonly pluginGuess: Nullable<GfxPlugin>;
	public readonly mouseSupport: HackControllerSupport;
	public readonly gamecubeControllerSupport: HackControllerSupport;
	public readonly saveType: Nullable<HackSaveType>;

	constructor( data: HackFileInfoData ) {
		this.patchFilename = data.patchFilename;
		this.hackSha1 = data.hackSha1;
		this.hackFileSize = data.hackFileSize;
		this.internalRomName = data.internalRomName;
		this.internalRomNameGood = data.internalRomNameGood;
		this.pluginGuess = data.pluginGuess;
		this.mouseSupport = data.mouseSupport;
		this.gamecubeControllerSupport = data.gamecubeControllerSupport;
		this.saveType = data.saveType;
		Object.freeze( this );
	}

}

export interface HackVersionData {
	download: DownloadInfo;
	patchedSha1: string;
	plugin: Nullable<GfxPlugin>;
	pluginFlags: GfxPluginFlag[];
	hackFlags: HackFlag[];
	consoleCompatibility: Nullable<ConsoleCompatibility>;
	approved: boolean;
	archived: boolean;
}

export interface HackData {
	hackId: HexId;
	title: string;
	uploader: Nullable<UserRef>;
	authors: WeakRichUserRef[];
	tags: string[];
	stars: number;
	description: string;
	consoleCompatibility: Nullable<ConsoleCompatibility>;
	versions: HackVersionData[];
	urlTitle: string;
	createdDate?: Optional<string>;
	uploadedDate: string;
	lastVersionUploadTime: string;
	layoutUpdateTime?: Optional<string>;
	numDownloads: number;
	mature: boolean;
	private: boolean;
	locked: boolean;
	approved: boolean;
	screenshots: DownloadInfo[];
	videos: string[];
	needsVerification: boolean;
	approvedBy?: Optional<UserId>;
	rating: Nullable<number>;
	difficulty: Nullable<number>;
	category: Category;
	completionBonus: Nullable<number>;
	layout: Nullable<DownloadInfo>;
	archived: boolean;
	competitionId?: Optional<Uuid>;
	competitionPlacement?: Optional<number>;
	paypal?: Optional<string>;
	actions: string[];
}

export class Hack {

	public readonly hackId: HexId;
	public readonly title: string;
	public readonly uploader: Nullable<Readonly<UserRef>>;
	public readonly authors: ConstArray<WeakRichUserRef>;
	public readonly tags: Readonly<string[]>;
	public readonly stars: number;
	public readonly description: string;
	public readonly consoleCompatibility: Nullable<ConsoleCompatibility>;
	public readonly versions: ConstArray<HackVersionData>;
	public readonly urlTitle: string;
	public readonly createdDate: Nullable<Readonly<Date>>;
	public readonly uploadedDate: Readonly<Date>;
	public readonly lastVersionUploadTime: Readonly<Date>;
	public readonly layoutUpdateTime: Nullable<Readonly<Date>>;
	public readonly numDownloads: number;
	public readonly mature: boolean;
	public readonly private: boolean;
	public readonly locked: boolean;
	public readonly approved: boolean;
	public readonly screenshots: ConstArray<DownloadInfo>;
	public readonly videos: Readonly<string[]>;
	public readonly needsVerification: boolean;
	public readonly approvedBy: Nullable<UserId>;
	public readonly rating: Nullable<number>;
	public readonly difficulty: Nullable<number>;
	public readonly category: Category;
	public readonly completionBonus: Nullable<number>;
	public readonly archived: boolean;
	public readonly layout: Nullable<Readonly<DownloadInfo>>;
	public readonly competitionId: Nullable<Uuid>;
	public readonly competitionPlacement: Nullable<number>;
	public readonly paypal: Nullable<string>;
	readonly #actions: string[];

	constructor( data: HackData ) {
		this.hackId = data.hackId;
		this.title = data.title;
		this.uploader = data.uploader ? Object.freeze( data.uploader ) : null;
		this.authors = freezeObjArray( data.authors );
		this.tags = Object.freeze( data.tags );
		this.stars = data.stars;
		this.description = data.description;
		this.consoleCompatibility = data.consoleCompatibility;
		this.versions = freezeObjArray( data.versions );
		this.urlTitle = data.urlTitle;
		this.createdDate = data.createdDate ? Object.freeze( new Date( data.createdDate ) ) : null;
		this.uploadedDate = Object.freeze( new Date( data.uploadedDate ) );
		this.lastVersionUploadTime = Object.freeze( new Date( data.lastVersionUploadTime ) );
		this.layoutUpdateTime =  data.layoutUpdateTime ? Object.freeze( new Date( data.layoutUpdateTime ) ) : null;
		this.numDownloads = data.numDownloads;
		this.mature = data.mature;
		this.private = data.private;
		this.locked = data.locked;
		this.approved = data.approved;
		this.screenshots = freezeObjArray( data.screenshots );
		this.videos = Object.freeze( data.videos );
		this.needsVerification = data.needsVerification;
		this.approvedBy = data.approvedBy || null;
		this.rating = data.rating;
		this.difficulty = data.difficulty;
		this.category = data.category;
		this.completionBonus = data.completionBonus;
		this.layout = Object.freeze( data.layout );
		this.archived = data.archived;
		this.competitionId = data.competitionId || null;
		this.competitionPlacement = data.competitionPlacement || null;
		this.paypal = data.paypal || null;
		this.#actions = data.actions;
		Object.freeze( this );
	}

	public get canApprove() : boolean {
		return this.#actions.includes( 'approve' );
	}

	public get canApproveVersions() : boolean {
		return this.#actions.includes( 'approve-versions' );
	}

	public get canEditInfo() : boolean {
		return this.#actions.includes( 'edit-info' );
	}

	public get canEditVersions() : boolean {
		return this.#actions.includes( 'edit-versions' );
	}

	public get canModEdit() : boolean {
		return this.#actions.includes( 'mod-edit' );
	}

	public get canDelete() : boolean {
		return this.#actions.includes( 'delete' );
	}

	public get canComment() : boolean {
		return this.#actions.includes( 'comment' );
	}

	public get canClaimOwnership() : boolean {
		return this.#actions.includes( 'claim-ownership' );
	}

	public get canDownloadArchived() : boolean {
		return this.#actions.includes( 'download-archived' );
	}

}

export class HacksApi {

	static searchHacksAsync(
		searchText: Weak<string>,
		sortBy: HackSortField,
		descending: boolean,
		matureFilter: MatureFilter,
		category: Weak<Category> = undefined,
		minimumCompatibility: Weak<ConsoleCompatibility> = undefined,
		pageSize = 10
	) : Promise<Page<Hack>> {
		const searchPart = searchText ? `search=${searchText}&` : '';
		const categoryPart = category ? `&category=${category}` : '';
		const compatPart = minimumCompatibility ? `&minCompatibility=${minimumCompatibility}` : '';
		const sortOrder = descending ? 'desc' : 'asc';
		return Page.fetchFirstAsync(
			`/v3/hacks?${searchPart}sortBy=${sortBy}&sortOrder=${sortOrder}&mature=${matureFilter}${categoryPart}${compatPart}&pageSize=${pageSize}`,
			(data: HackData) => new Hack( data )
		);
	}

	static getUserHacksAsync(
		userSlug: UserId | string,
		sortBy: HackSortField,
		descending: boolean,
		matureFilter: MatureFilter,
		pageSize = 10
	) : Promise<Page<Hack>> {
		const sortOrder = descending ? 'desc' : 'asc';
		return Page.fetchFirstAsync(
			`/v3/hacks/user/${userSlug}?sortBy=${sortBy}&sortOrder=${sortOrder}&mature=${matureFilter}&pageSize=${pageSize}`,
			(data: HackData) => new Hack( data )
		);
	}

	static async getHackAsync( hackSlug: HexId | string ) : Promise<Hack> {
		return new Hack( await HttpClient.getAsync<HackData>( `/v3/hacks/hack/${hackSlug}` ) );
	}

	static deleteHackAsync( hackSlug: HexId | string ) : Promise<void> {
		return HttpClient.deleteAsync<void>( `/v3/hacks/hack/${hackSlug}` );
	}

	static async updateHackAsync( hackSlug: HexId | string, update: HackUpdateDto | HackModUpdateDto ) : Promise<Hack> {
		return new Hack( await HttpClient.patchJsonAsync<HackData>( `/v3/hacks/hack/${hackSlug}`, update ) );
	}

	static async updateHackVideosAsync( hackSlug: HexId | string, videos: string[] ) : Promise<Hack> {
		return new Hack( await HttpClient.putJsonAsync<HackData>( `/v3/hacks/hack/${hackSlug}/videos`, videos ) );
	}

	static async addScreenshotAsync( hackSlug: HexId | string, screenshot: File ) : Promise<Hack> {
		return new Hack( await HttpClient.postFileAsync<HackData>( `/v3/hacks/hack/${hackSlug}/screenshots`, screenshot ) );
	}

	static async removeScreenshotAsync( hackSlug: HexId | string, index: number ) : Promise<Hack> {
		return new Hack( await HttpClient.deleteAsync<HackData>( `/v3/hacks/hack/${hackSlug}/screenshots/${index}` ) );
	}

	static async moveScreenshotAsync( hackSlug: HexId | string, index: number, before: number ) : Promise<Hack> {
		return new Hack( await HttpClient.postAsync<HackData>( `/v3/hacks/hack/${hackSlug}/screenshots/${index}/move/${before}` ) );
	}

	static async updateHackVersionAsync( hackSlug: HexId | string, version: string, update: HackVersionUpdateDto ) : Promise<Hack> {
		return new Hack( await HttpClient.patchJsonAsync<HackData>( `/v3/hacks/hack/${hackSlug}/version/${version}`, update ) );
	}

	static async deleteHackVersionAsync( hackSlug: HexId | string, version: string ) : Promise<Hack> {
		return new Hack( await HttpClient.deleteAsync<HackData>( `/v3/hacks/hack/${hackSlug}/version/${version}` ) );
	}

	static async updateStarLayoutAsync( hackSlug: HexId | string, layout: FileLike ) : Promise<Hack> {
		return new Hack( await HttpClient.putFileAsync<HackData>( `/v3/hacks/hack/${hackSlug}/layout`, layout ) );
	}

	static async removeStarLayoutAsync( hackSlug: HexId | string ) : Promise<Hack> {
		return new Hack( await HttpClient.deleteAsync( `/v3/hacks/hack/${hackSlug}/layout` ) );
	}

	static async uploadHackVersionAsync( hackSlug: HexId | string, patch: File ) : Promise<HackFileInfo> {
		return new HackFileInfo( await HttpClient.postFileAsync<HackFileInfoData>( `/v3/hacks/hack/${hackSlug}/newversion/upload`, patch ) );
	}

	static async commitHackVersionAsync( hackSlug: HexId | string, versionInfo: HackVersionUpdateDto ) : Promise<Hack> {
		return new Hack( await HttpClient.postJsonAsync<HackData>( `/v3/hacks/hack/${hackSlug}/newversion/finalize`, versionInfo ) );
	}

	static async promoteHackVersionAsync( hackSlug: HexId | string, version: string ) : Promise<Hack> {
		return new Hack( await HttpClient.postAsync<HackData>( `/v3/hacks/hack/${hackSlug}/version/${version}/move` ) );
	}

	static async approveHackAsync( hackSlug: HexId | string ) : Promise<Hack> {
		return new Hack( await HttpClient.postAsync( `/v3/hacks/hack/${hackSlug}/approve` ) );
	}

	static async rejectHackAsync( hackSlug: HexId | string, reason: Weak<string> ) : Promise<void> {
		if( reason ) {
			await HttpClient.postTextAsync<unknown>( `/v3/hacks/hack/${hackSlug}/reject`, reason );
		} else {
			await HttpClient.postAsync<unknown>( `/v3/hacks/hack/${hackSlug}/reject` );
		}
	}

	static async approveHackVersionAsync( hackSlug: HexId | string, version: string ) : Promise<Hack> {
		return new Hack( await HttpClient.postAsync( `/v3/hacks/hack/${hackSlug}/version/${version}/approve` ) );
	}

	static async rejectHackVersionAsync( hackSlug: HexId | string, version: string, reason: Weak<string> ) : Promise<Hack> {
		if( reason ) {
			return new Hack( await HttpClient.postTextAsync( `/v3/hacks/hack/${hackSlug}/version/${version}/reject`, reason ) );
		} else {
			return new Hack( await HttpClient.postAsync( `/v3/hacks/hack/${hackSlug}/version/${version}/reject` ) );
		}
	}

	static async removeHackRatingsAsync( hackSlug: HexId | string ) : Promise<Hack> {
		return new Hack( await HttpClient.deleteAsync<HackData>( `/v3/hacks/hack/${hackSlug}/ratings` ) );
	}

	static async removeHackDifficultyAsync( hackSlug: HexId | string ) : Promise<Hack> {
		return new Hack( await HttpClient.deleteAsync<HackData>( `/v3/hacks/hack/${hackSlug}/difficulty` ) );
	}

	static claimHackOwnershipAsync( hackSlug: HexId | string, replacedAuthor: Weak<string> = null, reason: Weak<string> = null ) : Promise<void> {
		let url = `/v3/hacks/hack/${hackSlug}/claim`;
		if( replacedAuthor ) {
			url += '?name=' + encodeURIComponent( replacedAuthor );
		}

		if( reason ) {
			return HttpClient.postTextAsync( url, reason );
		} else {
			return HttpClient.postAsync( url );
		}
	}

	static approveHackOwnershipClaimAsync( hackSlug: HexId | string, userSlug: UserId | string ) : Promise<void> {
		return HttpClient.postAsync<void>( `/v3/hacks/hack/${hackSlug}/claim/${userSlug}/approve` );
	}

	static rejectHackOwnershipClaimAsync( hackSlug: HexId | string, userSlug: UserId | string ) : Promise<void> {
		return HttpClient.postAsync<void>( `/v3/hacks/hack/${hackSlug}/claim/${userSlug}/reject` );
	}

	static deleteHackOwnershipClaimAsync( hackSlug: HexId | string, userSlug: UserId | string ) : Promise<void> {
		return HttpClient.deleteAsync<void>( `/v3/hacks/hack/${hackSlug}/claim/${userSlug}` );
	}

	static getRandomHackAsync() : Promise<RandomHackInfo> {
		return HttpClient.getAsync<RandomHackInfo>( '/v3/hacks/random' );
	}

}
