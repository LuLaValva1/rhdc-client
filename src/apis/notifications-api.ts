import { Nullable, ConstRef } from '../util/types';
import { HttpClient } from './http-client';
import { Page } from './pagination';

interface NotificationData {
	message: string;
	link: Nullable<string>;
	timestamp: string;
	unread: boolean;
}

interface NotificationStatusData {
	unread: number;
	latest: Nullable<string>;
	latestUnread: Nullable<string>;
}

export interface NotificationSettingsData {
	hackComment: boolean;
	commentReply: boolean;
}

export class Notification {

	public readonly message: string;
	public readonly link: Nullable<string>;
	public readonly timestamp: Readonly<Date>;
	public readonly unread: boolean;

	constructor( data: NotificationData ) {
		this.message = data.message;
		this.link = data.link;
		this.timestamp = Object.freeze( new Date( data.timestamp ) );
		this.unread = data.unread;
		Object.freeze( this );
	}

}

export class NotificationStatus {

	public readonly unread: number;
	public readonly latest: Nullable<Readonly<Date>>;
	public readonly latestUnread: Nullable<Readonly<Date>>;

	constructor( data: NotificationStatusData ) {
		this.unread = data.unread;
		this.latest = data.latest ? Object.freeze( new Date( data.latest ) ) : null;
		this.latestUnread = data.latestUnread ? Object.freeze( new Date( data.latestUnread ) ) : null;
		Object.freeze( this );
	}

}

export class NotificationSettings {

	public readonly hackComment: boolean;
	public readonly commentReply: boolean;

	constructor( data: NotificationSettingsData ) {
		this.hackComment = data.hackComment;
		this.commentReply = data.commentReply;
		Object.freeze( this );
	}

}

export class NotificationsApi {

	static getNotifications( pageSize = 10 ) : Promise<Page<Notification>> {
		return Page.fetchFirstAsync(
			`/v3/notifications?pageSize=${pageSize}`,
			(data: NotificationData) => new Notification( data )
		);
	}

	static async getNotificationStatus() : Promise<NotificationStatus> {
		return new NotificationStatus(
			await HttpClient.getAsync<NotificationStatusData>( '/v3/notifications/status' )
		);
	}

	static async markAsReadAsync( since: ConstRef<Date> ) : Promise<NotificationStatus> {
		return new NotificationStatus(
			await HttpClient.postAsync<NotificationStatusData>( `/v3/notifications/read?since=${since.toISOString()}` )
		);
	}

	static async getNotificationSettingsAsync() : Promise<NotificationSettings> {
		return new NotificationSettings(
			await HttpClient.getAsync<NotificationSettingsData>( '/v3/notifications/settings' )
		);
	}

	static async updateNotificationSettingsAsync( settings: NotificationSettingsData ) {
		await HttpClient.putJsonAsync<void>( '/v3/notifications/settings', settings );
		return new NotificationSettings( settings );
	}

}
