import { Nullable } from '../util/types';
import { HttpClient } from './http-client';

interface PageData<T> {
	bookmark: Nullable<string>,
	next: Nullable<string>,
	results: T[]
}

export class Page<T> {

	#results: readonly T[];
	#converter: (_:unknown) => T;
	#pageSize: number;
	#nextUrl: Nullable<string>;
	#thisUrl: string;
	#prevUrls: string[];
	#valid: boolean;

	private constructor(
		response: PageData<T>,
		prevUrls: string[],
		thisUrl: string,
		converter: (_:unknown) => T,
		pageSize: Nullable<number> = null
	) {
		this.#results = Object.freeze( response.results );
		this.#converter = converter;
		this.#pageSize = pageSize || response.results.length;
		this.#nextUrl = response.next;
		this.#thisUrl = thisUrl;
		this.#prevUrls = prevUrls;
		this.#valid = true;
	}

	public static async fetchFirstAsync<T,DT>( url: string, converter: (_:DT) => T ) : Promise<Page<T>> {
		return new Page<T>(
			await Page.#fetchPageAsync( url, converter ),
			[],
			url,
			converter as (_:unknown) => T
		);
	}

	public get values() : readonly T[] {
		return this.#results;
	}

	public get empty() : boolean {
		return this.#results.length === 0;
	}

	public get hasPrev() : boolean {
		return this.#prevUrls.length > 0;
	}

	public get hasNext() : boolean {
		return !!this.#nextUrl;
	}

	public get indexStart() : number {
		return this.#pageSize * this.#prevUrls.length;
	}

	public get indexEnd() : number {
		return this.#results.length + this.indexStart;
	}

	public get pageIndex() : number {
		return this.#prevUrls.length;
	}

	public get isCurrent() : boolean {
		return this.#valid;
	}

	public async prevAsync() : Promise<Page<T>> {
		if( !this.#valid ) {
			throw new Error( 'This page has been moved on from and is no longer in a valid state.' );
		}

		if( !this.hasPrev ) {
			throw new Error( 'Cannot fetch earlier than the first page.' );
		}

		this.#valid = false;
		const prevUrl = this.#prevUrls.pop()!;

		try {
			return new Page<T>(
				await Page.#fetchPageAsync( prevUrl, this.#converter ),
				this.#prevUrls,
				prevUrl,
				this.#converter,
				this.#pageSize
			);
		} catch( error : unknown ) {
			this.#prevUrls.push( prevUrl );
			this.#valid = true;
			throw error;
		}
	}

	public async nextAsync() : Promise<Page<T>> {
		if( !this.#valid ) {
			throw new Error( 'This page has been moved on from and is no longer in a valid state.' );
		}

		if( !this.hasNext ) {
			throw new Error( 'Cannot fetch any more pages.' );
		}

		this.#valid = false;
		this.#prevUrls.push( this.#thisUrl );
		try {
			return new Page<T>(
				await Page.#fetchPageAsync( this.#nextUrl!, this.#converter ),
				this.#prevUrls,
				this.#nextUrl!,
				this.#converter,
				this.#pageSize
			);
		} catch( error : unknown ) {
			this.#prevUrls.pop();
			this.#valid = true;
			throw error;
		}
	}

	static async #fetchPageAsync<DT,T>( route: string, converter: (_:DT) => T ) : Promise<PageData<T>> {
		const pageData = await HttpClient.getAsync<PageData<DT>>( route ) as PageData<DT|T>;
		(pageData as PageData<T>).results = (pageData as PageData<DT>).results.map( converter );
		return pageData as PageData<T>;
	}

}
