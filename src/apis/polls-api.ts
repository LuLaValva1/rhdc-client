import { ConstArray, ConstRef, Nullable, Optional, Uuid } from '../util/types';
import { freezeObjArray } from '../util/freeze';
import { HttpClient } from './http-client';

export enum PollResultVisibility {
	Always = 'Always',
	AfterVoting = 'AfterVoting',
	WhenLocked = 'WhenLocked',
	StaffOnly = 'StaffOnly'
}

export interface Vote {
	readonly index: number;
	readonly name: string;
}

export interface VoteCount {
	readonly option: string;
	readonly count: number;
}

export interface PollUpdate {
	name?: Optional<string>,
	options?: Optional<string[]>,
	resultVisibility?: Optional<PollResultVisibility>,
	locked: Optional<boolean>
}

interface PollData {
	pollId: Uuid;
	name: string;
	options: string[];
	resultVisibility: PollResultVisibility;
	locked: boolean;
	userVote: Optional<Vote>;
	results: Optional<VoteCount[]>;
	actions: string[];
}

export class Poll {

	public readonly pollId: Uuid;
	public readonly name: string;
	public readonly options: Readonly<string[]>;
	public readonly resultVisibility: PollResultVisibility;
	public readonly locked: boolean;
	public readonly userVote: Nullable<Readonly<Vote>>;
	public readonly results: Nullable<ConstArray<VoteCount>>;
	readonly #actions: string[];

	constructor( data: PollData ) {
		this.pollId = data.pollId;
		this.name = data.name;
		this.options = Object.freeze( data.options );
		this.resultVisibility = data.resultVisibility;
		this.locked = data.locked;
		this.userVote = data.userVote ? Object.freeze( data.userVote ) : null;
		this.results = data.results ? freezeObjArray( data.results ) : null;
		this.#actions = data.actions;
		Object.freeze( this );
	}

	public get canEdit() : boolean {
		return this.#actions.includes( 'edit' );
	}

	public get canVote() : boolean {
		return this.#actions.includes( 'vote' );
	}

	public get canResetVotes() : boolean {
		return this.#actions.includes( 'reset' );
	}

}

export class PollsApi {

	static async getPollAsync( pollId: Uuid ) : Promise<Poll> {
		return new Poll( await HttpClient.getAsync<PollData>( `/v3/polls/${pollId}?results=true` ) );
	}

	static async voteAsync( pollId: Uuid, option: number ) : Promise<void> {
		await HttpClient.postAsync( `/v3/polls/${pollId}/vote/${option}` );
	}

	static async createPollAsync(
		name: string,
		options: ConstRef<string[]>,
		resultVisibility: PollResultVisibility
	) : Promise<Poll> {
		return new Poll( await HttpClient.postJsonAsync<PollData>( '/v3/polls', {
			name: name,
			options: options,
			resultVisibility: resultVisibility
		}));
	}

	static async updatePollAsync( pollId: Uuid, update: ConstRef<PollUpdate> ) : Promise<Poll> {
		return new Poll( await HttpClient.patchJsonAsync( `/v3/polls/${pollId}`, update ) );
	}

	static deletePollAsync( pollId: Uuid ) : Promise<void> {
		return HttpClient.deleteAsync<void>( `/v3/polls/${pollId}` );
	}

	static async resetPollAsync( pollId: Uuid ) : Promise<void> {
		await HttpClient.postAsync( `/v3/polls/${pollId}/reset` );
	}


}
