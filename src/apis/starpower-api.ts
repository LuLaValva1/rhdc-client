import { ConstRef, HexId, UserId, Nullable, Optional, Weak } from '../util/types';
import { HttpClient } from './http-client';
import { UserRef } from './users-api';

interface StarpowerData {
	hackId: HexId;
	user: UserRef;
	starPoints: number;
	claimedStarPoints: number;
	playTime: number;
	rating: Nullable<number>;
	difficulty: Nullable<number>;
	hackComplete: boolean;
	claimedHackComplete: boolean;
	proofLink: Optional<string>;
	claimedStarCountLastUpdated: Nullable<string>;
	starCountLastUpdated: Nullable<string>;
	playTimeLastUpdated: Nullable<string>;
	actions: string[];
	approved: boolean;
}

export interface StarpowerUpdateDto {
	starPoints?: Optional<number>;
	playTime?: Optional<number>;
	rating?: Weak<number>;
	difficulty?: Weak<number>;
	hackComplete?: Optional<boolean>;
	proofLink?: Weak<string>;
}

export class Starpower {
	
	public readonly hackId: HexId;
	public readonly user: Readonly<UserRef>;
	public readonly starPoints: number;
	public readonly claimedStarPoints: number;
	public readonly playTime: number;
	public readonly rating: Nullable<number>;
	public readonly difficulty: Nullable<number>;
	public readonly hackComplete: boolean;
	public readonly claimedHackComplete: boolean;
	public readonly proofLink: Nullable<string>;
	public readonly claimedStarCountLastUpdated: Nullable<ConstRef<Date>>;
	public readonly starCountLastUpdated: Nullable<ConstRef<Date>>;
	public readonly playTimeLastUpdated: Nullable<ConstRef<Date>>;
	public readonly approved: boolean;
	readonly #actions: string[];

	constructor( data: StarpowerData ) {
		this.hackId = data.hackId;
		this.user = Object.freeze( data.user );
		this.starPoints = data.starPoints;
		this.claimedStarPoints = data.claimedStarPoints;
		this.playTime = data.playTime;
		this.rating = data.rating || null;
		this.difficulty = data.difficulty || null;
		this.hackComplete = data.hackComplete;
		this.claimedHackComplete = data.claimedHackComplete;
		this.proofLink = data.proofLink || null;
		this.claimedStarCountLastUpdated = data.claimedStarCountLastUpdated ? Object.freeze( new Date( data.claimedStarCountLastUpdated ) ) : null;
		this.starCountLastUpdated = data.starCountLastUpdated ? Object.freeze( new Date( data.starCountLastUpdated ) ) : null;
		this.playTimeLastUpdated = data.playTimeLastUpdated ? Object.freeze( new Date( data.playTimeLastUpdated ) ) : null;
		this.approved = data.approved;
		this.#actions = data.actions;
		Object.freeze( this );
	}

	public get canEdit() : boolean {
		return this.#actions.includes( 'edit' );
	}

	public get canDelete() : boolean {
		return this.#actions.includes( 'delete' );
	}

	public get canApprove() : boolean {
		return this.#actions.includes( 'approve' );
	}

}

export class StarpowerApi {

	static async getStarpowerAsync( userSlug: string | UserId, hackSlug: string | HexId ) : Promise<Starpower> {
		return new Starpower( await HttpClient.getAsync<StarpowerData>( `/unstable/starpower/user/${userSlug}/hack/${hackSlug}` ) );
	}

	static async updateStarpowerAsync( hackSlug: string | HexId, update: StarpowerUpdateDto ) : Promise<Starpower> {
		return new Starpower( await HttpClient.patchJsonAsync<StarpowerData>( `/unstable/starpower/hack/${hackSlug}`, update ) );
	}

	static async approveStarpowerAsync( userSlug: string | UserId, hackSlug: string | HexId ) : Promise<Starpower> {
		return new Starpower( await HttpClient.postAsync<StarpowerData>( `/unstable/starpower/user/${userSlug}/hack/${hackSlug}/approve` ) );
	}

	static async rejectStarpowerAsync( userSlug: string | UserId, hackSlug: string | HexId, reason?: Weak<string> ) : Promise<Starpower> {
		if( reason ) {
			return new Starpower( await HttpClient.postTextAsync<StarpowerData>( `/unstable/starpower/user/${userSlug}/hack/${hackSlug}/reject`, reason ) );
		} else {
			return new Starpower( await HttpClient.postAsync<StarpowerData>( `/unstable/starpower/user/${userSlug}/hack/${hackSlug}/reject` ) );
		}
	}

	static getUserTotalPlayTimeAsync( userSlug: string | UserId ) : Promise<number> {
		return HttpClient.getAsync<number>( `/v3/starpower/user/${userSlug}/playtime` );
	}

}
