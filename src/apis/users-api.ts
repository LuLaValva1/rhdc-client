import { Role } from '../auth/roles';
import { UserId, Nullable, Optional, ConstArray, Weak, ConstRef } from '../util/types';
import { HttpClient } from './http-client';
import { setAuthToken } from "../auth/auth-token";
import { setUserContext, UserAuth } from '../decorators/user-context';
import { Page } from './pagination';

export enum Gender {
	Masculine = 'Masculine',
	Feminine = 'Feminine',
	Neuter = 'Neuter'
}

export interface WeakUserRef {
	username: string;
	userId: Nullable<UserId>;
}

export interface UserRef {
	username: string;
	userId: UserId;
}

export interface WeakRichUserRef {
	username: string;
	userId: Nullable<UserId>;
	country: Nullable<string>;
	gender: Nullable<Gender>;
}

export interface RichUserRef {
	username: string;
	userId: UserId;
	country: Nullable<string>;
	gender: Nullable<Gender>;
}

export interface SocialHandle {
	name: string;
	href: string;
}

export interface SocialMediaLinks {
	twitch?: Optional<Readonly<SocialHandle>>;
	youtube?: Optional<Readonly<SocialHandle>>;
	twitter?: Optional<Readonly<SocialHandle>>;
	gitlab?: Optional<Readonly<SocialHandle>>;
	github?: Optional<Readonly<SocialHandle>>;
}

interface UserData {
	userId: UserId;
	username: string;
	gender: Nullable<Gender>;
	country: Nullable<string>;
	role: Role;
	birthday: Nullable<string>;
	joinedDate: string;
	email?: Optional<string>;
	starPoints: number;
	kaizoStarPoints: number;
	starsCollected: number;
	bio: string;
	social: SocialMediaLinks;
	actions: string[];
}

export interface UserUpdate {
	birthday?: Weak<ConstRef<Date>>;
	country?: Weak<string>;
	gender?: Weak<Gender>;
	bio?: Weak<string>;
	role?: Optional<Role>;

	twitch?: Weak<string>;
	youtube?: Weak<string>;
	twitter?: Weak<string>;
	gitlab?: Weak<string>;
	github?: Weak<string>;
}

interface UserUpdateDto extends Omit<UserUpdate, 'birthday'> {
	birthday?: Weak<string>;
}

interface GenericActivityData {
	hackName: string;
	hackHref: string;
	timestamp: string;
}

interface CommentActivityData extends GenericActivityData {
	message: string;
	isReply: boolean;
}

interface ProgressActivityData extends GenericActivityData {
	starsCollected: number;
	starsTotal: number;
}

interface PlayTimeActivityData extends GenericActivityData {
	playTime: number;
}

interface UserActivityData {
	recentComments: CommentActivityData[];
	recentProgress: ProgressActivityData[];
	recentlyPlayed: PlayTimeActivityData[];
	mostPlayed: PlayTimeActivityData[];
}

export class User implements Readonly<RichUserRef> {

	public readonly userId: UserId;
	public readonly username: string;
	public readonly gender: Nullable<Gender>;
	public readonly country: Nullable<string>;
	public readonly role: Role;
	public readonly birthday: Nullable<Readonly<Date>>;
	public readonly joinedDate: Readonly<Date>;
	public readonly email: Nullable<string>;
	public readonly starPoints: number;
	public readonly kaizoStarPoints: number;
	public readonly starsCollected: number;
	public readonly bio: string;
	public readonly social: Readonly<SocialMediaLinks>;
	readonly #actions: string[];

	public constructor( data: UserData ) {
		this.userId = data.userId;
		this.username = data.username;
		this.gender = data.gender;
		this.country = data.country;
		this.role = data.role;
		this.birthday = data.birthday ? Object.freeze( new Date( data.birthday ) ) : null;
		this.joinedDate = Object.freeze( new Date( data.joinedDate ) );
		this.email = data.email || null;
		this.starPoints = data.starPoints;
		this.kaizoStarPoints = data.kaizoStarPoints;
		this.starsCollected = data.starsCollected;
		this.bio = data.bio;
		this.social = Object.freeze( data.social );
		this.#actions = data.actions;
		Object.freeze( this );
	}

	public get canEdit() {
		return this.#actions.includes( 'edit' );
	}

	public get canReorderPlaylists() {
		return this.#actions.includes( 'reorder-playlists' );
	}

}

class IActivity {

	public readonly hackName: string;
	public readonly hackHref: string;
	public readonly timestamp: Readonly<Date>;

	protected constructor( data: GenericActivityData ) {
		this.hackName = data.hackName;
		this.hackHref = data.hackHref;
		this.timestamp = Object.freeze( new Date( data.timestamp ) );
	}

}

export class CommentActivity extends IActivity {

	public readonly message: string;
	public readonly isReply: boolean;

	constructor( data: CommentActivityData ) {
		super( data );
		this.message = data.message;
		this.isReply = data.isReply;
		Object.freeze( this );
	}
}

export class ProgressActivity extends IActivity {

	public readonly starsCollected: number;
	public readonly starsTotal: number;

	constructor( data: ProgressActivityData ) {
		super( data );
		this.starsCollected = data.starsCollected;
		this.starsTotal = data.starsTotal;
		Object.freeze( this );
	}
}

export class PlayTimeActivity extends IActivity {

	public readonly playTime: number;

	constructor( data: PlayTimeActivityData ) {
		super( data );
		this.playTime = data.playTime;
		Object.freeze( this );
	}

	public formatPlayTime() : string {
		const hours = Math.floor( this.playTime / 3600000 );
		const minutes = Math.floor( (this.playTime % 3600000) / 60000);
		const seconds = Math.floor( (this.playTime % 60000) / 1000);

		if( hours > 0 ) {
			return `${hours}h ${minutes}m ${seconds}s`;
		} else if( minutes > 0 ) {
			return `${minutes}m ${seconds}s`;
		} else {
			return `${seconds}s`;
		}
	}

}

export class UserActivity {

	public readonly recentComments: ConstArray<CommentActivity>;
	public readonly recentProgress: ConstArray<ProgressActivity>;
	public readonly recentlyPlayed: ConstArray<PlayTimeActivity>;
	public readonly mostPlayed: ConstArray<PlayTimeActivity>;

	constructor( data: UserActivityData ) {
		this.recentComments = Object.freeze( data.recentComments.map( a => new CommentActivity( a ) ) );
		this.recentProgress = Object.freeze( data.recentProgress.map( a => new ProgressActivity( a ) ) );
		this.recentlyPlayed = Object.freeze( data.recentlyPlayed.map( a => new PlayTimeActivity( a ) ) );
		this.mostPlayed = Object.freeze( data.mostPlayed.map( a => new PlayTimeActivity( a ) ) );
		Object.freeze( this );
	}

}

export class UsersApi {

	static async getUserAsync( userSlug: string | UserId ) : Promise<User> {
		return new User( await HttpClient.getAsync<UserData>( `/v3/user/${userSlug}` ) );
	}

	static async updateUserAsync( userSlug: string | UserId, update: ConstRef<UserUpdate> ) : Promise<User> {
		const updateDto = { ...update };
		if( updateDto.birthday ) (updateDto as UserUpdateDto).birthday = updateDto.birthday.toISOString();
		return new User( await HttpClient.patchJsonAsync<UserData>( `/v3/user/${userSlug}`, updateDto ) );
	}

	static async uploadAvatarAsync( userSlug: string | UserId, avatar: File ) : Promise<void> {
		return HttpClient.putFileAsync( `/v3/user/${userSlug}/avatar`, avatar );
	}

	static canRenameSelfAsync() : Promise<void> {
		return HttpClient.headAsync( '/v3/userinfo/rename' );
	}

	static async renameSelfAsync( password: string, newUsername: string ) : Promise<void> {
		const response = await HttpClient.postJsonAsync<UserAuth>( '/v3/userinfo/rename', {
			password: password,
			username: newUsername
		});
		setUserContext( response );
	}

	static async changePasswordAsync( oldPassword: string, newPassword: string ) : Promise<void> {
		const response = await HttpClient.postJsonAsync<{ token: string }>( '/v3/userinfo/changepassword', {
			oldPassword: oldPassword,
			newPassword: newPassword
		});
		setAuthToken( response.token );
	}

	static async getUserActivityAsync( userSlug: string | UserId ) : Promise<UserActivity> {
		return new UserActivity( await HttpClient.getAsync<UserActivityData>( `/v3/user/${userSlug}/activity` ) );
	}

	static searchUsersAsync( searchTerm: string, pageSize = 10 ) : Promise<Page<User>> {
		return Page.fetchFirstAsync<User,UserData>(
			`/v3/users/search/${encodeURIComponent( searchTerm )}?pagesize=${pageSize}`,
			data => new User( data )
		);
	}

	static async getAllUsernamesAsync() : Promise<Readonly<string[]>> {
		return Object.freeze( await HttpClient.getAsync<string[]>( '/v3/users/usernames' ) );
	}

}
