import { Nullable } from "../util/types";

const TOKEN_KEY = 'jwt';

let g_authToken: Nullable<string> = null;

export const initAuthToken = function() : Nullable<string> {
	g_authToken = window.localStorage.getItem( TOKEN_KEY );
	return g_authToken;
};

export const getAuthToken = function() : Nullable<string> {
	return g_authToken;
};

export const setAuthToken = function( token: Nullable<string> ) : void {
	g_authToken = token;
	if( token ) {
		window.localStorage.setItem( TOKEN_KEY, token );
	} else {
		window.localStorage.removeItem( TOKEN_KEY );
	}
}
