import { DEV_MODE } from '../config';

export const RHDC_DEV_KEY = DEV_MODE ? window.localStorage.getItem( 'rhdc-dev-key' ) : null;
