export enum Role {
	Banned = 'Banned',
	Unverified = 'Unverified',
	Restricted = 'Restricted',
	User = 'User',
	PowerUser = 'Power User',
	TrustedUser = 'Trusted User',
	Moderator = 'Moderator',
	Staff = 'Staff'
}
