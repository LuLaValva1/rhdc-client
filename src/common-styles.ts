import { css } from 'lit';

export const rhdcIconButtonStyles = css`
	sl-icon-button.danger::part(base):hover,
	sl-icon-button.danger::part(base):focus {
		color: var(--sl-color-danger-600);
	}

	sl-icon-button.danger::part(base):active {
		color: var(--sl-color-danger-700);
	}
`;

export const rhdcLinkStyles = css`
	a {
		color: var(--sl-color-primary-600);
		text-decoration: inherit;
	}

	a:hover, a:focus-visible {
		color: var(--sl-color-primary-700);
	}

	a:focus-visible {
		text-decoration: dotted underline;
	}
`;
