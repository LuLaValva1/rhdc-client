import { css } from 'lit';

export const commentStyles = css`
	sl-card {
		width: 100%;
	}

	sl-card > div {
		display: flex;
		flex-direction: column;
		align-items: stretch;
		gap: var(--sl-spacing-2x-small);
	}

	.header {
		display: flex;
		align-items: center;
		gap: var(--sl-spacing-x-small);
	}

	.footer {
		display: flex;
		align-items: baseline;
		gap: var(--sl-spacing-x-small);
	}

	.spacer {
		flex-grow: 1;
	}

	.actions {
		display: flex;
	}

	.timestamp {
		color: var(--sl-color-neutral-600);
	}

	p {
		white-space: pre-wrap;
	}
`;
