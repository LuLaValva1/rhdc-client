import { css, CSSResultGroup, html, HTMLTemplateResult, nothing, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { map } from 'lit/directives/map.js';
import { RhdcElement } from '../../rhdc-element';
import { Nullable, Uuid } from '../../util/types';
import { RhdcCommentCreatedEvent } from '../../events/rhdc-comment-created-event';
import { Comment, CommentsApi } from '../../apis/comments-api';
import { rhdcAsyncAppend } from '../../util/lit-extensions';
import { AsyncPipe } from '../../util/async-pipe';
import { Page } from '../../apis/pagination';
import { getErrorMessage } from '../../util/http-error-parser';
import { nextCycle } from '../../util/delay';
import { RhdcCommentDeletedEvent } from '../../events/rhdc-comment-deleted-event';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '../../widgets/rhdc-load-more';
import './rhdc-new-comment';

@customElement( 'rhdc-comment-replies' )
export class RhdcCommentReplies extends RhdcElement {

	@property({ attribute: false })
	parentId!: Uuid;

	@property({ attribute: 'replying', type: Boolean })
	replying = false;

	@state()
	loading = false;

	@state()
	done = false;

	@state()
	newReplies: Comment[] = [];

	#replies!: AsyncPipe<Comment>;
	#currentPage: Nullable<Page<Comment>> = null;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-x-small);
				margin-left: var(--sl-spacing-large);
				margin-top: var(--sl-spacing-x-small);
			}

			:host([hidden]) {
				display: none;
			}
		`;
	}

	override render() : unknown {
		let lazyLoader: unknown = nothing;
		if( !this.done ) {
			lazyLoader = html`<rhdc-load-more ?loading=${this.loading} @rhdc-load-more=${this.#loadMore}></rhdc-load-more>`;
		}

		let newReply: unknown = nothing;
		if( this.replying ) {
			newReply = html`<rhdc-new-comment .parentId=${this.parentId} reply @rhdc-comment-created-event=${this.#replySaved}></rhdc-new-comment>`;
		}

		return html`
			${newReply}
			${rhdcAsyncAppend( this.#replies, this.#renderComment.bind( this ) )}
			${lazyLoader}
			${map( this.newReplies, this.#renderComment.bind( this ) )}
		`;
	}

	constructor() {
		super();
		this.#replies = new AsyncPipe<Comment>();
	}

	protected override firstUpdated( changedProperties: PropertyValues ) : void {
		super.firstUpdated( changedProperties );
		this.#jumpToLinkedComment();
	}

	protected override willUpdate( changedProperties: PropertyValues ): void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'hack' ) ) {
			this.newReplies = [];
			this.#currentPage = null;
			this.#replies = new AsyncPipe<Comment>();
		}
	}

	async #replySaved( event: RhdcCommentCreatedEvent ) : Promise<void> {
		if( this.done ) {
			this.newReplies = [ ...this.newReplies, event.detail.comment ];
		} else {
			this.#replies.push( event.detail.comment );
		}

		await this.updateComplete;
		const reply = this.shadowRoot?.getElementById( event.detail.comment.commentId );
		if( reply ) reply.scrollIntoView({ behavior: 'smooth', block: 'center' });
	}

	#renderComment( comment: Comment ) : HTMLTemplateResult {
		return html`<rhdc-comment .id="${comment.commentId}" .comment=${comment} @rhdc-comment-deleted-event=${this.#commentDeleted}></rhdc-comment>`;
	}

	#commentDeleted( event: RhdcCommentDeletedEvent ) : void {
		const index = this.newReplies.findIndex( reply => reply.commentId === event.detail.commentId );
		// Don't need to update the reference since the commment already removes itself from the DOM on deletion
		if( index >= 0 ) this.newReplies.splice( index, 1 );
	}

	async #loadMore() : Promise<void> {
		this.loading = true;
		try {
			if( this.#currentPage ) {
				this.#currentPage = await this.#currentPage.nextAsync();
			} else {
				this.#currentPage = await CommentsApi.getRepliesAsync( this.parentId );
			}

			this.#replies.pushMany( this.#currentPage.values );
			if( !this.#currentPage.hasNext ) {
				this.done = true;
				this.#replies.end();
			}
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load replies', getErrorMessage( exception ) );
			this.done = true;
			this.#replies.end();
		} finally {
			this.loading = false;
		}
	}

	async #jumpToLinkedComment() : Promise<void> {
		const replyId = this.#tryGetLinkedReply();
		if( !replyId ) return;

		this.loading = true;
		try {
			while( !this.done ) {
				await this.#loadMore();
				this.loading = true;
				await this.updateComplete;
				await nextCycle();

				const foundComment = this.shadowRoot?.getElementById( replyId );
				if( foundComment ) {
					foundComment.focus( { focusVisible: true, preventScroll: true } as unknown as FocusOptions );
					foundComment.scrollIntoView({ behavior: 'smooth', block: 'center' });
					break;
				}
			}
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load some hack comments', getErrorMessage( exception ) );
			this.done = true;
			this.#replies.end();
		} finally {
			this.loading = false;
		}
	}

	#tryGetLinkedReply() : string {
		let fragment = window.location.hash;
		if( !fragment ) return '';
		fragment = fragment.substring( 1 );
		if( !fragment ) return '';
		const parts = fragment.split( '_' );
		if( !parts || parts.length < 2 ) return '';
		const commentId = parts[1];
		if( !/^[0-9a-f]{4}(?:[0-9a-f]{4}-){4}[0-9a-f]{12}$/i.test( commentId ) ) return '';
		return commentId;
	}

}
