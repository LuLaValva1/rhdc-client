import { CSSResultGroup, html, HTMLTemplateResult, nothing } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { Comment, HackComment, CommentsApi } from '../../apis/comments-api';
import { rhdcIconButtonStyles } from '../../common-styles';
import { getErrorMessage } from '../../util/http-error-parser';
import { ConstRef, Optional, Weak } from '../../util/types';
import { commentStyles } from './comment-styles';
import { RhdcCommentDeletedEvent } from '../../events/rhdc-comment-deleted-event';
import SlTextarea from '@shoelace-style/shoelace/dist/components/textarea/textarea';
import '../../widgets/rhdc-avatar';
import '../common/rhdc-username';
import './rhdc-comment-replies';
import '@shoelace-style/shoelace/dist/components/card/card';
import '@shoelace-style/shoelace/dist/components/tooltip/tooltip';
import '@shoelace-style/shoelace/dist/components/icon-button/icon-button';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/relative-time/relative-time';
import '@shoelace-style/shoelace/dist/components/textarea/textarea';

function relativeTime( date: ConstRef<Date> ) : HTMLTemplateResult {
	const isRecent = (Date.now() - date.getTime()) < 1000 * 60 * 60;
	return html`<sl-relative-time .date=${date as Date} ?sync=${isRecent}></sl-relative-time>`;
}

@customElement( 'rhdc-comment' )
export class RhdcComment extends RhdcElement {

	@property({ attribute: false })
	comment!: Comment;

	@state()
	editing = false;

	@state()
	expanded = false;

	@state()
	loading = false;

	@state()
	loadReplies = false;

	@state()
	replying = false;

	@query( 'sl-textarea' )
	textarea!: SlTextarea;

	static override get styles() : CSSResultGroup {
		return [ rhdcIconButtonStyles, commentStyles ];
	}

	override render() : unknown {
		let replies: unknown = nothing;
		if( this.loadReplies ) {
			replies = html`
				<rhdc-comment-replies
					.parentId=${this.comment.commentId}
					?replying=${this.replying}
					?hidden=${!this.expanded}
					@rhdc-comment-created-event=${this.#replyDone}
					@rhdc-comment-cancel-event=${this.#replyDone}
				></rhdc-comment-replies>
			`;
		}

		return html`
			<sl-card>
				<div tabindex="-1">
					${this.#renderHeader()}
					${this.#renderBody()}
					${this.#renderFooter()}
				</div>
			</sl-card>
			${replies}
		`;
	}

	public override focus( options?: Optional<FocusOptions> ): void {
		super.focus( options );
		const card = this.shadowRoot?.querySelector( 'sl-card' )?.shadowRoot?.querySelector( '::part( base )' ) as Weak<HTMLElement>;
		if( !card ) return;

		if( !(card.tabIndex >= 0) ) {
			card.tabIndex = -1;
		}

		card.focus( options );
	}

	public expand() : void {
		if( this.comment instanceof HackComment ) {
			if( this.comment.replyCount <= 0 ) return;
			this.expanded = true;
			this.loadReplies = true;
		}
	}

	#renderHeader() : HTMLTemplateResult {
		const actionButtons: HTMLTemplateResult[] = [];
		if( !this.editing ) {
			if( this.comment.canEdit ) {
				actionButtons.push( html`<sl-tooltip content="Edit Comment">
					<sl-icon-button
						name="pencil"
						label="Edit Comment"
						@click=${() => this.editing = true}
					></sl-icon-button>
				</sl-tooltip>` );
			}

			if( this.comment.canDelete ) {
				actionButtons.push( html`<sl-tooltip content="Delete Comment">
					<sl-icon-button
						class="danger"
						name="trash"
						label="Delete Comment"
						@click=${this.#deleteComment}
					></sl-icon-button>
				</sl-tooltip>` );
			}

			if( this.comment.canReport ) {
				actionButtons.push( html`<sl-tooltip content="Report Comment">
					<sl-icon-button
						class="danger"
						name="flag"
						label="Report Comment"
						@click=${this.#reportComment}
					></sl-icon-button>
				</sl-tooltip>` );
			}
		}

		let timestamp = relativeTime( this.comment.datePosted );
		if( this.comment.dateEdited ) {
			timestamp = html`${timestamp} (edited ${relativeTime( this.comment.dateEdited )})`;
		}

		return html`
			<div class="header">
				<rhdc-avatar .username=${this.comment.author.username}></rhdc-avatar>
				<div class="title">
					<rhdc-username .user=${this.comment.author}></rhdc-username><br/>
					<span class="timestamp">${timestamp}</span>
				</div>
				<div class="spacer"></div>
				<div class="actions">${actionButtons}</div>
			</div>
		`;
	}
	
	#renderBody() : HTMLTemplateResult {
		if( !this.editing ) {
			return html`<p>${this.comment.message}</p>`;
		}

		return html`<sl-textarea
			placeholder="Enter your comment here"
			minlength="1"
			?readonly=${this.loading}
			.value=${this.comment.message}
			autocapitalize='sentences'
			autocorrect='on'
			autocomplete='off'
			enterkeyhint='done'
			inputmode='text'
			spellcheck
			required
		></sl-textarea>`;
	}

	#renderFooter() : unknown {
		if( this.editing ) {
			return html`<div class="footer">
				<div class="spacer"></div>
				<sl-button
					variant="primary"
					?loading=${this.loading}
					@click=${this.#saveComment}
				>Save</sl-button>
				<sl-button
					variant="neutral"
					?disabled=${this.loading}
					@click=${this.#cancelEdit}
				>Cancel</sl-button>
			</div>`;
		}

		if( !(this.comment instanceof HackComment) ) return nothing;
		
		const buttons: HTMLTemplateResult[] = [];
		if( this.comment.replyCount > 0 ) {
			buttons.push( html`
				<sl-button variant="text" @click=${this.#expandOrCollapse}>
					<sl-icon slot="prefix" name=${this.expanded ? 'chevron-up' : 'chevron-down'}></sl-icon>
					<span>${this.comment.replyCount}&nbsp;${this.comment.replyCount === 1 ? 'reply' : 'replies'}</span>
				</sl-button>
			` );
		}

		if( this.comment.canReply ) {
			buttons.push( html`<sl-button @click=${this.#reply}>Reply</sl-button>` );
		}

		if( buttons.length === 0 ) return nothing;
		return html`<div class="footer">${buttons}</div>`;
	}

	async #saveComment() : Promise<void> {
		this.loading = true;
		try {
			this.comment = await CommentsApi.editCommentAsync( this.comment.commentId, this.textarea.value );
			this.toastSuccess( 'Comment updated.' );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to update comment.', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	#cancelEdit() : void {
		this.textarea.value = this.comment.message;
		this.editing = false;
	}

	async #deleteComment() : Promise<void> {
		if( await this.confirmAsync( 'Confirm Deletion', 'Are you sure you want to delete this comment?', 'Delete', 'Cancel' ) ) {
			this.loading = true;
			try {
				await CommentsApi.deleteCommentAsync( this.comment.commentId );
				this.parentNode?.removeChild( this );
				this.dispatchEvent( RhdcCommentDeletedEvent.create( this.comment.commentId ) );
				this.toastSuccess( 'Comment Deleted' );
			} catch( exception: unknown ) {
				this.toastError( 'Failed to delete comment', getErrorMessage( exception ) );
			} finally {
				this.loading = false;
			}
		}
	}

	async #reportComment() : Promise<void> {
		if( await this.confirmAsync( 'Report Comment', 'Are you sure you want to report this comment to moderators?', 'Report', 'Cancel' ) ) {
			this.loading = true;
			try {
				await CommentsApi.reportCommentAsync( this.comment.commentId );
				this.toastInfo( 'This comment has been reported to the moderators and staff.' );
			} catch( exception: unknown ) {
				this.toastError( 'Failed to report comment', getErrorMessage( exception ) );
			} finally {
				this.loading = false;
			}
		}
	}

	#reply() : void {
		this.loadReplies = true;
		this.expanded = true;
		this.replying = true;
	}

	#expandOrCollapse() : void {
		this.expanded = !this.expanded;
		this.loadReplies = true;
	}

	#replyDone( event: Event ) {
		this.replying = false;
		event.stopPropagation();
	}

}
