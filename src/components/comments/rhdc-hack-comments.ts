import { css, CSSResultGroup, html, HTMLTemplateResult, nothing, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { map } from 'lit/directives/map.js';
import { RhdcElement } from '../../rhdc-element';
import { Nullable, Optional } from '../../util/types';
import { RhdcCommentCreatedEvent } from '../../events/rhdc-comment-created-event';
import { Comment, CommentsApi } from '../../apis/comments-api';
import { Hack } from '../../apis/hacks-api';
import { rhdcAsyncAppend } from '../../util/lit-extensions';
import { AsyncPipe } from '../../util/async-pipe';
import { Page } from '../../apis/pagination';
import { getErrorMessage } from '../../util/http-error-parser';
import { nextCycle } from '../../util/delay';
import { RhdcCommentDeletedEvent } from '../../events/rhdc-comment-deleted-event';
import { RhdcComment } from './rhdc-comment';
import '../../widgets/rhdc-load-more';
import './rhdc-comment';
import './rhdc-new-comment';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '@shoelace-style/shoelace/dist/components/button/button';

function relevantHackDataChanged( a: Optional<Hack>, b: Optional<Hack> ) : boolean {
	return a?.hackId !== b?.hackId || a?.canComment !== b?.canComment;
}

@customElement( 'rhdc-hack-comments' )
export class RhdcHackComments extends RhdcElement {

	@property({ attribute: false, hasChanged: relevantHackDataChanged })
	hack!: Hack;

	@state()
	replying = false;

	@state()
	loading = false;

	@state()
	done = false;

	@state()
	newComments: Comment[] = [];

	#comments: AsyncPipe<Comment>;
	#currentPage: Nullable<Page<Comment>> = null;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-x-small);
			}

			:host([hidden]) {
				display: none;
			}

			h2 {
				margin: 0;
			}

			sl-button {
				align-self: flex-start;
			}
		`;
	}

	override render() : unknown {
		let lazyLoader: unknown = nothing;
		if( !this.done ) {
			lazyLoader = html`<rhdc-load-more ?loading=${this.loading} @rhdc-load-more=${this.#loadMore}></rhdc-load-more>`;
		}

		let newReply: unknown = nothing;
		if( this.replying ) {
			newReply = html`
				<rhdc-new-comment
					.parentId=${this.hack.hackId}
					@rhdc-comment-created-event=${this.#replySaved}
					@rhdc-comment-cancel-event=${this.#replyCanceled}
				></rhdc-new-comment>
			`;
		} else if( this.hack.canComment ) {
			newReply = html`<sl-button variant="primary" @click=${() => this.replying = true}>Add Comment</sl-button>`;
		}

		return html`
			<h2>Comments</h2>
			${newReply}
			${map( this.newComments, this.#renderComment.bind( this ) )}
			${rhdcAsyncAppend( this.#comments, this.#renderComment.bind( this ) )}
			${lazyLoader}
		`;
	}

	constructor() {
		super();
		this.#comments = new AsyncPipe<Comment>();
	}

	protected override firstUpdated( changedProperties: PropertyValues ) : void {
		super.firstUpdated( changedProperties );
		this.#jumpToLinkedComment();
	}

	protected override willUpdate( changedProperties: PropertyValues ): void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'hack' ) ) {
			this.newComments = [];
			this.#currentPage = null;
			this.#comments = new AsyncPipe<Comment>();
			this.done = false;
		}
	}

	override connectedCallback() : void {
		super.connectedCallback();
		if( this.#tryGetLinkedComment() ) {
			this.loading = true;
		}
	}

	#replySaved( event: RhdcCommentCreatedEvent ) {
		this.newComments = [ event.detail.comment, ...this.newComments ];
		this.replying = false;
		event.stopPropagation();
	}

	#replyCanceled( event: Event ) {
		this.replying = false;
		event.stopPropagation();
	}

	#renderComment( comment: Comment ) : HTMLTemplateResult {
		return html`<rhdc-comment .id="${comment.commentId}" .comment=${comment} @rhdc-comment-deleted-event=${this.#commentDeleted}></rhdc-comment>`;
	}

	async #loadMore() : Promise<void> {
		this.loading = true;
		try {
			if( this.#currentPage ) {
				this.#currentPage = await this.#currentPage.nextAsync();
			} else {
				this.#currentPage = await CommentsApi.getHackCommentsAsync( this.hack.hackId );
			}

			this.#comments.pushMany( this.#currentPage.values );
			if( !this.#currentPage.hasNext ) {
				this.done = true;
				this.#comments.end();
			}
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load hack comments', getErrorMessage( exception ) );
			this.done = true;
			this.#comments.end();
		} finally {
			this.loading = false;
		}
	}

	async #jumpToLinkedComment() : Promise<void> {
		const linkedComment = this.#tryGetLinkedComment();
		if( !linkedComment ) return;

		this.loading = true;
		try {
			while( !this.done ) {
				await this.#loadMore();
				this.loading = true;
				await this.updateComplete;
				await nextCycle();

				const foundComment = this.shadowRoot?.getElementById( linkedComment );
				if( foundComment ) {
					foundComment.focus( { focusVisible: true, preventScroll: true } as unknown as FocusOptions );
					foundComment.scrollIntoView({ behavior: 'smooth', block: 'center' });
					if( foundComment instanceof RhdcComment ) {
						const replyId = this.#tryGetLinkedReply();
						if( replyId ) foundComment.expand();
					}
					break;
				}
			}
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load hack comments', getErrorMessage( exception ) );
			this.done = true;
			this.#comments.end();
		} finally {
			this.loading = false;
		}
	}

	#tryGetLinkedComment() : string {
		let fragment = window.location.hash;
		if( !fragment ) return '';
		fragment = fragment.substring( 1 );
		if( !fragment ) return '';
		const parts = fragment.split( '_' );
		if( !parts ) return '';
		const commentId = parts[0];
		if( !/^[0-9a-f]{4}(?:[0-9a-f]{4}-){4}[0-9a-f]{12}$/i.test( commentId ) ) return '';
		return commentId;
	}

	#tryGetLinkedReply() : string {
		let fragment = window.location.hash;
		if( !fragment ) return '';
		fragment = fragment.substring( 1 );
		if( !fragment ) return '';
		const parts = fragment.split( '_' );
		if( !parts || parts.length < 2 ) return '';
		const commentId = parts[1];
		if( !/^[0-9a-f]{4}(?:[0-9a-f]{4}-){4}[0-9a-f]{12}$/i.test( commentId ) ) return '';
		return commentId;
	}

	#commentDeleted( event: RhdcCommentDeletedEvent ) : void {
		const index = this.newComments.findIndex( reply => reply.commentId === event.detail.commentId );
		// Don't need to update the reference since the commment already removes itself from the DOM on deletion
		if( index >= 0 ) this.newComments.splice( index, 1 );
	}

}
