import { CSSResultGroup, html, css, PropertyValues } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { CommentsApi } from '../../apis/comments-api';
import { getErrorMessage } from '../../util/http-error-parser';
import { HexId, Nullable, Optional, Uuid } from '../../util/types';
import { commentStyles } from './comment-styles';
import { RhdcCommentCreatedEvent } from '../../events/rhdc-comment-created-event';
import { rhdcUserContext, UserAuth } from '../../decorators/user-context';
import { RichUserRef, UsersApi } from '../../apis/users-api';
import { AlertFactory } from '../../util/alert-factory';
import SlTextarea from '@shoelace-style/shoelace/dist/components/textarea/textarea';
import '../../widgets/rhdc-avatar';
import '../common/rhdc-username';
import '@shoelace-style/shoelace/dist/components/card/card';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/textarea/textarea';

enum CommentStatus {
	Empty,
	Good,
	Troubleshooting
}

@customElement( 'rhdc-new-comment' )
export class RhdcNewComment extends RhdcElement {

	@property({ attribute: false })
	parentId!: HexId | Uuid;

	@property({ attribute: 'reply', type: Boolean })
	reply = false;

	@state()
	loading = false;

	@state()
	userDetails: Optional<RichUserRef>;

	@state()
	commentStatus = CommentStatus.Empty;

	@query( 'sl-textarea' )
	textarea!: SlTextarea;

	@rhdcUserContext()
	currentUser!: Nullable<UserAuth>;

	static override get styles() : CSSResultGroup {
		return css`
			${commentStyles}
			sl-alert { margin-bottom: var(--sl-spacing-x-small); }
		`;
	}

	override render() : unknown {
		const username = this.userDetails ?
			html`<rhdc-username .user=${this.userDetails}></rhdc-username>` :
			html`<span>you</span>`;

		const alert = AlertFactory.create({
			type: 'primary',
			title: 'Troubleshooting Help',
			message: 'Having trouble playing this hack? Try playing on Parallel Launcher using the recommended plugin before reporting an issue. If the hack does not have a recommended plugin, try using OGRE.',
			hidden: this.commentStatus !== CommentStatus.Troubleshooting
		});

		return html`
			${alert}
			<sl-card>
				<div>
					<div class="header">
						<rhdc-avatar .username=${this.currentUser?.username || 'self'}></rhdc-avatar>
						<div class="title">
							${username}<br/>
							<span class="timestamp">now</span>
						</div>
					</div>
					<sl-textarea
						placeholder="Enter your comment here"
						minlength="1"
						?readonly=${this.loading}
						autocapitalize='sentences'
						autocorrect='on'
						autocomplete='off'
						enterkeyhint='done'
						inputmode='text'
						spellcheck
						required
						@sl-input=${this.#contentChanged}
					></sl-textarea>
					<div class="footer">
						<div class="spacer"></div>
						<sl-button
							variant="primary"
							?loading=${this.loading}
							?disabled=${this.commentStatus === CommentStatus.Empty}
							@click=${this.#save}
						>Save</sl-button>
						<sl-button
							variant="neutral"
							?disabled=${this.loading}
							@click=${this.#cancel}
						>Cancel</sl-button>
					</div>
				</div>
			</sl-card>
		`;
	}

	protected override updated( changedProperties: PropertyValues ) : void {
		super.updated( changedProperties );
		
		this.#focusTextbox();
		if( changedProperties.has( 'currentUser' ) ) {
			this.#updateUserDetails();
		}
	}

	async #focusTextbox() : Promise<void> {
		if( !this.textarea ) return;
		if( this.textarea.isUpdatePending ) await this.textarea.updateComplete;
		this.textarea?.focus();
	}

	async #updateUserDetails() : Promise<void> {
		if( this.currentUser?.userId ) {
			this.userDetails = await UsersApi.getUserAsync( 'self' );
		} else {
			this.userDetails = undefined;
		}
	}

	async #save() : Promise<void> {
		this.loading = true;
		try {
			const comment = this.reply ?
				await CommentsApi.postCommentReplyAsync( this.parentId as Uuid, this.textarea.value ) :
				await CommentsApi.postHackCommentAsync( this.parentId as HexId, this.textarea.value );

			this.dispatchEvent( RhdcCommentCreatedEvent.create( comment ) );
			this.toastSuccess( 'Comment posted.' );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to post comment.', getErrorMessage( exception ) );
		} finally {
			this.textarea.value = '';
			this.loading = false;
		}
	}

	#cancel() : void {
		this.textarea.value = '';
		this.dispatchEvent( new CustomEvent( 'rhdc-comment-cancel-event', { bubbles: true, composed: true } ) );
	}

	#contentChanged() : void {
		const content = this.textarea.value;

		if( /^\s*$/.test( content ) ) {
			this.commentStatus = CommentStatus.Empty;
			return;
		}

		if( /\b(?:pj64|project\s?64|count(?:er)?\sfactor|plugin|black\sscreen|OGRE|parallel|gliden?(?:64)?|rice|angrylion|jabos?|ares|cen64)\b|\bmupen/i.test( content ) ) {
			this.commentStatus = CommentStatus.Troubleshooting;
			return;
		}

		this.commentStatus = CommentStatus.Good;
	}

}
