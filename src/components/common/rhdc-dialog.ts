import { css, CSSResultGroup, html } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { Optional } from '../../util/types';
import SlDialog from '@shoelace-style/shoelace/dist/components/dialog/dialog';
import SlRequestCloseEvent from '@shoelace-style/shoelace/dist/events/sl-request-close';
import '@shoelace-style/shoelace/dist/components/dialog/dialog';
import '@shoelace-style/shoelace/dist/components/button/button';

@customElement( 'rhdc-dialog' )
export class RhdcDialog extends RhdcElement {

	@property({ attribute: 'label', type: String })
	label = '';

	@property({ attribute: 'no-header', type: Boolean })
	noHeader = false;

	@property({ attribute: 'destructive', type: Boolean })
	destructive = false;

	@property({ attribute: 'disabled', type: Boolean })
	disabled = false;

	@property({ attribute: 'persistence', type: String })
	persistence: ('weak' | 'default' | 'strong') = 'default';

	@property({ attribute: false })
	confirmText = 'Confirm';

	@property({ attribute: false })
	rejectText = 'Cancel';

	@state()
	accepting = false;

	@state()
	rejecting = false;

	@query( 'sl-dialog' )
	innerDialog!: SlDialog;

	#resolver: (_:boolean) => void = () => {};

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: contents;
				--width: unset;
			}

			slot {
				display: contents;
			}

			sl-dialog {
				min-width: min( 90vw, var(--width) );
				max-width: min( 90vw, var(--width) );
			}
		`;
	}

	override render() : unknown {
		return html`
			<sl-dialog
				part="base"
				label=${this.label}
				?no-header=${this.noHeader}
				@sl-request-close=${this.#requestClose}
				@hide=${() => this.#resolver( false )}
			>
				<slot></slot>
				<sl-button slot="footer"
					variant=${this.destructive ? 'danger' : 'primary'}
					?loading=${this.accepting}
					?disabled=${this.rejecting || this.disabled}
					@click=${() => this.#resolver( true )}
				>${this.confirmText}</sl-button>
				<sl-button slot="footer"
					variant="neutral"
					@click=${() => this.#resolver( false )}
					?loading=${this.rejecting}
					?disabled=${this.accepting}
				>${this.rejectText}</sl-button>
			</sl-dialog>
		`;
	}

	#requestClose( event: SlRequestCloseEvent ) : void {
		if( this.persistence === 'strong' && event.detail.source === 'keyboard' ) {
			event.preventDefault();
		} else if( this.persistence !== 'weak' && event.detail.source === 'overlay' ) {
			event.preventDefault();
		}
	}

	public async runAsync(
		onAccept: Optional<() => Promise<void>> = undefined,
		onReject: Optional<() => Promise<void>> = undefined
	) : Promise<boolean> {
		if( !this.innerDialog ) await this.updateComplete;

		this.innerDialog.show();
		while( true ) {
			if( await new Promise<boolean>( resolver => this.#resolver = resolver ) ) {
				if( onAccept ) {
					this.accepting = true;
					try {
						await onAccept();
					} catch( exception: unknown ) {
						continue;
					} finally {
						this.accepting = false;
					}
				}
				this.innerDialog.hide();
				return true;
			} else {
				if( onReject ) {
					this.rejecting = true;
					try {
						await onReject();
					} catch( exception: unknown ) {
						continue;
					} finally {
						this.rejecting = false;
					}
				}
				this.innerDialog.hide();
				return false;
			}
		}

	}

	public close() : void {
		this.#resolver( false );
	}

}
