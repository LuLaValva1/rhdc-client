import { css, CSSResultGroup, html, nothing, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { Page } from '../../apis/pagination';
import { Nullable, Optional } from '../../util/types';
import { AsyncPipe } from '../../util/async-pipe';
import { rhdcAsyncAppend } from '../../util/lit-extensions';
import { AlertFactory } from '../../util/alert-factory';
import { getErrorMessage } from '../../util/http-error-parser';
import '../../widgets/rhdc-load-more';

interface RhdcDoneEventDetail {
	count: number;
	error: unknown;
}

export type RhdcDoneEvent = CustomEvent<RhdcDoneEventDetail>;

@customElement( 'rhdc-infinite-scroll' )
export class RhdcInfiniteScroll<T> extends RhdcElement {

	@property({ attribute: false })
	pageGetter: Optional<Promise<Page<T>>>;

	@property({ attribute: false })
	renderer!: (item:T) => unknown;

	@property({ attribute: false })
	noResultsMessage = 'No results.';

	@property({ attribute: false })
	errorTitle = 'Failed to fetch results.';

	@state()
	items: AsyncPipe<T> = new AsyncPipe<T>();

	@state()
	loading = true;

	@state()
	done = false;

	@state()
	noResults = false;

	@state()
	error: unknown = null;

	#currentPage: Nullable<Page<T>> = null;
	#count = 0;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-2x-small);
			}

			slot {
				display: contents;
			}

			span {
				color: var(--sl-color-neutral-700);
			}
		`;
	}

	override render() : unknown {
		if( this.noResults ) {
			return html`<slot name="no-results"><span>${this.noResultsMessage}</span></slot>`;
		}

		let error : unknown = nothing;
		if( this.error ) {
			error = AlertFactory.create({
				type: 'danger',
				title: this.errorTitle,
				message: getErrorMessage( this.error )
			});
			error = html`<slot name="error">${error}</slot>`;
		}

		let loadTrigger : unknown = nothing;
		if( !this.done ) {
			loadTrigger = html`<rhdc-load-more part="spinner" ?loading=${this.loading} @rhdc-load-more=${this.#loadMoreAsync}></rhdc-load-more>`;
		}

		return html`
			${rhdcAsyncAppend( this.items, this.renderer )}
			${error}
			${loadTrigger}
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'pageGetter' ) || changedProperties.has( 'renderer' ) ) {
			this.#loadFirstPageAsync();
		}

		if( changedProperties.has( 'loading' ) && changedProperties.get( 'loading' ) !== undefined ) {
			this.dispatchEvent( new CustomEvent( this.loading ? 'rhdc-load-start' : 'rhdc-load-end', {
				bubbles: false,
				composed: false
			}));
		}
	}

	async #loadFirstPageAsync() : Promise<void> {
		this.loading = true;
		this.done = false;
		this.noResults = false;
		this.error = null;
		this.items = new AsyncPipe<T>();
		this.#currentPage = null;
		this.#count = 0;

		if( !this.pageGetter ) return;

		try {
			this.#currentPage = await this.pageGetter;
			this.items.pushMany( this.#currentPage.values );

			this.noResults = (this.#currentPage.values.length === 0);
			this.#count += this.#currentPage.values.length;
			if( !this.#currentPage.hasNext ) this.#finish();
		} catch( exception: unknown ) {
			this.#finish( exception || new Error() );
		} finally {
			this.loading = false;
		}
	}

	async #loadMoreAsync() : Promise<void> {
		if( !this.#currentPage ) return;

		this.loading = true;
		try {
			this.#currentPage = await this.#currentPage.nextAsync();
			this.items.pushMany( this.#currentPage.values );
			this.#count += this.#currentPage.values.length;
			if( !this.#currentPage.hasNext ) this.#finish();
		} catch( exception: unknown ) {
			this.#finish( exception || new Error() );
		} finally {
			this.loading = false;
		}
	}

	#finish( exception: unknown = undefined ) {
		this.items.end();
		this.done = true;
		this.error = exception;
		this.dispatchEvent( new CustomEvent<RhdcDoneEventDetail>( 'rhdc-done', {
			bubbles: false,
			composed: false,
			detail: {
				count: this.#count,
				error: exception
			}
		}));
	}

}
