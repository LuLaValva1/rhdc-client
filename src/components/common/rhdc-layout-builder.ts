import { css, CSSResultGroup, html, HTMLTemplateResult, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { live } from 'lit/directives/live.js';
import { RhdcElement } from '../../rhdc-element';
import { deepFreeze } from '../../util/freeze';
import { BasicGroup, BasicStarLayout, getMimeType } from '../../util/star-layout';
import { Nullable, Optional, Weak } from '../../util/types';
import { DragUtil } from '../../util/drag-util';
import { RhdcFileDropEvent } from '../../events/rhdc-file-drop-event';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import SlSelect from '@shoelace-style/shoelace/dist/components/select/select';
import SlCheckbox from '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/select/select';
import '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '@shoelace-style/shoelace/dist/components/switch/switch';
import '@shoelace-style/shoelace/dist/components/radio/radio';
import '@shoelace-style/shoelace/dist/components/radio-group/radio-group';
import '@shoelace-style/shoelace/dist/components/icon-button/icon-button';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/option/option';
import '../../widgets/rhdc-file-dropzone';

interface DragContext {
	instance: RhdcElement;
	leftSide: boolean;
	index: number;
}

interface Size {
	width: number;
	height: number;
}

const INITIAL_LAYOUT: Readonly<BasicStarLayout> = Object.freeze({
	$schema: 'https://parallel-launcher.ca/layout/basic-01/schema.json',
	skipFileSelect: false,
	groups: []
});

const DEFAULT_LEFT_SIDE_ORDER = [
	1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 25, 16, 17, 18, 19, 23, 24, 20, 21, 22, 0
];

const DEFAULT_RIGHT_SIDE_ORDER = [
	0, 19, 23, 24, 20, 21, 22, 16, 17, 18, 25, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
];

interface LayoutRow {
	name: string;
	courseId?: Optional<number>;
	starMask?: Optional<number>;
}

const ACCEPTABLE_FORMATS = [
	'.png', '.bmp', '.gif', '.svg', '.jpg', '.jpeg', '.pbm', '.pgm', '.ppm', '.xbm', '.xpm',
	'image/png', 'image/bmp', 'image/gif', 'image/svg', 'image/svg+xml', 'image/jpeg',
	'image/x-portable-bitmap', 'image/x-portable-graymap', 'image/x-portable-pixmap',
	'image/x-xbitmap', 'image/x-xpixmap'
].join( ',' );

let g_dragContext: Nullable<DragContext> = null;

@customElement( 'rhdc-layout-builder' )
export class RhdcLayoutBuilder extends RhdcElement {

	@property({ attribute: false })
	value = INITIAL_LAYOUT;

	@state()
	eightStarMode = false;

	@state()
	skipFileSelect = false;

	#leftSide: LayoutRow[] = [];
	#rightSide: LayoutRow[] = [];

	#collectedStarIcon: Weak<string>;
	#missingStarIcon: Weak<string>;

	#collectedStarIconUrl = '/assets/img/star.png';
	#missingStarIconUrl = '/assets/img/star.png';
	#greyscaleIcon = true;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
			}
			
			sl-icon-button.delete::part(base):hover, sl-icon-button.delete::part(base):focus {
				color: var(--sl-color-danger-600);
			}

			sl-icon-button.delete::part(base):active {
				color: var(--sl-color-danger-700);
			}

			sl-icon-button[data-used].star::part(base) {
				color: #fdbe1a !important;
			}

			sl-icon-button.star::part(base) {
				color: var(--sl-color-neutral-500) !important;
			}

			sl-icon-button.star {
				transform: none;
				transition: transform var(--sl-transition-medium);
			}

			sl-icon-button.star:focus {
				transform: scale( 1.5, 1.5 );
			}

			td.drag {
				cursor: drag;
			}

			div.stars {
				display: flex;
			}

			sl-button.padded {
				margin: 0 var(--sl-spacing-x-small);
			}

			.layout {
				display: flex;
				align-items: flex-start;
			}

			.layout > table {
				flex-grow: 1;
			}

			.header {
				display: flex;
				align-items: flex-start;
				gap: var(--sl-spacing-small);
			}

			.header > div:first-child {
				display: flex;
				align-items: flex-start;
				flex-direction: column;
				flex-grow: 1;
				gap: var(--sl-spacing-x-small);
			}

			.header > div {
				display: flex;
				align-items: center;
				gap: var(--sl-spacing-2x-small);
			}

			.header span {
				text-align: end;
			}

			.header img {
				width: 24px;
				height: 24px;
				object-fit: fill;
			}
			
			img[data-grey] {
				filter: grayscale( 1.0 );
			}

			rhdc-file-dropzone {
				margin: 0;
			}

			rhdc-file-dropzone::part(base) {
				padding: var(--sl-spacing-2x-small);
				font-size: unset;
			}

			tr > td {
				padding-top: var(--sl-spacing-2x-small);
				padding-bottom: var(--sl-spacing-2x-small);
			}

		`;
	}

	override render() : unknown {
		return html`
			<div class="header">
				<div>
					<sl-checkbox
						id="skip-file-select"
						?checked=${live( this.skipFileSelect )}
						@sl-change=${this.#updateOptions}
					>Skips File Select (Single Save Slot)</sl-checkbox>
					<sl-checkbox
						id="eight-star-mode"
						?checked=${live( this.eightStarMode )}
						@sl-change=${this.#updateOptions}
					>8 Star Mode</sl-checkbox>
				</div>
				<div>
					<span>Collected Star Icon</span>
					<img src=${this.#collectedStarIconUrl}>
					<rhdc-file-dropzone accept=${ACCEPTABLE_FORMATS} @rhdc-file-drop-event=${this.#uploadStarIcon.bind( this, true )}>Change</rhdc-file-dropzone>
					<sl-icon-button name="backspace" label="reset" @click=${this.#resetIcons}></sl-icon-button>
				</div>
				<div></div>
				<div>
					<span>Missing Star Icon</span>
					<img src=${this.#missingStarIconUrl} ?data-grey=${this.#greyscaleIcon}>
					<rhdc-file-dropzone accept=${ACCEPTABLE_FORMATS} @rhdc-file-drop-event=${this.#uploadStarIcon.bind( this, false )}>Change</rhdc-file-dropzone>
					<sl-icon-button name="backspace" label="reset" @click=${this.#resetMissingStarIcon}></sl-icon-button>
				</div>
			</div>
			<div class="layout">
				${this.#renderLayoutBuilderSide( true )}
				${this.#renderLayoutBuilderSide( false )}
			</div>
		`;
	}

	#renderLayoutBuilderSide( leftSide: boolean ) : HTMLTemplateResult {
		const side = leftSide ? this.#leftSide : this.#rightSide;

		const rows: HTMLTemplateResult[] = [];
		for( let i = 0; i < side.length; i++ ) {
			const row = side[i];

			let rowContent: HTMLTemplateResult;
			if( row.courseId === undefined ) {
				rowContent = html`
					<td colspan="3">
						<sl-input type="text"
							.value=${row.name}
							placeholder="Group Name"
							autocomplete="off"
							enterkeyhint="done"
							inputmode="text"
							@sl-change=${this.#nameChanged.bind( this, leftSide, i )}
						></sl-input>
					</td>
				`;
			} else {
				rowContent = html`
					<td>
						<sl-select
							value="${side[i].courseId!}"
							@sl-change=${this.#courseIdChanged.bind( this, leftSide, i )}
						>
							<sl-option value="0">Castle Secret Stars</sl-option>
							<sl-option value="1">1 Bob-Omb Battlefield</sl-option>
							<sl-option value="2">2 Whomp's Fortress</sl-option>
							<sl-option value="3">3 Jolly Rodger Bay</sl-option>
							<sl-option value="4">4 Cool Cool Mountain</sl-option>
							<sl-option value="5">5 Big Boo's Haunt</sl-option>
							<sl-option value="6">6 Hazy Maze Cave</sl-option>
							<sl-option value="7">7 Lethal Lava Land</sl-option>
							<sl-option value="8">8 Shifting Sand Land</sl-option>
							<sl-option value="9">9 Dire, Dire Docks</sl-option>
							<sl-option value="10">10 Snowman's Land</sl-option>
							<sl-option value="11">11 Wet Dry World</sl-option>
							<sl-option value="12">12 Tall, Tall Mountain</sl-option>
							<sl-option value="13">13 Tiny-Huge Island</sl-option>
							<sl-option value="14">14 Tick Tock Clock</sl-option>
							<sl-option value="15">15 Rainbow Ride</sl-option>
							<sl-option value="16">Bowser in the Dark World</sl-option>
							<sl-option value="17">Bowser in the Fire Sea</sl-option>
							<sl-option value="18">Bowser in the Sky</sl-option>
							<sl-option value="19">Peach's Secret Slide</sl-option>
							<sl-option value="20">Cavern of the Metal Cap</sl-option>
							<sl-option value="21">Tower of the Wing Cap</sl-option>
							<sl-option value="22">Vanish Cap under the Moat</sl-option>
							<sl-option value="23">Winged Mario over the Rainbow</sl-option>
							<sl-option value="24">Secret Aquarium</sl-option>
							<sl-option value="25">End Screen</sl-option>
						</sl-select>
					</td>
					<td>
						<sl-input type="text"
							.value=${row.name}
							placeholder="Course Name"
							autocomplete="off"
							enterkeyhint="done"
							inputmode="text"
							@sl-change=${this.#nameChanged.bind( this, leftSide, i )}
						></sl-input>
					</td>
					<td>
						<div class="stars">
							${this.#renderStars( leftSide, i )}
						</div>
					</td>
				`;
			}

			rows.push( html`
				<tr
					@dragover=${this.#onDragOver}
					@dragleave=${this.#onDragLeave}
					@drop=${this.#onDrop}
					data-side=${leftSide ? 'left' : 'right'}
					data-index=${i}
				>
					<td
						class="drag"
						draggable="true"
						@dragstart=${this.#onDragStart}
						@dragend=${this.#onDragEnd}
					>
						<sl-icon name="grip-horizontal"></sl-icon>
					</td>
					${rowContent}
					<td>
						<sl-icon-button
							class="delete"
							name="x"
							label="delete row"
							@click=${this.#deleteRow.bind( this, leftSide, i )}
						></sl-icon-button>
					</td>
				</tr>
			`);
		}

		return html`
			<table>
				<tbody>
					${rows}
					<tr>
						<td colspan="5">
							<sl-button
								variant="primary"
								outline
								@click=${this.#addCourse.bind( this, leftSide )}
							>Add Course</sl-button>
							<sl-button
								class="padded"
								variant="primary"
								outline
								@click=${this.#addText.bind( this, leftSide )}
							>Add Text</sl-button>
						</td>
					</tr>
				</tbody>
			</table>
		`;
	}

	#renderStars( leftSide: boolean, index: number ) : HTMLTemplateResult[] {
		const side = leftSide ? this.#leftSide : this.#rightSide;
		const stars: HTMLTemplateResult[] = [];
		for( let i = 0; i < (this.eightStarMode ? 8 : 7); i++ ) {
			stars.push( html`
				<sl-icon-button
					class="star"
					name="star-fill"
					?data-used=${side[index].starMask! & (1 << i)}
					@click=${this.#toggleStar.bind( this, leftSide, index, i)}
				></sl-icon-button>
			`);
		}
		return stars;
	}

	#nameChanged( leftSide: boolean, index: number, event: Event ) : void {
		const input = event.target as SlInput;
		const side = leftSide ? this.#leftSide : this.#rightSide;
		side[index].name = input.value;
		this.#updateValue();
	}

	#courseIdChanged( leftSide: boolean, index: number, event: Event ) : void {
		const select = event.target as SlSelect;
		const side = leftSide ? this.#leftSide : this.#rightSide;
		side[index].courseId = +select.value;
		this.#updateValue();
	}

	#deleteRow( leftSide: boolean, index: number ) : void {
		const side = leftSide ? this.#leftSide : this.#rightSide;
		side.splice( index, 1 );
		this.#updateValue();
	}

	#addCourse( leftSide: boolean ) : void {
		const side = leftSide ? this.#leftSide : this.#rightSide;
		side.push({
			name: '',
			courseId: this.#getNextCourseId( leftSide ),
			starMask: this.#getPeviousStarMask( leftSide )
		});
		this.#updateValue();
	}

	#toggleStar( leftSide: boolean, index: number, starBit: number ) : void {
		const side = leftSide ? this.#leftSide : this.#rightSide;
		let mask = side[index].starMask!;
		if( mask & (1 << starBit) ) {
			mask -= 1 << starBit;
		} else {
			mask |= 1 << starBit;
		}
		side[index].starMask = mask;
		this.#updateValue();
	}

	#addText( leftSide: boolean ) : void {
		const side = leftSide ? this.#leftSide : this.#rightSide;
		side.push({ name: '' });
		this.#updateValue();
	}

	#onDragStart( event: DragEvent ) : void {
		const row = this.#getRowElement( event.target );
		if( !row ) return;

		const bbox = row.getBoundingClientRect();
		event.dataTransfer?.setDragImage( row, event.clientX - bbox.left, event.clientY - bbox.top );
		row.style.opacity = '0.25';

		g_dragContext = {
			instance: this,
			leftSide: row.dataset.side === 'left',
			index: +row.dataset.index!
		};
	}

	#onDragEnd( event: DragEvent ) : void {
		const row = this.#getRowElement( event.target );
		if( row ) {
			row.style.opacity = '1.0';
		}
		g_dragContext = null;
	}

	#onDragOver( event: DragEvent ) : void {
		if( g_dragContext?.instance !== this ) return;
		const row = this.#getRowElement( event.target );
		if( !row ) return;
		DragUtil.showDropIndicator( row, DragUtil.getSideV( event, row ) );
		event.preventDefault();
	}

	#onDragLeave( event: DragEvent ) : void {
		const row = this.#getRowElement( event.target );
		if( row ) {
			DragUtil.clearDropIndicator( row );
		}
	}

	#onDrop( event: DragEvent ) : void {
		const row = this.#getRowElement( event.target );
		if( !row ) return;

		DragUtil.clearDropIndicator( row as HTMLElement );
		const source = g_dragContext;
		g_dragContext = null;

		if( source?.instance !== this ) return;

		const leftSide = (row.dataset.side === 'left');
		let targetIndex = +row.dataset.index!;

		if( !DragUtil.isBeforeV( event, row as Element ) ) {
			targetIndex++;
		}

		if( leftSide === source.leftSide ) {
			if( targetIndex === source.index ) return;
			if( targetIndex === source.index + 1 ) return;

			if( leftSide ) {
				this.#leftSide = DragUtil.withMovedElement( this.#leftSide, source.index, targetIndex );
			} else {
				this.#rightSide = DragUtil.withMovedElement( this.#rightSide, source.index, targetIndex );
			}
		} else {
			const fromSide = source.leftSide ? this.#leftSide : this.#rightSide;
			const toSide = leftSide ? this.#leftSide : this.#rightSide;

			if( source.index >= fromSide.length ) return;
			if( targetIndex > toSide.length ) return;

			toSide.splice( targetIndex, 0, fromSide[source.index] );
			fromSide.splice( source.index, 1 );
		}
		
		this.#updateValue();
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'value' ) ) {
			this.skipFileSelect = this.value.skipFileSelect;

			const leftSide: LayoutRow[] = [];
			const rightSide: LayoutRow[] = [];

			for( const group of this.value.groups ) {
				const rows = (group.side === 'left') ? leftSide : rightSide;
				rows.push({ name: group.name });

				for( const course of group.courses ) {
					if( course.starMask & 0x80 ) {
						this.eightStarMode = true;
					}

					rows.push({
						name: course.name || '',
						courseId: course.courseId,
						starMask: course.starMask
					});
				}
			}

			this.#leftSide = leftSide;
			this.#rightSide = rightSide;

			if( this.#collectedStarIcon !== this.value.collectedStarIcon ) {
				this.#collectedStarIcon = this.value.collectedStarIcon;
				if( this.#collectedStarIcon ) {
					const mimeType = getMimeType( this.#collectedStarIcon );
					if( mimeType ) {
						this.#collectedStarIconUrl = `data:${mimeType};base64,${this.#collectedStarIcon}`;
					} else {
						this.#collectedStarIconUrl = 'about:blank';
					}
				} else {
					this.#collectedStarIconUrl = '/assets/img/star.png';
				}
			}
			
			if( this.#missingStarIcon !== this.value.missingStarIcon ) {
				this.#missingStarIcon = this.value.missingStarIcon;
				if( this.#missingStarIcon ) {
					const mimeType = getMimeType( this.#missingStarIcon );
					if( mimeType ) {
						this.#missingStarIconUrl = `data:${mimeType};base64,${this.#missingStarIcon}`;
					} else {
						this.#missingStarIconUrl = 'about:blank';
					}
					this.#greyscaleIcon = false;
				} else {
					this.#missingStarIconUrl = this.#collectedStarIconUrl;
					this.#greyscaleIcon = true;
				}
			} else if( !this.#missingStarIcon ) {
				this.#missingStarIconUrl = this.#collectedStarIconUrl;
			}
		}
	}

	#removeEighthStar( rows: LayoutRow[] ) : LayoutRow[] {
		return rows.map( row => ({
			name: row.name,
			courseId: row.courseId,
			starMask: row.starMask ? (row.starMask & 0x7F) : row.starMask
		}));
	}

	#updateOptions() : void {
		this.skipFileSelect = (this.shadowRoot!.getElementById( 'skip-file-select' ) as SlCheckbox).checked;
		this.eightStarMode = (this.shadowRoot!.getElementById( 'eight-star-mode' ) as SlCheckbox).checked;

		if( !this.eightStarMode ) {
			this.#leftSide = this.#removeEighthStar( this.#leftSide );
			this.#rightSide = this.#removeEighthStar( this.#rightSide );
		}

		this.#updateValue();
	}

	#updateValue() : void {
		const layout: BasicStarLayout = {
			$schema: 'https://parallel-launcher.ca/layout/basic-01/schema.json',
			skipFileSelect: this.skipFileSelect,
			groups: []
		};

		if( this.#collectedStarIcon ) {
			layout.collectedStarIcon = this.#collectedStarIcon;
		}

		if( this.#missingStarIcon ) {
			layout.missingStarIcon = this.#missingStarIcon;
		}

		this.#buildLayoutSide( layout.groups, this.#leftSide, 'left' );
		this.#buildLayoutSide( layout.groups, this.#rightSide, 'right' );

		deepFreeze( layout );
		this.value = layout;

		this.dispatchEvent( new CustomEvent( 'rhdc-layout-changed' ) );
	}

	#buildLayoutSide(
		groups: BasicGroup[],
		side: LayoutRow[],
		sideName: 'left' | 'right'
	) : void {
		let currentGroup: BasicGroup = {
			name: '',
			side: sideName,
			courses: []
		};

		if( side.length > 0 && side[0].courseId !== undefined ) {
			groups.push( currentGroup );
		}

		for( const row of side ) {
			if( row.courseId !== undefined ) {
				currentGroup.courses.push({
					name: row.name || undefined,
					courseId: row.courseId,
					starMask: row.starMask!
				});
			} else {
				currentGroup = {
					name: row.name,
					side: sideName,
					courses: []
				};
				groups.push( currentGroup );
			}
		}
	}

	async #uploadStarIcon( collected: boolean, event: RhdcFileDropEvent ) : Promise<void> {
		try {
			const img = await this.#getImageDataFromFile( event.detail.file );
			const dataUrl = `data:${img.mimeType};base64,${img.base64}`;
			if( !this.#validateImageSizeAsync( dataUrl ) ) return;

			if( collected ) {
				this.#collectedStarIcon = img.base64;
				this.#collectedStarIconUrl = dataUrl;
				if( !this.#missingStarIcon ) {
					this.#missingStarIconUrl = dataUrl;
				}
			} else {
				this.#missingStarIcon = img.base64;
				this.#missingStarIconUrl = dataUrl;
				this.#greyscaleIcon = false;
			}

			this.#updateValue();
		} catch( errorMessage: unknown ) {
			if( typeof errorMessage !== 'string' ) throw errorMessage;
			this.toastError( 'Image Rejected', errorMessage );
		}
	}

	async #getImageDataFromFile( file: File ) : Promise<{ base64: string, mimeType: string }> {
		if( file.size > 5120 ) {
			throw 'Image file must be 5 kiB or smaller.';
		}

		const dataUrl = await new Promise<Nullable<string>>( resolve => {
			const fileReader = new FileReader();
			fileReader.readAsDataURL( file );
			fileReader.addEventListener( 'loadend', () => {
				resolve( fileReader.result as Nullable<string> );
			});
		});

		if( !dataUrl ) {
			throw 'An unknown error occurred processing the uploaded file.';
		}

		const dataStart = 1 + dataUrl.indexOf( ',' );
		if( dataStart <= 0 ) {
			throw 'An unknown error occurred processing the uploaded file.';
		}

		const base64 = dataUrl.substring( dataStart );
		const mimeType = getMimeType( base64 );

		if( !mimeType ) {
			throw 'The uploaded image format is not supported.';
		}

		return {
			base64: base64,
			mimeType: mimeType
		};
	}

	#getNextCourseId( leftSide : boolean ) : number {
		const usedCourses = new Set<number>();
		for( const row of this.#leftSide ) {
			if( row.courseId === undefined ) continue;
			usedCourses.add( row.courseId );
		}

		for( const row of this.#rightSide ) {
			if( row.courseId === undefined ) continue;
			usedCourses.add( row.courseId );
		}

		const defaultOrder = leftSide ? DEFAULT_LEFT_SIDE_ORDER : DEFAULT_RIGHT_SIDE_ORDER;
		for( const courseId of defaultOrder ) {
			if( usedCourses.has( courseId ) ) continue;
			return courseId;
		}

		return 0;
	}

	#getPeviousStarMask( leftSide: boolean ) : number {
		const side = leftSide ? this.#leftSide : this.#rightSide;
		for( let i = side.length - 1; i >= 0; i-- ) {
			if( side[i].courseId === undefined ) continue;
			return side[i].starMask!;
		}
		return this.eightStarMode ? 255 : 127;
	}

	#resetIcons() : void {
		this.#collectedStarIcon = null;
		this.#missingStarIcon = null;
		this.#collectedStarIconUrl = '/assets/img/star.png';
		this.#missingStarIconUrl = '/assets/img/star.png';
		this.#greyscaleIcon = true;
		this.#updateValue();
	}

	#resetMissingStarIcon() {
		this.#missingStarIcon = null;
		this.#missingStarIconUrl = this.#collectedStarIconUrl;
		this.#greyscaleIcon = true;
		this.#updateValue();
	}

	async #validateImageSizeAsync( imgUrl: string ) : Promise<boolean> {
		if( !imgUrl ) return false;
		try {
			const size = await this.#getImageSizeAsync( imgUrl );
			if( !size.width || !size.height ) {
				this.toastError( 'Image Rejected', 'Image is blank or failed to load.' );
				return false;
			}

			if( size.width !== size.height ) {
				this.toastError( 'Image Rejected', 'Image must be square.' );
				return false;
			}

			return true;
		} catch( exception: unknown ) {
			console.error( exception );
			this.toastError( 'Image Rejected', 'Failed to load image.' );
			return false;
		}
	}

	#getImageSizeAsync( imgUrl: string ) : Promise<Size> {
		return new Promise( (resolve, reject) => {
			const img = new Image();
			img.onload = () => resolve({ width: img.width, height: img.height });
			img.onerror = reject;
			img.src = imgUrl;
		});
	}

	#getRowElement( target: Nullable<EventTarget> ) : Nullable<HTMLElement> {
		let node = target as Nullable<Element>;
		while( node && node.tagName !== 'TR' ) {
			node = node.parentElement;
		}

		return node as Nullable<HTMLElement>;
	}

}
