import { css, CSSResultGroup, html, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { UsersApi, RichUserRef } from '../../apis/users-api';
import { ConstRef, Optional, UserId } from '../../util/types';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import './rhdc-username';

@customElement( 'rhdc-lazy-username' )
export class RhdcLazyUsername extends RhdcElement {

	@property({ attribute: 'user-id', type: String })
	userId!: UserId | string;

	@state()
	error = false;

	@state()
	userRef: Optional<ConstRef<RichUserRef>>;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: inline-block;
			}

			sl-spinner {
				margin-inline-end: 0.5ch;
			}

			slot {
				display: contents;
			}

			slot.error {
				color: var(--sl-color-rose-600);
				text-decoration-line: underline;
				text-decoration-style: dotted;
				cursor: not-allowed;
			}
		`;
	}

	override render() : unknown {
		if( this.userRef ) {
			return html`<rhdc-username .user=${this.userRef}></rhdc-username>`;
		} else if( this.error ) {
			return html`<slot class="error"></slot>`;
		} else {
			return html`<sl-spinner></sl-spinner><slot></slot>`;
		}
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'userId' ) ) {
			this.userRef = undefined;
			this.error = false;

			if( !this.userId ) return;
			UsersApi.getUserAsync( this.userId ).then(
				userRef => this.userRef = userRef
			).catch(
				() => this.error = true
			);
		}
	}

}
