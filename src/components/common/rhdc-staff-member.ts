import { css, CSSResultGroup, html, HTMLTemplateResult, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { User, UsersApi } from '../../apis/users-api';
import { Optional } from '../../util/types';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import './rhdc-username';

@customElement( 'rhdc-staff-member' )
export class RhdcUsername extends RhdcElement {

	@property({ attribute: 'username', type: String })
	username!: string;

	@property({ attribute: 'alias', type: String })
	alias: Optional<string>;

	@state()
	user: Optional<User>;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: block;
			}

			div {
				font-size: var(--sl-font-size-large);
				display: flex;
				gap: 1ch;
			}

			sl-icon {
				color: var(--rhdc-text-color);
			}

			a:hover > sl-icon, a:focus-visible > sl-icon {
				color: var(--sl-color-primary-600);
			}
		`;
	}

	override render() : unknown {
		let header: unknown;
		if( this.user ) {
			const parts: HTMLTemplateResult[] = [];
			parts.push( html`<rhdc-username .user=${this.user}></rhdc-username>` );
			if( this.alias ) {
				parts.push( html`<span>(${this.alias})</span>` );
			}
			if( this.user.social.twitter ) {
				parts.push( this.#renderSocialLink( 'twitter', this.user.social.twitter.href ) );
			}
			if( this.user.social.youtube ) {
				parts.push( this.#renderSocialLink( 'youtube', this.user.social.youtube.href ) );
			}
			if( this.user.social.twitch ) {
				parts.push( this.#renderSocialLink( 'twitch', this.user.social.twitch.href ) );
			}
			if( this.user.social.gitlab ) {
				parts.push( this.#renderSocialLink( 'gitlab', this.user.social.gitlab.href ) );
			}
			if( this.user.social.github ) {
				parts.push( this.#renderSocialLink( 'github', this.user.social.github.href ) );
			}
			header = parts;
		} else if( this.alias ) {
			header = `${this.username} (${this.alias})`;
		} else {
			header = this.username;
		}

		return html`
			<div>${header}</div>
			<slot></slot>
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'username' ) ) {
			this.#fetchUserAsync();
		}
	}

	async #fetchUserAsync() : Promise<void> {
		if( !this.username ) {
			this.user = undefined;
			return;
		}

		try {
			this.user = await UsersApi.getUserAsync( this.username );
		} catch{
			this.user = undefined;
		}
	}

	#renderSocialLink( icon: string, href: string ) : HTMLTemplateResult {
		const slIcon = (icon === 'gitlab') ?
			html`<sl-icon src="/assets/icons/gitlab.svg"></sl-icon>` :
			html`<sl-icon name="${icon}"></sl-icon>`;

		return html`<a href=${href} target="_blank">${slIcon}</a>`;
	}

}
