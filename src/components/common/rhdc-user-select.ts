import { css, CSSResultGroup, html, nothing, PropertyValues } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';
import { map } from 'lit/directives/map.js';
import { RhdcElement } from '../../rhdc-element';
import { Optional, Weak } from '../../util/types';
import { UsersApi } from '../../apis/users-api';
import { getErrorMessage } from '../../util/http-error-parser';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '@shoelace-style/shoelace/dist/components/dropdown/dropdown';
import '@shoelace-style/shoelace/dist/components/menu/menu';
import '@shoelace-style/shoelace/dist/components/menu-item/menu-item';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import SlMenuItem from '@shoelace-style/shoelace/dist/components/menu-item/menu-item';
import SlMenu from '@shoelace-style/shoelace/dist/components/menu/menu';
import SlDropdown from '@shoelace-style/shoelace/dist/components/dropdown/dropdown';
import SlSelectEvent from '@shoelace-style/shoelace/dist/events/sl-select';

@customElement( 'rhdc-user-select' )
export class RhdcUserSelect extends RhdcElement {

	@property({ attribute: false })
	value = '';

	@property({ attribute: 'placeholder', type: String })
	placeholder = '';

	@property({ attribute: 'disabled', type: Boolean })
	disabled = false;

	@property({ attribute: 'label', type: String })
	label = '';

	@property({ attribute: 'required', type: Boolean })
	required = false;

	@property({ attribute: 'hoist', type: Boolean })
	hoist = false;

	@state()
	allUsernames: Optional<Readonly<string[]>>;

	@state()
	usernameMatches: string[] = [];

	@query( 'sl-input' )
	input!: SlInput;

	@query( 'sl-menu' )
	menu: Weak<SlMenu>;

	@query( 'sl-menu-item.first' )
	firstSuggestion: Weak<SlMenuItem>;

	@query( 'sl-dropdown' )
	dropdown: Weak<SlDropdown>;

	#resizeObserver!: ResizeObserver;
	#onBlur: () => void;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: contents;
				--visible-rows: 5;
			}

			sl-menu {
				overflow: unset;
				max-height: unset;
			}

			sl-menu::part(base) {
				overflow-x: visible;
				overflow-y: auto;
				max-height: calc(
					var(--sl-spacing-x-small) + (
						var(--visible-rows) * (
							(1em * var(--sl-line-height-normal)) +
							(var(--sl-spacing-2x-small) * 2)
						)
					)
				);
			}
		`;
	}

	override render() : unknown {
		let menu = null;
		if( this.usernameMatches.length ) {
			menu = html`
				<sl-menu part="menu" @sl-select=${this.#suggestionSelected}>
					${map( this.usernameMatches, this.#renderSuggestion.bind( this ) )}
				</sl-menu>
			`;
		}

		const prefix = this.allUsernames ? nothing : html`<sl-spinner slot="prefix"></sl-spinner>`;
		return html`
			<sl-dropdown
				part="base"
				.hoist=${this.hoist}
				?disabled=${this.disabled || !this.allUsernames}
				@sl-show=${this.#resizeMenu}
			>
				<sl-input type="text"
					part="input"
					slot="trigger"
					label=${this.label}
					placeholder="${this.placeholder}"
					?disabled=${this.disabled || !this.allUsernames}
					enterkeyhint="enter"
					inputmode="text"
					@sl-input=${this.#onInput}
					@keydown=${this.#onKeyDown}
				>${prefix}</sl-input>
				${menu}
			</sl-dropdown>
		`;
	}

	protected override updated( changedProperties: PropertyValues ) : void {
		super.updated( changedProperties );
		this.#resizeMenu();

		if( this.dropdown && !this.dropdown.open ) {
			this.dropdown.show();
		}
		
		if( !this.disabled && !this.allUsernames ) {
			this.#loadUsernames();
		}
	}

	constructor() {
		super();
		this.#onBlur = this.#processInput.bind( this, false );
	}

	override connectedCallback() : void {
		super.connectedCallback();
		this.addEventListener( 'blur', this.#onBlur );
		this.#resizeObserver = new ResizeObserver( this.#resizeMenu.bind( this ) );
		this.updateComplete.then(() => {
			this.#resizeObserver.observe( this );
		});
	}

	override disconnectedCallback(): void {
		this.#resizeObserver.unobserve( this );
		this.removeEventListener( 'blur', this.#onBlur );
		super.disconnectedCallback();
	}

	public reportValidity() : boolean {
		if( !this.required || this.value.length > 0 ) return true;
		this.input.reportValidity();
		return false;
	}

	public setCustomValidity( message: string ) : void {
		this.input.setCustomValidity( message );
	}

	#resizeMenu() : void {
		if( !this.menu ) return;
		this.menu.style.width = `${this.input.clientWidth}px`;
		window.requestAnimationFrame(() => {
			this.dropdown?.reposition();
		});
	}

	#renderSuggestion( tag: string, index: number ) : unknown {
		if( index === 0 ) {
			return html`<sl-menu-item .value=${tag} class="first" @keydown=${this.#onFirstSuggestionKeyDown}>${tag}</sl-menu-item>`;
		} else {
			return html`<sl-menu-item .value=${tag}>${tag}</sl-menu-item>`;
		}
	}

	#onInput() : void {
		if( /\s/g.test( this.input.value ) ) {
			this.#processInput( true );
		} else {
			this.#updateMatches();
		}
	}

	#onKeyDown( event: KeyboardEvent ) : void {
		event.stopPropagation();
		if( !this.input.value ) {
			return;
		}

		switch( event.key ) {
			case 'Spacebar':
				event.preventDefault();
			case 'Enter':
			case 'Accept':
			case 'Tab':
			case '':
				this.#processInput( true );
				break;
			case 'ArrowDown':
			case 'Down':
				if( this.menu && this.firstSuggestion ) {
					this.menu.focus();
					this.firstSuggestion.tabIndex = 0;
					this.firstSuggestion.focus();
					event.preventDefault();
				}
				break;
		}
	}

	#onFirstSuggestionKeyDown( event: KeyboardEvent ) {
		if( event.key === 'ArrowUp' || event.key === 'Up' ) {
			this.input.focus();
			event.stopPropagation();
			event.preventDefault();
		}
	}

	#processInput( autocomplete: boolean ) : boolean {
		if( autocomplete && this.usernameMatches.length > 0 ) {
			this.input.value = this.usernameMatches[0];
			this.value = this.usernameMatches[0];
			this.dispatchEvent( new CustomEvent( 'rhdc-change' ) );
			return true;
		}

		if( this.input.value.includes( ' ' ) ) {
			this.input.value = this.input.value.substring( 0, this.input.value.indexOf( ' ' ) );
		}

		if( this.allUsernames?.includes( this.input.value ) ) {
			this.value = this.input.value;
			this.dispatchEvent( new CustomEvent( 'rhdc-change' ) );
			return true;
		}

		const valueLc = this.input.value.toLowerCase();
		for( const username of this.usernameMatches ) {
			if( valueLc === username.toLowerCase() ) {
				this.input.value = username;
				this.value = username;
				this.dispatchEvent( new CustomEvent( 'rhdc-change' ) );
				return true;
			}
		}
		
		this.input.value = '';
		this.value = '';
		this.dispatchEvent( new CustomEvent( 'rhdc-change' ) );
		return false;
	}

	#suggestionSelected( event: SlSelectEvent ) : void {
		this.value = event.detail.item.value;
		this.input.value = event.detail.item.value;
		this.usernameMatches = [];
		this.dispatchEvent( new CustomEvent( 'rhdc-change' ) );
	}

	#updateMatches() : void {
		const search = this.input.value;
		if( !search || search.length < 3 || !this.allUsernames ) {
			this.usernameMatches = [];
			return;
		}

		const searchLower = search.toLocaleLowerCase();

		const p1 = [];
		const p2 = [];
		const p3 = [];

		for( const tag of this.allUsernames ) {
			if( tag.startsWith( search ) ) {
				p1.push( tag );
				continue;
			}

			const tagLower = tag.toLocaleLowerCase();
			if( tagLower.startsWith( searchLower ) ) {
				p2.push( tag );
				continue;
			}

			if( tagLower.includes( searchLower ) ) {
				p3.push( tag );
				continue;
			}
		}

		this.usernameMatches = [ p1, p2, p3 ].flat();
	}

	async #loadUsernames() : Promise<void> {
		try {
			this.allUsernames = await UsersApi.getAllUsernamesAsync();
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load user list', getErrorMessage( exception ) );
		}
	}

}
