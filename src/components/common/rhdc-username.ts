import { css, CSSResultGroup, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { RichUserRef } from '../../apis/users-api';
import { Gender } from '../../apis/users-api';
import { countryMap } from '../../util/countries';
import '../../widgets/rhdc-link';

@customElement( 'rhdc-username' )
export class RhdcUsername extends RhdcElement {

	@property({ attribute: false })
	user!: RichUserRef;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: inline-flex;
				align-items: center;
				font-family: var(--sl-font-sans);
				color: var(--rhdc-text-color);
			}

			.flag {
				font-family: EmojiFallback, var(--sl-font-sans);
			}

			.pronouns {
				font-size: 0.8em;
				color: var(--rhdc-text-color-faded);
			}
		`;
	}

	override render() : unknown {
		let flag: unknown = null;
		if( this.user.country ) {
			const flagEmoji = countryMap.get( this.user.country )?.flag;
			if( flagEmoji ) flag = html`<span class="flag">${flagEmoji}&nbsp;</span>`;
		}

		let pronouns: unknown = null;
		if( this.user.gender ) {
			let pronounText = '';
			switch( this.user.gender ) {
				case Gender.Masculine:
					pronounText = '(he/him)';
					break;
				case Gender.Feminine:
					pronounText = '(she/her)';
					break;
				case Gender.Neuter:
					pronounText = '(they/them)';
					break;
			}
			pronouns = html`<span class="pronouns">&nbsp;${pronounText}</span>`;
		}

		return html`
			${flag}
			<rhdc-link href="/user/${this.user.username}">${this.user.username}</rhdc-link>
			${pronouns}
		`;
	}

}
