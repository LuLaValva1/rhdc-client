import { css, CSSResultGroup, html, HTMLTemplateResult, nothing, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { map } from 'lit/directives/map.js';
import { RhdcElement } from '../../rhdc-element';
import { CompetitionSeries } from '../../apis/competitions-api';
import { FilesApi } from '../../apis/files';
import { RichUserRef } from '../../apis/users-api';
import { ConstRef } from '../../util/types';
import { toApiUrl } from '../../util/url';
import '@shoelace-style/shoelace/dist/components/details/details';
import '@shoelace-style/shoelace/dist/components/avatar/avatar';
import '../common/rhdc-username';
import '../../widgets/rhdc-markdown';
import '../../widgets/rhdc-link';
import '../../widgets/rhdc-icon';

@customElement( 'rhdc-competition-series-card' )
export class RhdcCompetitionSeriesCard extends RhdcElement {

	@property({ attribute: false })
	series!: CompetitionSeries;

	@state()
	iconHref = 'about:blank';

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				min-height: 96px;
			}

			sl-details {
				padding-top: 16px;
				padding-left: 16px;
			}

			sl-details::part(header) {
				position: relative;
				min-height: 64px;
			}

			.logo {
				position: absolute;
				left: -16px;
				top: -16px;
			}

			sl-avatar {
				--size: 96px;
			}

			.line {
				display: flex;
				align-items: center;
				overflow-x: hidden;
				max-width: 100%;
				gap: 0.5ch;
			}

			rhdc-link {
				text-overflow: ellipsis;
				white-space: nowrap;
				font-size: var(--sl-font-size-x-large);
			}

			rhdc-icon {
				flex-shrink: 0;
			}

			.spacer {
				flex-grow: 1;
			}

			span {
				color: var(--sl-color-neutral-600);
			}

			span:last-of-type {
				display: none;
			}

			div[slot="summary"] {
				margin-left: 64px;
				padding-left: var(--sl-spacing-x-small);
			}

			sl-details::part(content) {
				margin-top: var(--sl-spacing-x-small);
			}
		`;
	}

	override render() : unknown {
		let hiddenIcon: unknown = nothing;
		if( !this.series.public ) {
			hiddenIcon = html`<rhdc-icon name="eye-slash"></rhdc-icon>`;
		}

		return html`
			<sl-details>
				<div slot="summary">
					<rhdc-link class="logo" href="/competitions/series/${this.series.slug}">
						<sl-avatar shape="circle" .initials=${this.series.name.substring( 0, 1 )} .image=${this.iconHref}></sl-avatar>
					</rhdc-link>
					<div class="line">
						${hiddenIcon}
						<rhdc-link href="/competitions/series/${this.series.slug}">${this.series.name}</rhdc-link>
						<div class="spacer"></div>
					</div>
					<div class="line">
						${map( this.series.hosts, this.#renderUser.bind( this ) )}
						<div clas="spacer"></div>
					</div>
				</div>
				<rhdc-markdown trusted .markdown=${this.series.description}></rhdc-markdown>
			</sl-details>
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		if( changedProperties.has( 'series' ) && this.series ) {
			if( this.series.logo.secured ) {
				const seriesId = this.series.seriesId;
				FilesApi.getDownloadTokenAsync( this.series.logo.webHref ).then( token => {
					if( this.series.seriesId !== seriesId ) return;
					this.iconHref = toApiUrl( token.href );
				});
			} else {
				this.iconHref = toApiUrl( this.series.logo.directHref );
			}
		}
	}

	#renderUser( user: ConstRef<RichUserRef> ) : HTMLTemplateResult {
		return html`<rhdc-username .user=${user}></rhdc-username><span>&bull;</span>`;
	}

}
