import { css, CSSResultGroup, html, HTMLTemplateResult, nothing } from 'lit';
import { customElement, property, state, query } from 'lit/decorators.js';
import { guard } from 'lit/directives/guard.js';
import { RhdcElement } from '../../../rhdc-element';
import { HacksApi, Hack } from '../../../apis/hacks-api';
import { RhdcHackUpdatedEvent } from '../../../events/rhdc-hack-updated-event';
import { getErrorMessage } from '../../../util/http-error-parser';
import { StarLayout } from '../../../util/star-layout';
import { HttpClient } from '../../../apis/http-client';
import { getUrlPath } from '../../../util/url';
import { AlertFactory } from '../../../util/alert-factory';
import { RhdcLayoutBuilder } from '../../common/rhdc-layout-builder';
import { RhdcFileDropEvent } from '../../../events/rhdc-file-drop-event';
import { Nullable, ConstRef, FileLike } from '../../../util/types';
import SlDialog from '@shoelace-style/shoelace/dist/components/dialog/dialog';
import SlRadioGroup from '@shoelace-style/shoelace/dist/components/radio-group/radio-group';
import SlRequestCloseEvent from '@shoelace-style/shoelace/dist/events/sl-request-close';
import '@shoelace-style/shoelace/dist/components/radio-group/radio-group';
import '@shoelace-style/shoelace/dist/components/radio/radio';
import '@shoelace-style/shoelace/dist/components/dialog/dialog';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/tab-group/tab-group';
import '@shoelace-style/shoelace/dist/components/tab-panel/tab-panel';
import '@shoelace-style/shoelace/dist/components/tab/tab';
import '../../common/rhdc-layout-builder';
import '../../common/rhdc-layout-preview';
import '../../../widgets/rhdc-file-dropzone';

type LayoutType = 'none' | 'basic' | 'advanced';

@customElement( 'rhdc-hack-edit-layout-button' )
export class RhdcHackEditLayoutButton extends RhdcElement {

	@property({ attribute: false })
	hack!: Hack;

	@state()
	layout: Nullable<ConstRef<StarLayout>> = null;

	@state()
	layoutType: LayoutType = 'none';

	@state()
	loading = false;

	@query( 'sl-dialog' )
	dialog!: SlDialog;

	@query( 'sl-radio-group' )
	radioGroup!: SlRadioGroup;

	@query( 'rhdc-layout-builder' )
	layoutBuilder!: Nullable<RhdcLayoutBuilder>;

	static override get styles() : CSSResultGroup {
		return css`

			sl-dialog {
				--width: 1200px;
			}

			sl-dialog > div {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-x-small);
			}

			.layout-upload {
				display: flex;
				gap: var(--sl-spacing-small);
			}

			rhdc-file-dropzone {
				flex-shrink: 1;
				width: calc( 600px - var(--sl-spacing-small) );
				max-width: calc( 50vw - var(--sl-spacing-small) );
				min-width: 150px;
				min-height: 250px;
			}

			rhdc-layout-preview {
				width: min-content;
			}

			sl-tab-panel:last-of-type {
				padding-left: var(--sl-spacing-2x-small);
			}
		`;
	}

	override render() : unknown {
		if( !this.hack.canEditInfo ) return nothing;

		let layoutEditor: unknown = null;
		if( this.layoutType === 'basic' ) {
			let builder : HTMLTemplateResult;
			if( this.layout?.$schema === 'https://parallel-launcher.ca/layout/basic-01/schema.json' ) {
				builder = html`<rhdc-layout-builder .value=${this.layout} @rhdc-layout-changed=${this.#onLayoutBuilderChange}></rhdc-layout-builder>`;
			} else {
				builder = html`<rhdc-layout-builder @rhdc-layout-changed=${this.#onLayoutBuilderChange}></rhdc-layout-builder>`;
			}

			let preview : unknown = nothing;
			if( this.layout && this.layout.groups.length > 0 ) {
				preview = html`<rhdc-layout-preview .starLayout=${this.layout}></rhdc-layout-preview>`;
			}

			layoutEditor = html`
				<sl-tab-group>
					<sl-tab slot="nav" panel="edit">Edit</sl-tab>
					<sl-tab slot="nav" panel="preview" ?disabled=${!preview}>Preview</sl-tab>

					<sl-tab-panel name="edit">${guard( [ this.hack, this.layoutType ], () => builder )}</sl-tab-panel>
					<sl-tab-panel name="preview">${preview}</sl-tab-panel>
				</sl-tab-group>
			`;
		} else if( this.layoutType === 'advanced' ) {
			let layoutPreview: Nullable<HTMLTemplateResult> = null;
			if( this.layout && this.layout.groups.length > 0 ) {
				layoutPreview = html`<rhdc-layout-preview .starLayout=${this.layout}></rhdc-layout-preview>`;
			}

			layoutEditor = html`
				<p>
					Advanced layouts allow you to upload more complex layouts for hacks with custom save file formats.
					You can view the documentation on how to create and test and advanced layouts
					<a href="https://parallel-launcher.ca/layout/docs" rel="help" target="_blank">here</a>.
					<br/>
					If your hack uses the regular vanilla SM64 save format, then you probably want to create a basic
					layout instead, and should select the <i>Create Layout Now</i> option.
					<br/>
					Alternatively, you can upload star layouts from the legacy JSML format, and they will automatically
					be converted to the new format.
				</p>
				<div class="layout-upload"></div>
					<rhdc-file-dropzone
						?disabled=${this.loading}
						accept=".json,.jsml,application/json"
						@rhdc-file-drop-event=${this.#onLayoutUpload}
					>Upload Layout File</rhdc-file-dropzone>
					<h2>Preview</h2>
					${layoutPreview}
				</div>
			`;
		} else if( this.hack.layout && !this.loading ) {
			layoutEditor = AlertFactory.create({
				type: 'warning',
				message: 'Your star layout will be deleted.'
			});
		}

		return html`
			<sl-button variant="primary" @click=${this.#openDialog}>${this.hack.layout ? 'Edit' : 'Add'} Star Layout</sl-button>
			<sl-dialog label="Star Layout" @sl-request-close=${this.#onRequestDialogClose}>
				<div>
					<p>
						Star Layout files allow players to view their progress while playing your hack in Parallel Launcher.
						You may either upload a star layout file you have already created, or you can create one right now.
						Creating a star layout is not required; however, it is strongly recommended that you create one if
						your hack contains multiple stars in order to provide the best user experience.
					</p>
					<sl-radio-group label="Layout Type" .value=${this.layoutType} @sl-change=${this.#radioChanged}>
						<sl-radio value="none" ?disabled=${this.loading}>No Layout</sl-radio>
						<sl-radio value="advanced" ?disabled=${this.loading}>Upload Layout File</sl-radio>
						<sl-radio value="basic" ?disabled=${this.loading}>Basic Layout Editor</sl-radio>
					</sl-radio-group>
					${layoutEditor}
				</div>

				<sl-button slot="footer" variant="primary" ?loading=${this.loading} @click=${this.#save}>Save</sl-button>
				<sl-button slot="footer" variant="neutral" ?loading=${this.loading} @click=${() => this.dialog.hide()}>Cancel</sl-button>
			</sl-dialog>
		`;
	}

	#radioChanged() : void {
		this.layoutType = this.radioGroup.value as LayoutType;
	}

	#openDialog() : void {
		this.#reloadLayout();
		this.dialog.show();
	}

	#onLayoutBuilderChange() : void {
		const layout = this.layoutBuilder?.value;
		if( layout ) this.layout = layout;
	}

	async #onLayoutUpload( event: RhdcFileDropEvent ) : Promise<void> {
		this.loading = true;
		try {
			this.layout = await HttpClient.postFileAsync<StarLayout>( '/v3/layout/validate', event.detail.file );
		} catch{
			this.toastError( 'Invalid Star Layout' );
		} finally {
			this.loading = false;
		}
	}

	async #reloadLayout() : Promise<void> {
		this.layout = null;
		this.layoutType = 'none';
		if( !this.hack.layout ) return;

		this.loading = true;
		try {
			this.layout = await HttpClient.getAsync<StarLayout>( getUrlPath( this.hack.layout.directHref ) );
			this.layoutType = (this.layout.$schema === 'https://parallel-launcher.ca/layout/basic-01/schema.json') ? 'basic' : 'advanced';
		} catch( exception: unknown ) {
			this.toastError( 'Error loading star layout', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	async #save() : Promise<void> {
		this.loading = true;
		try {
			if( this.layoutType === 'none' || !this.layout ) {
				this.hack = await HacksApi.removeStarLayoutAsync( this.hack.hackId );
				this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
			} else {
				const file = new Blob( [ JSON.stringify( this.layout ) ], { type: 'application/json' } ) as FileLike;
				file.name = 'layout.json';

				this.hack = await HacksApi.updateStarLayoutAsync( this.hack.hackId, file );
				this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
			}
			this.dialog.hide();
		} catch( exception: unknown ) {
			this.toastError( 'Error saving star layout', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	#onRequestDialogClose( event: SlRequestCloseEvent ) : void {
		if( event.detail.source === 'overlay' || event.detail.source === 'keyboard' ) event.preventDefault();
	}

}
