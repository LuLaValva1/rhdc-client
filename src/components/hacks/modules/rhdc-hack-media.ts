import { css, CSSResultGroup, html, HTMLTemplateResult } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { RhdcElement } from '../../../rhdc-element';
import { Hack } from '../../../apis/hacks-api';
import { DownloadInfo } from '../../../apis/files';
import { ConstRef, Nullable } from '../../../util/types';
import { parseYouTubeLink } from '../../../util/youtube-link';
import '../../../widgets/rhdc-youtube-embed';
import '../../../widgets/rhdc-image';

interface ActiveMedia {
	type: 'image' | 'video';
	media: ConstRef<string | DownloadInfo>;
}

@customElement( 'rhdc-hack-media' )
export class RhdcHackMedia extends RhdcElement {

	@property({ attribute: false })
	hack!: Hack;

	@state()
	activeMedia: Nullable<ActiveMedia> = null;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				overflow-x: auto;
				gap: var(--sl-spacing-small);
				width: 0;
				min-width: 100%;
				align-items: center;
			}

			.modal {
				position: fixed;
				top: 0;
				left: 0;
				display: flex;
				width: 100vw;
				height: 100vh;
				z-index: var(--sl-z-index-dialog);
				justify-content: center;
				align-items: center;
				background-color: var(--sl-overlay-background-color);
			}

			.modal rhdc-youtube-embed {
				width: 720px;
				height: 405px;
			}

			.modal rhdc-youtube-embed, .modal rhdc-image::part(img) {
				max-width: 100vw;
				max-height: 100vh;
			}

			img {
				width: 320px;
				height: auto;
				object-position: center;
				object-fit: cover;
			}

			.image {
				cursor: pointer;
			}

			.video {
				position: relative;
			}

			.video-overlay {
				position: absolute;
				top: 0;
				left: 0;
				display: flex;
				width: 100%;
				height: 100%;
				justify-content: center;
				align-items: center;
				background-color: transparent;
				cursor: pointer;
			}

			rhdc-image::part(img) {
				max-width: 320px;
				max-height: 240px;
			}
		`;
	}

	override render() : unknown {
		const thumbnails: HTMLTemplateResult[] = [];

		for( const video of this.hack.videos ) {
			const link = parseYouTubeLink( video );
			if( !link ) continue;

			thumbnails.push( html`
				<div class="video">
					<img src="${link.smallThumbnailLink()}">
					<div class="video-overlay" @click=${() => this.activeMedia = { type: 'video', media: link.embedLink() }}>
						<svg width="68px" height="48px" version="1.1" viewBox="0 0 68 48">
							<path d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z" fill="#f00"></path>
							<path d="M 45,24 27,14 27,34" fill="#fff"></path>
						</svg>
					</div>
				</div>
			`
			);
		}

		for( const screenshot of this.hack.screenshots ) {
			thumbnails.push( html`
				<rhdc-image
					class="image"
					.downloadInfo=${screenshot}
					@click=${() => this.activeMedia = { type: 'image', media: screenshot }}
				></rhdc-image>`
			);
		}

		if( !this.activeMedia ) {
			return thumbnails;
		}

		const bigMedia = this.activeMedia.type === 'video' ?
			html`<rhdc-youtube-embed href=${this.activeMedia.media}></rhdc-youtube-embed>` :
			html`<rhdc-image .downloadInfo=${this.activeMedia.media}></rhdc-image>`;

		return html`
			<div class="modal" @click=${() => this.activeMedia = null}>${bigMedia}</div>
			${thumbnails}
		`;
	}

}
