import { css, CSSResultGroup, html } from 'lit';
import { customElement, property, state, query } from 'lit/decorators.js';
import { RhdcElement } from '../../../rhdc-element';
import { HacksApi, Hack, Category, HackModUpdateDto } from '../../../apis/hacks-api';
import { RhdcHackUpdatedEvent } from '../../../events/rhdc-hack-updated-event';
import { RhdcModqueueStatusChangeEvent } from '../../../events/rhdc-modqueue-status-change-event';
import { getErrorMessage } from '../../../util/http-error-parser';
import { RhdcDialog } from '../../common/rhdc-dialog';
import SlCheckbox from '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import SlSelect from '@shoelace-style/shoelace/dist/components/select/select';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/card/card';
import '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '@shoelace-style/shoelace/dist/components/select/select';
import '@shoelace-style/shoelace/dist/components/option/option';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '../../common/rhdc-dialog';

@customElement( 'rhdc-hack-mod-tools' )
export class RhdcHackModTools extends RhdcElement {

	@property({ attribute: false })
	hack!: Hack;

	@state()
	loading = false;

	@query( 'sl-checkbox' )
	matureCheckbox!: SlCheckbox;

	@query( 'sl-select' )
	categorySelect!: SlSelect;

	@query( 'sl-card sl-input' )
	completionBonusInput!: SlInput;

	@query( 'rhdc-dialog sl-input' )
	reasonInput!: SlInput;
	
	@query( 'rhdc-dialog' )
	rejectDialog!: RhdcDialog;

	static override get styles() : CSSResultGroup {
		return css`
			div > span {
				color: var(--sl-color-danger-600);
			}

			sl-card {
				width: 100%;
			}

			sl-card > div {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-x-small);
			}

			div > div {
				display: flex;
				flex-wrap: wrap;
				align-items: baseline;
				gap: var(--sl-spacing-medium);
			}

			div > div > div {
				flex-grow: 1;
			}

			sl-checkbox::part(base) {
				color: var(--rhdc-text-color);
			}

			sl-select::part(form-control), sl-card sl-input::part(form-control) {
				display: flex;
				align-items: baseline;
				gap: var(--sl-spacing-2x-small);
			}

			sl-card sl-input::part(input) {
				max-width: 10ch;
			}

			sl-button[hidden] {
				display: none;
			}
		`;
	}

	override render() : unknown {
		return html`
			<sl-card>
				<div>
					<span>Mod Tools</span>
					<div>
						<sl-checkbox ?checked=${this.hack.mature} ?disabled=${this.loading}>Mature Content</sl-checkbox>
						<sl-select label="Category" .value=${this.hack.category as string} ?disabled=${this.loading}>
							<sl-option value="${Category.Original}">${Category.Original}</sl-option>
							<sl-option value="${Category.Concept}">${Category.Concept}</sl-option>
							<sl-option value="${Category.Retexture}">${Category.Retexture}</sl-option>
							<sl-option value="${Category.Kaizo}">${Category.Kaizo}</sl-option>
						</sl-select>
						<sl-input type="number"
							label="Completion Bonus"
							min="0"
							enterkeyhint="next"
							inputmode="numeric"
							.value=${this.hack.completionBonus?.toString() || ''}
							?disabled=${this.loading}
						></sl-input>
						<div></div>
						<sl-button variant="danger" ?loading=${this.loading} @click=${this.#save}>Save</sl-button>
					</div>
					<div>
						<sl-button variant="success" @click=${this.#approve} ?loading=${this.loading} ?hidden=${this.hack.approved}><sl-icon slot="prefix" name="check2"></sl-icon>Approve</sl-button>
						<sl-button variant="danger" @click=${this.#reject} ?loading=${this.loading} ?hidden=${this.hack.approved}><sl-icon slot="prefix" name="x-lg"></sl-icon>Reject</sl-button>
						<sl-button variant="danger" @click=${this.#removeRatings} ?loading=${this.loading} ?hidden=${!this.hack.approved || this.hack.rating === null}>Remove All Ratings</sl-button>
						<sl-button variant="danger" @click=${this.#removeDifficulty} ?loading=${this.loading} ?hidden=${!this.hack.approved || this.hack.difficulty === null}>Reset Difficulty</sl-button>
					</div>
				</div>
			</sl-card>
			<rhdc-dialog no-header .confirmText=${'Submit'}>
				<div>
					<p>Enter a rejection reason. This will appear in the author's notifications.</p>
					<sl-input type="text"
						placeholder="Reason"
						autocorrect="on"
						enterkeyhint="done"
						inputmode="text"
						spellcheck
						clearable
						?disabled=${this.loading}
					></sl-input>
				</div>
			</rhdc-dialog>
		`;
	}

	async #save() : Promise<void> {
		const update: HackModUpdateDto = {
			mature: this.matureCheckbox.checked,
			category: this.categorySelect.value as Category
		};

		if( this.completionBonusInput.value ) {
			update.completionBonus = this.completionBonusInput.valueAsNumber;
		}

		this.loading = true;
		try {
			this.hack = await HacksApi.updateHackAsync( this.hack.hackId, update );
			this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
			this.dispatchEvent( RhdcModqueueStatusChangeEvent.createAsync() );
		} catch( exception: unknown ) {
			this.toastError( 'Hack Update Error', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	async #approve() : Promise<void> {
		const update: HackModUpdateDto = {
			mature: this.matureCheckbox.checked,
			category: this.categorySelect.value as Category
		};

		if( this.completionBonusInput.value ) {
			update.completionBonus = this.completionBonusInput.valueAsNumber;
		}
		
		this.loading = true;
		try {
			this.hack = await HacksApi.updateHackAsync( this.hack.hackId, update );
			this.hack = await HacksApi.approveHackAsync( this.hack.hackId );
			this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
			this.dispatchEvent( RhdcModqueueStatusChangeEvent.createAsync() );
			this.toastSuccess( 'Hack Approved' );
		} catch( exception: unknown ) {
			this.toastError( 'Server Error', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	#reject() : void {
		this.rejectDialog.runAsync( this.#rejectAsync.bind( this ) );
	}

	async #rejectAsync() : Promise<void> {
		try {
			await HacksApi.rejectHackAsync( this.hack.hackId, this.reasonInput.value || null );
			this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
			this.dispatchEvent( RhdcModqueueStatusChangeEvent.createAsync() );
			this.toastInfo( 'Hack Rejected' );
			window.history.back();
		} catch( exception: unknown ) {
			this.toastError( 'Server Error', getErrorMessage( exception ) );
			throw exception;
		} finally {
			this.loading = false;
		}
	}

	async #removeRatings() : Promise<void> {
		this.loading = true;
		try {
			this.hack = await HacksApi.removeHackRatingsAsync( this.hack.hackId );
			this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
			this.toastInfo( 'Hack Ratings Reset' );
		} catch( exception: unknown ) {
			this.toastError( 'Server Error', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	async #removeDifficulty() : Promise<void> {
		this.loading = true;
		try {
			this.hack = await HacksApi.removeHackDifficultyAsync( this.hack.hackId );
			this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
			this.toastInfo( 'Hack Difficulty Reset' );
		} catch( exception: unknown ) {
			this.toastError( 'Server Error', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

}
