import { css, CSSResultGroup, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { RhdcElement } from '../../../rhdc-element';
import { Category, Hack } from '../../../apis/hacks-api';
import '@shoelace-style/shoelace/dist/components/card/card';
import '@shoelace-style/shoelace/dist/components/rating/rating';
import '../../../widgets/rhdc-image';

@customElement( 'rhdc-hack-small-card' )
export class RhdcHackSmallCard extends RhdcElement {

	@property({ attribute: false })
	hack!: Hack;

	static override get styles() : CSSResultGroup {
		return css`
			sl-card {
				width: 322px;
			}

			rhdc-image {
				width: 320px;
				height: 240px;
			}

			rhdc-image::part(img) {
				margin: 1px;
			}

			sl-card > div {
				margin-top: 0.5em;
			}

			sl-rating.difficulty {
				--symbol-color-active: var(--sl-color-primary-600);
			}

			sl-rating.difficulty[data-kaizo] {
				--symbol-color-active: var(--sl-color-danger-500);
			}

			sl-rating {
				display: inline-block;
				vertical-align: text-bottom;
			}
		`;
	}

	override render() : unknown {
		return html`
			<sl-card>
				<rhdc-image slot="image" .downloadInfo=${this.hack.screenshots[0]}></rhdc-image>

				<div ?hidden=${!this.hack.createdDate}>
					<strong>Date Created</strong><br/>
					<span>${this.hack.createdDate?.toISOString()?.substring( 0, 10 )}</span>
				</div>

				<div>
					<strong>Date Uploaded</strong><br/>
					<span>${this.hack.uploadedDate.toISOString().substring( 0, 10 )}</span>
				</div>

				<div>
					<strong>Rating</strong><br/>
					<sl-rating
						tabindex="-1"
						max="10"
						value="${this.hack.rating || 0}"
						precision="0.1"
						readonly
						.getSymbol=${this.#iconGetter( this.hack.rating == null ? 'question-lg' : 'star-fill' )}
					></sl-rating>
					<span>${this.hack.rating?.toFixed( 1 )}</span>
				</div>

				<div>
					<strong>Difficulty</strong><br/>
					<sl-rating
						tabindex="-1"		
						class="difficulty"
						max="10"
						value="${this.hack.difficulty || 0}"
						precision="0.1"
						readonly
						.getSymbol=${this.#iconGetter( this.hack.rating == null ? 'question-lg' : 'fire' )}
						?data-kaizo=${this.hack.category === Category.Kaizo}
					></sl-rating>
					<span>${this.hack.difficulty?.toFixed( 1 )}<sup ?hidden=${!this.hack.rating || this.hack.category !== Category.Kaizo}>K</sup></span>
				</div>

				<div>
					<strong>Downloads</strong><br/>
					<span>${this.hack.numDownloads}</span>
				</div>
			</sl-card>
		`;
	}

	#iconGetter( name: string ) : (() => string) {
		return () => `<sl-icon name="${name}"></sl-icon>`;
	}

}
