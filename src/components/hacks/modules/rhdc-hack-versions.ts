import { css, CSSResultGroup, html, HTMLTemplateResult, nothing } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';
import { guard } from 'lit/directives/guard.js';
import { map } from 'lit/directives/map.js';
import { RhdcElement } from '../../../rhdc-element';
import { GfxPlugin, GfxPluginFlag, Hack, HackVersionData, HackVersionUpdateDto, HacksApi, HackFlag } from '../../../apis/hacks-api';
import { FilesApi, DownloadInfo } from '../../../apis/files';
import { AlertFactory } from '../../../util/alert-factory';
import { ConstRef, Nullable } from '../../../util/types';
import { rhdcIconButtonStyles, rhdcLinkStyles } from '../../../common-styles';
import { RhdcModqueueStatusChangeEvent } from '../../../events/rhdc-modqueue-status-change-event';
import { getUrlPath, toApiUrl } from '../../../util/url';
import { RhdcHackUpdatedEvent } from '../../../events/rhdc-hack-updated-event';
import { getErrorMessage } from '../../../util/http-error-parser';
import { RhdcPluginEditor } from '../rhdc-plugin-editor';
import { RhdcDialog } from '../../common/rhdc-dialog';
import { Auth } from '../../../auth/auth';
import SlDialog from '@shoelace-style/shoelace/dist/components/dialog/dialog';
import SlTextarea from '@shoelace-style/shoelace/dist/components/textarea/textarea';
import SlRequestCloseEvent from '@shoelace-style/shoelace/dist/events/sl-request-close';
import '@shoelace-style/shoelace/dist/components/icon-button/icon-button';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/tag/tag';
import '@shoelace-style/shoelace/dist/components/dialog/dialog';
import '@shoelace-style/shoelace/dist/components/tooltip/tooltip';
import '@shoelace-style/shoelace/dist/components/textarea/textarea';
import '../../common/rhdc-console-compatibility';
import '../../common/rhdc-dialog';
import '../rhdc-plugin-editor';
import './rhdc-hack-version-upload-button';

const GCC_TOOLTIP =
	'This hack version requires the emulator to support Gamecuber controller emulation. ' +
	'This may not work in emulators other than Parallel Launcher, and you will need a ' +
	'Gamecube controller passthrough adapter to to play on a real N64 console.';

const MOUSE_TOOLTIP =
	'This hack version requires the emulator to support the N64 mouse. ' +
	'This may not work in emulators other than Parallel Launcher, and you will need the ' +
	'N64 mouse peripheral to to play on a real N64 console.';

const DUAL_ANALOG_TOOLTIP =
	'This hack version expects the Dual Analog input mode to be enabled in Parallel Launcher. ' +
	'To play this hack on other emulators, you will need to manually configure your controller to ' +
	'map one stick to the player 2 analog stick.';

interface PluginFlagInfo {
	acronym: string;
	details: string;
}

@customElement( 'rhdc-hack-versions' )
export class RhdcHackVersions extends RhdcElement {

	@property({ attribute: false })
	hack!: Hack;

	@state()
	editingVersion = '';

	@state()
	editingPlugin: HackVersionUpdateDto = {};

	@state()
	loading = false;

	@query( 'sl-dialog' )
	editDialog!: SlDialog;

	@query( 'rhdc-plugin-editor' )
	pluginEditor!: RhdcPluginEditor;

	@query( '#approve-version-dialog' )
	approveVersionDialog!: RhdcDialog;

	@query( '#reject-version-dialog' )
	rejectVersionDialog!: RhdcDialog;

	@query( '#reason-input' )
	rejectionReasonInput!: SlTextarea;

	static override get styles() : CSSResultGroup {
		return css`
			${rhdcIconButtonStyles}
			${rhdcLinkStyles}

			table {
				border-spacing: var(--sl-spacing-x-small);
			}

			th {
				font-weight: normal;
				text-align: left;
			}

			td {
				vertical-align: middle;
			}

			td:first-child {
				text-align: end;
			}

			sl-tag[hidden], sl-icon-button[hidden] {
				display: none;
			}

			.plugin-flag {
				text-decoration: dotted underline;
			}

			.smol {
				font-size: 0.75em;
			}

			a {
				overflow-wrap: break-word;
				word-break: break-all;
			}

			rhdc-hack-version-upload-button::part(button) {
				margin-bottom: var(--sl-spacing-x-small);
			}

			sl-dialog {
				--width: min( 900px, 95vw );
			}

			.nowrap {
				display: flex;
			}

			tr:first-child > td > div > sl-icon-button[name="chevron-double-up"] {
				display: none;
			}

			.version-buttons {
				display: flex;
				gap: var(--sl-spacing-x-small);
				margin-top: var(--sl-spacing-2x-small);
			}

			.version-buttons > sl-button > sl-icon {
				font-size: var(--sl-font-size-x-large);
				margin: 0 calc( -1 * var(--sl-spacing-x-small) );
				vertical-align: middle;
			}
		`;
	}

	override render() : unknown {
		let alert: unknown = nothing;
		let addButton: unknown = nothing;
		if( this.hack.locked && this.hack.canEditInfo && !this.hack.canEditVersions ) {
			alert = AlertFactory.create({
				type: 'warning',
				message: 'You may no longer upload or edit hack versions because the deadline for the competition has passed. You will be able to upload and edit versions again once the competition has concluded.'
			});
		} else if( this.hack.canEditVersions ) {
			addButton = html`<rhdc-hack-version-upload-button .hack=${this.hack}></rhdc-hack-version-upload-button>`;
		}

		const versionRows = guard([ this.hack ], () => map(
			[...this.hack.versions].reverse(),
			this.#renderVersion.bind( this )
		));

		return html`
			<h2>Versions</h2>
			${alert}
			${addButton}
			<table>
				<thead>
					<tr>
						<th scope="col"></th>
						<th scope="col">Version</th>
						<th scope="col">Console Compatibility</th>
						<th scope="col">Recommended Plugin</th>
						<th scope="col"></th>
						<th scope="col"></th>
						<th scope="col"></th>
					</tr>
				</thead>
				<tbody>${versionRows}</tbody>
			</table>
			<sl-dialog @sl-request-close=${this.#onRequestDialogClose}>
				<rhdc-plugin-editor
					.defaultValue=${this.editingPlugin}
					?loading=${this.loading}
				></rhdc-plugin-editor>

				<sl-button
					slot="footer"
					variant="primary"
					?loading=${this.loading}
					@click=${this.#saveVersion}
				>Save</sl-button>

				<sl-button
					slot="footer"
					variant="neutral"
					?loading=${this.loading}
					@click=${() => this.editDialog.hide()}
				>Cancel</sl-button>
			</sl-dialog>
			<rhdc-dialog id="approve-version-dialog" label="Confirm Approval" .confirmText=${'Approve'}>
				<p>Are you sure you want to approve this hack version?</p>
			</rhdc-dialog>
			<rhdc-dialog id="reject-version-dialog" label="Confirm Rejection" .confirmText=${'Reject'} destructive>
				<p>Are you sure you want to reject this hack version?</p>
				<sl-textarea
					id="reason-input"
					inputmode="text"
					label="Reason (Optional)"
					placeholder="Optionally enter text here to be included in the rejection notification."
					rows="2"
					resize="vertical"
					autocapitalize="on"
					autocorrect="on"
					autocomplete="off"
					enterkeyhint="enter"
					spellcheck
				></sl-textarea>
			</rhdc-dialog>
		`;
	}

	#renderVersion( version: ConstRef<HackVersionData> ) : HTMLTemplateResult {
		const playNow = version.archived ? html`
			<sl-tooltip content="Archived">
				<sl-icon name="archive"></sl-icon>
			</sl-tooltip>
		`: html`
			<sl-button variant="success" href="rhdc://${this.hack.urlTitle}/${version.download.fileName}">
				<sl-icon src="/assets/icons/rhdc-link.svg" slot="prefix"></sl-icon>
				<span>Play Now</span>
			</sl-button>
		`;

		const downloadLink = (!version.archived || this.hack.canDownloadArchived) ?
			this.#renderDownloadLink( version.download ) :
			html`<span>${version.download.fileName}</span>`;

		const inputIcons: HTMLTemplateResult[] = [];
		if( version.hackFlags.includes( HackFlag.RequiresGcc ) ) {
			inputIcons.push( html`
				<sl-tooltip placement="top-start" content="${GCC_TOOLTIP}">
					<sl-icon tabindex="0" name="controller"></sl-icon>
				</sl-tooltip>
			` );
		}

		if( version.hackFlags.includes( HackFlag.RequiresMouse ) ) {
			inputIcons.push( html`
				<sl-tooltip placement="top-start" content="${MOUSE_TOOLTIP}">
					<sl-icon tabindex="0" name="mouse2"></sl-icon>
				</sl-tooltip>
			` );
		}

		if( version.hackFlags.includes( HackFlag.DualAnalog ) ) {
			inputIcons.push( html`
				<sl-tooltip placement="top-start" content="${DUAL_ANALOG_TOOLTIP}">
					<sl-icon tabindex="0" name="joystick"></sl-icon>
				</sl-tooltip>
			` );
		}

		let versionStatus = html`<sl-tag variant="warning" ?hidden=${version.approved} pill>Pending Approval</sl-tag>`;
		if( this.hack.canApproveVersions && !version.approved ) {
			versionStatus = html`
				${versionStatus}
				<div class="version-buttons">
					<sl-button variant="success" @click=${this.#approveVersion.bind( this, version.download.fileName )}><sl-icon name="check-lg"></sl-icon></sl-button>
					<sl-button variant="danger" @click=${this.#rejectVersion.bind( this, version.download.fileName )}><sl-icon name="x-lg"></sl-icon></sl-button>
				</div>
			`;
		}

		const hasMultipleApprovedVersions = this.hack.versions.filter( v => v.approved ).length > 1;
		const canDeleteVersion = this.hack.canEditVersions && ( !version.approved || hasMultipleApprovedVersions );

		return html`
			<tr>
				<td>
					${playNow}
				</td>
				<td>
					${downloadLink}
				</td>
				<td>
					<rhdc-console-compatibility .compatibility=${version.consoleCompatibility}></rhdc-console-compatibility>
				</td>
				<td>
					${this.#renderRecommendedPlugin( version )}
				</td>
				<td>
					<div class="nowrap">${inputIcons}</div>
				</td>
				<td>
					${versionStatus}
				</td>
				<td>
					<div class="nowrap">
						<sl-icon-button label="Edit Version" name="pencil" ?hidden=${!this.hack.canEditVersions} @click=${this.#editVersion.bind( this, version )}></sl-icon-button>
						<sl-icon-button label="Archive Version" class="danger" name="archive" ?hidden=${version.archived || !this.hack.canEditVersions} @click=${this.#archiveVersion.bind( this, version )}></sl-icon-button>
						<sl-icon-button label="Restore Version" name="recycle" ?hidden=${!version.archived || !this.hack.canEditVersions} @click=${this.#restoreVersion.bind( this, version )}></sl-icon-button>
						<sl-icon-button label="Delete Version" class="danger" name="trash" ?hidden=${!canDeleteVersion || !version.archived} @click=${this.#deleteVersion.bind( this, version )}></sl-icon-button>
						<sl-icon-button label="Promote Version" name="chevron-double-up" ?hidden=${!this.hack.canEditVersions} @click=${this.#moveToTop.bind( this, version )}></sl-icon-button>
					</div>
				</td>
			</tr>
		`;
	}

	#renderDownloadLink( download: ConstRef<DownloadInfo> ) : HTMLTemplateResult {
		if( download.secured ) {
			return html`<a href="javascript: void(0)" @click=${this.#downloadSecured.bind( this, download.webHref )}>${download.fileName}</a>`;
		} else {
			return html`<a href="${toApiUrl( download.directHref )}" download type="${download.mimeType}">${download.fileName}</a>`;
		}
	}

	async #downloadSecured( tokenHref: string ) : Promise<void> {
		const downloadToken = await FilesApi.getDownloadTokenAsync( getUrlPath( tokenHref ) );

		const virtualAnchor = document.createElement( 'a' );
		virtualAnchor.href = toApiUrl( downloadToken.href );
		virtualAnchor.type = downloadToken.mimeType;
		virtualAnchor.setAttribute( 'download', 'download' );
		virtualAnchor.click();
	}

	#renderRecommendedPlugin( version: ConstRef<HackVersionData> ) : unknown {
		if( version.plugin === GfxPlugin.ParaLLEl && version.pluginFlags.includes( GfxPluginFlag.AllowHleFallback ) ) {
			return html`
				<span class="smol">
					${this.#renderPluginFlags( GfxPlugin.ParaLLEl, version.pluginFlags )}
					<span> or<br/> </span>
					${this.#renderPluginFlags( GfxPlugin.GLideN64, version.pluginFlags )}
				</span>
			`;
		} else {
			return this.#renderPluginFlags( version.plugin, version.pluginFlags );
		}
	}

	#renderPluginFlags( plugin: ConstRef<Nullable<GfxPlugin>>, flags: ConstRef<GfxPluginFlag[]> ) : unknown {
		if( !plugin ) return html`<span style="color: var(--sl-color-neutral-500);">Unknown</span>`;

		const flagInfos: PluginFlagInfo[] = [];
		for( const flag of flags ) {
			switch( flag ) {
				case GfxPluginFlag.UpscaleTexrects: {
					if( plugin === GfxPlugin.ParaLLEl ) {
						flagInfos.push({ acronym: 'UTR', details: 'enable Upscale TEXTRECTs' })
					}
					break;
				}
				case GfxPluginFlag.EmulateFramebuffer: {
					if( plugin === GfxPlugin.GLideN64 ) {
						flagInfos.push({ acronym: 'EFB', details: 'enable Emulate Framebuffer' });
					}
					break;
				}
				case GfxPluginFlag.AccurateDepthCompare: {
					if( plugin === GfxPlugin.GLideN64 ) {
						flagInfos.push({ acronym: 'NDC', details: 'enable Emulate N64 Depth Compare' });
					}
					break;
				}
				case GfxPluginFlag.Widescreen: {
					if( plugin === GfxPlugin.GLideN64 || plugin === GfxPlugin.OGRE ) {
						flagInfos.push({ acronym: 'WVH', details: 'enable Widescreen (Viewport Hack)' });
					}
					break;
				}
				default: break;
			}
		}

		if( flagInfos.length <= 0 ) {
			return html`<span>${plugin}</span>`;
		}

		const parts: HTMLTemplateResult[] = [
			html`<span>${plugin} (</span>`
		];

		let first = true;
		for( const info of flagInfos ) {
			if( !first ) {
				parts.push( html`<span>, </span>` );
			}

			parts.push( html`
				<sl-tooltip content="${info.details}">
					<span class="plugin-flag">${info.acronym}</span>
				</sl-tooltip>
			`);

			first = false;
		}

		parts.push( html`<span>)</span>` );
		return parts;
	}

	#editVersion( version: HackVersionData ) : void {
		if( !this.hack.canEditVersions ) return;

		this.editingVersion = version.download.fileName;
		this.editingPlugin = {};
		this.editingPlugin = {
			plugin: version.plugin || undefined,
			pluginFlags: version.pluginFlags,
			hackFlags: version.hackFlags,
			consoleCompatibility: version.consoleCompatibility
		};

		this.editDialog.show();
	}

	async #archiveVersion( version: HackVersionData ) : Promise<void> {
		if( !this.hack.canEditVersions ) return;

		if( !(await this.confirmAsync(
			'Confirm Archival',
			'Are you sure you want to archive this version? This will prevent the version from being downloaded (except by moderators and staff). You can restore it at any time.',
			'Archive',
			'Cancel'
		)) ) return;

		try {
			this.hack = await HacksApi.updateHackVersionAsync( this.hack.hackId, version.download.fileName, { archived: true } );
			this.toastSuccess( 'Version Archived' );
			this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to archive hack version', getErrorMessage( exception ) );
		}
	}

	async #restoreVersion( version: HackVersionData ) : Promise<void> {
		if( !this.hack.canEditVersions ) return;

		if( !this.hack.canEditVersions ) return;

		if( !(await this.confirmAsync(
			'Confirm Restoration',
			'Are you sure you want to restore this version? This will allow it to be downloaded again.',
			'Restore',
			'Cancel'
		)) ) return;

		try {
			this.hack = await HacksApi.updateHackVersionAsync( this.hack.hackId, version.download.fileName, { archived: false } );
			this.toastSuccess( 'Version Restored' );
			this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to archive hack version', getErrorMessage( exception ) );
		}
	}

	async #deleteVersion( version: HackVersionData ) : Promise<void> {
		if( !this.hack.canEditVersions ) return;
		
		if( !(await this.confirmAsync(
			'Confirm Deletion',
			'Are you sure you want to permanently delete this version? Users who previously downloaded this version will no longer have it linked to your hack.',
			'Delete',
			'Cancel'
		)) ) return;

		try {
			this.hack = await HacksApi.deleteHackVersionAsync( this.hack.hackId, version.download.fileName );
			this.toastSuccess( 'Version deleted' );
			this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to delete hack version', getErrorMessage( exception ) );
		}
	}

	async #moveToTop( version: HackVersionData ) : Promise<void> {
		if( !this.hack.canEditVersions ) return;
		
		try {
			this.hack = await HacksApi.promoteHackVersionAsync( this.hack.hackId, version.download.fileName );
			this.toastSuccess( 'Version promoted to default' );
			this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to promote hack version', getErrorMessage( exception ) );
		}
	}

	async #saveVersion() : Promise<void> {
		const update = this.pluginEditor.value;
		if( !update ) {
			this.toastError( 'Failed to save version changes', 'You must select a graphics plugin' );
			return;
		}

		this.loading = true;
		try {
			this.hack = await HacksApi.updateHackVersionAsync( this.hack.hackId, this.editingVersion, update );
			this.toastSuccess( 'Version updated' );
			this.editDialog.hide();
			this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to save version changes', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	#approveVersion( version: string ) : void {
		this.approveVersionDialog.runAsync( async () => {
			try {
				this.hack = await HacksApi.approveHackVersionAsync( this.hack.hackId, version );
				if( Auth.isModOrStaff() ) {
					this.dispatchEvent( RhdcModqueueStatusChangeEvent.createAsync() );
				}
			} catch( exception: unknown ) {
				this.toastError( 'Failed to approve hack version', getErrorMessage( exception ) );
				throw exception;
			}
		});
	}

	#rejectVersion( version: string ) : void {
		this.rejectionReasonInput.value = '';
		this.rejectVersionDialog.runAsync( async () => {
			try {
				this.hack = await HacksApi.rejectHackVersionAsync( this.hack.hackId, version, this.rejectionReasonInput.value );
				if( Auth.isModOrStaff() ) {
					this.dispatchEvent( RhdcModqueueStatusChangeEvent.createAsync() );
				}
			} catch( exception: unknown ) {
				this.toastError( 'Failed to reject hack version', getErrorMessage( exception ) );
				throw exception;
			}
		});
	}

	#onRequestDialogClose( event: SlRequestCloseEvent ) : void {
		if( event.detail.source === 'overlay' ) event.preventDefault();
	}

}
