import { css, CSSResultGroup, html, nothing } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { map } from 'lit/directives/map.js';
import { RhdcElement } from '../../rhdc-element';
import { Category, Hack } from '../../apis/hacks-api';
import { RichUserRef, WeakRichUserRef } from '../../apis/users-api';
import { formatLocalDate } from '../../util/formatting';
import { ConstRef, Weak } from '../../util/types';
import '@shoelace-style/shoelace/dist/components/card/card';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/tag/tag';
import '@shoelace-style/shoelace/dist/components/tooltip/tooltip';
import '../common/rhdc-username';
import '../common/rhdc-console-compatibility';
import '../../widgets/rhdc-image';
import '../../widgets/rhdc-readonly-rating';

@customElement( 'rhdc-hack-card' )
export class RhdcHackCard extends RhdcElement {

	@property({ attribute: false })
	hack!: Readonly<Hack>;

	static override get styles() : CSSResultGroup {
		return css`

			sl-card {
				width: 100%;
				overflow-x: hidden;
				--padding: var(--sl-spacing-x-small);
			}

			sl-card > div {
				display: flex;
				align-items: center;
				white-space: nowrap;
				gap: var(--sl-spacing-x-small);
			}

			.body {
				flex-grow: 1;
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-2x-small);
				max-width: calc( 100% - 160px - var(--sl-spacing-x-small) );
			}

			.grid {
				flex-grow: 1;
				display: grid;
				grid-template-columns: 1fr 1fr 3fr 1fr;
				grid-auto-flow: row;
				gap: var(--sl-spacing-small);
			}

			@media only screen and (max-width: 750px) {
				.grid {
					grid-template-columns: 1fr 1fr 1fr 1fr;
				}

				rhdc-readonly-rating {
					display: none !important;
				}
			}

			.star-icon {
				color: var(--sl-color-amber-500);
			}

			rhdc-readonly-rating {
				color: var(--sl-color-amber-500);
				vertical-align: top;
				height: 1.2rem;
			}

			rhdc-readonly-rating.difficulty {
				color: var(--sl-color-primary-600);
			}

			rhdc-readonly-rating.difficulty[data-kaizo] {
				color: var(--sl-color-danger-500);
			}

			rhdc-image {
				width: 160px;
				height: 120px;
			}

			.title {
				display: flex;
				justify-content: space-between;
			}

			.title > b {
				font-size: var(--sl-font-size-large);
				text-overflow: ellipsis;
				overflow: hidden;
			}

			.authors {
				display: flex;
				overflow-x: hidden;
				gap: 0.5ch;
			}

			.grid > div > span:first-child {
				font-size: var(--sl-font-size-2x-small);
			}

			.last-column {
				text-align: end;
			}

			.dot {
				color: var(--sl-color-neutral-600);
			}

			.dot:last-child {
				display: none;
			}

			rhdc-readonly-rating ~ span {
				vertical-align: sub;
			}

			sup {
				font-size: var(--sl-font-size-2x-small);
			}

			a.card-link {
				display: contents;
				color: inherit;
				text-decoration: inherit;
			}

			sl-card:hover::part(base), sl-card:focus-visible::part(base) {
				background-color: var(--sl-color-neutral-100);
			}
		`;
	}

	override render() : unknown {
		let archivedIcon: unknown = nothing;
		if( this.hack.archived ) {
			archivedIcon = html`
				<sl-tooltip content="Archived">
					<sl-icon name="archive"></sl-icon>
				</sl-tooltip>
			`;
		}

		return html`
			<a class="card-link" href="/hack/${this.hack.urlTitle}" @click=${this.#gotoHack}>
				<sl-card tabindex="0">
					<div>
						<rhdc-image .downloadInfo=${this.hack.screenshots[0]}></rhdc-image>
						<div class="body">
							<div class="title">
								<b>${archivedIcon}${this.hack.title}</b>
								<sl-tag size="small" pill>${this.hack.category}</sl-tag>
							</div>
							<div class="authors">
								<b>${this.hack.authors.length === 1 ? 'Author:' : 'Authors:'}</b>
								${map(this.hack.authors, this.#renderAuthor)}
							</div>
							<div class="grid">
								<div>
									<span>Star Count</span><br/>
									<span>${this.hack.stars.toString()}</span>
									<sl-icon class="star-icon" name="star-fill"></sl-icon>
								</div>
								<div>
									<span>Upload Date</span><br/>
									<span>${this.#formatDate( this.hack.uploadedDate )}</span>
								</div>
								<div>
									<span>Rating</span><br/>
									<rhdc-readonly-rating
										max="10"
										value="${this.hack.rating || 0}"
										icon="${this.hack.rating == null ? 'question-lg' : 'star-fill'}"
									></rhdc-readonly-rating>
									<span>${this.hack.rating?.toFixed( 2 ) || '-'}</span>
								</div>
								<div class="last-column">
									<span>Console Compatibility</span><br/>
									<rhdc-console-compatibility hoist .compatibility=${this.hack.consoleCompatibility}></rhdc-console-compatibility>
								</div>
								<div>
									<span>Completion Bonus</span><br/>
									<span>${this.hack.completionBonus?.toString() || 'TBD'}</span>
								</div>
								<div>
									<span>Creation Date</span><br/>
									<span>${this.#formatDate( this.hack.createdDate )}</span>
								</div>
								<div>
									<span>Difficulty ${this.hack.category === Category.Kaizo ? ' (Kaizo)' : ''}</span><br/>
									<rhdc-readonly-rating
										class="difficulty"
										max="10"
										value="${this.hack.difficulty || 0}"
										icon="${this.hack.rating == null ? 'question-lg' : 'fire'}"
										?data-kaizo=${this.hack.category === Category.Kaizo}
									></rhdc-readonly-rating>
									<span>${this.hack.difficulty?.toFixed( 2 ) || '-'}</span>
									<sup ?hidden=${this.hack.category !== Category.Kaizo || !this.hack.difficulty}>K</sup>
								</div>
								<div class="last-column">
									<span>Downloads</span><br/>
									<span>${this.hack.numDownloads.toString()}</span>
									<sl-icon name="download"></sl-icon>
								</div>
							</div>
						</div>
					</div>
				</sl-card>
			</a>
		`;
	}

	#renderAuthor( author: ConstRef<WeakRichUserRef> ) {
		const delimiter = html`<span class="dot">&bull;</span>`;
		if( author.userId ) {
			return html`<rhdc-username .user=${author as RichUserRef}></rhdc-username>${delimiter}`;
		} else {
			return html`<span>${author.username}</span>${delimiter}`;
		}
	}

	#formatDate( dateTime: ConstRef<Weak<Date>> ) {
		return dateTime ? formatLocalDate( dateTime ) : 'unknown';
	}

	#gotoHack( event: Event ) : void {
		event.preventDefault();
		this.navigate( (event.currentTarget as HTMLAnchorElement).getAttribute( 'href' )! );
	}

}
