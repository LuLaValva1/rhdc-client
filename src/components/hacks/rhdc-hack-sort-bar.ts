import { css, CSSResultGroup, html, HTMLTemplateResult } from 'lit';
import { customElement, query, property } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { HackSortField } from '../../apis/hacks-api';
import SlTabGroup from '@shoelace-style/shoelace/dist/components/tab-group/tab-group';
import SlTab from '@shoelace-style/shoelace/dist/components/tab/tab';
import SlIcon from '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/tab-group/tab-group';
import '@shoelace-style/shoelace/dist/components/tab/tab';
import '@shoelace-style/shoelace/dist/components/icon/icon';

@customElement( 'rhdc-hack-sort-bar' )
export class RhdcHackSortBar extends RhdcElement {

	@property({ attribute: false })
	sortField = HackSortField.DateUploaded;

	@property({ attribute: false })
	sortDescending = true;

	@query( 'sl-tab-group' )
	sortBar!: SlTabGroup;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: contents;
			}

			sl-tab > sl-icon {
				display: inline-block;
				visibility: hidden;
				margin-left: 0.5ch;
			}

			sl-tab[active] > sl-icon {
				visibility: visible;
			}

			sl-tab::part(base) {
				font-size: var(--sl-font-size-medium);
			}
		`;
	}

	override render() : unknown {
		return html`
			<sl-tab-group part="base" @sl-tab-show=${this.#sort}>
				${this.#renderSortField( HackSortField.DateUploaded, 'Upload Date' )}
				${this.#renderSortField( HackSortField.DateCreated, 'Creation Date' )}
				${this.#renderSortField( HackSortField.Name, 'Name' )}
				${this.#renderSortField( HackSortField.Popularity, 'Downloads' )}
				${this.#renderSortField( HackSortField.Rating, 'Rating' )}
				${this.#renderSortField( HackSortField.Difficulty, 'Difficulty' )}
				${this.#renderSortField( HackSortField.Stars, 'Stars' )}
			</sl-tab-group>
		`;
	}

	#renderSortField( field: HackSortField, label: string ) : HTMLTemplateResult {
		return html`
			<sl-tab
				slot="nav"
				data-field="${field}"
				?active=${this.sortField === field}
				@click=${this.#clickTab}
				@keydown=${this.#tabKeypress}
			>
				<span>${label}</span>
				<sl-icon name="${this.sortDescending ? 'chevron-down' : 'chevron-up'}"></sl-icon>
			</sl-tab>
		`;
	}

	#sort() : void {
		for( const node of this.sortBar.childNodes ) {
			if( node.nodeName !== 'SL-TAB' ) continue;
			const field = node as SlTab;

			if( !field.active ) continue;

			const chevron = field.lastElementChild as SlIcon;
			if( !chevron || chevron.nodeName !== 'SL-ICON' ) continue;

			chevron.name = 'chevron-down';
			this.sortField = field.dataset.field as HackSortField;
			this.sortDescending = true;
		}

		this.dispatchEvent( new CustomEvent( 'rhdc-change' ) );
	}

	#clickTab( event: Event ) : void {
		const field = event.currentTarget as SlTab;
		if( !field.active ) return;
		this.sortDescending = !this.sortDescending;
		this.dispatchEvent( new CustomEvent( 'rhdc-change' ) );
	}

	#tabKeypress( event: KeyboardEvent ) : void {
		if( event.key !== 'Enter' && event.key !== ' ' && event.key !== 'Spacebar' ) return;
		const field = event.currentTarget as SlTab;
		if( !field.active ) return;
		this.sortDescending = !this.sortDescending;
		this.dispatchEvent( new CustomEvent( 'rhdc-change' ) );
	}

}
