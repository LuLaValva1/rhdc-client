import { css, CSSResultGroup, html, HTMLTemplateResult, nothing, PropertyValues } from 'lit';
import { customElement, property, state, query } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { RhdcElement } from '../../rhdc-element';
import { ConsoleCompatibility, GfxPlugin, HackFlag, HackSaveType, HackVersionUpdateDto, GfxPluginFlag } from '../../apis/hacks-api';
import { AlertFactory } from '../../util/alert-factory';
import { Nullable } from '../../util/types';
import SlDrawer from '@shoelace-style/shoelace/dist/components/drawer/drawer';
import SlCheckbox from '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import SlSelect from '@shoelace-style/shoelace/dist/components/select/select';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/select/select';
import '@shoelace-style/shoelace/dist/components/menu-label/menu-label';
import '@shoelace-style/shoelace/dist/components/divider/divider';
import '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '@shoelace-style/shoelace/dist/components/tooltip/tooltip';
import '@shoelace-style/shoelace/dist/components/drawer/drawer';
import '@shoelace-style/shoelace/dist/components/option/option';
import './submission/rhdc-plugin-help';

enum PluginFlagBits {
	None = 0x0,
	UpscaleTexrects = 0x1,
	AllowHleFallback = 0x2,
	EmulateFramebuffer = 0x4,
	AccurateDepthCompare = 0x8,
	Widescreen = 0x10
}

@customElement( 'rhdc-plugin-editor' )
export class RhdcPluginEditor extends RhdcElement {

	@property({ attribute: false })
	defaultValue: HackVersionUpdateDto = {};

	@property({ attribute: false })
	saveType: Nullable<HackSaveType> = null;

	@property({ attribute: 'loading', type: Boolean })
	loading = false;

	@state()
	error = '';

	@state()
	gfxPlugin: Nullable<GfxPlugin> = null;

	@state()
	consoleCompatibility: Nullable<ConsoleCompatibility> = null;

	@state()
	pluginFlags = PluginFlagBits.UpscaleTexrects;

	@query( 'sl-drawer' )
	popout!: SlDrawer;

	public get value() : Nullable<Omit<Required<HackVersionUpdateDto>,'archived'>> {
		if( !this.gfxPlugin ) {
			const pluginSelect = this.shadowRoot?.getElementById( 'gfx-plugin' ) as Nullable<SlSelect>;
			pluginSelect?.reportValidity();
			pluginSelect?.scrollIntoView();
			return null;
		}

		const pluginFlags: GfxPluginFlag[] = [];
		switch( this.gfxPlugin ) {
			case GfxPlugin.ParaLLEl:
				if( this.pluginFlags & PluginFlagBits.UpscaleTexrects ) {
					pluginFlags.push( GfxPluginFlag.UpscaleTexrects );
				}

				if( this.pluginFlags & PluginFlagBits.AllowHleFallback ) {
					pluginFlags.push( GfxPluginFlag.AllowHleFallback );
				} else break;
			case GfxPlugin.GLideN64:
				if( this.pluginFlags & PluginFlagBits.EmulateFramebuffer ) {
					pluginFlags.push( GfxPluginFlag.EmulateFramebuffer );
					if( this.pluginFlags & PluginFlagBits.AccurateDepthCompare ) {
						pluginFlags.push( GfxPluginFlag.AccurateDepthCompare );
					}
				}
				if( this.gfxPlugin === GfxPlugin.ParaLLEl ) break;
			case GfxPlugin.OGRE:
				if( this.pluginFlags & PluginFlagBits.Widescreen ) {
					pluginFlags.push( GfxPluginFlag.Widescreen );
				}
			default: break;
		}

		const hackFlags: HackFlag[] = [];
		for( const hflag of [ HackFlag.NoOverclock, HackFlag.DualAnalog, HackFlag.BigEEPROM ] ) {
			if( (this.shadowRoot?.getElementById( hflag ) as Nullable<SlCheckbox>)?.checked ) {
				hackFlags.push( hflag );
			}
		}

		return {
			plugin: this.gfxPlugin,
			pluginFlags: pluginFlags,
			hackFlags: hackFlags,
			consoleCompatibility: this.consoleCompatibility
		};
	}

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-small);
			}

			.indent {
				margin-left: var(--sl-spacing-large);
			}

			sl-drawer {
				--size: min( 100vw, 1250px );
			}

			sl-drawer::part(base) {
				z-index: var(--sl-z-index-dialog);
			}

			sl-select {
				max-width: 400px;
			}

			sl-checkbox {
				display: block;
				width: max-content;
				margin: var(--sl-spacing-2x-small) 0;
			}

			sl-checkbox > div {
				margin: var(--sl-spacing-2x-small) calc( -1 * var(--sl-toggle-size) - 0.5rem );
				font-size: var(--sl-font-size-x-small);
				white-space: normal;
			}

			sl-option > div {
				width: 100%;
				white-space: normal;
				font-size: var(--sl-font-size-2x-small);
			}

			h1 {
				margin-bottom: var(--sl-font-size-2x-small);
				font-size: var(--sl-font-size-large);
			}

			p {
				margin: var(--sl-font-size-2x-small) 0;
				font-size: var( --sl-font-size-small );
			}

			a {
				color: var(--sl-color-primary-600) !important;
				text-decoration: none;
			}

			a:hover {
				color: var(--sl-color-primary-500) !important;
			}

			a:focus {
				text-decoration: underline dotted;
			}

			sl-option > div::before {
				width: 100%;
				white-space: normal;
				font-size: var(--sl-font-size-2x-small);
			}

			sl-option[value="None"] > div::before {
				content: "The hack crashes and/or doesn't display correctly on console.";
			}

			sl-option[value="Unoptimized"] > div::before {
				content: "The hack displays correctly on console and does not crash, but it has too much lag to be playable on N64 hardware.";
			}

			sl-option[value="Playable"] > div::before {
				content: "The hack is playable on console, but may suffer from minor lag in many areas or major lag in at least one area.";
			}

			sl-option[value="Good"] > div::before {
				content: "The hack works well on console, but there is minor lag in some areas, the low resolution of N64 negatively affects gameplay, and/or there are other things that make emulator the recommended way to play the game.";
			}

			sl-option[value="Excellent"] > div::before {
				content: "The hack works perfectly on console. Console players will have a good experience.";
			}
		`;
	}

	override render() : unknown {
		let pluginSettings : unknown = nothing;
		switch( this.gfxPlugin ) {
			case GfxPlugin.ParaLLEl:
				pluginSettings = html`<div>
					${this.#renderPluginSetting( PluginFlagBits.UpscaleTexrects )}
					${this.#renderPluginSetting( PluginFlagBits.AllowHleFallback )}
					${this.#renderPluginSetting( PluginFlagBits.EmulateFramebuffer, PluginFlagBits.AllowHleFallback, true )}
					${this.#renderPluginSetting( PluginFlagBits.AccurateDepthCompare, PluginFlagBits.AllowHleFallback | PluginFlagBits.EmulateFramebuffer, true )}
				</div>`;
				break;
			case GfxPlugin.GLideN64:
				pluginSettings = html`<div>
					${this.#renderPluginSetting( PluginFlagBits.EmulateFramebuffer )}
					${this.#renderPluginSetting( PluginFlagBits.AccurateDepthCompare, PluginFlagBits.EmulateFramebuffer )}
					${this.#renderPluginSetting( PluginFlagBits.Widescreen )}
				</div>`;
				break;
			case GfxPlugin.OGRE:
				pluginSettings = html`<div>
					${this.#renderPluginSetting( PluginFlagBits.Widescreen )}
				</div>`;
				break;
		}

		let alert: unknown = nothing;
		if( this.consoleCompatibility && this.consoleCompatibility !== ConsoleCompatibility.None ) {
			switch( this.gfxPlugin ) {
				case GfxPlugin.GLideN64:
					alert = AlertFactory.create({
						type: 'primary',
						message: 'The ParaLLEl plugin usually makes the most sense for console compatible hacks, though GLideN64 can also be fine.'
					});
					break;
				case GfxPlugin.OGRE:
				case GfxPlugin.Glide64:
				case GfxPlugin.Rice:
					alert = AlertFactory.create({
						type: 'warning',
						message: `${this.gfxPlugin} is not an appropriate plugin for a console compatible hack. You probably want to recommend GLideN64 or ParaLLEl instead.`
					});
					break;
			}
		} else if( this.gfxPlugin === GfxPlugin.Glide64 || this.gfxPlugin === GfxPlugin.Rice ) {
			alert = AlertFactory.create({
				type: 'warning',
				message: 'The Glide64 and Rice plugins are mostly obsolete. While they are still appropriate in some niche cases, you most likely want to recommend OGRE instead.'
			});
		}

		return html`
			<sl-drawer label="Plugin Help" contained>
				<rhdc-plugin-help></rhdc-plugin-help>
			</sl-drawer>

			<div>
				<h1>Graphics Plugin</h1>
				<p>
					Select the graphics plugin that works best for your hack.
					Parallel Launcher will automatically select this plugin for your hack.
					If you are unfamilar with a plugin listed here or you don't know what to recommend,
					<a href="javascript: void(0)" @click=${this.#showPluginHelp}>click here for guidance</a>.
				</p>

				<sl-select
					id="gfx-plugin"
					?disabled=${this.loading}
					placeholder="Select a plugin"
					.value=${this.gfxPlugin || ''}
					label="Recommended Graphics Plugin"
					required
					@sl-change=${this.#pluginChanged}
				>
					<sl-menu-label>LLE Plugins</sl-menu-label>
					<sl-option value=${GfxPlugin.ParaLLEl}>${GfxPlugin.ParaLLEl}</sl-option>
					<sl-option value=${GfxPlugin.Angrylion}>${GfxPlugin.Angrylion}</sl-option>
					<sl-divider></sl-divider>
					<sl-menu-label>HLE Plugins</sl-menu-label>
					<sl-option value=${GfxPlugin.GLideN64}>${GfxPlugin.GLideN64}</sl-option>
					<sl-option value=${GfxPlugin.OGRE}>${GfxPlugin.OGRE}</sl-option>
					<sl-divider></sl-divider>
					<sl-menu-label>HLE Plugins (Legacy)</sl-menu-label>
					<sl-option value=${GfxPlugin.Glide64}>${GfxPlugin.Glide64}</sl-option>
					<sl-option value=${GfxPlugin.Rice}>${GfxPlugin.Rice}</sl-option>
				</sl-select>
				${pluginSettings}
			</div>

			<div>
				<h1>Other Emulation Settings</h1>

				<sl-checkbox
					id="${HackFlag.NoOverclock}"
					?disabled=${this.loading}
					?checked=${this.defaultValue?.hackFlags?.includes( HackFlag.NoOverclock )}
				>
					Disable CPU Overclocking
					<div>Parallel Launcher should default to disabling CPU overclocking on this hack. This is likely to cause lag, so only check this box if your hack actually requires it.</div>
				</sl-checkbox>

				<sl-checkbox
					id="${HackFlag.DualAnalog}"
					?disabled=${this.loading}
					?checked=${this.defaultValue?.hackFlags?.includes( HackFlag.DualAnalog )}
				>
					Dual Analog
					<div>This hack has analog camera support and Parallel Launcher should default to using the Dual Analog input mode.</div>
				</sl-checkbox>

				<sl-checkbox
					id="${HackFlag.BigEEPROM}"
					?disabled=${this.loading || this.saveType !== null}
					?checked=${this.defaultValue?.hackFlags?.includes( HackFlag.BigEEPROM ) || this.saveType === HackSaveType.Eeprom16k}
				>
					Use 16kb EEPROM
					<div>This hack requires extended 16kb EEPROM instead of the standard 4kb EEPROM. If you are unsure, you probably don't want this.</div>
				</sl-checkbox>
			</div>

			<sl-select
				id="console-compatibility"
				?disabled=${this.loading}
				.value=${this.consoleCompatibility || ''}
				label="Console Compatibility"
				@sl-change=${this.#compatibilityChanged}
			>
				<sl-option value=${''}>I don't know</sl-option>
				<sl-option value=${ConsoleCompatibility.None}>Not Compatible<div></div></sl-option>
				<sl-option value=${ConsoleCompatibility.Unoptimized}>Unoptimized<div></div></sl-option>
				<sl-option value=${ConsoleCompatibility.Playable}>Playable<div></div></sl-option>
				<sl-option value=${ConsoleCompatibility.Good}>Good<div></div></sl-option>
				<sl-option value=${ConsoleCompatibility.Excellent}>Excellent<div></div></sl-option>
			</sl-select>

			${alert}
		`;
	}

	#renderPluginSetting( flag: PluginFlagBits, dependencies = PluginFlagBits.None, indent = false ) : HTMLTemplateResult {
		let label, tooltip;
		switch( flag ) {
			case PluginFlagBits.UpscaleTexrects:
				label = 'Upscale TEXRECTs';
				tooltip = 'Upscales textures drawn using the TEX_RECT command.';
				break;
			case PluginFlagBits.AllowHleFallback:
				label = 'Allow GLideN64 fallback for old GPUs';
				tooltip = 'Indicates that the hack works well enough on GLideN64 for it to be an acceptable fallback plugin.';
				break;
			case PluginFlagBits.EmulateFramebuffer:
				label = 'Require framebuffer emulation';
				tooltip = 'Requires emulation of the native framebuffer. Necessary if you access the framebuffer on the CPU.';
				break;
			case PluginFlagBits.AccurateDepthCompare:
				label = 'Require correct decal emulation';
				tooltip = 'Correctly emulate depth comparisons. Required for decals and shadows to work correctly.';
				break;
			case PluginFlagBits.Widescreen:
				label = 'Supports widescreen viewport hack';
				tooltip = 'Indicates that the hack supports the increased FOV from a widescreen viewport.';
				break;
			default:
				throw 'Logic Error';
		}

		const callback = (event: Event) => {
			this.pluginFlags |= flag;
			if( !(event.target as SlCheckbox).checked ) {
				this.pluginFlags -= flag;
			}
		};

		const checked = (this.pluginFlags & flag) === flag;
		const disabled = (this.pluginFlags & dependencies) !== dependencies;

		return html`
			<sl-tooltip content=${tooltip} placement="right">
				<sl-checkbox
					class=${ifDefined( indent ? 'indent' : undefined )}
					?disabled=${disabled}
					?checked=${checked && !disabled}
					?indeterminate=${checked && disabled}
					@sl-change=${callback}
				>${label}</sl-checkbox>
			</sl-tooltip>
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'defaultValue' ) ) {
			this.gfxPlugin = this.defaultValue?.plugin || null;
			this.consoleCompatibility = this.defaultValue?.consoleCompatibility || null;

			this.pluginFlags = PluginFlagBits.UpscaleTexrects;
			if( this.defaultValue.plugin && this.defaultValue.pluginFlags ) {
				const flags = this.defaultValue.pluginFlags;
				switch( this.defaultValue.plugin ) {
					case GfxPlugin.ParaLLEl:
						if( !flags.includes( GfxPluginFlag.UpscaleTexrects ) ) {
							this.pluginFlags = PluginFlagBits.None;
						}
						
						if( flags.includes( GfxPluginFlag.AllowHleFallback ) ) {
							this.pluginFlags |= PluginFlagBits.AllowHleFallback;
						} else break;
					case GfxPlugin.GLideN64:
						if( flags.includes( GfxPluginFlag.EmulateFramebuffer ) ) {
							this.pluginFlags |= PluginFlagBits.EmulateFramebuffer;
							if( flags.includes( GfxPluginFlag.AccurateDepthCompare ) ) {
								this.pluginFlags |= PluginFlagBits.AccurateDepthCompare;
							}
						}
						if( this.defaultValue.plugin === GfxPlugin.ParaLLEl ) break;
					case GfxPlugin.OGRE:
						if( flags.includes( GfxPluginFlag.Widescreen ) ) {
							this.pluginFlags |= PluginFlagBits.Widescreen;
						}
					default: break;
				}
			}
		}
	}

	#showPluginHelp() : void {
		this.popout.show();
	}

	#pluginChanged() : void {
		this.gfxPlugin = (this.#getField<SlSelect>( 'gfx-plugin' ).value as (GfxPlugin | '')) || null;
	}

	#compatibilityChanged() : void {
		this.consoleCompatibility = (this.#getField<SlSelect>( 'console-compatibility' ).value as (ConsoleCompatibility | '')) || null;
	}

	#getField<T extends HTMLElement>( id: string ) : T {
		return this.shadowRoot!.getElementById( id ) as T;
	}

}

