import { css, CSSResultGroup, html } from 'lit';
import { customElement } from 'lit/decorators.js';
import { RhdcElement } from '../../../rhdc-element';
import '@shoelace-style/shoelace/dist/components/divider/divider';

@customElement( 'rhdc-plugin-help' )
export class RhdcPluginHelp extends RhdcElement {

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
			}

			.toc, .toc-spacer {
				width: 175px;
				max-width: 175px;
			}

			.toc {
				position: absolute;
			}

			.toc a {
				display: block;
				color: var(--sl-color-primary-500) !important;
				text-decoration: none !important;
			}

			.toc a:hover, .toc a:focus {
				text-decoration: underline dotted !important;
			}

			.toc > div {
				margin-left: 2ch;
			}

			.content {
				max-width: 1000px;
				width: calc( 100vw - 2 * var(--sl-spacing-large) );
			}

			@media (max-width: 1247px) {
				.toc { display: none }
				sl-divider { display: none; }
			}

			#flowchart {
				display: block;
				width: 762px;
				height: 627px;
				background-color: currentcolor;
				mask-size: contain;
				mask-repeat: no-repeat;
				mask-image: url( '/assets/img/plugin-flowchart.png' );
				-webkit-mask-image: url( '/assets/img/plugin-flowchart.png' );
			}

			h1 {
				font-size: var(--sl-font-size-2x-large);
				margin: var(--sl-spacing-large) 0;
			}

			h2 {
				font-size: var(--sl-font-size-x-large);
				margin: var(--sl-spacing-large) 0 var(--sl-spacing-x-small) 0;
			}

			h3 {
				font-size: var(--sl-font-size-large);
				margin: var(--sl-spacing-medium) 0 0 0;
			}

			h4 {
				font-size: var(--sl-font-size-medium);
				font-weight: bold;
				margin: 0;
			}

			h4 ~ p {
				margin-top: 0;
			}

			p {
				margin: var(--sl-spacing-x-small) 0 var(--sl-spacing-medium) 0;
				text-align: justify;
			}

			ul {
				padding-left: 2ch;
			}

			th > div {
				text-align: center;
				font-size: var(--sl-font-size-x-small);
			}

			a.ref {
				color: currentcolor !important;
				text-decoration: underline dotted;
			}

			a.ref:hover {
				text-decoration: underline;
			}

			.caption {
				font-size: var(--sl-font-size-2x-small);
			}
		`;
	}

	override render() : unknown {
		const scrollTo = (id: string) => {
			return () => { this.shadowRoot?.getElementById( id )?.scrollIntoView({ behavior: 'smooth' }); };
		};

		return html`
			<div class="toc">
				<a href="#" @click=${scrollTo('top')}>About</a>
				<a href="#" @click=${scrollTo('choosing-a-plugin')}>Choosing a Plugin</a>
				<a href="#" @click=${scrollTo('supported-plugins')}>Main Supported Plugins</a>
				<div>
					<a href="#" @click=${scrollTo('parallel')}>ParaLLEl</a>
					<a href="#" @click=${scrollTo('gliden64')}>GLideN64</a>
					<a href="#" @click=${scrollTo('ogre')}>OGRE</a>
				</div>
				<a href="#" @click=${scrollTo('deprecated-plugins')}>Other Supported Plugins</a>
				<div>
					<a href="#" @click=${scrollTo('angrylion')}>Angrylion</a>
					<a href="#" @click=${scrollTo('glide64')}>Glide64</a>
					<a href="#" @click=${scrollTo('rice')}>Rice</a>
				</div>
				<a href="#" @click=${scrollTo('obsolete-plugins')}>Obsolete Plugins</a>
				<div>
					<a href="#" @click=${scrollTo('jabo')}>Jabo's Direct3D</a>
					<a href="#" @click=${scrollTo('gln')}>gln64</a>
					<a href="#" @click=${scrollTo('arachnoid')}>Arachnoid</a>
					<a href="#" @click=${scrollTo('z64')}>Z64</a>
				</div>
				<a href="#" @click=${scrollTo('plugin-options')}>Plugin Options</a>
				<div>
					<a href="#" @click=${scrollTo('po-parallel')}>ParaLLEl</a>
					<a href="#" @click=${scrollTo('po-gliden64')}>GLideN64</a>
					<a href="#" @click=${scrollTo('po-ogre')}>OGRE</a>
				</div>
				<a href="#" @click=${scrollTo('plugin-issues')}>Common Plugin Issues</a>
				<div>
					<a href="#" @click=${scrollTo('decals')}>Shadows &amp; Decals</a>
					<a href="#" @click=${scrollTo('dithering')}>Alpha Dithering</a>
					<a href="#" @click=${scrollTo('overflow')}>Coordinate Overflow</a>
					<a href="#" @click=${scrollTo('mop2')}>MOP2</a>
				</div>
			</div>
			<div class="toc-spacer"></div>
			<sl-divider vertical></sl-divider>
			<div class="content">
				<h1 id="top">All About Graphics Plugins</h1>
				<p>This guide will explain the differences between graphics plugins and help you find the plugin that's right for your hack.</p>
				<p>This guide will make refrence to "LLE plugins" and "HLE plugins." LLE stands for Low Level Emulation and refers to plugins that accurately emulate the N64 at a low level, while HLE (High Level Emulation) plugins make more compromises by using the more modern equivalents of some things instead of trying to exactly match N64 behaviour.</p>
				<p>It is advisable to make your hack work on the most accurate plugins possible, as relying on emulation differences in less accurate plugins poses a problem if those plugins ever fix the inaccurate behaviour you were relying on, or if the plugin stops being supported by emulators in favour of more accurate ones. A hack that is console compatible will always have a plugin that works for it, while one that relies on the quirks of a specific plugin is in danger of becoming inaccessible in the future.</p>

				<h2 id="choosing-a-plugin">Choosing a Plugin: Where to Start?</h2>
				<p>While it is best to make a hack that works on ParaLLEl or other accurate plugins, this is not always possible when using older romhacking tools, as they actually rely on incorrect behaviour, and do not render as intended on a real N64 or a more accurate plugin. So how do you know which plugin you should start testing your hack on? The flowchart below will help you determine which plugin your hack best supports. Note that if you write any custom code to manually create display lists, this may change which plugins your hack works with.</p>
				<div id="flowchart"></div>
				<p>The plugins at the bottom of the flowchart are ordered from most accurate (on the left) to least accurate (on the right). If you discover an issue with your hack using the plugin suggested by the flowchart, try the next plugin to the left or right to see if that fixes the issue.</p>

				<h2 id="supported-plugins">Main Supported Plugins</h2>
				<p>Below is a list of the most useful plugins supported by Romhacking.com:</p>

				<h3 id="parallel">ParaLLEl</h3>
				<p>An extremely accurate LLE plugin that is great for console compatible hacks. Its goal is to render games exactly as they are on N64 or as they would be on an N64 if the N64 was capable of higher resolutions. While previous LLE plugins relied on software rendering, making them too slow to use in most cases, ParaLLEl is capable of hardware acceleration, making it able to run at a full framerate. It does require your graphics drivers to support Vulkan, so some very old graphics cards may not support it, and some onboard laptop GPUs may have performance issues. If your computer is not able to run ParaLLEl, then the next best plugin to try is GLideN64.</p>

				<h3 id="gliden64">GLideN64</h3>
				<p>GLideN64 is the most accurate of the currently available HLE plugins and provides a good balance of accuracy and performance. While less accurate than LLE plugins, it has lower hardware requirements, allowing it to be used on lower end computers, and it is still accurate enough to play most modern hacks without any major issues. This plugin also has options that, when enabled, allow it to accurately render some things normally exclusive to LLE plugins, though this comes at the cost of reduced performance. See the <a href="#po-gliden64" class="ref">plugin options</a> section for more details.</p>

				<h3 id="ogre">OGRE</h3>
				<p>OGRE (<b>O</b>ffshoot <b>G</b>LideN64: <b>R</b>omhack <b>E</b>dition) is a modification of GLideN64 designed to support older romhacks that don't work in the mainline version of GLideN64. Most retail games and newer hacks should also work with this plugin, though regular GLideN64 is recommended in this case.</p>

				<h2 id="deprecated-plugins">Other Supported Plugins</h2>
				<p>Below is a list of the remaining niche plugins supported by Romhacking.com:</p>

				<h3 id="angrylion">Angrylion</h3>
				<p>Like ParaLLEl, Angrylion is an LLE plugin that is extremely accurate to console. It does not perform upscaling or any other enhancements, and simply displays the game as it would on a real N64 console. Unlike ParaLLEl, this is a software renderer, so it is very slow, making it unsuitable for general use.</p>

				<h3 id="glide64">Glide64</h3>
				<p>Glide64 is a middle-of-the-road HLE plugin that is more accurate than Rice, but less accurate than GLideN64. It has good performance on low-end computers, but OGRE will generally give a better experience.</p>

				<h3 id="rice">Rice</h3>
				<p>Rice is an older plugin that was known for being the first plugin to support texture replacement. Nowadays, its main use is to play older hacks that rely on certain plugin inaccuracies common to Rice and Jabo; however, it has now been mostly obsoleted by OGRE, which is also able to play most of these hacks and has superior quality.</p>

				<h2 id="obsolete-plugins">Obsolete Plugins</h2>
				<p>This section lists some other notable graphics plugins that are not supported, and suggests alternative plugins to use.</p>

				<h3 id="jabo">Jabo's Direct3D</h3>
				<p>An old HLE plugin that used to come bundled with Project64, but has been dropped from more recent versions in favour of GLideN64 and Glide64. It is the only plugin listed here that uses DirectX instead of OpenGL or Vulkan. Due to it being included with older versions of Project64, it was once a popular plugin for SM64 romhacks, and even now still remains fairly popular. However, due to it only being available on Windows and only in Project64, it is not supported by this site. Romhacks that targeted Jabo should instead select OGRE as the recommended plugin (or Rice if OGRE does not work), as it handles most of Jabo's quirks and innacuracies that older hacks might rely on, and it is available on all operating systems and most emulators.</p>

				<h3 id="gln">gln64</h3>
				<p>The predecessor to GLideN64. It is now completely obsolete.</p>

				<h3 id="arachnoid">Arachnoid</h3>
				<p>Arachnoid is an old HLE plugin that was once useful for some games, but is now obsolete. It was never targeted by romhack developers. If you did happen to test your hack on Arachnoid, you may want to try Glide64 instead, or simply consult the <a href="#choosing-a-plugin" class="ref">flowchart</a> above.</p>

				<h3 id="z64">Z64</h3>
				<p>Z64 is an old LLE plugin. It has been completely obsoleted by Angrylion and ParaLLEl.</p>

				<h2 id="plugin-options">Plugin Options</h2>
				<p>Some plugins allow you to specify additional options when submitting a hack. When a user downloads your hack in Parallel Launcher, the settings you provide will be set by default.</p>

				<h3 id="po-parallel">ParaLLEl</h3>
				<br>
				<h4 id="upscale-texrects">Upscale TEXRECTs</h4>
				<p>Allows upscaling of a certain type of drawing command commonly used for drawing HUD elements or prerendered backgrounds. This allows for these elements to have a higher quality when using higher resolutions, but can cause seams between textures in some cases. When rendering at native N64 resolution, this option has no effect.</p>

				<h4 id="gliden-fallback">Allow GLideN64 fallback for old GPUs</h4>
				<p>As mentioned previously, the ParaLLEl plugin requires the Vulkan graphics API. While this is supported by all modern graphics cards and drivers, some older cards may not support it or may suffer from poor performance when using it. This option indicates that GLideN64 is still accurate enough to play the hack if the user is unable to use ParaLLEl. When selecting this option, you will also be able to specify whether or not framebuffer emulation and/or correct decal emulation is required if using GLideN64. Users who have enabled the <i>Prefer HLE plugins</i> option in Parallel Launcher will have the GLideN64 plugin selected by default instead of ParaLLEl.</p>

				<h3 id="po-gliden64">GLideN64</h3>
				<br>
				<h4 id="fbe">Require framebuffer emulation</h4>
				<p>Allows reading and writing to the framebuffer - an area of memory holding the image to be displayed. This allows for things such as copying parts of the screen to a texture or applying post-processing effects.</p>

				<h4 id="dbe">Require correct decal emulation</h4>
				<p>Allows GLideN64 to correctly render shadows and decals (see <a href="#decals" class="ref">Shadows &amp; Decals</a>). Enabling this option also requires framebuffer emulation to be enabled.</p>

				<h4 id="widescreen">Supports widescreen viewport hack</h4>
				<p>GLideN64 supports a hack to make the viewport wider, allowing for widescreen aspect ratios without stretching the image. To accomodate this option, hacks that wish to support it must ensure that objects that would normally be offscreen, but are now still visible due to the widened viewport, are not culled.</p>

				<h3 id="po-ogre">OGRE</h3>
				<br>
				<h4 id="widescreen">Supports widescreen viewport hack</h4>
				<p>See the GLideN64 option with the same name above.</p>

				<h2 id="plugin-issues">Common Plugin Issues</h2>
				<p>This section describes some common issues you may run into with certain plugins.</p>

				<h3 id="decals">Shadows &amp; Decals</h3>
				<p>The N64 has the capability to conditionally render things only when they are at or near the same depth as something else. For example, if a shadow is placed on a ledge, only the part of the shadow that is on the ground will be rendered and not the part that would hang over the ledge. However, HLE plugins (with the exception of GLideN64 with the correct settings enabled) do not emulate this behaviour correctly, and instead always draw the shadows or decals, even when there is nothing behind or below them.</p>
				<table>
					<tbody>
						<tr>
							<th>
								<div>LLE (ParaLLEl)</div>
								<img src="/assets/img/decals-parallel.png">
							</th>
							<th>
								<div>HLE (Glide64)</div>
								<img src="/assets/img/decals-glide.png">
							</th>
						</tr>
					</tbody>
				</table>

				<h3 id="dithering">Alpha Dithering</h3>
				<p>Alpha dithering is a method of achieving a sort of transparency where instead of blending the pixel with what is behind it, it instead simply renders a fraction of the pixels in the object. This effect can also be combined with regular blended transparency as seen in the example below.</p>
				<ul>
					<li>ParaLLEl displays it correctly and at the upscaled resolution.</li>
					<li>GLideN64 displays it correctly, but only at the native N64 resolution, making it appear pixelated when displayed at higher resolutions</li>
					<li>Glide64 displays the effect, but it is always at 50% intensity even if it should be at 1% or 99% intensity</li>
					<li>Rice and Jabo do not display the effect at all</li>
				</ul>
				<span class="caption">If this image is not animated, your browser does not support animated PNGs.</span>
				<img src="/assets/img/alpha-dithering.png">

				<h3 id="overflow">Coordinate Overflow</h3>
				<p>The N64 uses fixed point values for its transformation matrices, meaning there is a limit to the range of values it can support, and overflowing that range will cause major graphical artifacts such as missing triangles, deformed models, and objects rendering in the wrong locations. What this means in practical terms is that things that are too far away from the origin of the map and/or the current camera position will not render correctly on N64 or on LLE plugins.</p>
				<p>The HackerSM64 decomp repo has a fix for this built in, and you can avoid this issue in Bowser's Blueprints by increasing the render scale; however, neither ROM Manager nor SM64 Editor have fixes for this issue yet, so if you want to make a level that is much larger than vanilla SM64 levels using either of these tools, you will be restricted to only supporting HLE plugins, and your hack will not be console compatible.</p>
				<table>
					<tbody>
						<tr>
							<th>
								<div>HLE</div>
								<img src="/assets/img/overflow-hle.png">
							</th>
							<th>
								<div>Unpatched Overflow on LLE</div>
								<img src="/assets/img/overflow-lle.png">
							</th>
						</tr>
					</tbody>
				</table>

				<h3 id="mop2">The MOP2 Patch</h3>
				<p>The MOP2 patch (More Objects Patch 2) contains an error that prevents the red and blue buttons from rendering; however the Rice, Jabo, and OGRE plugins end up rendering the button anyways. If you use these objects in your hack, you must recommend either the OGRE or Rice plugin (OGRE is recommended).</p>
				<table>
					<tbody>
						<tr>
							<th>
								<div>Rice / Jabo / OGRE</div>
								<img src="/assets/img/mop2-rice.png">
							</th>
							<th>
								<div>Everything Else</div>
								<img src="/assets/img/mop2-glide.png">
							</th>
						</tr>
					</tbody>
				</table>
			</div>
		`;
	}

}
