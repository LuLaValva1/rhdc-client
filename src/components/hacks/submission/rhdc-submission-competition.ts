import { css, CSSResultGroup, html, HTMLTemplateResult, PropertyValues } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';
import { RhdcSubmissionComponent } from './rhdc-submission-component';
import { HackSubmissionSession, HackSubmissionApi } from '../../../apis/hack-submission-api';
import { RhdcHackSubmissionUpdatedEvent } from '../../../events/rhdc-hack-submission-updated-event';
import { Competition } from '../../../apis/competitions-api';
import { Nullable, Uuid } from '../../../util/types';
import { getErrorMessage } from '../../../util/http-error-parser';
import SlRadioGroup from '@shoelace-style/shoelace/dist/components/radio-group/radio-group';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/radio-group/radio-group';
import '@shoelace-style/shoelace/dist/components/radio/radio';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/icon/icon';

@customElement( 'rhdc-submission-competition' )
export class RhdcSubmissionCompetition extends RhdcSubmissionComponent {

	@property({ attribute: false })
	submission!: HackSubmissionSession;

	@property({ attribute: false })
	competitions!: Competition[];

	@query( 'sl-radio-group' )
	radioGroup!: SlRadioGroup;

	@query( 'sl-input' )
	paypalInput!: SlInput;

	@state()
	competitionId: Nullable<Uuid> = null;

	@state()
	loading = false;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-large);
			}

			:host([hidden]) {
				display: none !important;
			}
		
			sl-button {
				align-self: flex-end;
			}

			sl-radio {
				margin: var(--sl-spacing-2x-small) 0;
			}

			sl-input {
				max-width: 400px;
			}

			sl-input[hidden] {
				display: none;
			}
		`;
	}

	override render() : unknown {
		const radios: HTMLTemplateResult[] = [];
		for( const competition of this.competitions ) {
			radios.push( html`<sl-radio value=${competition.competitionId as string} ?disabled=${this.loading}>${competition.name}</sl-radio>` );
		}
		radios.push( html`<sl-radio value="" ?disabled=${this.loading}>This is not a competition submission</sl-radio>` );

		const showEmailInput = !!this.competitionId && !!this.competitions && this.competitions.some(
			c => c.competitionId === this.competitionId && c.hasCashPrize
		);

		return html`
			<sl-radio-group
				label="Submit to Competition"
				value=${this.competitionId as string}
				required
				@sl-change=${this.#competitionSelected}
			>${radios}</sl-radio-group>

			<sl-input type="email"
				?hidden=${!showEmailInput}
				?disabled=${this.loading}
				.value=${this.submission.competition?.paypal || ''}
				label="PayPal E-Mail (Optional)"
				clearable
				minlength="5"
				pattern="email"
				autocomplete="email"
				enterkeyhint="next"
				inputmode="email"
			></sl-input>
			
			<sl-button variant="primary" ?loading=${this.loading} @click=${this.#goNext}>
				<sl-icon slot="suffix" name="chevron-right"></sl-icon>
				<span>Next</span>
			</sl-button>
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'submission' ) && this.submission ) {
			this.competitionId = this.submission.competition?.competitionId || null;
		}
	}

	protected override updated( changedProperties: PropertyValues ) : void {
		super.updated( changedProperties );
		if( this.competitions?.length && this.submission ) {
			this.#autoselectCompetition();
		}
	}

	#autoselectCompetition() : void {
		if( window.location.hash.length !== 37 ) return;
		const competitionId = window.location.hash.substring( 1 ) as Uuid;
		window.history.replaceState( null, '', window.location.origin + window.location.pathname + window.location.search );
		if( this.competitions.some( c => c.competitionId === competitionId ) ) {
			this.competitionId = competitionId;
		}
	}

	#competitionSelected() : void {
		this.competitionId = (this.radioGroup.value || null) as Nullable<Uuid>;
	}

	protected override async saveAsync(): Promise<void> {
		this.loading = true;
		try {
			this.submission = this.submission.withCompetition(
				await HackSubmissionApi.updateCompetitionAsync(
					this.competitionId,
					this.competitionId ? this.paypalInput.value : null
				)
			);

			this.dispatchEvent( RhdcHackSubmissionUpdatedEvent.create( this.submission ) );
		} finally {
			this.loading = false;
		}
	}

	async #goNext() : Promise<void> {
		try {
			await this.saveAsync();
			this.dispatchEvent( new CustomEvent( 'rhdc-submission-next-event' ) );
		} catch( exception: unknown ) {
			this.toastError( 'Submission Error', getErrorMessage( exception ) );
		}
	}

}
