import { RhdcElement } from '../../../rhdc-element';

export class RhdcSubmissionComponent extends RhdcElement {

	protected async saveAsync() : Promise<void> {
		/* override */
	}

	public async trySaveAsync() : Promise<boolean> {
		try {
			await this.saveAsync();
			return true;
		} catch{
			return false;
		}
	}

}
