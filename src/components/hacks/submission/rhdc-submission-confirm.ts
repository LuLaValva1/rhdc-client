import { css, CSSResultGroup, html, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { live } from 'lit/directives/live.js';
import { rhdcUserContext, UserAuth } from '../../../decorators/user-context';
import { RhdcSubmissionComponent } from './rhdc-submission-component';
import { HackSubmissionApi, HackSubmissionSession } from '../../../apis/hack-submission-api';
import { getErrorMessage } from '../../../util/http-error-parser';
import { Nullable } from '../../../util/types';
import { Role } from '../../../auth/roles';
import SlCheckbox from '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/icon/icon';

@customElement( 'rhdc-submission-confirm' )
export class RhdcSubmissionConfirm extends RhdcSubmissionComponent {

	@property({ attribute: false })
	submission!: HackSubmissionSession;

	@state()
	confirms = [ false, false, false ];

	@state()
	submitting = false;

	@rhdcUserContext()
	currentUser!: Nullable<UserAuth>;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: flex-start;
				gap: var(--sl-spacing-small);
			}

			:host([hidden]) {
				display: none !important;
			}

			sl-checkbox {
				display: block;
			}

			sl-checkbox::part(base) {
				color: var(--rhdc-text-color);
			}

			.button-tray {
				display: flex;
				align-self: stretch;
			}

			.spacer {
				flex-grow: 1;
			}

			h2 {
				font-size: var(--sl-font-size-x-large);
				margin: var(--sl-spacing-small) 0 0 0;
				padding: 0;
			}

			ul {
				margin: 0;
			}

			li {
				margin-top: var(--sl-spacing-2x-small);
			}
		`;
	}

	override render() : unknown {
		const isAuthor = this.currentUser && this.submission.metadata?.authors?.includes( this.currentUser.username );
		const isSoleAuthor = isAuthor && this.submission.metadata!.authors!.length === 1;

		let authorText: string;
		if( isSoleAuthor ) {
			authorText = 'I am the sole author of this hack. (If this is not true, go back to the Metadata step and edit the authors)';
		} else if( isAuthor ) {
			authorText = 'I am one of the authors of this hack. (If this is not true, go back to the Metadata step and edit the authors)';
		} else {
			authorText = 'The author of this hack has given me permission to upload it here.';
		}

		return html`
			<div>Thank you for your submission. Please confirm that you have followed the submission rules below:</div>
			<sl-checkbox
				id="cb0"
				?disabled=${this.submitting}
				?checked=${live( this.confirms[0] )}
				@sl-change=${this.#checkboxClicked.bind( this, 0 )}
			>
				${authorText}
			</sl-checkbox>
			<sl-checkbox
				id="cb1"
				?disabled=${this.submitting}
				?checked=${live( this.confirms[1] )}
				@sl-change=${this.#checkboxClicked.bind( this, 1 )}
			>
				I have tested my hack in Parallel Launcher using the graphics plugin that I have recommended
				(${this.submission.emulation?.plugin}).
			</sl-checkbox>
			<sl-checkbox
				id="cb2"
				?disabled=${this.submitting}
				?checked=${live( this.confirms[2] )}
				@sl-change=${this.#checkboxClicked.bind( this, 2 )}
			>
				My hack follows the Code of Conduct.
			</sl-checkbox>
			<div>
				<h2>Code of Conduct</h2>
				<ul>
					<li>Absolutely no hate speech of any kind</li>
					<li>No jokes about rape or suicide</li>
					<li>No textures with pornographic content or shock images</li>
				</ul>
			</div>
			<div class="button-tray">
				<sl-button variant="primary" ?disabled=${this.submitting} @click=${this.#goPrev}>
					<sl-icon slot="prefix" name="chevron-left"></sl-icon>
					<span>Back</span>
				</sl-button>
				<div class="spacer"></div>
				<sl-button variant="primary" ?disabled=${this.confirms.includes( false )} ?loading=${this.submitting} @click=${this.#submit}>
					<sl-icon slot="suffix" name="chevron-right"></sl-icon>
					<span>Submit</span>
				</sl-button>
			</div>
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		if( changedProperties.has( 'submission' ) ) {
			this.confirms = [ false, false, false ];
		}
	}

	#checkboxClicked( index: number ) : void {
		const checkbox = this.shadowRoot?.getElementById( `cb${index}` ) as Nullable<SlCheckbox>;
		if( !checkbox ) return;
		this.confirms[index] = checkbox.checked;
		this.confirms = [ ...this.confirms ];
	}

	#goPrev() : void {
		this.dispatchEvent( new CustomEvent( 'rhdc-submission-prev-event' ) );
	}

	async #submit() : Promise<void> {
		this.submitting = true;
		try {
			const hackLinks = await HackSubmissionApi.submitAsync();
			if( this.submission.competition ) {
				this.toastSuccess( 'Hack Submitted', 'Your competition submission has been processed. You may still make changes to your hack up until the competition deadline, at which point it will become locked until the competition ends.' );
			} else if(
				this.currentUser?.role === Role.TrustedUser ||
				this.currentUser?.role === Role.Moderator ||
				this.currentUser?.role === Role.Staff
			) {
				this.toastSuccess( 'Hack Accepted', 'Your hack has been accepted.' );
			} else {
				this.toastSuccess( 'Hack Submitted', 'Your hack has been submitted. Please allow a few days for moderators to approve your submission.' );
			}
			this.navigate( hackLinks.webHref );
		} catch( exception: unknown ) {
			this.toastError( 'Submission Error', getErrorMessage( exception ) );
		} finally {
			this.submitting = false;
		}
	}

}
