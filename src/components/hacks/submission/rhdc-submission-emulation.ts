import { css, CSSResultGroup, html, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { RhdcSubmissionComponent } from './rhdc-submission-component';
import { query } from 'lit/decorators.js';
import { HackSubmissionApi, HackSubmissionSession } from '../../../apis/hack-submission-api';
import { getErrorMessage } from '../../../util/http-error-parser';
import { RhdcHackSubmissionUpdatedEvent } from '../../../events/rhdc-hack-submission-updated-event';
import { ConsoleCompatibility, GfxPlugin, GfxPluginFlag, HackFlag, HackSaveType, HackVersionUpdateDto } from '../../../apis/hacks-api';
import { Nullable } from '../../../util/types';
import { RhdcPluginEditor } from '../rhdc-plugin-editor';
import SlSelect from '@shoelace-style/shoelace/dist/components/select/select';
import '@shoelace-style/shoelace/dist/components/button/button';
import '../rhdc-plugin-editor';

@customElement( 'rhdc-submission-emulation' )
export class RhdcSubmissionEmulation extends RhdcSubmissionComponent {

	@property({ attribute: false })
	submission!: HackSubmissionSession;

	@state()
	defaultSettings: HackVersionUpdateDto = {};

	@state()
	loading = false;

	@query( 'rhdc-plugin-editor' )
	pluginEditor!: RhdcPluginEditor;

	static override get styles() : CSSResultGroup {
		return css`
			:host([hidden]) {
				display: none !important;
			}

			:host {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-small);
			}

			.button-tray {
				display: flex;
				align-self: stretch;
			}

			.spacer {
				flex-grow: 1;
			}
		`;
	}

	override render() : unknown {
		return html`
			<rhdc-plugin-editor
				.defaultValue=${this.defaultSettings}
				.saveType=${this.submission?.hackFile?.saveType || null}
				?loading=${this.loading}
			></rhdc-plugin-editor>

			<div class="button-tray">
				<sl-button variant="primary" ?disabled=${this.loading} @click=${this.#goPrev}>
					<sl-icon slot="prefix" name="chevron-left"></sl-icon>
					<span>Back</span>
				</sl-button>
				<div class="spacer"></div>
				<sl-button variant="primary" ?loading=${this.loading} @click=${this.#goNext}>
					<sl-icon slot="suffix" name="chevron-right"></sl-icon>
					<span>Next</span>
				</sl-button>
			</div>
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'submission' ) ) {
			const initialPlugin = this.submission?.emulation?.plugin || this.submission.hackFile?.pluginGuess || undefined;
			const initialPluginFlags = this.submission?.emulation?.pluginFlags || ((initialPlugin === GfxPlugin.ParaLLEl) ? [ GfxPluginFlag.UpscaleTexrects ] : []);
			const initialHackFlags = this.submission?.emulation?.hackFlags || ((this.submission?.hackFile?.saveType === HackSaveType.Eeprom16k) ? [ HackFlag.BigEEPROM ] : []);

			this.defaultSettings = {
				plugin: initialPlugin,
				pluginFlags: [...initialPluginFlags],
				hackFlags: [...initialHackFlags],
				consoleCompatibility: this.submission.emulation?.consoleCompatibility || (this.submission.hackFile?.pluginGuess ? ConsoleCompatibility.None : null)
			};
		}
	}

	protected override async saveAsync(): Promise<void> {
		const settings = this.pluginEditor.value;

		if( !settings ) {
			const pluginSelect = this.pluginEditor.shadowRoot?.getElementById( 'gfx-plugin' ) as Nullable<SlSelect>;
			pluginSelect?.reportValidity();
			pluginSelect?.scrollIntoView();
			return;
		}
		
		const emulationInfo = await HackSubmissionApi.updateEmulationInfoAsync(
			settings.plugin,
			settings.pluginFlags,
			settings.hackFlags,
			settings.consoleCompatibility
		);

		this.submission = this.submission.withEmulationInfo( emulationInfo );
		this.dispatchEvent( RhdcHackSubmissionUpdatedEvent.create( this.submission ) );
	}

	async #goNext() : Promise<void> {
		try {
			await this.saveAsync();
			this.dispatchEvent( new CustomEvent( 'rhdc-submission-next-event' ) );
		} catch( exception: unknown ) {
			this.toastError( 'Submission Error', getErrorMessage( exception ) );
		}
	}

	async #goPrev() : Promise<void> {
		await this.trySaveAsync();
		this.dispatchEvent( new CustomEvent( 'rhdc-submission-prev-event' ) );
	}

}
