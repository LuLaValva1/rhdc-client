import { css, CSSResultGroup, html, HTMLTemplateResult, PropertyValues } from 'lit';
import { customElement, property, state, query } from 'lit/decorators.js';
import { RhdcSubmissionComponent } from './rhdc-submission-component';
import { guard } from 'lit/directives/guard.js';
import { HackSubmissionApi, HackSubmissionSession } from '../../../apis/hack-submission-api';
import { RhdcHackSubmissionUpdatedEvent } from '../../../events/rhdc-hack-submission-updated-event';
import { getErrorMessage } from '../../../util/http-error-parser';
import { RhdcLayoutBuilder } from '../../common/rhdc-layout-builder';
import { RhdcFileDropEvent } from '../../../events/rhdc-file-drop-event';
import { AlertFactory } from '../../../util/alert-factory';
import { BasicStarLayout, StarLayout } from '../../../util/star-layout';
import { ConstRef, FileLike, Nullable } from '../../../util/types';
import SlRadioGroup from '@shoelace-style/shoelace/dist/components/radio-group/radio-group';
import '../../../widgets/rhdc-file-dropzone';
import '../../common/rhdc-layout-builder';
import '../../common/rhdc-layout-preview';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/radio/radio';
import '@shoelace-style/shoelace/dist/components/radio-group/radio-group';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/tab/tab';
import '@shoelace-style/shoelace/dist/components/tab-group/tab-group';
import '@shoelace-style/shoelace/dist/components/tab-panel/tab-panel';

const ADVANCED_SCHEMA = 'https://parallel-launcher.ca/layout/advanced-01/schema.json';
const BASIC_SCHEMA = 'https://parallel-launcher.ca/layout/basic-01/schema.json';

enum LayoutType {
	None = '',
	Upload = 'upload',
	Builder = 'builder'
}

@customElement( 'rhdc-submission-layout' )
export class RhdcSubmissionLayout extends RhdcSubmissionComponent {

	@property({ attribute: false })
	submission!: HackSubmissionSession;

	@state()
	uploading = false;

	@state()
	layoutType = LayoutType.None;

	@query( 'sl-radio-group' )
	radioGroup!: SlRadioGroup;

	@query( 'rhdc-layout-builder' )
	layoutBuilder!: Nullable<RhdcLayoutBuilder>;

	@state()
	transientLayout: Nullable<StarLayout> = null;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-x-small);
			}

			:host([hidden]) {
				display: none !important;
			}

			h2, p {
				margin: 0;
				padding: 0;
			}

			.layout-upload {
				display: flex;
				gap: var(--sl-spacing-small);
			}

			rhdc-file-dropzone {
				flex-shrink: 1;
				width: calc( 600px - var(--sl-spacing-small) );
				max-width: calc( 50vw - var(--sl-spacing-small) );
				min-width: 150px;
				min-height: 250px;
			}

			rhdc-layout-preview {
				width: min-content;
			}

			sl-tab-panel:last-of-type {
				padding-left: var(--sl-spacing-2x-small);
			}

			.button-tray {
				align-self: stretch;
				display: flex;
			}

			.spacer {
				flex-grow: 1;
			}
		`;
	}

	override render() : unknown {
		let layoutEdit: Nullable<HTMLTemplateResult> = null;
		switch( this.layoutType ) {
			case LayoutType.Upload: {
				let layoutPreview: Nullable<HTMLTemplateResult> = null;
				if( this.transientLayout && this.transientLayout.groups.length > 0 ) {
					layoutPreview = html`<rhdc-layout-preview .starLayout=${this.transientLayout}></rhdc-layout-preview>`;
				}

				layoutEdit = html`
					<p>
						Advanced layouts allow you to upload more complex layouts for hacks with custom save file formats.
						You can view the documentation on how to create and test and advanced layouts
						<a href="https://parallel-launcher.ca/layout/docs" rel="help" target="_blank">here</a>.
						<br/>
						If your hack uses the regular vanilla SM64 save format, then you probably want to create a basic
						layout instead, and should select the <i>Create Layout Now</i> option.
						<br/>
						Alternatively, you can upload star layouts from the legacy JSML format, and they will automatically
						be converted to the new format.
					</p>
					<div class ="layout-upload"></div>
						<rhdc-file-dropzone
							?disabled=${this.uploading}
							accept=".json,.jsml,application/json"
							@rhdc-file-drop-event=${this.#onLayoutUpload}
						>Upload Layout File</rhdc-file-dropzone>
						<h2>Preview</h2>
						${layoutPreview}
					</div>
				`;
				break;
			}
			case LayoutType.Builder: {
				let builder : HTMLTemplateResult;
				if( this.submission.starLayout?.layout?.$schema === BASIC_SCHEMA ) {
					builder = html`<rhdc-layout-builder .value=${this.submission.starLayout.layout} @rhdc-layout-changed=${this.#onLayoutBuilderChange}></rhdc-layout-builder>`;
				} else {
					builder = html`<rhdc-layout-builder @rhdc-layout-changed=${this.#onLayoutBuilderChange}></rhdc-layout-builder>`;
				}

				let preview: Nullable<HTMLTemplateResult> = null;
				if( this.transientLayout && this.transientLayout.groups.length > 0 ) {
					preview = html`<rhdc-layout-preview .starLayout=${this.transientLayout}></rhdc-layout-preview>`;
				}

				layoutEdit = html`
					<sl-tab-group>
						<sl-tab slot="nav" panel="edit">Edit</sl-tab>
						<sl-tab slot="nav" panel="preview" ?disabled=${!preview}>Preview</sl-tab>

						<sl-tab-panel name="edit">${guard( [ this.submission ], () => builder )}</sl-tab-panel>
						<sl-tab-panel name="preview">${preview}</sl-tab-panel>
					</sl-tab-group>
				`;
				break;
			}
			case LayoutType.None:
				if( this.submission.metadata?.stars && this.submission.metadata.stars > 1 )  {
					layoutEdit = AlertFactory.create({
						type: 'warning',
						title: 'Missing Star Layout',
						message: 'Your hack contains multiple stars, but you have not yet submitted a star layout. A star layout is not required to submit your hack, but it is recommended.'
					});
				}
				break;
		}

		return html`
			<h2>Star Layout</h2>
			<p>
				Star Layout files allow players to view their progress while playing your hack in Parallel Launcher.
				You may either upload a star layout file you have already created, or you can create one right now.
				Creating a star layout is not required; however, it is strongly recommended that you create one if
				your hack contains multiple stars in order to provide the best user experience.
			</p>
			<sl-radio-group value=${this.layoutType} required @sl-change=${this.#layoutTypeChanged}>
				<sl-radio value=${LayoutType.None} ?disabled=${this.uploading}>No Star Layout</sl-radio>
				<sl-radio value=${LayoutType.Upload} ?disabled=${this.uploading}>Upload Layout File</sl-radio>
				<sl-radio value=${LayoutType.Builder} ?disabled=${this.uploading}>Create Layout Now</sl-radio>
			</sl-radio-group>
			${layoutEdit}
			<div class="button-tray">
				<sl-button variant="primary" ?loading=${this.uploading} @click=${this.#goPrev}>
					<sl-icon slot="prefix" name="chevron-left"></sl-icon>
					<span>Back</span>
				</sl-button>
				<div class="spacer"></div>
				<sl-button variant="primary" ?loading=${this.uploading} @click=${this.#goNext}>
					<sl-icon slot="suffix" name="chevron-right"></sl-icon>
					<span>Next</span>
				</sl-button>
			</div>
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		if( changedProperties.has( 'submission' ) ) {
			this.transientLayout = this.submission.starLayout?.layout || null;
			switch( this.submission.starLayout?.layout?.$schema ) {
				case ADVANCED_SCHEMA:
					this.layoutType = LayoutType.Upload;
					break;
				case BASIC_SCHEMA:
					if( this.layoutType === LayoutType.None ) {
						this.layoutType = LayoutType.Builder;
					}
					break;
			}
		}
	}

	#onLayoutBuilderChange() : void {
		this.transientLayout = this.layoutBuilder?.value || null;
	}

	#onLayoutUpload( event: RhdcFileDropEvent ) : Promise<void> {
		return this.#safeUploadLayoutAsync( event.detail.file );
	}

	async #layoutTypeChanged() : Promise<void> {
		const prevType = this.layoutType;
		this.layoutType = this.radioGroup.value as LayoutType;

		if( this.layoutType === LayoutType.None && this.submission.starLayout?.layout ) {
			await this.#safeRemoveLayoutAsync();
		} else if( this.layoutType === LayoutType.Upload && prevType === LayoutType.Builder ) {
			const layoutFile = this.#toLayoutFile( this.layoutBuilder!.value );
			await this.#safeUploadLayoutAsync( layoutFile );
		} else if( this.layoutType === LayoutType.Builder && prevType === LayoutType.Upload ) {
			if( this.submission.starLayout?.layout?.$schema === BASIC_SCHEMA ) return;
			await this.#safeRemoveLayoutAsync();
		}
	}

	async #safeUploadLayoutAsync( layoutFile: FileLike ) : Promise<void> {
		this.uploading = true;
		try {
			const starLayoutInfo = await HackSubmissionApi.uploadStarLayoutAsync( layoutFile );
			this.submission = this.submission.withStarLayout( starLayoutInfo );
			this.transientLayout = starLayoutInfo.layout;
			this.dispatchEvent( RhdcHackSubmissionUpdatedEvent.create( this.submission ) );
		} catch( exception: unknown ) {
			this.toastError( 'Star Layout Rejected', getErrorMessage( exception ) );
		} finally {
			this.uploading = false;
		}
	}

	async #safeRemoveLayoutAsync() : Promise<void> {
		this.uploading = true;
		try {
			await HackSubmissionApi.removeStarLayoutAsync();
			this.toastInfo( 'Layout removed' );
			this.submission = this.submission.withStarLayout( null );
			this.transientLayout = null;
			this.dispatchEvent( RhdcHackSubmissionUpdatedEvent.create( this.submission ) );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to remove layout', getErrorMessage( exception ) );
		} finally {
			this.uploading = false;
		}
	}

	public override async saveAsync() : Promise<void> {
		if( this.layoutType === LayoutType.None && this.submission.starLayout?.layout ) {
			await HackSubmissionApi.removeStarLayoutAsync();
			this.submission = this.submission.withStarLayout( null );
			this.transientLayout = null;
			this.dispatchEvent( RhdcHackSubmissionUpdatedEvent.create( this.submission ) );
		} else if( this.layoutType === LayoutType.Builder ) {
			const layoutFile = this.#toLayoutFile( this.layoutBuilder!.value );
			const layoutInfo = await HackSubmissionApi.uploadStarLayoutAsync( layoutFile );
			this.submission = this.submission.withStarLayout( layoutInfo );
			this.transientLayout = layoutInfo.layout;
			this.dispatchEvent( RhdcHackSubmissionUpdatedEvent.create( this.submission ) );
		}
	}

	async #goPrev() : Promise<void> {
		await this.trySaveAsync();
		this.dispatchEvent( new CustomEvent( 'rhdc-submission-prev-event' ) );
	}

	async #goNext() : Promise<void> {
		try {
			await this.saveAsync();
			if(
				this.submission.metadata &&
				this.submission.starLayout &&
				this.submission.metadata.stars !== this.submission.starLayout.stars &&
				!await this.confirmAsync(
					'Star Count Mismatch',
					`The number of stars in the layout (${this.submission.starLayout.stars}) does not match the number of stars provided in a previous step (${this.submission.metadata.stars}). Automatic star progress submissions will not work correctly. Are you sure you want to continue?`,
					'Continue',
					'Cancel'
				)
			) return;
			this.dispatchEvent( new CustomEvent( 'rhdc-submission-next-event' ) );
		} catch( exception: unknown ) {
			this.toastError( 'Star Layout Error', getErrorMessage( exception ) );
		}
	}

	#toLayoutFile( layout: ConstRef<BasicStarLayout> ) : FileLike {
		const file = new Blob( [ JSON.stringify( layout ) ], { type: 'application/json' } ) as FileLike;
		file.name = 'layout.json';
		return file;
	}

}
