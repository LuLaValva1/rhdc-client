import { css, CSSResultGroup, html } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { RhdcSubmissionComponent } from './rhdc-submission-component';
import { HackSubmissionApi, HackSubmissionSession } from '../../../apis/hack-submission-api';
import { HackControllerSupport, HackSaveType } from '../../../apis/hacks-api';
import { RhdcFileDropEvent } from '../../../events/rhdc-file-drop-event';
import { RhdcFileDropzone } from '../../../widgets/rhdc-file-dropzone';
import { getErrorMessage } from '../../../util/http-error-parser';
import { UploadProgress } from '../../../apis/http-client';
import { RhdcHackSubmissionUpdatedEvent } from '../../../events/rhdc-hack-submission-updated-event';
import { formatBytes } from '../../../util/formatting';
import { Nullable } from '../../../util/types';
import '../../../widgets/rhdc-file-dropzone';
import '@shoelace-style/shoelace/dist/components/progress-bar/progress-bar';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/alert/alert';
import '@shoelace-style/shoelace/dist/components/icon/icon';

const makeRow = function( name: string, value: string ) : unknown {
	return html`
		<tr>
			<td>${name}</td>
			<td>${value}</td>
		</tr>
	`;
};

const formatSupport = function( controllerSupport: HackControllerSupport ) {
	switch( controllerSupport ) {
		case HackControllerSupport.Required:
			return 'Yes (Required)';
		case HackControllerSupport.Supported:
			return 'Yes';
		default:
			return 'No';
	}
};

const formatSaveType = function( saveType: Nullable<HackSaveType> ) {
	if( !saveType ) return 'Unknown';
	switch( saveType ) {
		case HackSaveType.None: return 'None (or MemPak only)';
		case HackSaveType.Eeprom4k: return 'EEPROM 4kb';
		case HackSaveType.Eeprom16k: return 'EEPROM 16 kb';
		case HackSaveType.Sram: return 'SRAM (256 kb)';
		case HackSaveType.FlashRam: return 'Flash RAM';
		default: return 'Unknown';
	}
}

@customElement( 'rhdc-submission-patch' )
export class RhdcSubmissionPatch extends RhdcSubmissionComponent {

	@property({ attribute: false })
	submission!: HackSubmissionSession;

	@property({ attribute: 'allow-prev', type: Boolean })
	showPrevButton = false;

	@state()
	error = '';

	@state()
	uploadProgress = 0;

	@state()
	loading = false;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-small);
				width: 100%;
			}

			:host([hidden]) {
				display: none !important;
			}

			.loading-bar {
				display: flex;
				gap: var(--sl-spacing-small);
				font-family: var(--sl-font-sans);
				color: var(--rhdc-text-color);
			}

			.loading-bar > span {
				flex-grow: 0;
				flex-shrink: 0;
			}

			.loading-bar > sl-progress-bar {
				flex-grow: 1;
				flex-shrink: 1;
			}

			.body {
				display: flex;
				flex-direction: row;
				flex-wrap: wrap-reverse;
				align-items: flex-start;
				gap: var(--sl-spacing-x-small) var(--sl-spacing-large);
				min-height: 600px;
			}

			.body > div, .body > rhdc-file-dropzone {
				flex-grow: 1;
				min-width: min( 100%, 400px );
				align-self: stretch;
			}

			.dropzone-text {
				max-width: 75%;
			}

			th:last-child {
				width: 99%;
			}

			table {
				border-spacing: var(--sl-spacing-small) 0;
			}

			td {
				color: var(--sl-color-neutral-600);
				font-weight: normal;
				text-align: left;
			}

			rhdc-file-dropzone {
				box-sizing: border-box;
				min-height: 300px;
				max-height: 600px;
			}

			sl-alert {
				margin-bottom: var(--sl-spacing-small);
			}

			.button-tray {
				display: flex;
			}

			.spacer {
				flex-grow: 1;
			}
		`;
	}

	override render() : unknown {
		let loadingBar: unknown = null;
		if( this.loading ) {
			if( this.uploadProgress < 100 ) {
				loadingBar = html`
					<div class="loading-bar">
						<span>Uploading Patch</span>
						<sl-progress-bar value="${this.uploadProgress}"></sl-progress-bar>
					</div>
				`;
			} else {
				loadingBar = html`
					<div class="loading-bar">
						<span>Validating Patch</span>
						<sl-progress-bar indeterminate></sl-progress-bar>
					</div>
				`;
			}
		}

		let alert: unknown = null;
		if( this.submission.hackFile ) {
			alert = html`
				<sl-alert variant="success" open>
					<sl-icon slot="icon" name="check2-circle"></sl-icon>
					<strong>Patch File Accepted</strong><br/>
					<span>Click the <i>Next</i> button to continue.</span>
				</sl-alert>
			`;
		} else if( this.error ) {
			alert = html`
				<sl-alert variant="danger" open>
					<sl-icon slot="icon" name="exclamation-octagon"></sl-icon>
					<strong>Patch File Rejected</strong><br/>
					<span>${this.error}</span>
				</sl-alert>
			`;
		}

		let rightSide;
		if( this.submission.hackFile ) {
			let warning: unknown = null;
			if( !this.submission.hackFile.internalRomNameGood ) {
				warning = html`
					<sl-alert variant="warning" open>
						<sl-icon slot="icon" name="exclamation-triangle"></sl-icon>
						<strong>Warning</strong><br/>
						<span>Your ROM's internal name is not encoded properly. It should be in Shift-JIS format.</span>
					</sl-alert>
				`;
			}

			rightSide = html`
				${warning}
				<table>
					<tbody>
						${makeRow( 'Patch File', this.submission.hackFile.patchFilename )}
						${makeRow( 'Internal Name', this.submission.hackFile.internalRomName )}
						${makeRow( 'Sha1 Checksum', this.submission.hackFile.hackSha1 )}
						${makeRow( 'Hack File Size', formatBytes( this.submission.hackFile.hackFileSize ) )}
						${makeRow( 'Mouse Support', formatSupport( this.submission.hackFile.mouseSupport ) )}
						${makeRow( 'GCC Support', formatSupport( this.submission.hackFile.gamecubeControllerSupport ) )}
						${makeRow( 'Save Type', formatSaveType( this.submission.hackFile.saveType ) )}
					</tbody>
				</table>
			`;
		} else {
			rightSide = html`
				<strong>Upload a Patch File</strong>
				<p>
					You may upload either a .bps patch file or a .zip file containing a patch file.
					Romhacking.com does <strong>not</strong> accept full .z64 ROM files;
					instead you must upload your hack as a patch file which you can generate using
					<a href="https://www.smwcentral.net/?p=section&a=details&id=11474">Flips</a>.
				</p>
			`;
		}

		let prevButton: unknown = null;
		if( this.showPrevButton ) {
			prevButton = html`
				<sl-button
					variant="primary"
					?disabled=${this.loading}
					@click=${this.#goPrev}
				>
					<sl-icon slot="prefix" name="chevron-left"></sl-icon>
					<span>Back</span>
				</sl-button>
			`;
		}

		return html`
			${loadingBar}
			<div class="body">
				<rhdc-file-dropzone
					accept=".bps,.zip,application/x-bps-patch,application/zip"
					?disabled=${this.loading}
					@rhdc-file-drop-event=${this.#patchUploaded}
				>
					<p class="dropzone-text">Upload a file by dragging it here or clicking here</p>
				</rhdc-file-dropzone>
				<div>
					${alert}
					${rightSide}
				</div>
			</div>
			<div class="button-tray">
				${prevButton}
				<div class="spacer"></div>
				<sl-button
					variant="primary"
					?disabled=${this.loading || !this.submission.hackFile}
					@click=${this.#goNext}
				>
					<sl-icon slot="suffix" name="chevron-right"></sl-icon>
					<span>Next</span>
				</sl-button>
			</div>
		`;
	}

	#onUploadProgress( progress: UploadProgress ) : void {
		this.uploadProgress = progress.percentage;
	}

	async #patchUploaded( event: RhdcFileDropEvent ) : Promise<void> {
		if( event.detail.file.size > 300 * 1024 * 1024 ) {
			this.#reject( event, 'Patch file is too large. The maximum acceptable file size is 256 MiB.' );
		}

		if(
			!event.detail.file.name.toLowerCase().endsWith( '.bps' ) &&
			!event.detail.file.name.toLowerCase().endsWith( '.zip' )
		) {
			this.#reject( event, 'Upload must be a .bps or .zip file.' );
		}

		try {
			this.loading = true;
			this.uploadProgress = 0;

			const hackFileInfo = await HackSubmissionApi.uploadPatchAsync( event.detail.file, this.#onUploadProgress.bind( this ) );
			this.submission = this.submission.withHackFile( hackFileInfo );
			this.dispatchEvent( RhdcHackSubmissionUpdatedEvent.create( this.submission ) );
		} catch( exception: unknown ) {
			this.#reject( event, getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}

	}

	#reject( event: RhdcFileDropEvent, message: string ) : void {
		(event.target as RhdcFileDropzone).value = null;
		if( this.submission.hackFile ) {
			this.submission = this.submission.withHackFile( null );
			this.dispatchEvent( RhdcHackSubmissionUpdatedEvent.create( this.submission ) );
		}

		this.error = message;
	}

	#goPrev() : void {
		this.dispatchEvent( new CustomEvent( 'rhdc-submission-prev-event' ) );
	}

	#goNext() : void {
		this.dispatchEvent( new CustomEvent( 'rhdc-submission-next-event' ) );
	}

}
