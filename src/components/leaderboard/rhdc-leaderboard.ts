import { css, CSSResultGroup, html, HTMLTemplateResult, nothing, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { Nullable } from '../../util/types';
import { LeaderboardApi, LeaderboardEntry } from '../../apis/leaderboard-api';
import { rhdcUserContext, UserAuth } from '../../decorators/user-context';
import { Role } from '../../auth/roles';
import { rhdcAsyncAppend } from '../../util/lit-extensions';
import { AsyncPipe } from '../../util/async-pipe';
import { Page } from '../../apis/pagination';
import { AlertFactory } from '../../util/alert-factory';
import { getErrorMessage } from '../../util/http-error-parser';
import { getOrdinalSuffix } from '../../util/formatting';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '../common/rhdc-username';
import '../../widgets/rhdc-load-more';

interface LeaderboardContext {
	index: number;
	rank: number;
	prevStarpower: number;
}

@customElement( 'rhdc-leaderboard' )
export class RhdcLeaderboard extends RhdcElement {

	@property({ attribute: 'kaizo', type: Boolean })
	kaizo = false;

	@state()
	leaderboard = new AsyncPipe<LeaderboardEntry>();

	@state()
	loading = false;

	@state()
	loadingRank = false;

	@state()
	done = false;

	@state()
	rank: Nullable<number> = null;

	@rhdcUserContext()
	currentUser!: Nullable<UserAuth>;

	#currentPage: Nullable<Page<LeaderboardEntry>> = null;
	#context: LeaderboardContext = { index: 0, rank: 1, prevStarpower: Number.MAX_SAFE_INTEGER };

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-2x-small);
			}

			td:last-child {
				width: 99%;
			}

			sl-icon {
				color: var(--sl-color-amber-500);
				vertical-align: top;
				margin-top: -1px;
			}

			sl-icon[data-kaizo] {
				color: var(--sl-color-rose-600);
			}

			h3 {
				padding: 0;
			}

			table, h3 {
				font-size: 1.5rem;
			}

			th {
				font-weight: bold;
				text-align: start;
			}

			td, th {
				padding: var(--sl-spacing-2x-small) var(--sl-spacing-small);
			}

			sl-spinner, h3 {
				margin: var(--sl-spacing-small) 0;
			}
		`;
	}

	override render() : unknown {
		let loadTrigger: unknown = nothing;
		if( !this.done ) {
			loadTrigger = html`
				<rhdc-load-more
					?loading=${this.loading}
					@rhdc-load-more=${this.#loadMore}
				></rhdc-load-more>
			`;
		}

		let alertOrRank: unknown = nothing;
		if( this.loadingRank ) {
			alertOrRank = html`<sl-spinner></sl-spinner>`;
		} else if( this.rank ) {
			alertOrRank = html`<h3>Your rank: ${this.rank}${getOrdinalSuffix( this.rank )}</h3>`;
		} else switch( this.currentUser?.role ) {
			case Role.Banned:
				alertOrRank = AlertFactory.create({
					type: 'danger',
					message: 'You do not appear in the leaderboard because you are banned.'
				});
				break;
			case Role.Unverified:
				alertOrRank = AlertFactory.create({
					type: 'primary',
					message: 'You do not appear in the leaderboard because you have not yet verified your account.'
				});
				break;
			case Role.Restricted:
				alertOrRank = AlertFactory.create({
					type: 'warning',
					message: 'You do not appear in the leaderboard because your account is restricted.'
				});
				break;
			default: break;
		}

		return html`
			${alertOrRank}
			<table>
				<thead>
					<tr>
						<th>Rank</th>
						<th>User</th>
						<th>${this.kaizo ? 'Kaizo Star Points' : 'Star Points'}</th>
					</tr>
				</thead>
				<tbody>
					${rhdcAsyncAppend( this.leaderboard, this.#renderLeaderboardEntry.bind( this ) )}
				</tbody>
			</table>
			${loadTrigger}
		`;
	}

	#renderLeaderboardEntry( entry: LeaderboardEntry ) : HTMLTemplateResult {
		this.#context.index++;
		const starpower = this.kaizo ? entry.kaizoStarPoints : entry.starPoints;
		if( this.#context.prevStarpower !== starpower ) {
			this.#context.rank = this.#context.index;
			this.#context.prevStarpower = starpower;
		}

		return html`
			<tr>
				<td><b>${this.#context.rank}${getOrdinalSuffix( this.#context.rank )}</b></td>
				<td><rhdc-username .user=${entry.user}></rhdc-username></td>
				<td>
					<div>
						<span>${starpower.toString()}</span>
						<sl-icon name="star-fill" ?data-kaizo=${this.kaizo}></sl-icon>
					</div>
				</td>
			</tr>
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'kaizo' ) ) {
			this.#initLeaderboard();
			this.#getRank();
		} else if( changedProperties.has( 'currentUser' ) ) {
			this.#getRank();
		}
	}

	async #initLeaderboard() : Promise<void> {
		this.loading = true;
		this.done = false;
		this.leaderboard = new AsyncPipe<LeaderboardEntry>();
		this.#currentPage = null;
		this.#context = { index: 0, rank: 1, prevStarpower: Number.MAX_SAFE_INTEGER };
		try {
			this.#currentPage = this.kaizo ?
				await LeaderboardApi.getKaizoLeaderboardAsync() :
				await LeaderboardApi.getMainLeaderboardAsync();
				
			this.leaderboard.pushMany( this.#currentPage.values );
			if( !this.#currentPage.hasNext ) {
				this.done = true;
				this.leaderboard.end();
			}
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load leaderboard', getErrorMessage( exception ) );
			this.leaderboard.end();
			this.done = true;
		} finally {
			this.loading = false;
		}
	}

	async #loadMore() : Promise<void> {
		if( !this.#currentPage ) return;

		this.loading = true;
		try {
			this.#currentPage = await this.#currentPage.nextAsync();
			this.leaderboard.pushMany( this.#currentPage.values );
			if( !this.#currentPage.hasNext ) {
				this.done = true;
				this.leaderboard.end();
			}
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load leaderboard', getErrorMessage( exception ) );
			this.leaderboard.end();
			this.done = true;
		} finally {
			this.loading = false;
		}
	}

	async #getRank() : Promise<void> {
		this.rank = null;
		if( !this.currentUser ) return;

		this.loadingRank = true;
		try {
			this.rank = this.kaizo ?
				await LeaderboardApi.getKaizoRankAsync() :
				await LeaderboardApi.getMainRankAsync();
		} catch( exception: unknown ) {
			this.toastError( 'Failed to get your rank', getErrorMessage( exception ) );
		} finally {
			this.loadingRank = false;
		}
	}

}
