import { html, HTMLTemplateResult } from 'lit';
import { customElement } from 'lit/decorators.js';
import { ModqueueApi, PendingHack } from '../../apis/modqueue-api';
import { Page } from '../../apis/pagination';
import { RhdcModqueueListBase } from './rhdc-modqueue-list-base';
import './rhdc-modqueue-item';

@customElement( 'rhdc-modqueue-hack-list' )
export abstract class RhdcModqueueHackList extends RhdcModqueueListBase<PendingHack> {

	protected override get emptyQueueMessage() : string {
		return 'There are no more hacks pending approval.'
	}

	protected override get loadErrorMessage() : string {
		return 'Failed to load pending hacks.';
	}

	protected override loadFirstPageAsync() : Promise<Page<PendingHack>> {
		return ModqueueApi.getPendingHacksAsync();
	}

	protected override renderItem( item: PendingHack ) : HTMLTemplateResult {
		return html`
			<rhdc-modqueue-item
				.thumbnail=${item.thumbnail}
				.hackName=${item.title}
				.user=${item.uploader}
				.datePrefix=${'Uploaded'}
				.date=${item.uploadedDate}
				.href="/hack/${item.slug}"
			><span>${item.pendingVersions} pending ${item.pendingVersions === 1 ? 'version' : 'versions'}</span></rhdc-modqueue-item>
		`;
	}

}
