import { html, css, HTMLTemplateResult, CSSResultGroup } from 'lit';
import { state } from 'lit/decorators.js';
import { Page } from '../../apis/pagination';
import { RhdcElement } from '../../rhdc-element';
import { Optional } from '../../util/types';
import '@shoelace-style/shoelace/dist/components/alert/alert';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '../common/rhdc-infinite-scroll';

export abstract class RhdcModqueueListBase<T> extends RhdcElement {

	@state()
	private itemPromise: Optional<Promise<Page<T>>>;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: contents;
			}

			rhdc-infinite-scroll {
				width: 100%;
				box-sizing: border-box;
				padding: var(--sl-spacing-x-small);
				gap: var(--sl-spacing-small);
			}
		`;
	}

	constructor() {
		super();
		this.renderItem = this.renderItem.bind( this );
	}

	override render() : unknown {
		return html`
			<rhdc-infinite-scroll
				.pageGetter=${this.itemPromise}
				.renderer=${this.renderItem}
				.errorTitle=${this.loadErrorMessage}
			>
				<sl-alert slot="no-results" variant="success" open>
					<sl-icon slot="icon" name="check2-circle"></sl-icon>
					<strong>All Caught Up!</strong><br/>
					<span>${this.emptyQueueMessage}</span>
				</sl-alert>
			</rhdc-infinite-scroll>
		`;
	}

	public loadItems() : void {
		this.itemPromise = this.loadFirstPageAsync();
	}

	protected abstract loadFirstPageAsync() : Promise<Page<T>>;
	protected abstract renderItem( item: T ) : HTMLTemplateResult;

	protected abstract get emptyQueueMessage() : string;
	protected abstract get loadErrorMessage() : string;

}
