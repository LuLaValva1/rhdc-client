import { css, CSSResultGroup, html, HTMLTemplateResult } from 'lit';
import { customElement, query } from 'lit/decorators.js';
import { ModqueueApi, PendingClaim } from '../../apis/modqueue-api';
import { HacksApi } from '../../apis/hacks-api';
import { Page } from '../../apis/pagination';
import { RhdcModqueueListBase } from './rhdc-modqueue-list-base';
import { RhdcModqueueStatusChangeEvent } from '../../events/rhdc-modqueue-status-change-event';
import { getErrorMessage } from '../../util/http-error-parser';
import { RhdcDialog } from '../common/rhdc-dialog';
import SlCheckbox from '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '../common/rhdc-dialog';
import './rhdc-modqueue-item';

@customElement( 'rhdc-modqueue-pending-claim-list' )
export abstract class RhdcModqueuePendingClaimList extends RhdcModqueueListBase<PendingClaim> {

	@query( '#approve-dialog' )
	approveDialog!: RhdcDialog;

	@query( '#reject-dialog' )
	rejectDialog!: RhdcDialog;

	@query( 'sl-checkbox' )
	allowResubmissionCheckbox!: SlCheckbox;
	
	static override get styles() : CSSResultGroup {
		return css`
			${super.styles}

			rhdc-infinite-scroll::part(author) {
				color: var(--rhdc-font-color);
				overflow-x: hidden;
				text-overflow: ellipsis;
				flex-grow: 1;
			}

			rhdc-infinite-scroll::part(action-icon) {
				font-size: var(--sl-font-size-x-large);
				vertical-align: middle;
				margin: 0 calc( -1 * var(--sl-spacing-x-small) );
			}

			rhdc-infinite-scroll::part(explanation) {
				color: var(--rhdc-font-color);
				white-space: pre-wrap;
			}

			rhdc-dialog {
				--width: 500px;
			}
		`;
	}

	override render() : unknown {
		return html`
			${super.render()}
			<rhdc-dialog id="approve-dialog" label="Confirm Approval" .confirmText=${'Approve'}>
				<p>Are you sure you want to approve this hack ownership claim?</p>
			</rhdc-dialog>
			<rhdc-dialog id="reject-dialog" label="Confirm Rejection" .confirmText=${'Reject'} destructive>
				<p>Are you sure you want to reject this hack ownership claim?</p>
				<sl-checkbox checked>Allow this user to resubmit claims on this hack</sl-checkbox>
			</rhdc-dialog>
		`;
	}

	protected override get emptyQueueMessage() : string {
		return 'There are no more hacks ownership claims.'
	}

	protected override get loadErrorMessage() : string {
		return 'Failed to load hack ownership claims.';
	}

	protected override loadFirstPageAsync() : Promise<Page<PendingClaim>> {
		return ModqueueApi.getPendingClaimsAsync();
	}

	protected override renderItem( item: PendingClaim ) : HTMLTemplateResult {
		let reason: unknown = null;
		if( item.explanation ) {
			reason = html`
				<br/>
				<b>Explanation: </b>
				<span part="explanation">${item.explanation}</span>
			`;
		}

		return html`
			<rhdc-modqueue-item
				id="${item.hackId}:${item.claimant.userId}"
				.thumbnail=${item.hackThumbnail}
				.hackName=${item.hackTitle}
				.user=${item.claimant}
				.datePrefix=${'Submitted'}
				.date=${item.dateSubmitted}
				.href="/hack/${item.hackSlug}"
			>
				<div>
					<b>Alias: </b>
					<span part="author">${item.targetAuthor || 'n/a'}</span>
					${reason}
				</div>
				<sl-button
					slot="action"
					variant="success"
					size="large"
					@click=${this.#approve.bind( this, item )}
				><sl-icon part="action-icon" name="check-lg"></sl-icon></sl-button>
				<sl-button
					slot="action"
					variant="danger"
					size="large"
					@click=${this.#reject.bind( this, item )}
				><sl-icon part="action-icon" name="x-lg"></sl-icon></sl-button>
			</rhdc-modqueue-item>
		`;
	}

	#approve( claim: PendingClaim, event: Event ) : void {
		this.approveDialog.runAsync( this.#approveAsync.bind( this, claim ) );
		event.stopPropagation();
		event.preventDefault();
	}

	#reject( claim: PendingClaim, event: Event ) : void {
		this.rejectDialog.runAsync( this.#rejectAsync.bind( this, claim ) );
		event.stopPropagation();
		event.preventDefault();
	}

	async #approveAsync( claim: PendingClaim ) : Promise<void> {
		try {
			await HacksApi.approveHackOwnershipClaimAsync( claim.hackId, claim.claimant.userId );
			this.dispatchEvent( RhdcModqueueStatusChangeEvent.createAsync() );
			this.#removeItemFromDOM( claim );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to approve hack ownership claim', getErrorMessage( exception ) );
			throw exception;
		}
	}

	async #rejectAsync( claim: PendingClaim ) : Promise<void> {
		try {
			if( this.allowResubmissionCheckbox.checked ) {
				await HacksApi.deleteHackOwnershipClaimAsync( claim.hackId, claim.claimant.userId );
			} else {
				await HacksApi.rejectHackOwnershipClaimAsync( claim.hackId, claim.claimant.userId );
			}
			this.dispatchEvent( RhdcModqueueStatusChangeEvent.createAsync() );
			this.#removeItemFromDOM( claim );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to reject hack ownership claim', getErrorMessage( exception ) );
			throw exception;
		}
	}

	#removeItemFromDOM( claim: PendingClaim ) : void {
		const scroller = this.shadowRoot?.querySelector( 'rhdc-infinite-scroll' );
		scroller?.shadowRoot?.getElementById( `${claim.hackId}:${claim.claimant.userId}` )?.remove();
	}

}
