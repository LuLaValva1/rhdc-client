import { css, CSSResultGroup, html, HTMLTemplateResult } from 'lit';
import { customElement, query, state } from 'lit/decorators.js';
import { ModqueueApi, PendingStarpower } from '../../apis/modqueue-api';
import { StarpowerApi } from '../../apis/starpower-api';
import { Page } from '../../apis/pagination';
import { RhdcModqueueListBase } from './rhdc-modqueue-list-base';
import { RhdcModqueueStatusChangeEvent } from '../../events/rhdc-modqueue-status-change-event';
import { getErrorMessage } from '../../util/http-error-parser';
import { RhdcDialog } from '../common/rhdc-dialog';
import SlTextarea from '@shoelace-style/shoelace/dist/components/textarea/textarea';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/textarea/textarea';
import '../common/rhdc-dialog';
import './rhdc-modqueue-item';

@customElement( 'rhdc-modqueue-starpower-list' )
export abstract class RhdcModqueueStarpowerList extends RhdcModqueueListBase<PendingStarpower> {

	@state()
	rejecting = false;

	@query( '#approve-dialog' )
	approveDialog!: RhdcDialog;

	@query( '#reject-dialog' )
	rejectDialog!: RhdcDialog;

	@query( 'sl-textarea' )
	reasonInput!: SlTextarea;

	static override get styles() : CSSResultGroup {
		return css`
			${super.styles}

			rhdc-infinite-scroll::part(info) {
				display: flex;
				gap: 0.5ch;
				color: var(--rhdc-text-color);
			}

			rhdc-infinite-scroll::part(spacer) {
				flex-grow: 1;
			}

			rhdc-infinite-scroll::part(link) {
				overflow-x: hidden;
				text-overflow: ellipsis;
				color: var(--sl-color-primary-600);
				text-decoration: inherit;
			}

			rhdc-infinite-scroll::part(link):hover {
				color: var(--sl-color-primary-700);
			}

			rhdc-infinite-scroll::part(link):focus-visible {
				color: var(--sl-color-primary-700);
				text-decoration: dotted underline;
			}

			rhdc-infinite-scroll::part(complete) {
				color: var(--sl-color-success-600);
			}
			
			rhdc-infinite-scroll::part(star) {
				color: var(--sl-color-amber-500);
			}

			rhdc-infinite-scroll::part(action-icon) {
				font-size: var(--sl-font-size-x-large);
				vertical-align: middle;
				margin: 0 calc( -1 * var(--sl-spacing-x-small) );
			}

			rhdc-dialog {
				--width: 500px;
			}
		`;
	}

	override render() : unknown {
		return html`
			${super.render()}
			<rhdc-dialog label="Confirm Approval" id="approve-dialog" .confirmText=${'Approve'}>
				<p>Are you sure you want to approve this starpower submission?</p>
			</rhdc-dialog>
			<rhdc-dialog label="Confirm Rejection" id="reject-dialog" .confirmText=${'Reject'} destructive>
				<p>Are you sure you want to reject this starpower submission?</p>
				<sl-textarea
					inputmode="text"
					label="Reason (Optional)"
					placeholder="Optionally enter text here to be included in the rejection notification."
					rows="2"
					resize="vertical"
					autocapitalize="on"
					autocorrect="on"
					autocomplete="off"
					enterkeyhint="enter"
					spellcheck
					?disabled=${this.rejecting}
				></sl-textarea>
			</rhdc-dialog>
		`;
	}

	protected override get emptyQueueMessage() : string {
		return 'There are no more starpower submissions needing approval.'
	}

	protected override get loadErrorMessage() : string {
		return 'Failed to load pending starpower submissions.';
	}

	protected override loadFirstPageAsync() : Promise<Page<PendingStarpower>> {
		return ModqueueApi.getPendingStarpowerAsync();
	}

	protected override renderItem( item: PendingStarpower ) : HTMLTemplateResult {
		let claim: HTMLTemplateResult;
		if( item.claimedStarPoints <= 1 && item.claimedHackComplete ) {
			claim = html`<span part="complete" class="complete">Completed</span>`;
		} else {
			claim = html`
				<span>${item.claimedStarPoints}</span>
				<sl-icon part="star" name="star-fill"></sl-icon>
			`;
		}

		let link: HTMLTemplateResult;
		if( item.proofLink ) {
			link = html`
				<a
					part="link"
					href=${item.proofLink}
					referrerpolicy="no-referrer"
					target="_blank"
					@click=${(event: Event) => event.stopPropagation()}
				>${item.proofLink || 'n/a'}</a>
			`;
		} else {
			link = html`<span>n/a</span>`;
		}

		return html`
			<rhdc-modqueue-item
				id="${item.hackId}:${item.user.userId}"
				.hackName=${item.hackTitle}
				.user=${item.user}
				.datePrefix=${'Submitted'}
				.date=${item.claimedStarCountLastUpdated}
				.href="/hack/${item.hackSlug}"
			>
				<div part="info">
					<b>Proof:</b>
					<div></div>
					${link}
					<div part="spacer"></div>
					${claim}
				</div>
				<sl-button
					slot="action"
					variant="success"
					size="large"
					@click=${this.#approve.bind( this, item )}
				><sl-icon part="action-icon" name="check-lg"></sl-icon></sl-button>
				<sl-button
					slot="action"
					variant="danger"
					size="large"
					@click=${this.#reject.bind( this, item )}
				><sl-icon part="action-icon" name="x-lg"></sl-icon></sl-button>
			</rhdc-modqueue-item>
		`;
	}

	#approve( sps: PendingStarpower, event: Event ) : void {
		this.approveDialog.runAsync( this.#approveAsync.bind( this, sps ) );
		event.stopPropagation();
		event.preventDefault();
	}

	#reject( sps: PendingStarpower, event: Event ) : void {
		this.reasonInput.value = '';
		this.rejectDialog.runAsync( this.#rejectAsync.bind( this, sps ) );
		event.stopPropagation();
		event.preventDefault();
	}

	async #approveAsync( sps: PendingStarpower ) : Promise<void> {
		try {
			await StarpowerApi.approveStarpowerAsync( sps.user.userId, sps.hackId );
			this.dispatchEvent( RhdcModqueueStatusChangeEvent.createAsync() );
			this.#removeItemFromDOM( sps );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to approve starpower submission', getErrorMessage( exception ) );
			throw exception;
		}
	}

	async #rejectAsync( sps: PendingStarpower ) : Promise<void> {
		this.rejecting = true;
		try {
			await StarpowerApi.rejectStarpowerAsync( sps.user.userId, sps.hackId, this.reasonInput.value || null );
			this.dispatchEvent( RhdcModqueueStatusChangeEvent.createAsync() );
			this.#removeItemFromDOM( sps );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to reject starpower submission', getErrorMessage( exception ) );
			throw exception;
		} finally {
			this.rejecting = false;
		}
	}

	#removeItemFromDOM( sps: PendingStarpower ) : void {
		const scroller = this.shadowRoot?.querySelector( 'rhdc-infinite-scroll' );
		scroller?.shadowRoot?.getElementById( `${sps.hackId}:${sps.user.userId}` )?.remove();
	}

}
