import { css, CSSResultGroup, html } from 'lit';
import { customElement, query, queryAll, state } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { NewsApi } from '../../apis/news-api';
import { PollsApi, PollResultVisibility } from '../../apis/polls-api';
import { Optional, Uuid, Weak } from '../../util/types';
import { RhdcMarkdownEditor } from '../../widgets/rhdc-markdown-editor';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import SlRadioGroup from '@shoelace-style/shoelace/dist/components/radio-group/radio-group';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/divider/divider';
import '@shoelace-style/shoelace/dist/components/radio-group/radio-group';
import '@shoelace-style/shoelace/dist/components/radio/radio';
import '@shoelace-style/shoelace/dist/components/icon-button/icon-button';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '../../widgets/rhdc-markdown-editor';

@customElement( 'rhdc-news-post-creator' )
export class RhdcNewsPostCreator extends RhdcElement {

	@state()
	loading = false;

	@state()
	hasPoll = false;

	@state()
	choices = [ 'Option 1', 'Option 2' ];

	@query( '#title' )
	titleInput!: SlInput;

	@query( '#content' )
	contentInput!: RhdcMarkdownEditor;

	@query( '#poll-title' )
	pollTitleInput!: SlInput;

	@query( '#result-visibility' )
	resultVisibilityRadioGroup!: SlRadioGroup;

	@queryAll( '.poll-option > sl-input' )
	pollOptionInputs!: NodeListOf<SlInput>;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-x-small);
			}

			.button-tray {
				display: flex;
				gap: var(--sl-spacing-x-small);
			}

			.spacer {
				flex-grow: 1;
			}

			.poll-option {
				display: flex;
				align-items: center;
			}

			.poll-option > sl-input {
				flex-grow: 1;
			}

			.add-choice-button {
				align-self: flex-start;
			}

			h2, h3 {
				color: var(--sl-color-primary-600);
				margin: 0;
			}

			h2 { font-size: 1.25rem; }
			h3 { font-size: 1.5rem; }
		`;
	}

	override render() : unknown {
		let poll: unknown = null;
		if( this.hasPoll ) {
			const pollOptions = this.choices.map( (choiceText, i) => {
				let deleteButton: unknown = null;
				if( this.choices.length > 2 ) {
					deleteButton = html`
						<sl-icon-button
							name="x"
							label="remove"
							@click=${this.#removeChoice.bind( this, i )}
						></sl-icon-button>
					`;
				}

				return html`
					<div class="poll-option">
						<sl-input
							data-index="${i}"
							type="text"
							.value=${choiceText}
							?disabled=${this.loading}
							placeholder="${`Option ${i + 1}`}"
							required
							autocapitalize="off"
							autocorrect="on"
							autocomplete="off"
							enterkeyhint="next"
							spellcheck="true"
							inputmode="text"
							@sl-change=${this.#choiceChanged.bind( this, i )}
						/></sl-input>
						${deleteButton}
					</div>
				`;
			});
			

			poll = html`
				<sl-divider></sl-divider>
				<h2>Poll</h2>
				<sl-input
					id="poll-title"
					type="text"
					name="Poll Title"
					label="Poll Title"
					placeholder="Poll Title"
					?disabled=${this.loading}
					required
					autocapitalize="on"
					autocorrect="on"
					autocomplete="off"
					enterkeyhint="next"
					spellcheck="true"
					inputmode="text"
				></sl-input>
				<sl-radio-group id="result-visibility" label="Result Visibility" fieldset value="${PollResultVisibility.AfterVoting}">
					<sl-radio value="${PollResultVisibility.Always}">Always Visible</sl-radio>
					<sl-radio value="${PollResultVisibility.AfterVoting}">Visible After Voting</sl-radio>
					<sl-radio value="${PollResultVisibility.WhenLocked}">Visible After Poll Closes</sl-radio>
					<sl-radio value="${PollResultVisibility.StaffOnly}">Only Visible to Staff</sl-radio>
				</sl-radio-group>
				<h3>Poll Choices</h3>
				${pollOptions}
				<sl-button class="add-choice-button" variant="primary" @click=${this.#addChoice}>
					<sl-icon slot="prefix" name="plus"></sl-icon>
					<span>Add Choice</span>
				</sl-button>
			`;
		}

		return html`
			<sl-input
				id="title"
				type="text"
				name="Title"
				label="Title"
				placeholder="Title"
				?disabled=${this.loading}
				required
				autocapitalize="on"
				autocorrect="on"
				autocomplete="off"
				autofocus
				enterkeyhint="next"
				spellcheck="true"
				inputmode="text"
			></sl-input>
			<rhdc-markdown-editor
				trusted
				id="content"
				label="Content"
				placeholder="News post content"
				rows="8"
				?disabled=${this.loading}
				required
				enterkeyhint="enter"
				spellcheck
			></rhdc-markdown-editor>
			${poll}
			<sl-divider></sl-divider>
			<div class="button-tray">
				<sl-button variant=${this.hasPoll ? "danger" : "primary"} @click=${this.#togglePoll}>${this.hasPoll ? "Remove Poll" : "Add Poll"}</sl-button>
				<div class="spacer"></div>
				<sl-button variant="primary" @click=${this.#createPost}>Create Post</sl-button>
				<sl-button variant="neutral" @click=${this.#cancel}>Cancel</sl-button>
			</div>
		`;
	}

	#togglePoll() : void {
		this.hasPoll = !this.hasPoll;
	}

	async #addChoice() : Promise<void> {
		this.choices.push( '' );
		this.requestUpdate();
		await this.updateComplete;
		const choiceInput = this.shadowRoot?.querySelector( `.poll-option > sl-input[data-index="${this.choices.length - 1}"]` ) as Weak<SlInput>;
		if( choiceInput ) choiceInput.focus();
	}

	#removeChoice( index: number ) : void {
		this.choices.splice( index, 1 );
		this.requestUpdate();
	}

	#choiceChanged( index: number ) : void {
		const choiceInput = this.shadowRoot?.querySelector( `.poll-option > sl-input[data-index="${index}"]` ) as SlInput;
		this.choices[index] = choiceInput.value;
	}

	async #createPost() : Promise<void> {
		if( !this.titleInput.reportValidity() || !this.contentInput.reportValidity() ) return;
		if( this.hasPoll ) {
			if( !this.pollTitleInput.reportValidity() ) return;
			for( let i = 0; i < this.pollOptionInputs.length; i++ ) {
				if( !this.pollOptionInputs[i].reportValidity() ) return;
				this.choices[i] = this.pollOptionInputs[i].value;
			}
		}

		this.loading = true;
		try {
			let pollId: Optional<Uuid> = undefined;
			if( this.hasPoll ) {
				pollId = (await PollsApi.createPollAsync(
					this.pollTitleInput.value,
					this.choices,
					this.resultVisibilityRadioGroup.value as PollResultVisibility
				)).pollId;
			}
			await NewsApi.createNewsPostAsync( this.titleInput.value, this.contentInput.value, pollId );
			this.dispatchEvent( new CustomEvent( 'rhdc-reload-news-event', { bubbles: false } ) );
		} catch( exception: unknown ) {
			this.toastError( 'Server Error', 'Failed to create news post.' );
			console.error( exception );
		} finally {
			this.loading = false;
		}
	}

	#cancel() : void {
		this.dispatchEvent( new CustomEvent( 'rhdc-cancel-news-post-event', { bubbles: false } ) );
	}

}
