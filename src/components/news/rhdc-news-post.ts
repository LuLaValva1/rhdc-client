import { css, CSSResultGroup, html } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { NewsPost } from '../../apis/news-api';
import { NewsApi } from '../../apis/news-api';
import { PollsApi } from '../../apis/polls-api';
import { RhdcMarkdownEditor } from '../../widgets/rhdc-markdown-editor';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/button-group/button-group';
import '@shoelace-style/shoelace/dist/components/icon-button/icon-button';
import '@shoelace-style/shoelace/dist/components/card/card';
import '@shoelace-style/shoelace/dist/components/input/input';
import '../../widgets/rhdc-markdown-editor';
import '../../widgets/rhdc-markdown';
import '../../widgets/rhdc-avatar';
import '../common/rhdc-username';
import './rhdc-poll';

@customElement( 'rhdc-news-post' )
export class RhdcNewsPost extends RhdcElement {

	@property({ attribute: 'editable', type: Boolean, reflect: true })
	editMode = false;

	@property({ attribute: false })
	post!: NewsPost;

	@query( 'rhdc-markdown-editor' )
	editor!: RhdcMarkdownEditor;

	@query( 'sl-input' )
	titleInput!: SlInput;

	@state()
	loading = false;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				color: var(--rhdc-text-color);
				font-family: var(--sl-font-sans);
			}

			sl-card {
				width: 100%;
			}

			rhdc-avatar {
				float: left;
				margin-right: 1ch;
				vertical-align: top;
				--size: 3rem;
			}

			rhdc-poll {
				margin-top: var(--sl-spacing-medium);
			}

			.title {
				color: var(--sl-color-neutral-1000);
				font-size: 1.25em;
				width: 100%;
			}

			.subtitle {
				display: flex;
				align-items: center;
				color: var(--rhdc-text-color-faded);
			}

			.header {
				display: flex;
				align-items: center;
			}

			.header-text {
				flex-grow: 1;
			}

			.footer {
				display: flex;
				justify-content: flex-end;
				gap: 1ch;
				width: 100%;
			}

			.button-tray {
				display: flex;
				justify-content: flex-end;
				gap: var(--sl-spacing-x-small);
			}

			sl-card::part(header) {
				padding-left: var(--sl-spacing-small);
				padding-right: var(--sl-spacing-small);
			}

			sl-card::part(body) {
				padding: var(--sl-spacing-medium) var(--sl-spacing-small);
			}

			sl-card::part(footer) {
				padding: var(--sl-spacing-x-small);
			}

			sl-input {
				width: 100%;
			}

			rhdc-markdown-editor {
				--preview-width: 960px;
			}
		`;
	}

	override render() : unknown {
		let content: unknown = null;
		if( this.editMode ) {
			content = html`
				<rhdc-markdown-editor
					trusted
					rows="12"
					enterkeyhint="enter"
					.value=${this.post.message}
					spellcheck
				></rhdc-markdown-editor>
				<div slot="footer" class="button-tray">
					<sl-button variant="primary" @click=${this.#saveChanges}>Save</sl-button>
					<sl-button variant="neutral" @click=${this.#switchMode}>Cancel</sl-button>
				</div>
			`;
		} else {
			let poll: unknown = null;
			if( this.post.pollId ) {
				poll = html`<rhdc-poll .pollId=${this.post.pollId}></rhdc-poll>`;
			}

			content = html`
				<rhdc-markdown trusted .markdown=${this.post.message}></rhdc-markdown>
				${poll}
			`;
		}

		let header: unknown;
		if( this.editMode ) {
			header = html`
				<sl-input
					type="text"
					autocapitalize="on"
					autocorrect="on"
					autocomplete="off"
					enterkeyhint="next"
					spellcheck="true"
					inputmode="text"
					.value=${this.post.title}
					required
				></sl-input>
			`;
		} else {
			let actionButtons: unknown = null;
			if( this.post.canEdit ) {
				actionButtons = html`
					<sl-icon-button name="pencil" label="Edit Post" @click=${this.#switchMode}></sl-icon-button>
					<sl-icon-button name="trash" label="Delete Post" @click=${this.#delete}></sl-icon-button>
				`;
			}

			header = html`
				<rhdc-avatar .username=${this.post.author.username}></rhdc-avatar>
				<div class="header-text">
					<div class="title">${this.post.title}</div>
					<div class="subtitle">
						<span>By&nbsp;</span>
						<rhdc-username .user=${this.post.author}></rhdc-username>
						<span>&nbsp;on ${this.post.datePosted.toLocaleDateString( 'sv' )}</span>
					</div>
				</div>
				${actionButtons}
			`;
		}

		return html`
			<sl-card>
				<div slot="header" class="header">${header}</div>
				${content}
			</sl-card>
		`;
	}

	#switchMode() : void {
		this.editMode = !this.editMode;
	}

	async #saveChanges() : Promise<void> {
		this.loading = true;
		try {
			this.post = await NewsApi.updateNewsPostAsync( this.post.postId, this.titleInput.value, this.editor.value );
			this.editMode = false;
		} catch( exception: unknown ) {
			this.toastError( 'Server Error', 'Failed to edit news post.' );
			console.error( exception );
		} finally {
			this.loading = false;
		}
	}

	async #delete() : Promise<void> {
		if( await this.confirmAsync( 'Confirm Deletion', 'Are you sure you want to delete this news post? This action cannot be undone.' ) ) {
			this.loading = true;
			try {
				if( this.post.pollId ) {
					await PollsApi.deletePollAsync( this.post.pollId );
				}
				await NewsApi.deleteNewsPostAsync( this.post.postId );
				this.editMode = false;
				this.#fireReloadEvent();
			} catch( exception: unknown ) {
				this.toastError( 'Server Error', 'Failed to delete news post.' );
				console.error( exception );
			} finally {
				this.loading = false;
			}
		}
	}

	#fireReloadEvent() : void {
		this.dispatchEvent( new CustomEvent( 'rhdc-reload-news-event', { bubbles: false	} ) );
	}

}
