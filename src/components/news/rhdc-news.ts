import { css, CSSResultGroup, html, HTMLTemplateResult, PropertyValues } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { rhdcAsyncAppend } from '../../util/lit-extensions';
import { NewsApi, NewsPost } from '../../apis/news-api';
import { Nullable, Weak } from '../../util/types';
import { AsyncPipe } from '../../util/async-pipe';
import { RhdcToastEvent } from '../../events/rhdc-toast-event';
import { getErrorMessage } from '../../util/http-error-parser';
import { rhdcUserContext, UserAuth } from '../../decorators/user-context';
import { Page } from '../../apis/pagination';
import { Role } from '../../auth/roles';
import '@shoelace-style/shoelace/dist/components/card/card';
import '@shoelace-style/shoelace/dist/components/skeleton/skeleton';
import '../../widgets/rhdc-load-more';
import './rhdc-news-post';
import './rhdc-news-post-skeleton';
import './rhdc-news-post-creator';

enum LoadingState {
	Initializing,
	Idle,
	Loading,
	Done
}

@customElement( 'rhdc-news' )
export class RhdcNews extends RhdcElement {

	@state()
	loadingState = LoadingState.Initializing;

	@state()
	creatingPost = false;

	@rhdcUserContext()
	currentUser!: Nullable<UserAuth>;

	#newsPosts!: AsyncPipe<NewsPost>;
	#currentPage: Nullable<Page<NewsPost>> = null;

	override connectedCallback(): void {
		super.connectedCallback();
		this.#reloadNews();
	}

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-medium);
			}

			h2 {
				margin: 0;
				font-size: 1.75rem;
				color: var(--sl-color-primary-600);
			}

			.header {
				display: flex;
				align-items: center;
			}

			.spacer {
				flex-grow: 1;
			}
		`;
	}

	override render() : unknown {
		if( this.loadingState === LoadingState.Initializing ) {
			return html`
				<h2>News</h2>
				<rhdc-news-post-skeleton></rhdc-news-post-skeleton>
				<rhdc-news-post-skeleton></rhdc-news-post-skeleton>
				<rhdc-news-post-skeleton></rhdc-news-post-skeleton>
			`;
		}

		if( this.creatingPost ) {
			return html`
				<rhdc-news-post-creator
					@rhdc-reload-news-event=${this.#postCreated}
					@rhdc-cancel-news-post-event=${this.#createCancelled}
				></rhdc-news-post-creator>
			`;
		}

		let loadTrigger: Nullable<HTMLTemplateResult> = null;
		if( this.loadingState !== LoadingState.Done ) {
			loadTrigger = html`<rhdc-load-more ?loading=${this.loadingState === LoadingState.Loading} @rhdc-load-more=${this.#loadMore}></rhdc-load-more>`;
		}

		let createButton: Nullable<HTMLTemplateResult> = null;
		if( this.currentUser?.role === Role.Staff ) {
			createButton = html`<sl-button variant="primary" @click=${this.#toggleCreateMode}>Create News Post</sl-button>`;
		}

		return html`
			<div class="header">
				<h2>News</h2>
				<div class="spacer"></div>
				${createButton}
			</div>
			${rhdcAsyncAppend( this.#newsPosts, this.#renderPost.bind( this ) )}
			${loadTrigger}
		`;
	}

	override willUpdate( changedProperties: PropertyValues ): void {
		super.willUpdate( changedProperties );
		const prevContext : Weak<UserAuth> = changedProperties.get( 'currentUser' );
		if( prevContext !== undefined && prevContext?.role !== this.currentUser?.role ) {
			this.#reloadNews();
		}
	}

	#reloadNews() : void {
		this.loadingState = LoadingState.Initializing;
		this.#currentPage = null;
		this.#newsPosts = new AsyncPipe<NewsPost>();

		const stateToken = this.#newsPosts;
		NewsApi.getNewsPostsPagedAsync().then( page => {
			if( this.#newsPosts !== stateToken ) return;

			this.#currentPage = page;
			this.#newsPosts.pushMany( page.values );
			if( page.hasNext ) {
				this.loadingState = LoadingState.Idle;
			} else {
				this.#newsPosts.end();
				this.loadingState = LoadingState.Done;
			}
		}).catch( exception => {
			this.toastError( 'Failed to load news posts.', getErrorMessage( exception ) );
			this.#newsPosts.end();
		});
	}

	#renderPost( post: NewsPost ) : HTMLTemplateResult {
		return html`<rhdc-news-post .post=${post} @rhdc-reload-news-event=${this.#reloadNews}></rhdc-news-post>`;
	}

	#toggleCreateMode() : void {
		this.creatingPost = !this.creatingPost;
	}

	#postCreated() : void {
		this.creatingPost = false;
		this.#reloadNews();
	}

	#createCancelled() : void {
		this.creatingPost = false;
	}

	async #loadMore() : Promise<void> {
		this.loadingState = LoadingState.Loading;

		try {
			const stateToken = this.#newsPosts;
			this.#currentPage = await this.#currentPage!.nextAsync();
			if( stateToken !== this.#newsPosts ) return;
		} catch( error: unknown ) {
			this.dispatchEvent( RhdcToastEvent.error( 'Failed to load more news posts.' ));
			this.#newsPosts.end();
			this.loadingState = LoadingState.Done;
			console.error( error );
			return;
		}

		this.#newsPosts.pushMany( this.#currentPage.values );
		if( this.#currentPage.hasNext ) {
			this.loadingState = LoadingState.Idle;
		} else {
			this.#newsPosts.end();
			this.loadingState = LoadingState.Done;
		}
	}

}
