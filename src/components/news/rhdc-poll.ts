import { css, CSSResultGroup, html, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { rhdcUserContext, UserAuth } from '../../decorators/user-context';
import { PollsApi, Poll } from '../../apis/polls-api';
import { Nullable, Uuid } from '../../util/types';
import { Role } from '../../auth/roles';
import { getErrorMessage } from '../../util/http-error-parser';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import './rhdc-poll-result';

@customElement( 'rhdc-poll' )
export class RhdcPoll extends RhdcElement {

	@property({ attribute: false })
	pollId!: Uuid;

	@state()
	poll: Nullable<Poll> = null;

	@state()
	resultsView = false;

	@state()
	loading = false;

	@state()
	clickedOption = -1;

	@rhdcUserContext()
	currentUser!: Nullable<UserAuth>;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-small);
			}

			h3 {
				font-size: 1.25rem;
				margin: 0;
			}

			sl-spinner {
				font-size: 2rem;
			}

			sl-button.option::part( base ) {
				justify-content: left;
			}

			.poll-options-container {
				display: flex;
			}

			.poll-options {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				min-width: 400px;
				gap: var(--sl-spacing-x-small);
			}

			.button-tray {
				display: flex;
			}

			.spacer {
				flex-grow: 1;
			}

			@media (max-width: 500px) {
				
				.poll-options {
					width: calc( 100% - 10px );
					min-width: unset;
				}

			}
		`;
	}

	override render() : unknown {
		if( !this.poll ) {
			return html`<sl-spinner></sl-spinner>`;
		}

		const parts = [];

		parts.push( html`<h3>${this.poll.name}</h3>` );
		
		let options = [];
		if( this.resultsView ) {
			const totalVotes = this.poll.results!.reduce( (sum, value) => sum + value.count, 0 );
			const mostVotes = Math.max( ...this.poll.results!.map( x => x.count ) );
			options = this.poll.results!.map( (choice, index) => html`
				<rhdc-poll-result
					.name=${choice.option}
					.votes=${choice.count}
					.total=${totalVotes}
					.winner=${choice.count === mostVotes}
					?checked=${index === this.poll!.userVote?.index}
				></rhdc-poll-result>
			` );
		} else {
			options = this.poll.options.map( (choice, index) => html`
				<sl-button
					class="option"
					variant="primary"
					?disabled=${!this.poll?.canVote || (this.clickedOption >= 0 && this.clickedOption !== index)}
					?loading=${this.clickedOption === index}
					outline
					@click=${this.#vote.bind( this, index )}
				>${choice}</sl-button>
			`);
		}

		parts.push( html`
			<div class="poll-options-container">
				<div class="poll-options">${options}</div>
				<div class="spacer"></div>
			</div>
		`);

		if( !this.currentUser ) {
			parts.push( html`<strong>Log in to cast a vote.</strong>` );
		} else if( this.currentUser.role === Role.Unverified ) {
			parts.push( html`<strong>Verify your account to cast a vote</strong>` );
		}

		const buttons = [];
		if( this.poll.canVote  && !!this.poll.results ) {
			buttons.push( html`
				<sl-button @click=${this.#toggleView}>${this.resultsView ? 'Cast Vote' : 'View Results'}</sl-button>
			`);
		}

		if( this.poll.canEdit ) {
			if( this.poll.locked ) {
				buttons.push( html`
					<sl-button variant="primary" @click=${this.#reopenPoll}>Reopen Poll</sl-button>
				`);
			} else {
				buttons.push( html`
					<sl-button variant="primary" @click=${this.#closePoll}>Close Poll</sl-button>
				`);
			}
		}

		buttons.push( html`<div class="spacer"></div>` );

		if( this.poll.canResetVotes ) {
			buttons.push( html`
				<sl-button variant="danger" @click=${this.#resetPoll}>Reset Votes</sl-button>
			`);
		}

		parts.push( html`<div class="button-tray">${buttons}</div>` );
		return parts;
	}

	protected override willUpdate( changedProperties: PropertyValues ): void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'pollId' ) ) {
			PollsApi.getPollAsync( this.pollId ).then( poll => {
				this.poll = poll;
				this.resultsView = !!poll.results || (poll.canVote && !poll.userVote);
			})
		} else if( changedProperties.has( 'poll' ) && !this.poll?.results ) {
			this.resultsView = false;
		}
	}

	async #vote( index: number ) : Promise<void> {
		this.clickedOption = index;
		try {
			await PollsApi.voteAsync( this.pollId, index );
			this.poll = await PollsApi.getPollAsync( this.pollId );
			this.resultsView = !!this.poll.results;
		} catch( exception: unknown ) {
			this.toastError( 'Server Error', 'Failed to cast vote.' );
			console.error( exception );
		} finally {
			this.clickedOption = -1;
		}
	}

	#toggleView() : void {
		this.resultsView = !this.resultsView;
	}

	async #reopenPoll() : Promise<void> {
		if( !this.poll || !this.poll.locked ) return;

		try {
			await PollsApi.updatePollAsync( this.pollId, { locked: false } );
			this.poll = await PollsApi.getPollAsync( this.pollId );
			this.toastSuccess( 'Poll reopened' );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to reopen poll', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	async #closePoll() : Promise<void> {
		if( !this.poll || this.poll.locked ) return;

		try {
			await PollsApi.updatePollAsync( this.pollId, { locked: true } );
			this.poll = await PollsApi.getPollAsync( this.pollId );
			this.toastSuccess( 'Poll closed' );
			this.resultsView = true;
		} catch( exception: unknown ) {
			this.toastError( 'Failed to close poll', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	async #resetPoll() : Promise<void> {
		if( await this.confirmAsync( 'Confirm Reset', 'Are you sure you want to delete all votes in this poll?' ) ) {
			this.loading = true;
			try {
				await PollsApi.resetPollAsync( this.pollId );
				this.poll = await PollsApi.getPollAsync( this.pollId );
				this.toastSuccess( 'Poll Reset.' );
			} catch( exception: unknown ) {
				this.toastError( 'Failed to reset poll.', getErrorMessage( exception ) );
			} finally {
				this.loading = false;
			}
		}
	}

}
