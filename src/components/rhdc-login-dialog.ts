import { css, CSSResultGroup, html, nothing, HTMLTemplateResult } from 'lit';
import { customElement, state, query } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { Auth } from '../auth/auth';
import { Weak } from '../util/types';
import { guard } from 'lit/directives/guard.js';
import { getErrorMessage } from '../util/http-error-parser';
import generatePassword from '../util/generate-password';
import SlDialog from '@shoelace-style/shoelace/dist/components/dialog/dialog';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/dialog/dialog';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/icon/icon';

const hasCryptoSupport = !!window.crypto?.getRandomValues;

enum DialogView {
	LoginView,
	RegisterView,
	PasswordResetView
}

@customElement( 'rhdc-login-dialog' )
export class RhdcLoginDialog extends RhdcElement {

	#redirect: Weak<string>;

	@state()
	view = DialogView.LoginView;

	@state()
	waiting = false;

	@state()
	captchaImage = new String( 'about:blank' );

	@query( 'sl-dialog' )
	dialog: Weak<SlDialog>;

	@query( '#username' )
	usernameInput!: SlInput;

	@query( '#password' )
	passwordInput!: SlInput;

	@query( '#email' )
	emailInput!: SlInput;

	@query( '#confirm-password' )
	passwordConfirmInput!: SlInput;

	@query( '#captcha' )
	captchaInput!: SlInput;

	static override get styles() : CSSResultGroup {
		return css`
			.username-note {
				color: var(--rhdc-text-color-faded);
				margin-left: 10ch;
				padding-left: var(--sl-spacing-medium);
			}

			.username-note > span {
				font-size: var(--sl-font-size-x-small);
			}

			.login-view, .register-view, .reset-password-view {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-x-small);
				align-items: stretch;
			}

			.reset-password-view {
				gap: var(--sl-spacing-medium);
			}

			.login-view > span, .register-view > span {
				font-size: var(--sl-font-size-small);
				color: var(--rhdc-text-color);
			}

			.register-button-tray {
				display: flex;
				justify-content: space-between;
			}

			a {
				color: var(--sl-color-primary-600) !important;
			}

			sl-button {
				align-self: flex-end;
			}

			sl-input::part(form-control) {
				display: flex;
				align-items: center;
				gap: var(--sl-spacing-medium);
			}

			sl-input::part(form-control-label) {
				flex-shrink: 0;
				width: 10ch;
				text-align: right;
			}

			sl-input::part(form-control-input) {
				flex-grow: 1;
			}

			sl-input#confirm-password::part(form-control-label) {
				font-size: 0.8em;
				white-space: pre-line;
				width: 12.5ch;
				margin-top: -1em;
			}

			sl-input#captcha::part(form-control) {
				display: block;
			}

			sl-input#captcha::part(form-control-label) {
				width: 100%;
				text-align: left;
				font-size: var(--sl-font-size-x-small);
			}

			.captcha {
				display: flex;
				align-items: flex-end;
				gap: var(--sl-spacing-x-small);
			}

			.captcha > img {
				width: 300px;
				height: 80px;
			}

			p {
				margin: 0;
				padding: 0;
			}
		`;
	}

	override render() : unknown {
		let title: string;
		let dialogContent: HTMLTemplateResult;
		switch( this.view ) {
			case DialogView.LoginView:
				title = 'Sign In';
				dialogContent = this.#renderLoginView();
				break;
			case DialogView.RegisterView:
				title = 'Register';
				dialogContent = this.#renderRegisterView();
				break;
			case DialogView.PasswordResetView:
				title = 'Reset Password';
				dialogContent = this.#renderResetPasswordView();
				break;
			default:
				return nothing;
		}

		return html`<sl-dialog label=${title}>${dialogContent}</sl-dialog>`;
	}

	#renderLoginView() : HTMLTemplateResult {
		return html`
			<div class="login-view">
				<div class="username-note">
					<span>Usernames are case-sensitive</span>
				</div>
				<sl-input type="text"
					id="username"
					label="Username"
					placeholder="username"
					autocapitalize="off"
					autocorrect="off"
					autocomplete="username"
					inputmode="text"
					minlength="1"
					maxlength="30"
					autofocus
					?disabled=${this.waiting}
				></sl-input>
				<sl-input type="password"
					id="password"
					label="Password"
					autocapitalize="off"
					autocorrect="off"
					autocomplete="current-password"
					inputmode="text"
					minlength="8"
					password-toggle
					?disabled=${this.waiting}
					@keydown=${this.#submitOnEnter.bind( this, this.#signIn.bind( this ) )}
				></sl-input>
				<div></div>
				<sl-button variant="primary" @click=${this.#signIn} ?loading=${this.waiting}>Sign In</sl-button>
				<div></div>
				<span>Forgot password? Click <a href="javascript:void(0)" @click=${this.#switchView.bind( this, DialogView.PasswordResetView )}>here</a> to request a password reset.</span>
				<div></div>
				<span>Don't have an RHDC account yet? Click <a href="#" @click=${this.#switchView.bind( this, DialogView.RegisterView )}>here</a> to register a new account.</span>
			</div>
		`;
	}

	#renderRegisterView() : HTMLTemplateResult {
		const generatePasswordButton = hasCryptoSupport ? html`
			<sl-button @click=${this.#generatePassword} ?disabled=${this.waiting}>Generate Password</sl-button>
		` : nothing;

		return html`
			<div class="register-view">
				<sl-input type="text"
					id="username"
					label="Username"
					placeholder="username"
					autocapitalize="off"
					autocorrect="off"
					autocomplete="username"
					inputmode="text"
					minlength="1"
					maxlength="30"
					autofocus
					?disabled=${this.waiting}
				></sl-input>
				<sl-input type="email"
					id="email"
					label="Email"
					placeholder="email"
					autocapitalize="off"
					autocorrect="off"
					autocomplete="email"
					inputmode="email"
					minlength="5"
					?disabled=${this.waiting}
				></sl-input>
				<sl-input type="password"
					id="password"
					label="Password"
					autocapitalize="off"
					autocorrect="off"
					autocomplete="new-password"
					inputmode="text"
					minlength="8"
					password-toggle
					?disabled=${this.waiting}
				></sl-input>
				<sl-input type="password"
					id="confirm-password"
					label="${'Verify\nPassword'}"
					autocapitalize="off"
					autocorrect="off"
					autocomplete="off"
					inputmode="text"
					minlength="8"
					password-toggle
					?disabled=${this.waiting}
				></sl-input>
				<div class="captcha">
					<img src=${guard( this.captchaImage, () => this.captchaImage.toString() )} decoding="sync">
					<sl-button ?disabled=${this.waiting} @click=${this.#requestNewCaptcha}>
						<sl-icon name="arrow-clockwise" label="request a different CAPTCHA"></sl-icon>
					</sl-button>
				</div>
				<sl-input type="text"
					id="captcha"
					label="Enter the above CAPTCHA (all letters are lower case)"
					placeholder="captcha"
					autocapitalize="off"
					autocorrect="off"
					autocomplete="off"
					inputmode="text"
					minlength="6"
					maxlength="6"
					?disabled=${this.waiting}
					@keydown=${this.#submitOnEnter.bind( this, this.#register.bind( this ) )}
				></sl-input>
				<div></div>
				<div class="register-button-tray">
					${generatePasswordButton}
					<sl-button variant="primary" @click=${this.#register} ?loading=${this.waiting}>Register</sl-button>
				</div>
				<div></div>
				<span>Already have an account? Click <a href="javascript:void(0)" @click=${this.#switchView.bind( this, DialogView.LoginView )}>here</a> to sign in.</span>
			</div>
		`;
	}

	#renderResetPasswordView() : HTMLTemplateResult {
		return html`
			<div class="reset-password-view">
				<p>Enter your e-mail below. A password reset link will be sent to your e-mail.</p>
				<sl-input type="email"
					id="email"
					label="Email"
					placeholder="email"
					autocapitalize="off"
					autocorrect="off"
					autocomplete="email"
					inputmode="email"
					minlength="5"
					?disabled=${this.waiting}
					@keydown=${this.#submitOnEnter.bind( this, this.#resetPassword.bind( this ) )}
				></sl-input>
				<sl-button variant="primary" @click=${this.#resetPassword} ?loading=${this.waiting}>Request Password Reset</sl-button>
				<span>Remembered your password? <a href="javascript:void(0)" @click=${this.#switchView.bind( this, DialogView.LoginView )}>here</a> to return to sign in.</span>
			</div>
		`;
	}

	public show( redirect: Weak<string> ) : void {
		this.#redirect = redirect;
		this.view = DialogView.LoginView;
		this.dialog?.show();
	}

	async #signIn() : Promise<void> {
		this.waiting = true;
		try {
			await Auth.loginAsync( this.usernameInput.value, this.passwordInput.value );
			if( this.#redirect ) this.navigate( this.#redirect );
			this.dialog?.hide();
		} catch( errorMessage: unknown ) {
			this.toastError( 'Failed to sign in', errorMessage as string );
		}
		this.waiting = false;
	}

	async #register() : Promise<void> {
		if( this.passwordInput.value !== this.passwordConfirmInput.value ) {
			this.toastError( 'Registration Failed', 'Passwords do not match.' );
			return;
		}

		this.waiting = true;
		try {
			await Auth.registerAccount( this.usernameInput.value, this.emailInput.value, this.passwordInput.value, this.captchaInput.value );
			await Auth.loginAsync( this.usernameInput.value, this.passwordInput.value );
			this.dialog?.hide();
			this.view = DialogView.LoginView;
		} catch( errorMessage: unknown ) {
			this.toastError( 'Registration Failed', errorMessage as string );
			this.#requestNewCaptcha();
		} finally {
			this.waiting = false;
		}
	}

	async #resetPassword() : Promise<void> {
		if( !this.emailInput.reportValidity() ) return;

		this.waiting = true;
		try {
			await Auth.requestPasswordResetAsync( this.emailInput.value );
			this.dialog?.hide();
			this.view = DialogView.LoginView;
		} catch( exception: unknown ) {
			this.toastError( 'Failed to send password reset link', getErrorMessage( exception ) );
		} finally {
			this.waiting = false;
		}
	}

	#switchView( view: DialogView ) : void {
		this.view = view;
		if( view === DialogView.RegisterView ) {
			this.#requestNewCaptcha();
		}
	}

	async #requestNewCaptcha() : Promise<void> {
		const image = await Auth.requestCaptcha();
		this.captchaImage = await new Promise( resolve => {
			const fileReader = new FileReader();
			fileReader.readAsDataURL( image );
			fileReader.addEventListener( 'loadend', () => {
				resolve( new String( fileReader.result || 'about:blank' ) );
			});
		});
	}

	#generatePassword() : void {
		this.passwordInput.value = generatePassword();
		if( this.passwordConfirmInput ) {
			this.passwordConfirmInput.value = '';
		}

		this.passwordInput.passwordVisible = true;
		this.passwordInput.focus();
	}

	#submitOnEnter( action: () => unknown, event: KeyboardEvent ) : void {
		if( event.key === 'Enter' && !event.metaKey && !event.ctrlKey && !event.shiftKey && !event.altKey ) action();
	}

}
