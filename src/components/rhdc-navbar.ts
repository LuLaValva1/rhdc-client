import { css, CSSResultGroup, html, HTMLTemplateResult, nothing, PropertyValues } from 'lit';
import { customElement, property, state, query } from 'lit/decorators.js';
import { rhdcMediaQuery } from '../decorators/media-query';
import { RhdcElement } from '../rhdc-element';
import { Theme } from '../util/theme';
import { HacksApi } from '../apis/hacks-api';
import { rhdcUserContext, UserAuth } from '../decorators/user-context';
import { Nullable, Optional } from '../util/types';
import { Auth } from '../auth/auth';
import { NotificationsApi, NotificationSettings } from '../apis/notifications-api';
import { RhdcModqueueStatusChangeEvent } from '../events/rhdc-modqueue-status-change-event';
import { getErrorMessage } from '../util/http-error-parser';
import { ModqueueStatus } from '../apis/modqueue-api';
import { Role } from '../auth/roles';
import { DEV_MODE } from '../config';
import SlDialog from '@shoelace-style/shoelace/dist/components/dialog/dialog';
import SlCheckbox from '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/button-group/button-group';
import '@shoelace-style/shoelace/dist/components/icon-button/icon-button';
import '@shoelace-style/shoelace/dist/components/tooltip/tooltip';
import '@shoelace-style/shoelace/dist/components/dropdown/dropdown';
import '@shoelace-style/shoelace/dist/components/menu/menu';
import '@shoelace-style/shoelace/dist/components/menu-item/menu-item';
import '@shoelace-style/shoelace/dist/components/divider/divider';
import '@shoelace-style/shoelace/dist/components/dialog/dialog';
import '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '@shoelace-style/shoelace/dist/components/badge/badge';
import '../widgets/rhdc-avatar';
import '../widgets/rhdc-link';
import './rhdc-notifications';

@customElement( 'rhdc-navbar' )
export class RhdcNavbar extends RhdcElement {

	@property({ attribute: false })
	modqueueStatus: Optional<ModqueueStatus>;

	@state()
	fetchingRandomHack = false;

	@state()
	darkMode = Theme.isDarkMode;

	@state()
	notificationSettings: Nullable<NotificationSettings> = null;

	@state()
	saving = false;
	
	@rhdcMediaQuery( 'only screen and (max-width: 630px)' )
	compact!: boolean;

	@rhdcUserContext()
	currentUser!: Nullable<UserAuth>;

	@query( 'sl-dialog' )
	dialog!: SlDialog;

	@query( 'sl-input.search' )
	searchBar!: SlInput;

	@query( '#ns-hack-comment' )
	hackCommentCheckbox!: SlCheckbox;

	@query( '#ns-comment-reply' )
	commentReplyCheckbox!: SlCheckbox;

	#refreshTask = -1;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				justify-content: space-between;
				flex-wrap: wrap;
				align-items: center;
				gap: 6px;

				padding: var(--sl-spacing-2x-small) var(--sl-spacing-x-small);

				background-color: var(--sl-color-neutral-50);
				border-bottom: 1px solid var(--sl-panel-border-color);
			}

			.group {
				display: flex;
				justify-content: space-between;
				align-items: center;
				flex-grow: 1;
				gap: 6px;
			}

			.tight {
				justify-content: unset;
				flex-grow: 0;
			}

			h1 {
				margin: 0;
				font-size: 1.5rem;
				font-family: var(--sl-font-sans);
				color: var(--sl-color-primary-600);
				align-self: flex-end;
			}

			h1[data-dev] {
				color: var(--sl-color-danger-600);
			}

			h1[data-dev]::after {
				content: ' (Dev)';
			}

			h1 > rhdc-link::part(base) {
				color: inherit !important;
				text-decoration: none;
				cursor: pointer;
			}

			sl-input.search {
				flex-grow: 100000;
				max-width: 600px;
			}

			div.spacer {
				flex-grow: 1000;
				flex-shrink: 1000;
			}

			rhdc-avatar {
				--size: var(--sl-input-height-medium);
			}

			sl-icon-button {
				font-size: 1.5em;
			}

			sl-menu {
				margin-top: var(--sl-spacing-2x-small);
			}

			sl-icon-button::part(base) {
				color: var(--sl-color-neutral-1000);
			}

			sl-button:not([slot="footer"])::part(base) {
				color: var(--rhdc-text-color);
			}

			sl-checkbox {
				margin-bottom: var(--sl-spacing-x-small);
			}

			sl-spinner {
				margin: auto;
				font-size: 3rem;
			}

			sl-badge {
				translate: 5px -3px;
				z-index: 100;
			}

			sl-badge[hidden] {
				display: none;
			}

			.logo {
				width: 2rem;
				height: 2rem;
			}

			.home {
				font-size: 1.5rem;
			}

			.modqueue > sl-icon {
				font-size: var(--sl-font-size-x-large);
				vertical-align: middle;
			}

			.modqueue::part(label) {
				padding: 0 var(--sl-spacing-x-small);
			}

			@media only screen and (max-width: 540px) {
				:host {
					flex-wrap: nowrap;
				}

				sl-input.search {
					max-width: calc( 100vw - (2 * var(--sl-spacing-2x-small)) - 24px - var(--sl-input-height-medium) - 4.5rem - (6 * var(--sl-spacing-x-small)) );
				}
			}
		`;
	}

	override connectedCallback() : void {
		super.connectedCallback();
		this.#refreshTask = window.setInterval(
			() => {
				if( this.currentUser && (this.currentUser.role === Role.Moderator || this.currentUser.role === Role.Staff) ) {
					this.dispatchEvent( RhdcModqueueStatusChangeEvent.createAsync() );
				}
			},
			5 * 60 * 1000
		);
	}

	override disconnectedCallback(): void {
		window.clearInterval( this.#refreshTask );
		super.disconnectedCallback();
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'currentUser' ) ) {
			if( this.currentUser && (this.currentUser.role === Role.Moderator || this.currentUser.role === Role.Staff) ) {
				this.dispatchEvent( RhdcModqueueStatusChangeEvent.createAsync() );
			} else {
				this.modqueueStatus = undefined;
			}
		}
	}

	override render() : unknown {
		const themeIcon = this.darkMode ? 'brightness-high' : 'moon-stars';
		const themeTooltip = this.darkMode ? 'Switch to light mode' : 'Switch to dark mode';

		const searchBar = html`
			<sl-input
				class="search"
				type="search"
				placeholder="Search"
				autocapitalize="off"
				autocorrect="off"
				autocomplete="off"
				enterkeyhint="search"
				inputmode="search"
				required
				@keydown=${this.#onSearchKeyDown}
			>
				<sl-icon slot="suffix" name="search"></sl-icon>
			</sl-input>
		`;

		const themeSwitcher = html`
			<sl-tooltip content=${themeTooltip}>
				<sl-button circle @click=${this.#switchTheme}>
					<sl-icon name=${themeIcon} label=${themeTooltip}></sl-icon>
				</sl-button>
			</sl-tooltip>
		`;

		const notifications = this.currentUser ? html`<rhdc-notifications></rhdc-notifications>` : nothing;
		const modqueueButton = this.modqueueStatus ? html`
			<sl-button class="modqueue" @click=${this.navAction( '/modqueue' )}>
				<sl-badge pill
					variant=${this.modqueueStatus.overall.priority}
					?pulse=${this.modqueueStatus.overall.important}
					?hidden=${this.modqueueStatus.overall.count === 0}
				>${this.modqueueStatus.overall.count}</sl-badge>
				<sl-icon name="hammer"></sl-icon>
			</sl-button>
		` : nothing;

		if( this.compact ) {
			let profileOrSignIn: HTMLTemplateResult;
			if( this.currentUser ) {
				profileOrSignIn = html`
					<sl-menu-item @click=${this.navAction(`/user/${this.currentUser.username}`)}>User Profile</sl-menu-item>
					<sl-menu-item @click=${this.#openNotificationSettings}>Notification Settings</sl-menu-item>
					<sl-menu-item @click=${Auth.logout}>Sign Out</sl-menu-item>
				`;
			} else {
				profileOrSignIn = html`
					<sl-menu-item @click=${this.requireLoginAction( null )}>Sign In</sl-menu-item>
				`;
			}

			return html`
				<sl-icon-button class="home" name="house-door" @click=${this.navAction( '/' )}></sl-icon-button>
				${searchBar}
				${themeSwitcher}
				${notifications}
				${modqueueButton}
				<sl-dropdown>
					<sl-icon-button slot="trigger" name="list" label="navigation"></sl-icon-button>
					<sl-menu>
						<sl-menu-item @click=${this.#randomHack}>Random Hack</sl-menu-item>
						<sl-menu-item @click=${this.navAction('/hacks')}>Browse Hacks</sl-menu-item>
						<sl-menu-item @click=${this.navAction('/competitions')}>Competitions</sl-menu-item>
						<sl-menu-item @click=${this.navAction('/leaderboard')}>Leaderboard</sl-menu-item>
						<sl-divider></sl-divider>
						${profileOrSignIn}
					</sl-menu>
				</sl-dropdown>
			`;
		}

		let userNav: HTMLTemplateResult;
		if( this.currentUser ) {
			userNav = html`
				<sl-dropdown>
					<a slot="trigger" href="javascript: void(0)" aria-label="${this.currentUser.username}">
						<rhdc-avatar .username=${this.currentUser.username}></rhdc-avatar>
					</a>
					<sl-menu>
						<sl-menu-item @click=${this.navAction(`/user/${this.currentUser.username}`)}>User Profile</sl-menu-item>
						<sl-menu-item @click=${this.#openNotificationSettings}>Notification Settings</sl-menu-item>
						<sl-menu-item @click=${Auth.logout}>Sign Out</sl-menu-item>
					</sl-menu>
				</sl-dropdown>
			`;
		} else {
			userNav = html`
				<sl-button @click=${this.requireLoginAction( null )}>
					<sl-icon slot="prefix" name="person"></sl-icon>
					<span>Sign In</span>
				</sl-button>
			`;
		}

		const notificationSettings = this.notificationSettings ? html`
			<sl-checkbox checked disabled>
				Notify me when my hack, hack version, starpower submission, or hack ownership claim is approved or rejected.
			</sl-checkbox>
			<sl-checkbox id="ns-hack-comment" ?checked=${this.notificationSettings.hackComment}>
				Notify me when somebody comments on my hack.
			</sl-checkbox>
			<sl-checkbox id="ns-comment-reply" ?checked=${this.notificationSettings.commentReply}>
				Notify me when somebody replies to my comment.
			</sl-checkbox>

			<sl-button slot="footer" variant="primary" @click=${this.#saveNotificationSettings}>Save</sl-button>
			<sl-button slot="footer" variant="neutral" @click=${() => this.dialog.hide()}>Cancel</sl-button>
		` : html`<sl-spinner></sl-spinner>`;

		return html`
			<sl-icon class="logo" src="/assets/icons/rhdc-logo.svg"></sl-icon>
			<h1 ?data-dev=${DEV_MODE}><rhdc-link href="/">ROMHACKING.com</rhdc-link></h1>
			<div class="spacer"></div>
			${searchBar}
			<div class="spacer"></div>
			<div class="tight group">
				${themeSwitcher}
				${notifications}
				${modqueueButton}
			</div>
			<div class="group">
				<sl-button-group>
					<sl-button @click=${this.#randomHack} ?loading=${this.fetchingRandomHack}>Random Hack</sl-button>
					<sl-button @click=${this.navAction('/hacks')}>Browse Hacks</sl-button>
					<sl-button @click=${this.navAction('/competitions')}>Competitions</sl-button>
					<sl-button @click=${this.navAction('/leaderboard')}>Leaderboard</sl-button>
				</sl-button-group>
				${userNav}
			</div>
			<sl-dialog label="Notification Settings">
				${notificationSettings}
			</sl-dialog>
		`;
	}

	async #randomHack() : Promise<void> {
		this.fetchingRandomHack = true;
		try {
			const hackInfo = await HacksApi.getRandomHackAsync();
			this.navigate( `/hack/${hackInfo.slug}` );
		} finally {
			this.fetchingRandomHack = false;
		}
	}

	#switchTheme() : void {
		Theme.switchTheme();
		this.darkMode = Theme.isDarkMode;
	}

	async #openNotificationSettings() : Promise<void> {
		this.dialog.show();
		this.notificationSettings = null;
		try {
			this.notificationSettings = await NotificationsApi.getNotificationSettingsAsync();
		} catch( exception: unknown ) {
			this.toastError( 'Failed to fetch notification settings', getErrorMessage( exception ) );
		}
	}

	async #saveNotificationSettings() : Promise<void> {
		this.saving = true;
		try {
			this.notificationSettings = await NotificationsApi.updateNotificationSettingsAsync({
				hackComment: this.hackCommentCheckbox.checked,
				commentReply: this.commentReplyCheckbox.checked
			});
			this.dialog.hide();
		} catch( exception: unknown ) {
			this.toastError( 'Failed to save notification settings', getErrorMessage( exception ) );
		} finally {
			this.saving = false;
		}
	}

	#onSearchKeyDown( event: KeyboardEvent ) : void {
		if( !this.searchBar.value?.trim() ) return;
		if( event.key === 'Enter' || event.key === 'Accept' ) {
			this.navigate( '/search/' + encodeURIComponent( this.searchBar.value ) );
		}
	}

}
