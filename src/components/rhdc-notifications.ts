import { css, CSSResultGroup, html, HTMLTemplateResult, nothing } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { Notification, NotificationsApi } from '../apis/notifications-api';
import { RhdcElement } from '../rhdc-element';
import { Nullable } from '../util/types';
import { Page } from '../apis/pagination';
import { AsyncPipe } from '../util/async-pipe';
import { rhdcAsyncAppend } from '../util/lit-extensions';
import { getErrorMessage } from '../util/http-error-parser';
import '@shoelace-style/shoelace/dist/components/dropdown/dropdown';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/badge/badge';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/menu/menu';
import '@shoelace-style/shoelace/dist/components/menu-item/menu-item';
import '@shoelace-style/shoelace/dist/components/relative-time/relative-time';
import '../widgets/rhdc-load-more';

@customElement( 'rhdc-notifications' )
export class RhdcNotifications extends RhdcElement {

	@state()
	unread = 0;

	@state()
	notifications = new AsyncPipe<Notification>();

	@state()
	loading = false;

	#intervalId: (NodeJS.Timer | number) = -1;
	#latest: Nullable<Date> = null;
	#latestUnread: Nullable<Date> = null;
	#pendingRefresh = true;
	#currentPage: Nullable<Page<Notification>> = null;

	public override connectedCallback() : void {
		super.connectedCallback();

		this.notifications = new AsyncPipe<Notification>();
		this.#currentPage = null;

		this.#updateNotifications();
		this.#intervalId = setInterval( this.#updateNotifications.bind( this ), 5 * 60 * 1000 );
	}

	public override disconnectedCallback() : void {
		clearInterval( this.#intervalId );
		super.disconnectedCallback();
	}

	static override get styles() : CSSResultGroup {
		return css`
			sl-button > sl-icon {
				font-size: var(--sl-font-size-x-large);
				vertical-align: middle;
			}

			sl-button::part(label) {
				padding: 0 var(--sl-spacing-x-small);
			}

			sl-menu {
				max-width: 400px;
				padding: 0;
				--auto-size-available-height: 500px;
				--auto-size-available-width: 400px;
			}

			sl-menu-item::part(base) {
				border-bottom: 1px solid var(--sl-panel-border-color);
			}

			sl-menu-item[data-unread]::part(base) {
				font-weight: bold;
			}

			sl-menu-item::part(base) {
				padding-top: var(--sl-spacing-small);
			}

			sl-menu-item:last-child::part(base) {
				border-bottom: none;
			}

			sl-menu-item::part(label) {
				white-space: normal;
				margin-left: -0.75em;
				margin-right: -0.75em;
				line-height: 1.375rem;
			}

			sl-menu-item:not(:hover)::part(label), sl-menu-item[disabled]::part(label) {
				color: var(--rhdc-text-color);
			}

			sl-menu-item[disabled]::part(base) {
				cursor: unset;
				opacity: unset;
			}

			sl-menu-item > span {
				white-space: pre-wrap;
			}

			sl-relative-time {
				display: block;
				font-size: var(--sl-font-size-x-small);
				line-height: var(--sl-font-size-x-small);
				margin-bottom: var(--sl-spacing-2x-small);
			}

			sl-relative-time::first-letter {
				text-transform: capitalize;
			}

			sl-badge {
				translate: 5px -3px;
				z-index: 100;
			}
		`;
	}

	override render() : unknown {
		let lazyLoader: unknown = nothing;
		if( !this.#currentPage || this.#currentPage.hasNext ) {
			lazyLoader = html`
				<sl-menu-item disabled>
					<rhdc-load-more ?loading=${this.loading} @rhdc-load-more=${this.#loadPage}></rhdc-load-more>
				</sl-menu-item>
			`;
		}

		let badge: unknown = nothing;
		if( this.unread > 0 ) {
			badge = html`<sl-badge variant="danger" pill pulse>${this.unread}</sl-badge>`;
		}

		let items: HTMLTemplateResult;
		if( this.loading || this.notifications.any() ) {
			items = html`
				${rhdcAsyncAppend( this.notifications, this.#renderNotification.bind( this ) )}
				${lazyLoader}
			`;
		} else {
			items = html`
				<sl-menu-item disabled>
					<span><i>You do not have any notifications.</i></span>
				</sl-menu-item>
			`;
		}

		return html`
			<sl-dropdown placement="bottom-start" @sl-show=${this.#onOpen}>
				<sl-button slot="trigger">
					<sl-icon name="bell" label="Notifications"></sl-icon>
					${badge}
				</sl-button>
				<sl-menu>${items}</sl-menu>
			</sl-dropdown>
		`;
	}

	async #updateNotifications() : Promise<void> {
		const status = await NotificationsApi.getNotificationStatus();
		this.unread = status.unread;
		if( this.#latest && status.latestUnread !== this.#latestUnread ) {
			this.toastInfo( 'New Notifications', 'You have new unread notifications.' );
			this.#pendingRefresh = true;
		} else if( status.latest !== this.#latest ) {
			this.#pendingRefresh = true;
		}
	}

	#onOpen() : void {
		if( !this.#pendingRefresh ) return;

		this.notifications = new AsyncPipe<Notification>();
		this.#currentPage = null;
		this.#pendingRefresh = false;

		this.#loadPage();
	}

	#renderNotification( notification: Notification ) : HTMLTemplateResult {
		return html`
			<sl-menu-item
				?data-unread=${notification.unread}
				?disabled=${!notification.link}
				@click=${notification.link ? this.navAction( notification.link ) : () => null}
			>
				<sl-relative-time .date=${notification.timestamp as Date} sync></sl-relative-time>
				<span>${notification.message}</span>
			</sl-menu-item>
		`;
	}

	async #loadPage() : Promise<void> {
		this.loading = true;
		try {
			this.#currentPage = this.#currentPage ?
				await this.#currentPage!.nextAsync() :
				await NotificationsApi.getNotifications();
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load notifications', getErrorMessage( exception ) );
			this.notifications.end();
			this.#pendingRefresh = true;
			return;
		} finally {
			this.loading = false;
		}
		
		this.notifications.pushMany( this.#currentPage.values );
		if( !this.#currentPage.hasNext ) this.notifications.end();

		if( this.#currentPage.values.some( notif => notif.unread ) ) {
			const status = await NotificationsApi.markAsReadAsync(
				this.#currentPage.values[this.#currentPage.values.length - 1].timestamp
			);
			this.unread = status.unread;
			this.#pendingRefresh = true;
		}
		
	}

}
