import { css, CSSResultGroup, html, PropertyValues } from 'lit';
import { customElement, state, query } from 'lit/decorators.js';
import { RhdcNavEvent } from '../events/rhdc-nav-event';
import { RhdcToastEvent } from '../events/rhdc-toast-event';
import { RhdcRequireLoginEvent } from '../events/rhdc-login-event';
import { RhdcPromptEvent } from '../events/rhdc-prompt-event';
import { RhdcLinkMetadataEvent } from '../events/rhdc-link-metadata-event';
import { RhdcModqueueStatusChangeEvent } from '../events/rhdc-modqueue-status-change-event';
import { RhdcElement } from '../rhdc-element';
import { rhdcUserContext, UserAuth } from '../decorators/user-context';
import { ModqueueStatus } from '../apis/modqueue-api';
import { Auth } from '../auth/auth';
import { Nullable, Optional, Weak } from '../util/types';
import { RhdcLoginDialog } from './rhdc-login-dialog';
import { RhdcConfirmDialog } from './rhdc-confirm-dialog';
import { Role } from '../auth/roles';
import { OpenGraph } from '../util/ogp';
import SlAlert from '@shoelace-style/shoelace/dist/components/alert/alert';
import SlIcon from '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/alert/alert';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import './rhdc-router';
import './rhdc-navbar';
import './rhdc-login-dialog';
import './rhdc-confirm-dialog';

function linkMetadata( event: RhdcLinkMetadataEvent ) : void {
	OpenGraph.setData( event.detail.previewData );
	document.head.querySelector( 'script#rhdc-metadata' )?.remove();
	const linker = document.createElement( 'script' );
	linker.id = 'rhdc-metadata';
	linker.textContent = JSON.stringify( event.detail.linkData );
	linker.setAttribute( 'type', 'application/ld+json' );
	document.head.appendChild( linker );
}

@customElement( 'rhdc-page' )
export class RhdcPage extends RhdcElement {

	@state()
	path: string = document.location.pathname;

	@state()
	show404 = false;

	@state()
	ready = false;

	@state()
	modqueueStatus: Optional<ModqueueStatus>;

	@query( 'rhdc-login-dialog' )
	loginDialog: Weak<RhdcLoginDialog>;

	@query( 'rhdc-confirm-dialog' )
	confirmDialog!: RhdcConfirmDialog;

	@rhdcUserContext()
	currentUser!: Nullable<UserAuth>;

	constructor() {
		super();
		this._navigate = this._navigate.bind( this );
		this._pushToast = this._pushToast.bind( this );
		this._login = this._login.bind( this );
		this._onHistoryNav = this._onHistoryNav.bind( this );
		this._showConfirmPrompt = this._showConfirmPrompt.bind( this );
		this._updateModqueueStatus = this._updateModqueueStatus.bind( this );
		this._redirect404 = this._redirect404.bind( this );

		Auth.initAsync().finally(() => {
			this.ready = true;
		});
	}

	override connectedCallback() {
		super.connectedCallback();
		this.addEventListener( 'rhdc-nav-event', this._navigate );
		this.addEventListener( 'rhdc-toast-event', this._pushToast );
		this.addEventListener( 'rhdc-login-event', this._login );
		this.addEventListener( 'rhdc-prompt-event', this._showConfirmPrompt );
		this.addEventListener( 'rhdc-modqueue-status-change-event', this._updateModqueueStatus );
		this.addEventListener( 'rhdc-404-event', this._redirect404 );
		this.addEventListener( 'rhdc-link-metadata-event', linkMetadata );
		window.addEventListener( 'popstate', this._onHistoryNav );
	}

	override disconnectedCallback() {
		window.removeEventListener( 'popstate', this._onHistoryNav );
		this.removeEventListener( 'rhdc-link-metadata-event', linkMetadata );
		this.removeEventListener( 'rhdc-404-event', this._redirect404 );
		this.removeEventListener( 'rhdc-modqueue-status-change-event', this._updateModqueueStatus );
		this.removeEventListener( 'rhdc-prompt-event', this._showConfirmPrompt );
		this.removeEventListener( 'rhdc-login-event', this._login );
		this.removeEventListener( 'rhdc-toast-event', this._pushToast );
		this.removeEventListener( 'rhdc-nav-event', this._navigate );
		super.disconnectedCallback();
	}

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				font-family: var(--sl-font-sans);
				display: flex;
				flex-direction: column;
				align-items: stretch;
				width: 100%;
				min-height: 100vh;
			}

			rhdc-navbar {
				position: sticky;
				flex-shrink: 1;
				flex-grow: 0;
				top: 0;
				left: 0;
				z-index: calc( var(--sl-z-index-dialog) - 1 );
			}

			rhdc-router {
				flex-grow: 1;
			}

			.unverified-warning {
				margin: var(--sl-spacing-x-small);
			}

			a {
				color: var(--sl-color-primary-600) !important;
			}

			sl-spinner {
				margin-top: 25vh;
				align-self: center;
				font-size: 120px;
				--track-width: 6px;
			}
		`;
	}
	
	override render() : unknown {
		if( !this.ready ) {
			return html`<sl-spinner></sl-spinner>`;
		}

		let unverifiedWarning = html``;
		if( this.currentUser?.role === Role.Unverified ) {
			unverifiedWarning = html`
				<sl-alert variant="warning" class="unverified-warning" open closable>
					<sl-icon slot="icon" name="exclamation-triangle"></sl-icon>
					<span>
						Your account is not verified. You will not be able to submit hacks, leave comments, or track your progress until you verify your e-mail.
						You should have received an e-mail within 10 minutes of registering your account.
						If you did not receive a verification e-mail, click <a href="#" @click=${this.#resendVerificationEmail}>here</a> to send another.
					</span>
				</sl-alert>
			`;
		}

		return html`
			<rhdc-navbar .modqueueStatus=${this.modqueueStatus}></rhdc-navbar>
			<rhdc-login-dialog></rhdc-login-dialog>
			<rhdc-confirm-dialog></rhdc-confirm-dialog>
			${unverifiedWarning}
			<rhdc-router .path=${this.path} ?not-found=${this.show404}></rhdc-router>
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );

		if( changedProperties.has( 'path' ) ) {
			this.show404 = false;
			OpenGraph.clearData();
			document.head.querySelector( 'script#rhdc-metadata' )?.remove();
			document.getElementById( 'meta-robots' )?.setAttribute( 'content', 'index' );
		} else if( changedProperties.has( 'currentUser' ) && this.currentUser ) {
			this.show404 = false;
		}

		const prevContext : Weak<UserAuth> = changedProperties.get( 'currentUser' );
		if( !prevContext || !this.currentUser || prevContext.role === this.currentUser?.role ) return;

		if( this.currentUser.role === Role.Banned ) {
			this._pushToast( RhdcToastEvent.warn( 'Role Changed', 'You have been banned.' ) );
		} else if( prevContext.role === Role.Unverified ) {
			this._pushToast( RhdcToastEvent.success( 'Email Verified', 'Your account has been verified.' ) );
		} else {
			this._pushToast( RhdcToastEvent.info( 'Role Changed', `Your account has been changed to the ${this.currentUser.role} role.` ) );
		}
	}

	_navigate( event: RhdcNavEvent ) : void {
		window.history.pushState( {}, '', event.detail.route );
		this.path = event.detail.route.split( /\?|#/, 1 )[0];
	}

	_redirect404() : void {
		this.show404 = true;
		document.getElementById( 'meta-robots' )?.setAttribute( 'content', 'noindex' );
	}

	_login( event: RhdcRequireLoginEvent ) : void {
		this.loginDialog?.show( event.detail.redirect );
	}

	_pushToast( event: RhdcToastEvent ) : void {
		const toast = document.createElement( 'sl-alert' ) as SlAlert;
		toast.closable = true;
		toast.variant = event.detail.type;
		toast.duration = 3000;

		const icon = document.createElement( 'sl-icon' ) as SlIcon;
		icon.slot = 'icon';
		icon.name = event.detail.icon;
		toast.appendChild( icon );

		if( event.detail.summary ) {
			const summary = document.createElement( 'strong' );
			summary.textContent = event.detail.summary;
			toast.appendChild( summary );

			if( event.detail.description ) {
				toast.appendChild( document.createElement( 'br' ) );
			}
		}

		if( event.detail.description ) {
			toast.appendChild( document.createTextNode( event.detail.description ) );
		}

		document.body.appendChild( toast );
		toast.toast();
	}

	_showConfirmPrompt( event: RhdcPromptEvent ) : void {
		this.confirmDialog.show( event.detail );
	}

	async _updateModqueueStatus( event: RhdcModqueueStatusChangeEvent ) : Promise<void> {
		try {
			this.modqueueStatus = await event.detail.status;
		} catch( exception: unknown ) {
			console.warn( 'Failed to update modqueue status', exception );
		}
	}

	_onHistoryNav() : void {
		this.path = document.location.pathname;
	}

	async #resendVerificationEmail() : Promise<void> {
		try {
			await Auth.sendVerificationEmail();
			this._pushToast( RhdcToastEvent.success( 'Verification Email Sent', 'You should receive an e-mail shortly. It may appear in your junk mail.' ) );
		} catch( errorMessage : unknown ) {
			this._pushToast( RhdcToastEvent.error( 'Failed to Send Email', errorMessage as string ) );
		}
	}
	
}
