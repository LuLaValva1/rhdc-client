import { css, CSSResultGroup, html, HTMLTemplateResult, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { Hack, HackSortField, MatureFilter, HacksApi } from '../../apis/hacks-api';
import { Page } from '../../apis/pagination';
import { Optional } from '../../util/types';
import '../common/rhdc-infinite-scroll';
import '../hacks/rhdc-hack-card';

@customElement( 'rhdc-hack-search' )
export class RhdcHackSearch extends RhdcElement {

	@property({ attribute: false })
	searchTerm!: string;

	@state()
	hacksPromise: Optional<Promise<Page<Hack>>>;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: contents;
			}

			p {
				font-size: var(--sl-font-size-large);
				color: var(--sl-color-neutral-700);
			}
		`;
	}

	override render() : unknown {
		return html`
			<rhdc-infinite-scroll
				.pageGetter=${this.hacksPromise}
				.renderer=${this.#renderHack}
				.errorTitle=${'Failed to get hack search results.'}
			>
				<p slot="no-results">No results found.</p>
			</rhdc-infinite-scroll>
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'searchTerm' ) && this.searchTerm ) {
			this.hacksPromise = HacksApi.searchHacksAsync( this.searchTerm, HackSortField.Popularity, true, MatureFilter.Include );
		}
	}

	#renderHack( hack: Hack ) : HTMLTemplateResult {
		return html`<rhdc-hack-card .hack=${hack}></rhdc-hack-card>`;
	}

}
