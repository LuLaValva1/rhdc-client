import { css, CSSResultGroup, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { User } from '../../apis/users-api';
import { ConstRef } from '../../util/types';
import '@shoelace-style/shoelace/dist/components/card/card';
import '../../widgets/rhdc-avatar';
import '../../widgets/rhdc-link';
import '../common/rhdc-username';

@customElement( 'rhdc-user-search-card' )
export class RhdcUserSearchCard extends RhdcElement {

	@property({ attribute: false })
	user!: ConstRef<User>;

	static override get styles() : CSSResultGroup {
		return css`
			sl-card > div {
				display: flex;
				flex-direction: column;
				align-items: center;
				gap: var(--sl-spacing-x-small);
				color: var(--rhdc-text-color);
			}

			rhdc-avatar {
				--size: 200px;
			}

			b[data-role="Banned"] {
				color: var(--sl-color-red-500);
			}

			b[data-role="Unverified"] {
				color: var(--sl-color-sky-700);
			}

			b[data-role="Restricted"] {
				color: var(--sl-color-amber-500);
			}

			b[data-role="User"] {
				color: var(--sl-color-neutral-1000);
			}

			b[data-role="Power User"] {
				color: var(--sl-color-blue-500);
			}

			b[data-role="Trusted User"] {
				color: var(--sl-color-violet-600);
			}

			b[data-role="Moderator"] {
				color: var(--sl-color-green-500);
			}

			b[data-role="Staff"] {
				color: var(--sl-color-lime-600);
			}
		`;
	}

	override render() : unknown {
		return html`
			<rhdc-link href="/user/${this.user.username}">
				<sl-card>
					<div>
						<rhdc-avatar .username=${this.user.username}></rhdc-avatar>
						<rhdc-username .user=${this.user}></rhdc-username>
						<b data-role="${this.user.role}">${this.user.role}</b>
					</div>
				</sl-card>
			</rhdc-link>
		`;
	}

}

