import { css, CSSResultGroup, html, HTMLTemplateResult, nothing, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { User, UsersApi } from '../../apis/users-api';
import { Page } from '../../apis/pagination';
import { AsyncPipe } from '../../util/async-pipe';
import { rhdcAsyncAppend } from '../../util/lit-extensions';
import { ConstRef, Nullable } from '../../util/types';
import { getErrorMessage } from '../../util/http-error-parser';
import '../../widgets/rhdc-load-more';
import './rhdc-user-search-card';

@customElement( 'rhdc-user-search' )
export class RhdcHackSearch extends RhdcElement {

	@property({ attribute: false })
	searchTerm!: string;

	@state()
	users = new AsyncPipe<User>();

	@state()
	loading = false;

	@state()
	done = false;

	@state()
	usersFound = false;

	#currentPage: Nullable<Page<User>> = null;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-2x-small);
				overflow-y: auto;
				max-height: 300px;
			}

			div {
				display: flex;
				flex-direction: row;
				flex-wrap: wrap;
				align-items: stretch;
				gap: var(--sl-spacing-small);
			}

			p {
				font-size: var(--sl-font-size-large);
				color: var(--sl-color-neutral-700);
			}
		`;
	}

	override render() : unknown {
		if( this.done && !this.usersFound ) {
			return html`<p>No results found.</p>`;
		}

		const loadTrigger = this.done ? nothing : html`
			<rhdc-load-more ?loading=${this.loading} @rhdc-load-more=${this.#loadMore}></rhdc-load-more>
		`;

		return html`
			<div>${rhdcAsyncAppend( this.users, this.#renderUser )}</div>
			${loadTrigger}
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'searchTerm' ) && this.searchTerm ) {
			this.#beginSearch();
		}
	}

	async #beginSearch() : Promise<void> {
		this.users = new AsyncPipe<User>();
		this.done = false;
		this.usersFound = false;
		this.loading = true;
		try {
			this.#currentPage = await UsersApi.searchUsersAsync( this.searchTerm );

			this.users.pushMany( this.#currentPage.values );
			this.usersFound = this.#currentPage.values.length > 0;
			if( !this.#currentPage.hasNext ) {
				this.users.end();
				this.done = true;
			}
		} catch( exception: unknown ) {
			this.toastError( 'Failed to get user search results', getErrorMessage( exception ) );
			this.users.end();
			this.done = true;
		} finally {
			this.loading = false;
		}
	}

	async #loadMore() : Promise<void> {
		if( !this.#currentPage || !this.#currentPage.hasNext ) return;

		this.loading = true;
		try {
			this.#currentPage = await this.#currentPage.nextAsync();
			this.users.pushMany( this.#currentPage.values );
			if( !this.#currentPage.hasNext ) {
				this.users.end();
				this.done = true;
			}
		} catch( exception: unknown ) {
			this.toastError( 'Failed to get more user search results', getErrorMessage( exception ) );
			this.users.end();
			this.done = true;
		} finally {
			this.loading = false;
		}
	}

	#renderUser( user: ConstRef<User> ) : HTMLTemplateResult {
		return html`<rhdc-user-search-card .user=${user}></rhdc-user-search-card>`;
	}

}
