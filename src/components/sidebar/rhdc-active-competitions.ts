import { css, CSSResultGroup, html, HTMLTemplateResult } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { map } from 'lit/directives/map.js';
import { RhdcElement } from '../../rhdc-element';
import { Competition, CompetitionsApi } from '../../apis/competitions-api';
import { ConstArray, ConstRef, Nullable } from '../../util/types';
import { getErrorMessage } from '../../util/http-error-parser';
import '@shoelace-style/shoelace/dist/components/format-date/format-date';
import '@shoelace-style/shoelace/dist/components/divider/divider';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '../../widgets/rhdc-link';

@customElement( 'rhdc-active-competitions' )
export class RhdcActiveCompetitions extends RhdcElement {

	@state()
	competitions: Nullable<ConstArray<Competition>> = null;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				box-sizing: border-box;
				padding: var(--sl-spacing-x-small) 0;
				white-space: normal;
				min-width: 100%;
				width: 0;
			}

			sl-divider:last-of-type {
				display: none;
			}

			rhdc-link {
				font-weight: bold;
			}

			div {
				font-size: var(--sl-font-size-x-small);
				color: var(--sl-color-neutral-600);
			}
		`;
	}

	override render() : unknown {
		if( !this.competitions ) {
			return html`<sl-spinner></sl-spinner>`;
		}

		if( this.competitions.length <= 0 ) {
			return html`<span>There are no active competitions at the moment.</span>`;
		}
		
		return map( this.competitions, this.#renderCompetitionLink );
	}

	override connectedCallback() : void {
		super.connectedCallback();
		this.#initAsync();
	}

	async #initAsync() {
		try {
			this.competitions = await CompetitionsApi.getActiveCompetitionsAsync();
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load active competitions', getErrorMessage( exception ) );
		}
	}

	#renderCompetitionLink( competition: ConstRef<Competition> ) : HTMLTemplateResult {
		return html`
			<rhdc-link href="/competitions/series/${competition.seriesId}#${competition.competitionId}">${competition.name}</rhdc-link>
			<div>Ends <sl-format-date .date=${competition.endDate as Date} month="short" day="numeric" hour="numeric" minute="2-digit" hour-format="12"></sl-format-date></div>
			<sl-divider></sl-divider>
		`;
	}

}
