import { css, CSSResultGroup, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { rhdcUserContext, UserAuth } from '../../decorators/user-context';
import { Nullable } from '../../util/types';
import '@shoelace-style/shoelace/dist/components/button/button';
import './rhdc-active-competitions';

@customElement( 'rhdc-sidebar' )
export class RhdcSidebar extends RhdcElement {

	@property({ attribute: 'mobile', type: Boolean, reflect: true })
	mobile = false;

	@rhdcUserContext()
	currentUser!: Nullable<UserAuth>;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				font-family: var(--sl-font-sans);
				color: var(--rhdc-text-color);
				text-overflow: ellipsis;
				white-space: nowrap;
				padding: var(--sl-spacing-2x-small);
				padding-top: 0;
			}

			:host([mobile]) {
				box-sizing: border-box;
				white-space: normal;
				max-width: 100%;
			}

			h2 {
				font-size: var(--sl-font-size-large);
				margin: var(--sl-spacing-2x-small) calc( -1 * var(--sl-spacing-2x-small) );
				padding: var(--sl-spacing-2x-small) var(--sl-spacing-x-small);
				background-color: var(--sl-color-primary-400);
				color: white;
			}

			h2:first-child {
				margin-top: 0;
			}

			sl-button::part(base) {
				height: unset;
				line-height: unset;
				padding: var(--sl-spacing-x-small) 0;
			}

			sl-button::part(label) {
				width: 100%;
				text-align: left;
				font-weight: bold;
				padding: 0;
			}
		`;
	}

	override render() : unknown {
		return html`
			<h2>Links</h2>
			<sl-button variant="text" size="large" @click=${this.navAction('/gettingstarted')}>Getting Started</sl-button>
			<sl-button variant="text" size="large" href="https://parallel-launcher.ca/" target="_blank">Download Parallel Launcher</sl-button>
			<sl-button variant="text" size="large" @click=${this.requireLoginAction( this.currentUser, '/submit', false, true )}>Submit a Hack</sl-button>
			<sl-button variant="text" size="large" @click=${this.navAction('/rules')}>Rules</sl-button>
			<sl-button variant="text" size="large" @click=${this.navAction('/legal')}>Legal / Privacy Policy</sl-button>
			<h2>Help</h2>
			<sl-button variant="text" size="large" href="https://discord.gg/JdX2UcSyKa" target="_blank">Join the Discord</sl-button>
			<sl-button variant="text" size="large" @click=${this.navAction('/staff')}>About Us</sl-button>
			<h2>Active Competitions</h2>
			<rhdc-active-competitions></rhdc-active-competitions>
		`;
	}

}
