import { css, CSSResultGroup, html, HTMLTemplateResult, PropertyValues } from 'lit';
import { customElement, state, query, property } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { HacksApi, Hack, HackSortField, MatureFilter } from '../../apis/hacks-api';
import { Page } from '../../apis/pagination';
import { Nullable, Optional, UserId } from '../../util/types';
import { RhdcHackSortBar } from '../hacks/rhdc-hack-sort-bar';
import { RhdcInfiniteScroll, RhdcDoneEvent } from '../common/rhdc-infinite-scroll';
import '../common/rhdc-infinite-scroll';
import '../hacks/rhdc-hack-sort-bar';
import '../hacks/rhdc-hack-card';

@customElement( 'rhdc-user-hacks' )
export class RhdcUserHacks extends RhdcElement {

	@property({ attribute: false })
	userSlug!: UserId | string;

	@state()
	hacksPromise: Optional<Promise<Page<Hack>>>;

	@state()
	noResults = false;

	@query( 'rhdc-hack-sort-bar' )
	sortBar!: Nullable<RhdcHackSortBar>;

	@query( 'rhdc-infinite-scroll' )
	scrollArea!: Nullable<RhdcInfiniteScroll<Hack>>;

	#minHeight = 0;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				padding: var(--sl-spacing-small) 0;
				box-sizing: border-box;
			}

			rhdc-infinite-scroll {
				max-height: 600px;
				overflow-y: auto;
				position: relative;
				padding-right: var(--sl-spacing-x-small);
				margin-top: var(--sl-spacing-medium);
			}

			p {
				margin: 0;
				color: var(--sl-color-neutral-600);
			}

			h3 {
				margin: 0;
				padding: 0;
			}
		`;
	}

	override render() : unknown {
		if( this.noResults ) {
			return html`
				<h3>Published Hacks</h3>
				<p>This user has not published any hacks.</p>
			`;
		}

		return html`
			<h3>Published Hacks</h3>
			<rhdc-hack-sort-bar @rhdc-change=${this.#getHacks}></rhdc-hack-sort-bar>
			<rhdc-infinite-scroll
				.pageGetter=${this.hacksPromise}
				.renderer=${this.#renderHack}
				.errorTitle=${'Failed to load hacks.'}
				@rhdc-done=${this.#doneLoading}
				@scroll=${this.#updateScrollBorder}
			></rhdc-infinite-scroll>
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ): void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'userSlug' ) ) {
			this.#getHacks();
		}
	}

	#renderHack( hack: Hack ) : HTMLTemplateResult {
		return html`<rhdc-hack-card .hack=${hack}></rhdc-hack-card>`;
	}

	#doneLoading( event: RhdcDoneEvent ) : void {
		if( event.detail.error ) return;
		this.noResults = !event.detail.count;
	}

	async #getHacks() : Promise<void> {
		this.#minHeight = 0;
		this.noResults = false;
		this.hacksPromise = HacksApi.getUserHacksAsync(
			this.userSlug,
			this.sortBar ? this.sortBar.sortField : HackSortField.DateUploaded,
			this.sortBar ? this.sortBar.sortDescending : true,
			MatureFilter.Include,
		);
	}

	#updateScrollBorder() : void {
		if( this.clientHeight > this.#minHeight ) {
			this.#minHeight = this.clientHeight;
			this.style.minHeight = this.#minHeight + 'px';
		}

		if( !this.scrollArea ) return;

		if( this.scrollArea.clientHeight + this.scrollArea.scrollTop < this.scrollArea.scrollHeight ) {
			this.scrollArea.style.borderBottom = '1px dashed var(--sl-panel-border-color)';
		} else {
			this.scrollArea.style.borderBottom = 'unset';
		}
	}

}

