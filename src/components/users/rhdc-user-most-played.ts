import { css, CSSResultGroup, html, HTMLTemplateResult } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { map } from 'lit/directives/map.js';
import { PlayTimeActivity } from '../../apis/users-api';
import { RhdcElement } from '../../rhdc-element';
import { ConstArray, Optional } from '../../util/types';
import { AlertFactory } from '../../util/alert-factory';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '../../widgets/rhdc-link';

@customElement( 'rhdc-user-most-played' )
export class RhdcUserMostPlayed extends RhdcElement {

	@property({ attribute: false })
	activity: Optional<ConstArray<PlayTimeActivity>>;

	@property({ attribute: 'loading', type: Boolean })
	loading = false;

	static override get styles() : CSSResultGroup {
		return css`
			sl-spinner {
				font-size: 2rem;
			}

			h3 {
				margin: 0;
				padding: 0;
			}

			table {
				border-collapse: collapse;
			}

			td {
				padding: var(--sl-spacing-2x-small) var(--sl-spacing-3x-small);
			}

			td:first-child {
				width: 99%;
				padding-right: var(--sl-spacing-x-small);
			}

			td:last-child {
				white-space: nowrap;
			}

			p {
				color: var(--sl-color-neutral-700);
			}
		`;
	}

	override render() : unknown {
		if( this.loading ) {
			return html`<sl-spinner></sl-spinner>`;
		}

		if( !this.activity ) {
			return AlertFactory.create({
				type: 'danger',
				message: 'Failed to load recent user activity'
			});
		}

		if( this.activity.length === 0 ) {
			return html`
				<h3>Most Played Hacks</h3>
				<p>This user is not tracking their play time.</p>
			`;
		}

		return html`
			<h3>Most Played Hacks</h3>
			<table>
				<tbody>
					${map( this.activity, this.#renderItem.bind( this ) )}
				</tbody>
			</table>
		`;
	}

	#renderItem( item: Readonly<PlayTimeActivity> ) : HTMLTemplateResult {
		return html`
			<tr>
				<td>
					<rhdc-link href=${item.hackHref}>${item.hackName}</rhdc-link>
				</td>
				<td>
					${item.formatPlayTime()}
				</td>
			</tr>
		`;
	}

}
