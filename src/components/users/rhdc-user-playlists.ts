import { css, CSSResultGroup, html, nothing, HTMLTemplateResult, PropertyValues } from 'lit';
import { map } from 'lit/directives/map.js';
import { customElement, property, state, query } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { PlaylistsApi, Playlist } from '../../apis/playlists-api';
import { Hack } from '../../apis/hacks-api';
import { Page } from '../../apis/pagination';
import { Nullable, UserId, ConstArray, Optional } from '../../util/types';
import { getErrorMessage } from '../../util/http-error-parser';
import { rhdcUserContext, UserAuth } from '../../decorators/user-context';
import { RhdcDragEvent } from '../../widgets/rhdc-drag-item';
import { DragUtil } from '../../util/drag-util';
import { Role } from '../../auth/roles';
import { RhdcPlaylistLink } from './rhdc-playlist-link';
import { RhdcHackSortBar } from '../hacks/rhdc-hack-sort-bar';
import { RhdcDoneEvent } from '../common/rhdc-infinite-scroll';
import SlDrawer from '@shoelace-style/shoelace/dist/components/drawer/drawer';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '@shoelace-style/shoelace/dist/components/drawer/drawer';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/icon-button/icon-button';
import '../../widgets/rhdc-drag-item';
import '../common/rhdc-infinite-scroll';
import '../hacks/rhdc-hack-sort-bar';
import '../hacks/rhdc-hack-card';
import './rhdc-playlist-link';

@customElement( 'rhdc-user-playlists' )
export class RhdcUserPlaylists extends RhdcElement {

	@property({ attribute: false })
	userSlug!: UserId | string;

	@state()
	playlists: Nullable<ConstArray<Playlist>> = null;

	@state()
	playlistHacksPromise: Optional<Promise<Page<Hack>>>;

	@state()
	openPlaylist = '';

	@state()
	creating = false;

	@state()
	createLoading = false;

	@state()
	moving = false;

	@state()
	numHidden = 0;

	@query( 'rhdc-hack-sort-bar' )
	sortBar!: RhdcHackSortBar;

	@query( 'sl-drawer' )
	drawer!: SlDrawer;

	@query( '.create > sl-input' )
	createInput!: SlInput;

	@rhdcUserContext()
	userContext!: Nullable<UserAuth>;

	#expectedCount = 0;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-2x-small);
			}

			sl-spinner {
				font-size: 2rem;
			}

			sl-drawer {
				--size: min( 1200px, 100vw );
			}

			sl-drawer::part(base) {
				z-index: var(--sl-z-index-dialog);
			}

			sl-drawer > div {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-2x-small);
			}

			rhdc-hack-sort-bar::part(base) {
				margin-bottom: var(--sl-spacing-small);
			}

			h3 {
				margin: 0;
				padding: 0;
			}

			.left {
				align-self: flex-start;
			}

			.create {
				display: flex;
				align-items: center;
				gap: 1ch;
			}

			.create > sl-icon-button {
				padding: var(--sl-spacing-3x-small);
			}

			.create > sl-input {
				flex-grow: 1;
			}

			rhdc-drag-item::part(base) {
				padding: 0;
			}

			h3, sl-button {
				margin-left: calc( 1rem + var(--sl-spacing-x-small) );
			}

			.missing {
				color: var(--sl-color-neutral-700);
			}

		`;
	}

	override render() : unknown {
		if( !this.playlists ) {
			return html`<sl-spinner></sl-spinner>`;
		}

		let createButton: unknown = nothing;
		if( this.creating ) {
			createButton = html`
				<div class="create">
					<sl-input
						type="text"
						placeholder="Playlist Name"
						required
						autocapitalize="on"
						autocorrect="on"
						autocomplete="off"
						autofocus
						enterkeyhint="done"
						inputmode="text"
						?disabled=${this.createLoading}
						@keydown=${this.#onCreateInput}
					></sl-input>
					<sl-icon-button
						name="check"
						label="Create"
						?disabled=${this.createLoading}
						@click=${this.#createPlaylist}
					></sl-icon-button>
					<sl-icon-button
						name="x"
						label="Cancel"
						?disabled=${this.createLoading}
						@click=${() => this.creating = false}
					></sl-icon-button>
				</div>
			`;
		} else if( this.#canCreatePlaylist() ) {
			createButton = html`
				<sl-button variant="primary" class="left" outline @click=${() => this.creating = true}>
					<sl-icon slot="prefix" name="plus-lg"></sl-icon>
					New Playlist
				</sl-button>
			`;
		}

		let missingHacks: unknown = nothing;
		if( this.numHidden > 0 ) {
			missingHacks = html`<span class="missing">...and ${this.numHidden} more hack${this.numHidden === 1 ? '' : 's'} that you do not have permission to view.</span>`;
		}

		return html`
			<h3>Playlists</h3>
			${map( this.playlists, this.#renderPlaylist.bind( this ) )}
			${createButton}
			<sl-drawer label=${this.openPlaylist}>
				<div>
					<rhdc-hack-sort-bar @rhdc-change=${this.#loadHacks}></rhdc-hack-sort-bar>
					<rhdc-infinite-scroll
						.pageGetter=${this.playlistHacksPromise}
						.renderer=${this.#renderHack}
						.noResultsMessage=${''}
						.errorTitle=${'Failed to load hacks.'}
						@rhdc-done=${this.#hacksLoaded}
					></rhdc-infinite-scroll>
					${missingHacks}
				</div>
			</sl-drawer>
		`;
	}

	#renderPlaylist( playlist: Readonly<Playlist>, index: number ) : HTMLTemplateResult {
		const renderedPlaylist = html`
			<rhdc-playlist-link
				.playlist=${playlist}
				?movable=${playlist.canEdit && !this.moving}
				@rhdc-playlist-clicked=${this.#viewPlaylist}
				@rhdc-playlist-renamed=${this.#playlistChanged.bind( this, index )}
				@rhdc-playlist-deleted=${this.#playlistDeleted.bind( this, index )}
			></rhdc-playlist-link>
		`;

		if( playlist.canEdit && !this.moving ) {
			return html`
				<rhdc-drag-item
					.data=${index}
					.groupId=${'playlists'}
					@rhdc-drag-event=${this.#movePlaylist}
				>${renderedPlaylist}</rhdc-drag-item>`;
		}

		return renderedPlaylist;
	}

	#renderHack( hack: Hack ) : HTMLTemplateResult {
		return html`<rhdc-hack-card .hack=${hack}></rhdc-hack-card>`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'userSlug' ) ) {
			this.#loadPlaylists();
		}
	}

	#canCreatePlaylist() : boolean {
		if( !this.userContext ) return false;
		if( this.userContext.role === Role.Banned ) return false;
		return (
			this.userSlug === 'self' ||
			this.userSlug === 'current' ||
			this.userSlug === this.userContext.userId ||
			this.userSlug === this.userContext.username
		);
	}

	async #loadPlaylists() : Promise<void> {
		this.playlists = null;
		try {
			this.playlists = await PlaylistsApi.getUserPlaylistsAsync( this.userSlug );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load user playlists', getErrorMessage( exception ) );
		}
	}

	#viewPlaylist( event: Event ) : void {
		this.openPlaylist = (event.target as RhdcPlaylistLink).playlist.name;
		this.#expectedCount = (event.target as RhdcPlaylistLink).playlist.hackCount;
		this.drawer.show();
		this.#loadHacks();
	}

	async #loadHacks() : Promise<void> {
		this.numHidden = 0;
		this.playlistHacksPromise = PlaylistsApi.getPlaylistHacksAsync(
			this.userSlug,
			this.openPlaylist,
			this.sortBar.sortField,
			this.sortBar.sortDescending
		);
	}

	#hacksLoaded( event: RhdcDoneEvent ) : void {
		this.numHidden = this.#expectedCount - event.detail.count;
	}

	#playlistChanged( index: number, event: Event ) : void {
		if( !this.playlists ) return;
		const newPlaylists = [ ...this.playlists ];
		newPlaylists.splice( index, 1, (event.target as RhdcPlaylistLink).playlist );
		this.playlists = Object.freeze( newPlaylists );
	}

	#playlistDeleted( index: number ) : void {
		if( !this.playlists ) return;
		const newPlaylists = [ ...this.playlists ];
		newPlaylists.splice( index, 1 );
		this.playlists = Object.freeze( newPlaylists );
	}

	async #createPlaylist() : Promise<void> {
		if( !this.playlists || !this.createInput.reportValidity() ) return;
		
		this.createLoading = true;
		try {
			const playlist = await PlaylistsApi.createPlaylistAsync( this.createInput.value, true );
			this.playlists = Object.freeze( [ ...this.playlists, playlist ] );
			this.creating = false;
		} catch( exception: unknown ) {
			this.toastError( 'Failed to create playlist', getErrorMessage( exception ) );
			this.createInput.focus();
		} finally {
			this.createLoading = false;
		}
	}

	async #movePlaylist( event: RhdcDragEvent ) : Promise<void> {
		if( !this.playlists || this.moving ) return;

		const index = event.detail.source.data as number;
		const before = event.detail.target.data as number + (event.detail.before ? 0 : 1);

		const oldOrder = this.playlists;
		this.playlists = Object.freeze( DragUtil.withMovedElement( this.playlists, index, before ) );
		this.moving = true;
		try {
			this.playlists = await PlaylistsApi.movePlaylistAsync( this.userSlug, index, before );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to move playlist', getErrorMessage( exception ) );
			this.playlists = oldOrder;
		} finally {
			this.moving = false;
		}
	}

	#onCreateInput( event: KeyboardEvent ) : void {
		switch( event.key ) {
			case 'Enter':
			case 'Accept':
				this.#createPlaylist();
				break;
			case 'Escape':
			case 'Esc':
			case 'Cancel':
				this.creating = false;
				break;
			default: return;
		}
	}

}
