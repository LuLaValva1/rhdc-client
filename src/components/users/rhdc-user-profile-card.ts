import { css, CSSResultGroup, html, HTMLTemplateResult, nothing, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { map } from 'lit/directives/map.js';
import { User, UserUpdate, UsersApi, Gender, SocialHandle } from '../../apis/users-api';
import { StarpowerApi } from '../../apis/starpower-api';
import { RhdcElement } from '../../rhdc-element';
import { Nullable, UserId, Weak } from '../../util/types';
import { formatUtcDate } from '../../util/formatting';
import { rhdcLinkStyles } from '../../common-styles';
import { countryList, countryMap, CountryInfo } from '../../util/countries';
import { ImmutableDate } from '../../util/time';
import { rhdcUserContext, UserAuth } from '../../decorators/user-context';
import { Role } from '../../auth/roles';
import { RhdcDateInput } from '../../widgets/rhdc-date';
import { RhdcFileDropEvent } from '../../events/rhdc-file-drop-event';
import { getErrorMessage } from '../../util/http-error-parser';
import { HttpError } from '../../apis/http-client';
import { RhdcLinkMetadataEvent } from '../../events/rhdc-link-metadata-event';
import { AlertFactory } from '../../util/alert-factory';
import { LinkedData } from '../../util/linked-data';
import { OgUser } from '../../util/ogp';
import { invalidateAvatarCache } from '../../widgets/rhdc-avatar';
import generatePassword from '../../util/generate-password';
import SlSelect from '@shoelace-style/shoelace/dist/components/select/select';
import SlTextarea from '@shoelace-style/shoelace/dist/components/textarea/textarea';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import SlDialog from '@shoelace-style/shoelace/dist/components/dialog/dialog';
import '@shoelace-style/shoelace/dist/components/card/card';
import '@shoelace-style/shoelace/dist/components/icon-button/icon-button';
import '@shoelace-style/shoelace/dist/components/select/select';
import '@shoelace-style/shoelace/dist/components/option/option';
import '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '@shoelace-style/shoelace/dist/components/textarea/textarea';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '@shoelace-style/shoelace/dist/components/tooltip/tooltip';
import '@shoelace-style/shoelace/dist/components/relative-time/relative-time';
import '@shoelace-style/shoelace/dist/components/dialog/dialog';
import '../../widgets/rhdc-date';
import '../../widgets/rhdc-avatar';
import '../../widgets/rhdc-file-dropzone';
import '../common/rhdc-username';
import '../common/rhdc-info-bubble';

function makeSimpleRow( leftSide: string | HTMLTemplateResult, rightSide: string | HTMLTemplateResult ) : HTMLTemplateResult {
	return html`<tr><td>${leftSide}</td><td>${rightSide}</td></tr>`;
}

function getPronouns( gender: Gender ) : string {
	switch( gender ) {
		case Gender.Masculine: return 'he/him';
		case Gender.Feminine: return 'she/her';
		case Gender.Neuter: return 'they/them';
		default: return '';
	}
}

function renderSocialLink( icon: string, handle: Readonly<SocialHandle> ) : HTMLTemplateResult {
	return html`<a href=${handle.href}><sl-icon name="${icon}"></sl-icon>&nbsp;${handle.name}</a>`;
}

function renderCountry( code: string ) : HTMLTemplateResult | string {
	const country = countryMap.get( code );
	if( !country ) return code;
	return html`<span><span class="flag">${country.flag}</span>&nbsp;${country.name}</span>`;
}

function renderCountryItem( country: CountryInfo ) : HTMLTemplateResult {
	return html`
		<sl-option value=${country.code}>
			<span slot="prefix" class="flag">${country.flag}</span>
			${country.name}
		</sl-option>
	`;
}

// workaround limitation with sl-option not supporting spaces
function roleToRoleOption( role: Role ) : string {
	return role.replace( ' ', '_' );
}
function roleOptionToRole( role: string ) : Role {
	return role.replace( '_', ' ' ) as Role;
}

@customElement( 'rhdc-user-profile-card' )
export class RhdcUserProfileCard extends RhdcElement {

	@property({ attribute: false })
	userSlug!: string | UserId;

	@rhdcUserContext()
	userContext!: Nullable<UserAuth>;

	@state()
	user: Nullable<User> = null;

	@state()
	playTime: Nullable<number> = 0;

	@state()
	editing = false;

	@state()
	loading = false;

	@state()
	dialogLoading = false;

	@state()
	uploadingAvatar = false;

	@state()
	showAge = false;

	@state()
	renameLockoutExpires: Nullable<Date> = null;

	static override get styles() : CSSResultGroup {
		return css`
			${rhdcLinkStyles}

			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-x-small);
			}

			:host > div {
				display: flex;
				gap: var(--sl-spacing-x-small);
			}

			sl-card > sl-spinner {
				font-size: 3rem;
			}

			sl-card > div {
				display: flex;
				align-items: flex-start;
				gap: var(--sl-spacing-medium);
			}

			.vertical {
				flex-direction: column;
				gap: var(--sl-spacing-medium);
				flex-grow: 0;
			}

			rhdc-avatar {
				flex-grow: 0;
				--size: 200px;
			}

			rhdc-file-dropzone {
				width: 200px;
				height: 200px;
				margin: 0;
				--padding: var(--sl-spacing-x-small);
			}

			rhdc-file-dropzone > span {
				font-size: var(--sl-font-size-large);
			}

			sl-card > div > div {
				flex-grow: 1;
				display: flex;
				flex-direction: column;
				align-items: flex-start;
			}

			.header {
				display: flex;
				gap: var(--sl-spacing-x-small);
				font-size: var(--sl-font-size-2x-large);
			}

			.body {
				display: flex;
				gap: var(--sl-spacing-small);
			}

			td:first-child {
				font-weight: bold;
				white-space: nowrap;
				padding-right: var(--sl-spacing-x-small);
				vertical-align: top;
			}

			td:last-child {
				width: 99%;
			}

			b[data-role="Banned"] {
				color: var(--sl-color-red-500);
			}

			b[data-role="Unverified"] {
				color: var(--sl-color-sky-700);
			}

			b[data-role="Restricted"] {
				color: var(--sl-color-amber-500);
			}

			b[data-role="User"] {
				color: var(--sl-color-neutral-1000);
			}

			b[data-role="Power User"] {
				color: var(--sl-color-blue-500);
			}

			b[data-role="Trusted User"] {
				color: var(--sl-color-violet-600);
			}

			b[data-role="Moderator"] {
				color: var(--sl-color-green-500);
			}

			b[data-role="Staff"] {
				color: var(--sl-color-lime-600);
			}

			.star {
				color: var(--sl-color-amber-500);
			}
			
			.red-star {
				color: var(--sl-color-rose-600);
			}

			.bio {
				display: -webkit-box;
				box-orient: vertical;
				-webkit-box-orient: vertical;
				font-weight: normal;
				white-space: pre-wrap;
				line-clamp: 10;
				-webkit-line-clamp: 10;
				overflow: hidden;
				text-overflow: ellipsis;
			}

			.socials {
				display: flex;
				flex-wrap: wrap;
				gap: var(--sl-spacing-medium);
			}

			.socials-edit {
				display: flex;
				flex-wrap: wrap;
				align-items: flex-start;
				gap: var(--sl-spacing-small);
			}

			.socials-edit > table {
				flex-grow: 1;
				max-width: 311px;
			}

			.flag {
				font-family: EmojiFallback, var(--sl-font-sans);
			}

			.button-tray {
				display: flex;
				flex-direction: row-reverse;
				gap: var(--sl-spacing-x-small);
				align-self: stretch;
			}

			sl-input[data-user-invalid]:not(:focus)::part(base) {
				border-color: var(--sl-color-danger-500);
			}

			.grey {
				color: var(--sl-color-neutral-600);
			}

			sl-dialog > div {
				display: flex;
				flex-direction: column;
				align-items: flex-start;
				gap: var(--sl-spacing-x-small);
			}

			sl-dialog sl-input {
				min-width: 350px;
			}

			sl-alert {
				align-items: stretch;
			}

			sl-icon-button {
				font-size: 1.5rem;
			}

			a:not(:hover):not(:focus) > sl-icon {
				color: var(--rhdc-text-color);
			}

			.edit td {
				padding-bottom: var(--sl-spacing-x-small);
			}

			sl-select {
				max-width: 300px;
			}

			.bio-desktop {
				flex-grow: 1;
			}

			.bio-mobile {
				display: none;
			}

			@media only screen and (max-width: 781px) {
				.bio-mobile {
					display: table-row;
				}

				.bio-desktop {
					display: none;
				}
			}

		`;

	}

	override render() : unknown {
		if( !this.user ) {
			return html`<sl-card><sl-spinner></sl-spinner></sl-card>`;
		}

		const card = this.editing ? this.#renderEditView( this.user ) : this.#renderReadonlyView( this.user );
		let buttonTray: unknown = nothing;
		if( this.user.canEdit && this.user.userId === this.userContext?.userId ) {
			let renameButton: HTMLTemplateResult;
			if( !this.renameLockoutExpires ) {
				renameButton = html`<sl-button variant="primary" loading>Change Username</sl-button>`;
			} else if( this.renameLockoutExpires < new Date() ) {
				renameButton = html`<sl-button variant="primary" @click=${this.#changeUsername}>Change Username</sl-button>`;
			} else {
				renameButton = html`
					<sl-tooltip>
						<span slot="content">
							<span>Username changes may only be done once every 6 months. You will be able to change your username again </span>
							<sl-relative-time .date=${this.renameLockoutExpires}></sl-relative-time>.
						</span>
						<sl-button variant="primary" disabled>Change Username</sl-button>
					</sl-tooltip>
				`;
			}

			const nameChangeAlert = AlertFactory.create({
				type: 'primary',
				message: 'After changing your username, you must wait 6 months before you may change it again.'
			});

			buttonTray = html`
				<div>
					${renameButton}
					<sl-button variant="primary" @click=${this.#changePassword}>Change Password</sl-button>
				</div>
				<sl-dialog id="change-username-dialog" label="Change Username">
					<div>
						${nameChangeAlert}
						<div></div>
						<sl-input id="password"
							type="password"
							label="Verify Password"
							placeholder="Password"
							password-toggle
							?disabled=${this.dialogLoading}
							minlength="8"
							required
							autocapitalize="off"
							autocorrect="off"
							autocomplete="current-password"
							enterkeyhint="done"
							inputmode="text"
						></sl-input>
						<sl-input id="new-username"
							type="text"
							label="New Username"
							placeholder="New Username"
							?disabled=${this.dialogLoading}
							minlength="1"
							required
							autocapitalize="off"
							autocorrect="off"
							autocomplete="username"
							enterkeyhint="done"
							inputmode="text"
						></sl-input>
						<div></div>
						<div slot="footer" class="button-tray">
							<sl-button variant="primary" @click=${this.#saveUsernameChange} ?loading=${this.dialogLoading}>Apply</sl-button>
							<sl-button variant="neutral" @click=${this.#closeDialog} ?disabled=${this.dialogLoading}>Cancel</sl-button>
						</div>
					</div>
				</sl-dialog>
				<sl-dialog id="change-password-dialog" label="Change Password">
					<div>
						<sl-input id="old-password"
							type="password"
							label="Current Password"
							placeholder="Current Password"
							password-toggle
							?disabled=${this.dialogLoading}
							minlength="8"
							required
							autocapitalize="off"
							autocorrect="off"
							autocomplete="current-password"
							enterkeyhint="done"
							inputmode="text"
						></sl-input>
						<div></div>
						<sl-input id="new-password"
							type="password"
							label="New Password"
							placeholder="New Password"
							password-toggle
							?disabled=${this.dialogLoading}
							minlength="8"
							required
							autocapitalize="off"
							autocorrect="off"
							autocomplete="new-password"
							enterkeyhint="done"
							inputmode="text"
						></sl-input>
						<sl-input id="retype-password"
							type="password"
							label="New Password (Verify)"
							placeholder="New Password (Verify)"
							password-toggle
							?disabled=${this.dialogLoading}
							minlength="8"
							required
							autocapitalize="off"
							autocorrect="off"
							autocomplete="off"
							enterkeyhint="done"
							inputmode="text"
						></sl-input>
						<sl-button variant="primary" ?disabled=${this.dialogLoading} @click=${this.#generatePassword}>Generate Password</sl-button>
						<div></div>
						<div slot="footer" class="button-tray">
							<sl-button variant="primary" @click=${this.#savePasswordChange} ?loading=${this.dialogLoading}>Apply</sl-button>
							<sl-button variant="neutral" @click=${this.#closeDialog} ?disabled=${this.dialogLoading}>Cancel</sl-button>
						</div>
					</div>
				</sl-dialog>
			`;
		}

		return html`${card}${buttonTray}`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );

		if( changedProperties.has( 'userSlug' ) && this.userSlug ) {
			this.#loadUser();
			this.playTime = null;
			StarpowerApi.getUserTotalPlayTimeAsync( this.userSlug ).then( playTime => this.playTime = playTime );
		}

		if( changedProperties.has( 'user' ) || changedProperties.has( 'userContext' ) ) {
			this.renameLockoutExpires = null;
			UsersApi.canRenameSelfAsync().then(
				() => this.renameLockoutExpires = new Date( 0 )
			).catch( (exception: unknown) => {
				if( exception instanceof HttpError ) {
					const retryAfter = exception.tryGetHeader( 'Retry-After' );
					if( retryAfter && !Number.isNaN( +retryAfter ) ) {
						this.renameLockoutExpires = new Date( Date.now() + Math.floor( 1000 * (+retryAfter) ) );
						return;
					}
				}

				console.error( exception );
			});
		}
	}

	#renderReadonlyView( user: User ) : HTMLTemplateResult {
		let editButton: unknown = nothing;
		if( user.canEdit ) {
			editButton = html`<sl-icon-button name="pencil" @click=${() => this.editing = true}></sl-icon-button>`;
		}

		let age = 0;
		if( user.birthday ) {
			const now = new Date();
			age = now.getUTCFullYear() - user.birthday.getUTCFullYear();
			if(
				now.getUTCMonth() < user.birthday.getUTCMonth() ||
				(
					now.getUTCMonth() === user.birthday.getUTCMonth() &&
					now.getUTCDate() < user.birthday.getUTCDate()
				)
			) age--;
			if( age < 0 ) age = 0;
		}

		let desktopBio: unknown = nothing;
		if( user.bio ) {
			desktopBio = html`
				<div class="bio-desktop">
					<b>Bio</b><br/>
					<span class="bio">${user.bio}</span>
				</div>
			`;
		}

		const roleRow = makeSimpleRow( 'Role', html`<b data-role=${user.role}>${user.role}</b>` );
		const ageRow = user.birthday ? makeSimpleRow( 'Age', age.toString() ) : nothing;
		const locationRow = user.country ? makeSimpleRow( 'Location', renderCountry( user.country ) ) : nothing;
		const pronounsRow = user.gender ? makeSimpleRow( 'Pronouns', getPronouns( user.gender ) ) : nothing;
		const joinDateRow = makeSimpleRow( 'Joined', formatUtcDate( user.joinedDate ) );
		const starPointsRow = makeSimpleRow( 'Star Points', html`${user.starPoints} <sl-icon name="star-fill" class="star"></sl-icon>` );
		const kaizoPointsRow = makeSimpleRow( 'Kaizo Points', html`${user.kaizoStarPoints} <sl-icon name="star-fill" class="red-star"></sl-icon>` );
		const starsCollectedRow = makeSimpleRow( 'Stars Found', html`${user.starsCollected} <sl-icon name="star-fill" class="star"></sl-icon>` );
		const playTimeRow = makeSimpleRow( 'Play Time', this.#renderPlayTime() );
		const bioRow = user.bio ? html`<tr class="bio-mobile"><td>Bio</td><td><span class="bio">${user.bio}</span></td></tr>` : nothing;

		const twitch = user.social.twitch ? renderSocialLink( 'twitch', user.social.twitch ) : nothing;
		const youtube = user.social.youtube ? renderSocialLink( 'youtube', user.social.youtube ) : nothing;
		const twitter = user.social.twitter ? renderSocialLink( 'twitter', user.social.twitter ) : nothing;
		const gitlab = user.social.gitlab ? html`<a href=${user.social.gitlab.href}><sl-icon src="/assets/icons/gitlab.svg"></sl-icon>&nbsp;${user.social.gitlab.name}</a>` : nothing;
		const github = user.social.github ? renderSocialLink( 'github', user.social.github ) : nothing;

		return html`
			<sl-card>
				<div>
					<rhdc-avatar .username=${user.username}></rhdc-avatar>
					<div>
						<div class="header">
							<rhdc-username .user=${user}></rhdc-username>
							${editButton}
						</div>
						<div class="body">
							<table>
								<tbody>
									${roleRow}
									${ageRow}
									${locationRow}
									${pronounsRow}
									${joinDateRow}
									${starPointsRow}
									${kaizoPointsRow}
									${starsCollectedRow}
									${playTimeRow}
									${bioRow}
								</tbody>
							</table>
							${desktopBio}
						</div>
						<div class="socials">
							${twitch} ${youtube} ${twitter} ${gitlab} ${github}
						</div>
					</div>
				</div>
			</sl-card>
		`;
	}

	#renderEditView( user: User ) : HTMLTemplateResult {
		let roleRow: HTMLTemplateResult;
		if(
			this.userContext?.role === Role.Staff ||
			(
				this.userContext?.role === Role.Moderator &&
				user.role !== Role.Moderator &&
				user.role !== Role.Staff
			)
		) {
			roleRow = html`
				<tr>
					<td>Role</td>
					<td>
						<sl-select id="edit-role" .value=${roleToRoleOption( user.role )} ?disabled=${this.loading}>
							<sl-option value="${roleToRoleOption( Role.Banned )}">Banned</sl-option>
							<sl-option value="${roleToRoleOption( Role.Unverified )}">Unverified</sl-option>
							<sl-option value="${roleToRoleOption( Role.Restricted )}">Restricted</sl-option>
							<sl-option value="${roleToRoleOption( Role.User )}">User</sl-option>
							<sl-option value="${roleToRoleOption( Role.PowerUser )}">Power User</sl-option>
							<sl-option value="${roleToRoleOption( Role.TrustedUser )}">Trusted User</sl-option>
							<sl-option value="${roleToRoleOption( Role.Moderator )}" ?disabled=${this.userContext.role !== Role.Staff}>Moderator</sl-option>
							<sl-option value="${roleToRoleOption( Role.Staff )}" ?disabled=${this.userContext.role !== Role.Staff}>Staff</sl-option>
						</sl-select>
					</td>
				</tr>
			`;
		} else {
			roleRow = html`
				<tr>
					<td>Role</td>
					<td><b data-role="${user.role}">${user.role}</b></td>
				</tr>
			`;
		}

		return html`
			<sl-card>
				<div>
					<div class="vertical">
						<rhdc-avatar .username=${user.username}></rhdc-avatar>
						<rhdc-file-dropzone accept="image/png" ?disabled=${this.uploadingAvatar} @rhdc-file-drop-event=${this.#uploadAvatar}>
							<span>Upload<br/>Avatar</span>
						</rhdc-file-dropzone>
					</div>
					<div>
						<div class="header">
							<rhdc-username .user=${user}></rhdc-username>
						</div>
						<table>
							<tbody class="edit">
								${roleRow}
								<tr>
									<td>Birthday</td>
									<td>	
										<sl-checkbox ?checked=${this.showAge} @sl-change=${() => this.showAge = !this.showAge}>Make my birthday and age public</sl-checkbox>
										<br/>
										<rhdc-date
											id="edit-birthday"
											.value=${user.birthday ? new ImmutableDate( user.birthday, true ) : ImmutableDate.today( true )}
											?disabled=${this.loading || !this.showAge}
											utc
										></rhdc-date>
									</td>
								</tr>
								<tr>
									<td>Location</td>
									<td>
										<sl-select id="edit-country" .value=${user.country || ''} ?disabled=${this.loading} clearable>
											<sl-option value="">Not Specified</sl-option>
											${map( countryList, renderCountryItem )}
										</sl-select>
									</td>
								</tr>
								<tr>
									<td>Pronouns</td>
									<td>
										<sl-select id="edit-gender" .value=${user.gender || ''} ?disabled=${this.loading} clearable>
											<sl-option value="">Not Specified</sl-option>
											<sl-option value="${Gender.Masculine}">he/him</sl-option>
											<sl-option value="${Gender.Feminine}">she/her</sl-option>
											<sl-option value="${Gender.Neuter}">they/them</sl-option>
										</sl-select>
									</td>
								</tr>
								<tr>
									<td>Bio</td>
									<td>
										<sl-textarea
											id="edit-bio"
											.value=${user.bio}
											placeholder="You can enter a bio here"
											rows="4"
											resize="vertical"
											?disabled=${this.loading}
											maxlength="4096"
											autocapitalize="on"
											autocorrect="on"
											autocomplete="off"
											enterkeyhint="done"
											inputmode="text"
											spellcheck
										></sl-textarea>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="socials-edit">
							<table>
								<tbody>
									${this.#renderSocialInput( 'edit-twitch', 'twitch', 'Twitch', user.social.twitch?.name, 'username' )}
									${this.#renderSocialInput( 'edit-youtube', 'youtube', 'YouTube', user.social.youtube?.name, '@handle' )}
									${this.#renderSocialInput( 'edit-twitter', 'twitter', 'Twitter', user.social.twitter?.name, '@handle' )}
								</tbody>
							</table>
							<table>
								<tbody>
									${this.#renderSocialInput( 'edit-gitlab', 'gitlab', 'GitLab', user.social.gitlab?.name, 'username' )}
									${this.#renderSocialInput( 'edit-github', 'github', 'GitHub', user.social.github?.name, 'username' )}
								</tbody>
							</table>
						</div>
						<div class="button-tray">
							<sl-button variant="primary" @click=${this.#save} ?loading=${this.loading}>Save</sl-button>
							<sl-button variant="neutral" @click=${() => this.editing = false} ?disabled=${this.loading}>Cancel</sl-button>
						</div>
					</div>
				</div>
			</sl-card>
		`;
	}

	async #loadUser() : Promise<void> {
		try {
			this.user = await UsersApi.getUserAsync( this.userSlug );
			this.showAge = !!this.user.birthday;
			invalidateAvatarCache( this.user.username );

			this.dispatchEvent(
				RhdcLinkMetadataEvent.create(
					LinkedData.fromUser( this.user ),
					new OgUser( this.user )
				)
			);
		} catch{
			this.redirect404();
		}
	}

	async #save() : Promise<void> {
		let birthday: Nullable<Date> = null;
		if( this.showAge ) {
			birthday = this.#getForm<RhdcDateInput>( 'edit-birthday' ).value.toDate();
		}

		const update : UserUpdate = {
			birthday: birthday,
			country: this.#getForm<SlSelect>( 'edit-country' ).value as string || null,
			gender: (this.#getForm<SlSelect>( 'edit-gender' ).value || null) as Nullable<Gender>,
			bio: this.#getForm<SlTextarea>( 'edit-bio' ).value,
			twitch: this.#getForm<SlInput>( 'edit-twitch' ).value || null,
			youtube: this.#getForm<SlInput>( 'edit-youtube' ).value || null,
			twitter: this.#getForm<SlInput>( 'edit-twitter' ).value || null,
			gitlab: this.#getForm<SlInput>( 'edit-gitlab' ).value || null,
			github: this.#getForm<SlInput>( 'edit-github' ).value || null
		};

		const roleEditor = this.#getForm<SlSelect>( 'edit-role' );
		if( roleEditor ) {
			update.role = roleOptionToRole( roleEditor.value as string );
		}

		this.loading = true;
		try {
			this.user = await UsersApi.updateUserAsync( this.userSlug, update );
			this.editing = false;
		} catch( exception: unknown ) {
			if( exception instanceof HttpError ) {
				let invalidForm: Nullable<RhdcDateInput|SlTextarea|SlInput> = null;
				switch( exception.tryGetHeader( 'X-Invalid-Parameter' ) ) {
					case 'birthday': invalidForm = this.#getForm<RhdcDateInput>( 'edit-birthday' ); break;
					case 'bio': invalidForm = this.#getForm<SlTextarea>( 'edit-bio' ); break;
					case 'twitch': invalidForm = this.#getForm<SlInput>( 'edit-twitch' ); break;
					case 'youtube': invalidForm = this.#getForm<SlInput>( 'edit-youtube' ); break;
					case 'twitter': invalidForm = this.#getForm<SlInput>( 'edit-twitter' ); break;
					case 'gitlab': invalidForm = this.#getForm<SlInput>( 'edit-gitlab' ); break;
					case 'github': invalidForm = this.#getForm<SlInput>( 'edit-gitub' ); break;
				}

				if( invalidForm ) {
					invalidForm.setCustomValidity( getErrorMessage( exception ) );
					invalidForm.focus();
					return;
				}
			}

			this.toastError( 'Failed to update user', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}

	}

	async #uploadAvatar( event: RhdcFileDropEvent ) : Promise<void> {
		this.uploadingAvatar = true;
		try {
			await UsersApi.uploadAvatarAsync( this.userSlug, event.detail.file );
			invalidateAvatarCache( this.user!.username );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to update avatar', getErrorMessage( exception ) );
		} finally {
			this.uploadingAvatar = false;
		}
	}

	#renderPlayTime() : HTMLTemplateResult | string {
		if( this.playTime == null ) {
			return html`<sl-spinner></sl-spinner>`;
		} else if( this.playTime < 1000 ) {
			const info = (this.user && this.userContext && this.user.userId === this.userContext.userId) ?
				html`&nbsp;<rhdc-info-bubble>Enable Romhacking.com integration in Parallel Launcher to track your play time.</rhdc-info-bubble>` :
				nothing;
			
			return html`<span class="grey">No play time on record</span>${info}`;
		} else if( this.playTime < 2000 ) {
			return '1 second';
		} else if( this.playTime < 60000 ) {
			return `${Math.floor( this.playTime / 1000 )} seconds`;
		} else if( this.playTime < 120000 ) {
			return '1 minute';
		} else if( this.playTime < 3600000 ) {
			return `${Math.floor( this.playTime / 60000 )} minutes`;
		} else if( this.playTime < 7200000 ) {
			return '1 hour';
		} else {
			return `${Math.floor( this.playTime / 3600000 )} hours`;
		}
	}

	#renderSocialInput( id: string, icon: string, name: string, value: Weak<string>, placeholder: string ) : HTMLTemplateResult {
		const slIcon = (icon === 'gitlab') ?
			html`<sl-icon src="/assets/icons/gitlab.svg"></sl-icon>` :
			html`<sl-icon name="${icon}"></sl-icon>`;

		return html`
			<tr>
				<td>
					${slIcon}
					${name}
				</td>
				<td>
					<sl-input
						id=${id}
						type="text"
						.value=${value || ''}
						?disabled=${this.loading}
						placeholder="${placeholder}"
						autocapitalize="off"
						autocorrect="off"
						autocomplete="username"
						enterkeyhint="next"
						inputmode="text"
					></sl-input>
				</td>
			</tr>
		`;
	}

	#getForm<T extends HTMLElement>( id: string ) : T {
		return this.shadowRoot?.getElementById( id ) as T;
	}

	#changeUsername() : void {
		this.#getForm<SlInput>( 'new-username' ).value = '';
		this.#getForm<SlInput>( 'password' ).value = '';
		this.#getForm<SlDialog>( 'change-username-dialog' ).show();
	}

	#changePassword() : void {
		this.#getForm<SlInput>( 'old-password' ).value = '';
		this.#getForm<SlInput>( 'new-password' ).value = '';
		this.#getForm<SlInput>( 'retype-password' ).value = '';
		this.#getForm<SlDialog>( 'change-password-dialog' ).show();
	}

	#generatePassword() : void {
		const passwordInput = this.#getForm<SlInput>( 'new-password' );
		passwordInput.value = generatePassword();
		passwordInput.passwordVisible = true;
		passwordInput.focus();
		this.#getForm<SlInput>( 'retype-password' ).value = '';
	}
	
	#closeDialog() : void {
		this.#getForm<SlDialog>( 'change-username-dialog' ).hide();
		this.#getForm<SlDialog>( 'change-password-dialog' ).hide();
	}

	async #saveUsernameChange() : Promise<void> {
		const usernameInput = this.#getForm<SlInput>( 'new-username' );
		const passwordInput = this.#getForm<SlInput>( 'password' );

		if( !usernameInput.reportValidity() || !passwordInput.reportValidity() ) return;

		this.dialogLoading = true;
		try {
			await UsersApi.renameSelfAsync( passwordInput.value, usernameInput.value );
			this.userSlug = usernameInput.value;
			this.user = await UsersApi.getUserAsync( this.userSlug );
			this.#closeDialog();
		} catch( exception: unknown ) {
			this.toastError( 'Failed to update username', getErrorMessage( exception ) );
		} finally {
			this.dialogLoading = false;
		}
	}

	async #savePasswordChange() : Promise<void> {
		const oldPasswordInput = this.#getForm<SlInput>( 'old-password' );
		const newPasswordInput = this.#getForm<SlInput>( 'new-password' );
		const verifyPasswordInput = this.#getForm<SlInput>( 'retype-password' );

		verifyPasswordInput.setCustomValidity( '' );
		if(
			!oldPasswordInput.reportValidity() ||
			!newPasswordInput.reportValidity() ||
			!verifyPasswordInput.reportValidity()
		) return;

		if( newPasswordInput.value !== verifyPasswordInput.value ) {
			verifyPasswordInput.setCustomValidity( 'Passwords do not match.' );
			verifyPasswordInput.focus();
			return;
		}

		this.dialogLoading = true;
		try {
			await UsersApi.changePasswordAsync( oldPasswordInput.value, newPasswordInput.value );
			this.#closeDialog();
		} catch( exception: unknown ) {
			this.toastError( 'Failed to change password', getErrorMessage( exception ) );
		} finally {
			this.dialogLoading = false;
		}
	}

}
