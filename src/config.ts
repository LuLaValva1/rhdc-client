declare const __RHDC_CONFIG_API_HOST__ : string;
declare const __RHDC_CONFIG_DEV_MODE__ : boolean;

export const API_HOST = __RHDC_CONFIG_API_HOST__;
export const DEV_MODE = __RHDC_CONFIG_DEV_MODE__;
