import { ReactiveController, ReactiveElement } from 'lit';
import { state } from 'lit/decorators/state.js';
import { Nullable, UserId } from '../util/types';
import { Role } from '../auth/roles';

interface MutableUserAuth {
	userId: UserId;
	username: string;
	role: Role;
}

export type UserAuth = Readonly<MutableUserAuth>;
type DecoratedElement = ReactiveElement & { [index: PropertyKey]: Nullable<UserAuth>; };

interface UserContextListener {
	notifyAuthChanged() : void;
}

const g_listeners = new Set<UserContextListener>();
let g_userContext: Nullable<UserAuth> = null;

class RhdcUserContextController implements ReactiveController, UserContextListener {

	#host: DecoratedElement;
	#prop: PropertyKey;

	constructor( host: ReactiveElement, prop: PropertyKey ) {
		this.#host = host as DecoratedElement;
		this.#prop = prop;
		this.notifyAuthChanged();
	}

	hostConnected() : void {
		g_listeners.add( this );
		this.notifyAuthChanged();
	}

	hostDisconnected() : void {
		g_listeners.delete( this );
	}

	public notifyAuthChanged() : void {
		this.#host[this.#prop] = g_userContext;
	}

}

export function setUserContext( newValue: Nullable<UserAuth> ) : void {
	g_userContext = newValue;
	g_listeners.forEach( listener => listener.notifyAuthChanged() );
}

export function getUserContext() : Nullable<UserAuth> {
	return g_userContext;
}

/**
 * Injects the current user context into the element. This property will update
 * when the user context changes, causing a re-render.
 *
 * Example:
 * ```
 * @rhdcUserContext()
 * userContext!: Nullable<UserAuth>;
 * ```
 * @category Decorator
 */
export function rhdcUserContext() {
	return function( target: ReactiveElement, name: PropertyKey ) {
		const proto = target.constructor as typeof ReactiveElement;
		proto.addInitializer( instance => {
			instance.addController( new RhdcUserContextController( instance, name ) );
		});

		return state({
			hasChanged: (value: Nullable<UserAuth>, oldValue: Nullable<UserAuth>) =>
				value?.role !== oldValue?.role ||
				value?.userId !== oldValue?.userId ||
				value?.username !== oldValue?.username
		})( target, name );
	}
}
