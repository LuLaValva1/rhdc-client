import { Comment } from "../apis/comments-api";

interface RhdcCommentCreatedEventDetails {
	comment: Comment;
}

export class RhdcCommentCreatedEvent extends CustomEvent<RhdcCommentCreatedEventDetails> {

	static create( comment: Comment ) : RhdcCommentCreatedEvent {
		return new RhdcCommentCreatedEvent( 'rhdc-comment-created-event', {
			bubbles: true,
			composed: true,
			detail: { comment: comment }
		});
	}

}

declare global {
	interface GlobalEventHandlersEventMap {
		'rhdc-comment-created-event': RhdcCommentCreatedEvent;
	}
}
