import { Uuid } from "../util/types";

interface RhdcCommentDeletedEventDetails {
	commentId: Uuid;
}

export class RhdcCommentDeletedEvent extends CustomEvent<RhdcCommentDeletedEventDetails> {

	static create( commentId: Uuid ) : RhdcCommentDeletedEvent {
		return new RhdcCommentDeletedEvent( 'rhdc-comment-deleted-event', {
			bubbles: false,
			composed: true,
			detail: { commentId: commentId }
		});
	}

}

declare global {
	interface GlobalEventHandlersEventMap {
		'rhdc-comment-deleted-event': RhdcCommentDeletedEvent;
	}
}
