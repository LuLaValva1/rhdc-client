interface RhdcFileDropEventDetails {
	file: File;
}

export class RhdcFileDropEvent extends CustomEvent<RhdcFileDropEventDetails> {

	static create( file: File ) : RhdcFileDropEvent {
		return new RhdcFileDropEvent( 'rhdc-file-drop-event', {
			bubbles: true,
			composed: true,
			detail: { file: file }
		});
	}

}

declare global {
	interface GlobalEventHandlersEventMap {
		'rhdc-file-drop-event': RhdcFileDropEvent;
	}
}
