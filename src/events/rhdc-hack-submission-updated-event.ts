import { HackSubmissionSession } from '../apis/hack-submission-api';

interface RhdcHackSubmissionUpdatedEventDetails {
	session: HackSubmissionSession;
}

export class RhdcHackSubmissionUpdatedEvent extends CustomEvent<RhdcHackSubmissionUpdatedEventDetails> {

	static create( session: HackSubmissionSession ) : RhdcHackSubmissionUpdatedEvent {
		return new RhdcHackSubmissionUpdatedEvent( 'rhdc-hack-submission-updated-event', {
			bubbles: true,
			composed: true,
			detail: { session: session }
		});
	}

}

declare global {
	interface GlobalEventHandlersEventMap {
		'rhdc-hack-submission-updated-event': RhdcHackSubmissionUpdatedEvent;
	}
}
