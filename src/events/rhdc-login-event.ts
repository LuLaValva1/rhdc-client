import { Weak } from '../util/types';

interface RhdcRequireLoginEventDetails {
	redirect: Weak<string>;
}

export class RhdcRequireLoginEvent extends CustomEvent<RhdcRequireLoginEventDetails> {

	static create( redirect: Weak<string> ) : RhdcRequireLoginEvent {
		return new RhdcRequireLoginEvent( 'rhdc-login-event', {
			bubbles: true,
			composed: true,
			detail: { redirect: redirect }
		});
	}

}

declare global {
	interface GlobalEventHandlersEventMap {
		'rhdc-login-event': RhdcRequireLoginEvent;
	}
}
