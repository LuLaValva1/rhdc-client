import { ModqueueApi, ModqueueStatus } from "../apis/modqueue-api";

interface RhdcModqueueStatusChangeEventDetails {
	status: Promise<ModqueueStatus>;
}

export class RhdcModqueueStatusChangeEvent extends CustomEvent<RhdcModqueueStatusChangeEventDetails> {

	static create( status: ModqueueStatus ) : RhdcModqueueStatusChangeEvent {
		return new RhdcModqueueStatusChangeEvent( 'rhdc-modqueue-status-change-event', {
			bubbles: true,
			composed: true,
			detail: { status: Promise.resolve( status ) }
		});
	}

	static createAsync() : RhdcModqueueStatusChangeEvent {
		return new RhdcModqueueStatusChangeEvent( 'rhdc-modqueue-status-change-event', {
			bubbles: true,
			composed: true,
			detail: { status: ModqueueApi.getQueueStatusAsync() }
		});
	}

}

declare global {
	interface GlobalEventHandlersEventMap {
		'rhdc-modqueue-status-change-event': RhdcModqueueStatusChangeEvent;
	}
}
