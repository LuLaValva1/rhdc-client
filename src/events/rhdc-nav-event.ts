interface RhdcNavEventDetails {
	route: string;
}

export class RhdcNavEvent extends CustomEvent<RhdcNavEventDetails> {

	static create( route: string ) : RhdcNavEvent {
		return new RhdcNavEvent( 'rhdc-nav-event', {
			bubbles: true,
			composed: true,
			detail: { route: route }
		});
	}

}

declare global {
	interface GlobalEventHandlersEventMap {
		'rhdc-nav-event': RhdcNavEvent;
	}
}
