import '@shoelace-style/shoelace/dist/themes/light.css';
import '@shoelace-style/shoelace/dist/themes/dark.css';
import './common.css';

import SlButton from '@shoelace-style/shoelace/dist/components/button/button';

(SlButton.prototype.constructor as unknown as { styles: { cssText: string } }).styles.cssText += `
	slot[part="prefix"], slot[part="suffix"] {
		font-size: var(--sl-font-size-medium);
	}

	:host([size="small"]) slot[part="prefix"], :host([size="small"]) slot[part="suffix"] {
		font-size: var(--sl-font-size-small);
	}

	:host([size="large"]) slot[part="prefix"], :host([size="large"]) slot[part="suffix"] {
		font-size: var(--sl-font-size-large);
	}
`;

import { setBasePath } from '@shoelace-style/shoelace/dist/utilities/base-path.js';
import { Theme } from './util/theme';
import { roleChangeHandler } from './auth/role-change-handler';
import { HttpClient } from './apis/http-client';
import './components/rhdc-page';

setBasePath( '/shoelace' );
HttpClient.registerGlobalResponseHandler( roleChangeHandler );
Theme.init();

if( window.location.origin === 'https://romhacking.com' ) {
	const sitemapRel = document.createElement( 'link' );
	sitemapRel.type = 'application/xml';
	sitemapRel.href = 'https://api.romhacking.com/v3/sitemap/sitemap.xml';
	sitemapRel.crossOrigin = 'anonymous';
	document.head.appendChild( sitemapRel );
}
