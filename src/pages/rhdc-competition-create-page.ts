import { css, CSSResultGroup, html } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { CompetitionsApi } from '../apis/competitions-api';
import { getErrorMessage } from '../util/http-error-parser';
import { Uuid } from '../util/types';
import { RhdcDateTime } from '../widgets/rhdc-date-time';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import SlTextarea from '@shoelace-style/shoelace/dist/components/textarea/textarea';
import SlCheckbox from '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/avatar/avatar';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/textarea/textarea';
import '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '../widgets/rhdc-date-time';

@customElement( 'rhdc-competition-create-page' )
export class RhdcCompetitionCreatePage extends RhdcElement {

	@property({ attribute: false })
	seriesSlug!: string | Uuid;

	@state()
	canOfferPrize = false;

	@state()
	loading = false;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				max-width: min( 100vw, calc( 800px + 2 * var(--sl-spacing-x-small) ) );
				margin: var(--sl-spacing-small) auto;
				padding: var(--sl-spacing-x-small);
				gap: var(--sl-spacing-small);
				box-sizing: border-box;
			}

			.button-tray {
				display: flex;
				flex-direction: row-reverse;
				gap: var(--sl-spacing-x-small);
			}
		`;
	}

	override render() : unknown {
		return html`
			<sl-input type="text"
				id="name"
				label="Title"
				placeholder="Title"
				required
				autocapitalize="on"
				autocorrect="on"
				autocomplete="off"
				autofocus
				enterkeyhint="next"
				spellcheck
				inputmode="text"
				?disabled=${this.loading}
			></sl-input>

			<sl-textarea
				id="description"
				label="Description"
				placeholder="Enter the competition rules and information here."
				rows="6"
				resize="vertical"
				required
				autocapitalize="on"
				autocorrect="on"
				autocomplete="off"
				enterkeyhint="next"
				spellcheck
				inputmode="text"
				?disabled=${this.loading}
			></sl-textarea>

			<rhdc-date-time
				id="startDate"
				label="Start Date (Your Local Time)"
				?disabled=${this.loading}
			></rhdc-date-time>

			<rhdc-date-time
				id="endDate"
				label="End Date (Your Local Time)"
				?disabled=${this.loading}
			></rhdc-date-time>

			<sl-checkbox
				id="hasCashPrize"
				?checked=${this.canOfferPrize}
				?disabled=${this.loading || !this.canOfferPrize}
			>Offers Cash Prize</sl-checkbox>

			<div class="button-tray">
				<sl-button slot="footer" variant="primary" ?loading=${this.loading} @click=${this.#createCompetition}>Create</sl-button>
				<sl-button slot="footer" variant="neutral" ?disabled=${this.loading} @click=${this.navAction( `/competitions/series/${this.seriesSlug}` )}>Cancel</sl-button>
			</div>
		`;
	}

	override connectedCallback() : void {
		super.connectedCallback();
		this.#init();
	}

	async #init() : Promise<void> {
		try {
			const competition = await CompetitionsApi.getCompetitionSeriesAsync( this.seriesSlug );
			this.canOfferPrize = competition.canOfferPrizes;
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load competition series.', getErrorMessage( exception ) );
		}
	}

	async #createCompetition() : Promise<void> {
		const titleInput = this.#getInput<SlInput>( 'name' );
		const descriptionInput = this.#getInput<SlTextarea>( 'description' );

		if(
			!titleInput.reportValidity() ||
			!descriptionInput.reportValidity()
		) return;

		this.loading = true;
		try {
			const competition = await CompetitionsApi.createCompetitionAsync( this.seriesSlug, {
				name: titleInput.value,
				description: descriptionInput.value,
				startDate: this.#getInput<RhdcDateTime>( 'startDate' ).value,
				endDate: this.#getInput<RhdcDateTime>( 'endDate' ).value,
				hasCashPrize: this.canOfferPrize && this.#getInput<SlCheckbox>( 'hasCashPrize' ).checked
			});
			this.navigate( `/competitions/${competition.competitionId}` );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to create competition', getErrorMessage( exception ) );
			this.loading = false;
		}
	}

	#getInput<T extends HTMLElement>( id: string ) : T {
		return this.shadowRoot?.getElementById( id ) as T;
	}

}
