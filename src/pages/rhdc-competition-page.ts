import { css, CSSResultGroup, html, HTMLTemplateResult, nothing, PropertyValues } from 'lit';
import { customElement, state, property, query } from 'lit/decorators.js';
import { map } from 'lit/directives/map.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { RhdcElement } from '../rhdc-element';
import { CompetitionsApi, Competition } from '../apis/competitions-api';
import { Hack } from '../apis/hacks-api';
import { getErrorMessage } from '../util/http-error-parser';
import { getOrdinalSuffix } from '../util/formatting';
import { ConstArray, ConstRef, Nullable, Uuid } from '../util/types';
import { RhdcDateTime } from '../widgets/rhdc-date-time';
import { RhdcDragEvent } from '../widgets/rhdc-drag-item';
import { RhdcDialog } from '../components/common/rhdc-dialog';
import { RhdcMarkdownEditor } from '../widgets/rhdc-markdown-editor';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/divider/divider';
import '@shoelace-style/shoelace/dist/components/format-date/format-date';
import '@shoelace-style/shoelace/dist/components/card/card';
import '../components/hacks/rhdc-hack-card';
import '../components/common/rhdc-dialog';
import '../widgets/rhdc-markdown-editor';
import '../widgets/rhdc-markdown';
import '../widgets/rhdc-drag-item';
import '../widgets/rhdc-date-time';
import '../widgets/rhdc-link';

const RANKED_PLACEHOLDER = Symbol( 'ranked-placeholder' );
const UNRANKED_PLACEHOLDER = Symbol( 'unranked-placeholder' );

function stringCmp( a: string, b: string ) : number {
	const ai = a.toLowerCase();
	const bi = b.toLocaleLowerCase();
	if( ai < bi ) {
		return -1;
	} else if( ai > bi ) {
		return 1;
	} else if( a < b ) {
		return -1;
	} else if( a > b ) {
		return 1;
	} else {
		return 0;
	}
}

@customElement( 'rhdc-competition-page' )
export class RhdcCompetitionPage extends RhdcElement {

	@property({ attribute: false })
	competitionId!: Uuid;

	@state()
	competition: Nullable<Competition> = null;

	@state()
	rankedSubmissions: Nullable<ConstArray<Hack>> = null;

	@state()
	unrankedSubmissions: Nullable<ConstArray<Hack>> = null;

	@state()
	editing = false;

	@state()
	judging = false;

	@state()
	savingPlacements = false;

	@state()
	updating = false;

	@state()
	publishing = false;

	@query( 'sl-input' )
	titleInput!: SlInput;

	@query( 'rhdc-markdown-editor' )
	descriptionInput!: RhdcMarkdownEditor;

	@query( '#start-date' )
	startDateInput!: RhdcDateTime;

	@query( '#end-date' )
	endDateInput!: RhdcDateTime;

	@query( 'rhdc-dialog' )
	deleteDialog!: RhdcDialog;

	#prevRanked: Nullable<ConstArray<Hack>> = null;
	#prevUnranked: Nullable<ConstArray<Hack>> = null;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				max-width: min( 100vw, calc( 1200px + 2 * var(--sl-spacing-x-small) ) );
				margin: auto;
				padding: var(--sl-spacing-x-small);
				gap: var(--sl-spacing-medium);
				box-sizing: border-box;
			}

			.submissions {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-x-small);
			}

			.submission {
				display: flex;
				align-items: center;
				gap: var(--sl-spacing-small);
			}

			.submission > sl-card {
				flex-grow: 1;
			}

			.submission > sl-card::part(base) {
				justify-content: space-around;
				min-height: 4.5rem;
			}

			.placing {
				position: relative;
				max-width: 6rem;
				width: 6rem;
				height: 4.5rem;
				flex-grow: 0;
			}

			.placing > div {
				position: absolute;
				top: 1rem;
				font-weight: bold;
				font-size: 2rem;
				text-align: center;
				vertical-align: middle;
				width: 100%;
				height: 100%;
				z-index: 2;
			}

			.placing[data-place] > div {
				font-size: 1rem;
				color: #1a1a1e;
				top: 0.7rem;
			}

			.placing[data-place="4"] > div, .placing[data-place="5"] > div {
				color: #e8e8ec;
				top: 1rem;
			}

			.submissions[data-not-judged] .placing, .unranked .placing {
				display: none;
			}

			rhdc-hack-card {
				flex-grow: 1;
			}

			.placing > sl-icon {
				position: absolute;
				left: 0.75rem;
				font-size: 4.5rem;
				z-index: 1;
			}

			div[data-place="1"] > sl-icon {
				color: #ffd700;
			}

			div[data-place="2"] > sl-icon {
				color: #c0c0c0;
			}

			div[data-place="3"] > sl-icon {
				color: #cd7f32;
			}

			div[data-place="4"] > sl-icon {
				color: #1d8541;
			}

			div[data-place="5"] > sl-icon {
				color: #5724a5;
			}

			sl-spinner {
				font-size: 2rem;
			}

			rhdc-drag-item[data-placeholder]::part(base) {
				padding: 0;
			}

			rhdc-drag-item[before-only]::part(base) {
				align-self: stretch;
				flex-grow: 1;
				min-height: var(--sl-spacing-x-large);
				padding: 0;
			}

			.placeholder {
				padding: var(--sl-spacing-x-small);
				box-sizing: border-box;
				cursor: default;
			}

			.placeholder > div {
				width: 100%;
				border: 1px dashed var(--sl-color-neutral-500);
				border-radius: var(--sl-spacing-2x-small);
				padding: var(--sl-spacing-x-small);
				box-sizing: border-box;
			}

			.button-tray {
				display: flex;
				flex-direction: row-reverse;
				gap: var(--sl-spacing-x-small);
				padding: 0 var(--sl-spacing-x-small);
				align-self: stretch;
			}

			.actions {
				display: flex;
				gap: var(--sl-spacing-x-small);
			}

			.columns {
				display: flex;
				flex-direction: row;
				align-items: stretch;
			}

			.column {
				display: flex;
				flex-direction: column;
				align-items: flex-start;
				flex-grow: 1;
			}

			.draggable-hack {
				display: flex;
				gap: 1ch;
				overflow-wrap: break-word;
			}

			.draggable-hack > sl-icon {
				color: var(--sl-color-neutral-1000);
			}
			
			.small-placing-gutter {
				display: flex;
				align-items: center;
				gap: 0.5ch;
				min-width: calc( 1rem + 2.25ch );
			}

			.icon-spacer {
				width: 1rem;
				height: 1rem;
			}

			.info {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-x-small);
			}

			.info > p {
				white-space: pre-wrap;
			}

			.edit > sl-input, .edit > sl-textarea {
				align-self: stretch;
			}

			.missing {
				margin-left: 6rem;
				padding-left: var(--sl-spacing-small);
				color: var(--sl-color-neutral-700);
			}
		`;
	}

	override render() : unknown {
		if( this.judging ) {
			return this.#renderJudgingView();
		} else {
			const infoView = this.editing ? this.#renderEditInfoView() : this.#renderReadonlyInfoView();
			return html`
				${infoView}
				${this.#renderSubmissionsView()}
			`;
		}
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'competitionId' ) ) {
			this.#loadCompetitionInfo();
			this.#loadSubmissions();
		} else if( changedProperties.has( 'judging' ) ) {
			if( this.judging ) {
				this.#prevRanked = [ ...this.rankedSubmissions! ];
				this.#prevUnranked = [ ...this.unrankedSubmissions! ];

				const sortedUnranked = [ ...this.unrankedSubmissions! ];
				sortedUnranked.sort( (a, b) => stringCmp( a.title, b.title ) );
				this.unrankedSubmissions = sortedUnranked;
			} else if( this.#prevRanked && this.#prevUnranked ) {
				this.rankedSubmissions = this.#prevRanked;
				this.unrankedSubmissions = this.#prevUnranked;
			}
		}
	}

	async #loadCompetitionInfo() : Promise<void> {
		this.competition = null;
		try {
			this.competition = await CompetitionsApi.getCompetitionAsync( this.competitionId );
			if( !this.competition.published && !this.competition.canEdit && !this.competition.canPublish && !this.competition.canJudge ) {
				this.navigate( '/competitions' );
			}
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load competition info', getErrorMessage( exception ) );
			this.redirect404();
		}
	}

	async #loadSubmissions() : Promise<void> {
		this.rankedSubmissions = null;
		this.unrankedSubmissions = null;
		try {
			const submissions = await CompetitionsApi.getCompetitionHacksAsync( this.competitionId );
			this.rankedSubmissions = submissions.filter( s => !!s.competitionPlacement );
			this.unrankedSubmissions = submissions.filter( s => !s.competitionPlacement );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load submissions', getErrorMessage( exception ) );
		}
	}

	#renderEditInfoView() : HTMLTemplateResult {
		return html`
			<div class="edit info">
				<sl-input
					label="Title"
					placeholder="Title"
					required
					autocapitalize="on"
					autocorrect="on"
					autocomplete="off"
					autofocus
					enterkeyhint="next"
					spellcheck
					inputmode="text"
					size="large"
					?disabled=${this.updating}
					.value=${this.competition!.name}
				></sl-input>

				<rhdc-markdown-editor
					trusted
					label="Description"
					placeholder="Enter the competition rules and information here."
					rows="8"
					required
					enterkeyhint="next"
					spellcheck
					?disabled=${this.updating}
					.value=${this.competition!.description}
				></rhdc-markdown-editor>

				<rhdc-date-time
					id="start-date"
					label="Start Date (Local Time)"
					?disabled=${this.updating}
					.value=${this.competition!.startDate}
				></rhdc-date-time>

				<rhdc-date-time
					id="end-date"
					label="End Date (Local Time)"
					?disabled=${this.updating}
					.value=${this.competition!.endDate}
				></rhdc-date-time>

				<div class="button-tray">
					<sl-button slot="footer" variant="primary" ?loading=${this.updating} @click=${this.#updateInfo}>Save</sl-button>
					<sl-button slot="footer" variant="neutral" ?disabled=${this.updating} @click=${() => this.editing = false}>Cancel</sl-button>
				</div>
			</div>
		`;
	}

	#renderReadonlyInfoView() : HTMLTemplateResult {
		if( !this.competition ) return html`<sl-spinner></sl-spinner>`;

		let editButton: unknown = nothing;
		let deleteButton: unknown = nothing;
		if( !this.judging && this.competition.canEdit ) {
			editButton = html`<sl-button variant="primary" ?disabled=${this.publishing} @click=${() => this.editing = true}>Edit Competition</sl-button>`;
			if( this.competition.submissionCount === 0 ) {
				deleteButton = html`<sl-button variant="danger" ?disabled=${this.publishing} @click=${this.#deleteCompetition}>Delete Competition</sl-button>`;
			}
		}

		let publishButton: unknown = nothing;
		if( !this.judging && this.competition.canPublish && this.competition.submissionCount !== 0 ) {
			publishButton = html`<sl-button variant="primary" ?loading=${this.publishing} @click=${this.#publish}>Publish Submissions</sl-button>`;
		}

		let judgeButton: unknown = nothing;
		if( !this.judging && this.competition.canJudge && this.rankedSubmissions && this.unrankedSubmissions ) {
			judgeButton = html`<sl-button variant="primary" ?disabled=${this.publishing} @click=${() => this.judging = true}>Enter Results</sl-button>`;
		}

		return html`
			<div class="info">
				<h2>${this.competition.name}</h2>

				<div>
					<b>${this.competition.startDate < new Date() ? 'Started on: ' : 'Starts on: '}</b>
					<sl-format-date .date=${this.competition.startDate as Date} year="numeric" month="long" day="numeric"></sl-format-date>
					<span> at </span>
					<sl-format-date .date=${this.competition.startDate as Date} hour="numeric" minute="2-digit" hour-format="12"></sl-format-date>
				</div>
				<div>
					<b>${this.competition.endDate < new Date() ? 'Ended on: ' : 'Ends on: '}</b>
					<sl-format-date .date=${this.competition.endDate as Date} year="numeric" month="long" day="numeric"></sl-format-date>
					<span> at </span>
					<sl-format-date .date=${this.competition.endDate as Date} hour="numeric" minute="2-digit" hour-format="12"></sl-format-date>
				</div>

				<rhdc-markdown trusted .markdown=${this.competition.description}></rhdc-markdown>

				<div class="actions">
					${editButton}
					${deleteButton}
					${publishButton}
					${judgeButton}
				</div>
			</div>
		`;
	}

	#renderSubmissionsView() : HTMLTemplateResult {
		if( !this.rankedSubmissions || !this.unrankedSubmissions ) return html`<sl-spinner></sl-spinner>`;

		let expectedRank = 0;
		const rankedSubmissions: HTMLTemplateResult[] = [];
		for( let i = 0; i < this.rankedSubmissions.length; i++ ) {
			while( ++expectedRank < this.rankedSubmissions[i].competitionPlacement! ) {
				rankedSubmissions.push( this.#renderSubmission( true, null, expectedRank - 1 ) );
			}
			rankedSubmissions.push( this.#renderSubmission( true, this.rankedSubmissions[i], expectedRank - 1 ) );
		}

		let missingLabel: unknown = nothing;
		if( this.competition && this.competition.submissionCount !== undefined ) {
			const missingSubmissions = this.competition.submissionCount - rankedSubmissions.length - this.unrankedSubmissions.length;
			if( missingSubmissions > 0 ) {
				missingLabel = html`<span class="missing">...and ${missingSubmissions} more submission${missingSubmissions === 1 ? '' : 's'} that you do not have permissions to view</span>`;
			}
		}

		return html`
			<div class="submissions" ?data-not-judged=${!this.judging && !this.rankedSubmissions?.length}>
				<h2>Submissions</h2>
				${rankedSubmissions}
				${map( this.unrankedSubmissions, this.#renderSubmission.bind( this, false ))}
				${missingLabel}
			</div>
			<rhdc-dialog label="Delete Competition?" destructive .confirmText=${"Delete"}>
				<p>Are you sure you want to delete this competition? This action cannot be undone.</p>
			</rhdc-dialog>
		`;
	}

	#renderJudgingView() : HTMLTemplateResult {
		if( !this.rankedSubmissions || !this.unrankedSubmissions ) {
			console.error( 'Oops' );
			return html``;
		}

		const ranked = (this.rankedSubmissions.length > 0) ?
			map( this.rankedSubmissions, this.#renderDragItem.bind( this, true ) ) :
			this.#makePlaceholder( RANKED_PLACEHOLDER );

		const unranked = (this.unrankedSubmissions.length > 0) ?
			map( this.unrankedSubmissions, this.#renderDragItem.bind( this, false ) ) :
			this.#makePlaceholder( UNRANKED_PLACEHOLDER );

		return html`
			<div class="submissions">
				<div class="columns">
					<div class="column">
						<h3>Ranked Submissions</h3>
						${ranked}
						${this.#makeSpacer( RANKED_PLACEHOLDER )}
					</div>

					<sl-divider vertical></sl-divider>

					<div class="column unranked">
						<h3>Unranked / Disqualified Submissions</h3>
						${unranked}
						${this.#makeSpacer( UNRANKED_PLACEHOLDER )}
					</div>
				</div>

				<div class="button-tray">
					<sl-button variant="primary" ?loading=${this.savingPlacements} @click=${this.#savePlacements}>Save</sl-button>
					<sl-button variant="neutral" ?disabled=${this.savingPlacements} @click=${() => this.judging = false}>Cancel</sl-button>
				</div>
			</div>
		`;
	}

	#makePlaceholder( placeholder: Symbol ) : HTMLTemplateResult {
		return html`
			<rhdc-drag-item
				.data=${placeholder}
				.groupId=${'comp-rank'}
				?disabled=${this.savingPlacements}
				@rhdc-drag-event=${this.#onRank}
				data-placeholder
			>
				<div class="placeholder" draggable="true" @dragstart=${this.#cancelEvent}>
					<div>Drag hacks here</div>
				</div>
			</rhdc-drag-item>
		`;
	}

	#makeSpacer( placeholder: Symbol ) : HTMLTemplateResult {
		return html`
			<rhdc-drag-item
				before-only
				.data=${placeholder}
				.groupId=${'comp-rank'}
				?disabled=${this.savingPlacements}
				@rhdc-drag-event=${this.#onRank}
			><div></div></rhdc-drag-item>
		`;
	}

	#renderSubmission( ranked: boolean, submission: Nullable<ConstRef<Hack>>, index: number ) : HTMLTemplateResult {
		const topRank = (ranked && index < 5) ? (index + 1) : undefined;
		const award = topRank ? html`<sl-icon name=${topRank <= 3 ? 'trophy-fill' : 'award-fill'}></sl-icon>` : nothing;

		let placing: unknown = nothing;
		if( ranked ) {
			placing = [
				html`<span>${index + 1}</span>`,
				html`<sup>${getOrdinalSuffix( index + 1 )}</sup>`
			];

			placing = html`<div>${placing}</div>`;
		}

		const card = submission ?
			html`<rhdc-hack-card .hack=${submission}></rhdc-hack-card>` :
			html`<sl-card>You do not have permission to view this hack.</sl-card>`;

		return html`
			<div class="submission">
				<div class="placing" data-place=${ifDefined( topRank )}>
					${award}
					${placing}
				</div>
				${card}
			</div>
		`;
	}

	#renderDragItem( ranked: boolean, submission: ConstRef<Hack>, index: number ) : HTMLTemplateResult {
		let body = html`<span>${submission.title}</span>`;
		if( ranked ) {
			const placement = html`<span>${index + 1}<sup>${getOrdinalSuffix( index + 1 )}</sup></span>`;

			let icon: HTMLTemplateResult;
			if( index < 3 ) {
				icon = html`<sl-icon name="trophy-fill"></sl-icon>`;
			} else if( index < 5 ) {
				icon = html`<sl-icon name="award-fill"></sl-icon>`;
			} else {
				icon = html`<div class="icon-spacer"></div>`;
			}
			
			body = html`
				<div class="small-placing-gutter" data-place="${index + 1}">
					${icon}
					${placement}
				</div>
				${body}
			`;
		}

		return html`
			<rhdc-drag-item
				.data=${submission}
				.groupId=${'comp-rank'}
				?disabled=${this.savingPlacements}
				@rhdc-drag-event=${this.#onRank}
			>
				<div class="draggable-hack">
					<sl-icon name="grip-vertical"></sl-icon>
					${body}
				</div>
			</rhdc-drag-item>
		`;
	}

	#onRank( event: RhdcDragEvent ) : void {
		if( !this.rankedSubmissions || !this.unrankedSubmissions ) return;

		const sourceHack = event.detail.source.data as Hack | Symbol;
		const targetHack = event.detail.target.data as Hack | Symbol;

		if( !(sourceHack instanceof Hack) ) return;
		if( sourceHack === targetHack ) return;

		const newRanked = this.rankedSubmissions.filter( s => s !== sourceHack );
		const newUnranked = this.unrankedSubmissions.filter( s => s !== sourceHack );

		if( targetHack === RANKED_PLACEHOLDER ) {
			newRanked.push( sourceHack );
		} else if( targetHack === UNRANKED_PLACEHOLDER ) {
			newUnranked.push( sourceHack );
		} else {
			let index = newRanked.indexOf( targetHack as Hack );
			if( index >= 0 ) {
				if( !event.detail.before ) index++;
				newRanked.splice( index, 0, sourceHack );
			} else {
				index = newUnranked.indexOf( targetHack as Hack );
				if( index >= 0 ) {
					if( !event.detail.before ) index++;
					newUnranked.splice( index, 0, sourceHack );
				}
			}
		}

		this.rankedSubmissions = newRanked;
		this.unrankedSubmissions = newUnranked;
	}

	#cancelEvent( event: Event ) {
		event.stopPropagation();
		event.preventDefault();
	}

	async #updateInfo() : Promise<void> {
		if(
			!this.titleInput.reportValidity() ||
			!this.descriptionInput.reportValidity()
		) return;

		this.updating = true;
		try {
			this.competition = await CompetitionsApi.updateCompetitionAsync( this.competitionId, {
				name: this.titleInput.value,
				description: this.descriptionInput.value,
				startDate: this.startDateInput.value,
				endDate: this.endDateInput.value
			});
			this.editing = false;
		} catch( exception: unknown ) {
			this.toastError( 'Failed to update competition', getErrorMessage( exception ) );
		} finally {
			this.updating = false;
		}
	}

	async #publish() : Promise<void> {
		this.publishing = true;
		try {
			await CompetitionsApi.publishCompetitionAsync( this.competitionId );
			await this.#loadCompetitionInfo();
		} catch( exception: unknown ) {
			this.toastError( 'Failed to publish competition', getErrorMessage( exception ) );
		} finally {
			this.publishing = false;
		}
	}

	async #savePlacements() : Promise<void> {
		this.savingPlacements = true;
		try {
			await CompetitionsApi.judgeCompetitionAsync( this.competitionId, this.rankedSubmissions!.map( s => s.hackId ) );
			this.judging = false;

			this.#prevRanked = null;
			this.#prevUnranked = null;
			this.#loadSubmissions();
		} catch( exception: unknown ) {
			this.toastError( 'Failed to save competition placements', getErrorMessage( exception ) );
		} finally {
			this.savingPlacements = false;
		}
	}

	async #deleteCompetition() : Promise<void> {
		this.deleteDialog.runAsync( async () => {
			try {
				await CompetitionsApi.deleteCompetitionAsync( this.competitionId );
				this.navigate( '/competitions' );
			} catch( exception: unknown ) {
				this.toastError( 'Failed to delete competition', getErrorMessage( exception ) );
				throw exception;
			}
		});
	}

}
