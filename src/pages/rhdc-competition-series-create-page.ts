import { css, CSSResultGroup, html } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { CompetitionsApi, CompetitionSeries } from '../apis/competitions-api';
import { UsersApi } from '../apis/users-api';
import { getErrorMessage } from '../util/http-error-parser';
import { RhdcFileDropEvent } from '../events/rhdc-file-drop-event';
import { RhdcFileDropzone } from '../widgets/rhdc-file-dropzone';
import { RhdcTagInput } from '../widgets/rhdc-tag-input';
import { Nullable } from '../util/types';
import { RhdcMarkdownEditor } from '../widgets/rhdc-markdown-editor';
import generateSlug from '../util/generate-slug';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import SlCheckbox from '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/avatar/avatar';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '../widgets/rhdc-markdown-editor';
import '../widgets/rhdc-file-dropzone';
import '../widgets/rhdc-tag-input';

@customElement( 'rhdc-competition-series-create-page' )
export class RhdcCompetitionSeriesCreatePage extends RhdcElement {

	@state()
	logoDataUrl = 'about:blank';

	@state()
	loading = false;

	@state()
	slug = '-';

	@state()
	userList: Nullable<Readonly<string[]>> = null;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				max-width: min( 100vw, calc( 800px + 2 * var(--sl-spacing-x-small) ) );
				margin: var(--sl-spacing-small) auto;
				padding: var(--sl-spacing-x-small);
				gap: var(--sl-spacing-x-small);
				box-sizing: border-box;
			}

			.logo-col {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-x-small);
			}

			.info-col {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-small);
				flex-grow: 1;
			}

			sl-avatar {
				--size: 96px;
			}

			rhdc-file-dropzone {
				width: 96px;
				height: 96px;
				margin: 0;
				--pading: 0;
				font-size: 2rem;
			}

			.button-tray {
				display: flex;
				flex-direction: row-reverse;
				gap: var(--sl-spacing-x-small);
			}

			.slug {
				font-size: var(--sl-font-size-x-small);
				color: var(--sl-color-neutral-400);
				margin-top: var(--sl-spacing-2x-small);
			}

			.logo-col span {
				font-size: var(--sl-font-size-small);
			}
		`;
	}

	override render() : unknown {
		return html`
			<div class="logo-col">
				<sl-avatar shape="rounded" initials="?" image="${this.logoDataUrl}"></sl-avatar>
				<rhdc-file-dropzone id="logo" accept="image/png" ?disabled=${this.loading} @rhdc-file-drop-event=${this.#updateLogo}>
					<sl-icon name="upload"></sl-icon>
				</rhdc-file-dropzone>
				<span>.PNG</span>
			</div>
			<div class="info-col">
				<div>
					<sl-input type="text"
						id="name"
						placeholder="Series Name"
						?disabled=${this.loading}
						label="Title"
						required
						autocorrect="on"
						enterkeyhint="next"
						inputmode="text"
						@sl-input=${this.#updateSlug}
					></sl-input>
					<div class="slug">
						<b>URL Title:</b> ${this.slug}
					</div>
				</div>
				<rhdc-tag-input
					id="hosts"
					?loading=${!this.userList}
					case-sensitive
					require-match
					placeholder="Enter username here. Tab to autocomplete."
					?disabled=${this.loading}
					label="Hosts"
					required
					.autocomplete=${this.userList || []}
				></rhdc-tag-input>
				<rhdc-markdown-editor
					trusted
					id="description"
					placeholder="description"
					rows="8"
					?disabled=${this.loading}
					label="Description (Markdown supported)"
					required
					enterkeyhint="next"
					spellcheck
				></rhdc-markdown-editor>
				<sl-checkbox id="canOfferPrizes" ?disabled=${this.loading}>Can Offer Cash Prizes</sl-checkbox>
				<sl-checkbox id="locked" ?disabled=${this.loading}>Locked</sl-checkbox>
				<div class="button-tray">
					<sl-button variant="primary" ?loading=${this.loading} @click=${this.#createSeries}>Create</sl-button>
					<sl-button variant="neutral" ?disabled=${this.loading} @click=${this.navAction( '/competitions' )}>Cancel</sl-button>
				</div>
			</div>
		`;
	}

	override connectedCallback() : void {
		super.connectedCallback();
		this.#init();
	}

	async #init() : Promise<void> {
		try {
			this.userList = await UsersApi.getAllUsernamesAsync();
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load list of usernames.', getErrorMessage( exception ) );
		}
	}

	#updateSlug() : void {
		this.slug = generateSlug( this.#getInput<SlInput>( 'name' ).value );
	}

	async #updateLogo( event: RhdcFileDropEvent ) : Promise<void> {
		try {
			await CompetitionsApi.validateCompetitionSeriesLogoAsync( event.detail.file );
			this.logoDataUrl = await new Promise<string>( resolve => {
				const fileReader = new FileReader();
				fileReader.readAsDataURL( event.detail.file );
				fileReader.addEventListener( 'loadend', () => {
					resolve( fileReader.result as string );
				});
			});
		} catch( exception: unknown ) {
			this.toastError( 'Image rejected. Please upload a .PNG image that is at least 96x96 pixels.', getErrorMessage( exception ) );
		}
	}

	async #createSeries() : Promise<void> {
		let series: CompetitionSeries;

		this.loading = true;
		try {
			series = await CompetitionsApi.createCompetitionSeriesAsync({
				name: this.#getInput<SlInput>( 'name' ).value || '',
				description: this.#getInput<RhdcMarkdownEditor>( 'description' ).value || '',
				canOfferPrizes: this.#getInput<SlCheckbox>( 'canOfferPrizes' ).checked,
				locked: this.#getInput<SlCheckbox>( 'locked' ).checked,
				hosts: this.#getInput<RhdcTagInput>( 'hosts' ).value
			});
		} catch( exception: unknown ) {
			this.toastError( 'Failed to create competition series', getErrorMessage( exception ) );
			this.loading = false;
			return;
		}

		const logo = this.#getInput<RhdcFileDropzone>( 'logo' ).value;
		if( logo ) {
			try {
				await CompetitionsApi.updateCompetitionSeriesLogoAsync( series.seriesId, logo );
				this.toastSuccess( 'Competition series created' );
			} catch( exception: unknown ) {
				this.toastWarn( 'The competition series was created, but the logo failed to upload.', getErrorMessage( exception ) );
			}
		}

		this.navigate( '/competitions' );
	}

	#getInput<T extends HTMLElement>( id: string ) : T {
		return this.shadowRoot?.getElementById( id ) as T;
	}

}
