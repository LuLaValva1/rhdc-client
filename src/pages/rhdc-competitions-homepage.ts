import { css, CSSResultGroup, html, nothing } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { guard } from 'lit/directives/guard.js';
import { map } from 'lit/directives/map.js';
import { RhdcElement } from '../rhdc-element';
import { CompetitionsApi, Competition, CompetitionSeries } from '../apis/competitions-api';
import { ConstArray, Nullable } from '../util/types';
import { rhdcUserContext, UserAuth } from '../decorators/user-context';
import { Role } from '../auth/roles';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '../components/competitions/rhdc-competition-series-card';
import '../components/competitions/rhdc-competition-card';
import '../widgets/rhdc-link';

@customElement( 'rhdc-competitions-homepage' )
export class RhdcCompetitionsHomepage extends RhdcElement {

	@state()
	series: Nullable<ConstArray<CompetitionSeries>> = null;

	@state()
	activeCompetitions: Nullable<ConstArray<Competition>> = null;

	@rhdcUserContext()
	currentUser!: Nullable<UserAuth>;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				max-width: min( 100vw, calc( 800px + 2 * var(--sl-spacing-x-small) ) );
				margin: auto;
				padding: var(--sl-spacing-x-small);
				gap: var(--sl-spacing-small);
				box-sizing: border-box;
				margin-top: var(--sl-spacing-small);
			}

			h1 {
				font-size: var(--sl-font-size-2x-large);
				font-weight: normal;
				margin: 0;
			}

			div {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-x-small);
				margin-bottom: var(--sl-spacing-medium);
			}

			i {
				font-size: var(--sl-font-size-large);
				color: var(--sl-color-neutral-700);
			}
		`;
	}

	override render() : unknown {
		let createButton: unknown = nothing;
		if( this.currentUser?.role === Role.Staff ) {
			createButton = html`
				<rhdc-link href="/competitions/new-series">
					<sl-button variant="primary">
						<sl-icon slot="prefix" name="plus-lg"></sl-icon>
						New Series
					</sl-button>
				</rhdc-link>
			`;
		}

		return html`
			<h1>Competition Series</h1>
			<div>
				${createButton}
				${guard( this.series, this.#renderSeries.bind( this ) )}
			</div>

			<h1>Active Competitions</h1>
			<div>
				${guard( this.activeCompetitions, this.#renderActiveCompetitions.bind( this ) )}
			</div>
		`;
	}

	override connectedCallback() : void {
		super.connectedCallback();
		this.#initAsync();
	}

	async #initAsync() : Promise<void> {
		this.series = await CompetitionsApi.getAllCompetitionSeriesAsync();
		this.activeCompetitions = await CompetitionsApi.getActiveCompetitionsAsync();
	}

	#renderSeries() : unknown {
		if( !this.series ) return html`<sl-spinner></sl-spinner>`;
		return map( this.series, series => html`<rhdc-competition-series-card .series=${series}></rhdc-competition-series-card>` );
	}

	#renderActiveCompetitions() : unknown {
		if( !this.activeCompetitions ) return html`<sl-spinner></sl-spinner>`;
		if( this.activeCompetitions.length > 0 ) {
			return map( this.activeCompetitions, competition => html`<rhdc-competition-card .competition=${competition}></rhdc-competition-card>` );
		} else {
			return html`<i>No competitions are currently open for submission.</i>`;
		}
	}

}
