import { css, CSSResultGroup, html, PropertyValues, nothing } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { rhdcUserContext, UserAuth } from '../decorators/user-context';
import { Starpower, StarpowerApi } from '../apis/starpower-api';
import { Hack, HacksApi } from '../apis/hacks-api';
import { Nullable, HexId } from '../util/types';
import { Role } from '../auth/roles';
import { RhdcHackUpdatedEvent } from '../events/rhdc-hack-updated-event';
import { RhdcLinkMetadataEvent } from '../events/rhdc-link-metadata-event';
import { LinkedData } from '../util/linked-data';
import { AlertFactory } from '../util/alert-factory';
import { OgHack } from '../util/ogp';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '@shoelace-style/shoelace/dist/components/divider/divider';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '../components/hacks/modules/rhdc-hack-info';
import '../components/hacks/modules/rhdc-hack-media';
import '../components/hacks/modules/rhdc-hack-media-edit';
import '../components/hacks/modules/rhdc-hack-small-card';
import '../components/hacks/modules/rhdc-hack-starpower';
import '../components/hacks/modules/rhdc-hack-versions';
import '../components/hacks/modules/rhdc-hack-playlist-button';
import '../components/hacks/modules/rhdc-hack-mod-tools';
import '../components/hacks/modules/rhdc-hack-edit-layout-button';
import '../components/comments/rhdc-hack-comments';
import '../components/common/rhdc-info-bubble';
import '../widgets/rhdc-link';

@customElement( 'rhdc-hack-page' )
export class RhdcHackPage extends RhdcElement {

	@property({ attribute: false })
	hackSlug!: string | HexId;

	@rhdcUserContext()
	currentUser!: Nullable<UserAuth>;

	@state()
	hack: Nullable<Hack> = null;

	@state()
	showPlayHelp = false;

	@state()
	editingMedia = false;

	@state()
	starpower: Nullable<Starpower> = null;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				max-width: calc( 1200px + 2 * var(--sl-spacing-x-small) );
				margin: auto;
				box-sizing: border-box;
				gap: var( --sl-spacing-x-small );
				padding: var(--sl-spacing-x-small);
			}

			#placeholder {
				margin: auto;
				margin-top: var(--sl-spacing-4x-large);
				font-size: 4em;
				--track-width: 1em;
			}

			.main, .right {
				display: flex;
				flex-direction: column;
				align-items: stretch;
			}

			.main {
				flex-grow: 1;
				flex-shrink: 1;
				gap: var(--sl-spacing-x-small);
			}

			.right {
				flex-grow: 0;
				gap: var(--sl-spacing-small);
			}

			.buttons {
				display: flex;
				justify-content: space-between;
			}

			.buttons sl-button {
				display: block;
			}

			.buttons-left {
				display: flex;
				gap: var(--sl-spacing-x-small);
			}

			.play-help {
				font-size: var(--sl-font-size-x-small);
			}

			rhdc-hack-versions {
				width: 0;
				min-width: 100%;
			}

			sl-button > span {
				vertical-align: baseline;
			}

			sl-button > sl-icon {
				font-size: 1rem;
			}

			@media (max-width: 950px) {
				
				:host {
					flex-direction: column-reverse;
					max-width: unset;
				}

				.right {
					display: flex;
					flex-direction: row;
					flex-wrap: wrap;
				}

			}
		`;
	}

	override render() : unknown {
		if( !this.hack ) {
			return html`<sl-spinner id="placeholder"></sl-spinner>`;
		}

		let starpower: unknown = nothing;
		if( this.starpower ) {
			starpower = html`<rhdc-hack-starpower .hack=${this.hack} .starpower=${this.starpower}></rhdc-hack-starpower>`;
		}

		let playlistsButton: unknown = nothing;
		if( this.currentUser && this.currentUser.role !== Role.Banned ) {
			playlistsButton = html`<rhdc-hack-playlist-button .hackId=${this.hack.hackId}></rhdc-hack-playlist-button>`;
		}

		let layoutEdit: unknown = nothing;
		if( this.hack.canEditInfo ) {
			layoutEdit = html`<rhdc-hack-edit-layout-button .hack=${this.hack} @rhdc-hack-updated-event=${this.#updateHack}></rhdc-hack-edit-layout-button>`;
		}

		let modTools: unknown = nothing;
		if( this.hack.canModEdit ) {
			modTools = html`<rhdc-hack-mod-tools .hack=${this.hack} @rhdc-hack-updated-event=${this.#updateHack}></rhdc-hack-mod-tools>`;
		}

		let editMediaButton: unknown = nothing;
		if( !this.editingMedia && this.hack.canEditInfo ) {
			editMediaButton = html`<sl-button variant="primary" @click=${this.#editMedia}>Edit Screenshots & Videos</sl-button>`;
		}

		let layoutAndMediaButtons: unknown = nothing;
		if( this.hack.canEditInfo ) {
			layoutAndMediaButtons = html`
				<div class="buttons-left">
					${layoutEdit}
					${editMediaButton}
				</div>
			`;
		}

		let unapprovedAlert: unknown = nothing;
		if( !this.hack.approved ) {
			unapprovedAlert = AlertFactory.create({
				type: 'primary',
				title: 'Pending Approval',
				message: 'This hack is currently pending approval from moderators. It will not be visible to other users until it has been approved.',
				icon: 'eye-slash'
			});
		}

		const media = this.editingMedia ?
			html`<rhdc-hack-media-edit
				.hack=${this.hack}
				@rhdc-editing-done=${() => this.editingMedia = false}
				@rhdc-hack-updated-event=${(event: RhdcHackUpdatedEvent) => this.hack = event.detail.hack}
			></rhdc-hack-media-edit>` :
			html`<rhdc-hack-media .hack=${this.hack}></rhdc-hack-media>`;

		const playNowButton = this.hack.archived ? html`<span></span>` : html`
			<sl-button variant="success" href="rhdc://${this.hack.urlTitle}/any" @click=${() => this.showPlayHelp = true}>
				<sl-icon src="/assets/icons/rhdc-link.svg" slot="prefix"></sl-icon>
				<span>Play Now</span>
			</sl-button>
		`;

		return html`
			<div class="main">
				${unapprovedAlert}
				<rhdc-hack-info .hack=${this.hack} @rhdc-hack-updated-event=${this.#updateHack}></rhdc-hack-info>
				${layoutAndMediaButtons}
				<div class="buttons">
					${playNowButton}
					${playlistsButton}
				</div>
				<span class="play-help" ?hidden=${!this.showPlayHelp}>
					Not working? <rhdc-link href="/gettingstarted">Download Parallel Launcher</rhdc-link> to launch hacks directly from the website.
				</span>
				${media}
				${modTools}
				<rhdc-hack-versions .hack=${this.hack} @rhdc-hack-updated-event=${(event: RhdcHackUpdatedEvent) => this.hack = event.detail.hack}></rhdc-hack-versions>
				<sl-divider></sl-divider>
				<rhdc-hack-comments .hack=${this.hack}></rhdc-hack-comments>
			</div>
			<div class="right">
				<rhdc-hack-small-card .hack=${this.hack}></rhdc-hack-small-card>
				${starpower}
			</div>
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'hackSlug' ) || changedProperties.has( 'currentUser' ) ) {
			this.#reloadAsync();
		} else if( changedProperties.has( 'hack' ) && this.hack ) {
			this.dispatchEvent( RhdcLinkMetadataEvent.create(
				LinkedData.fromHack( this.hack ),
				new OgHack( this.hack )
			));
		}
	}

	async #reloadAsync() : Promise<void> {
		this.hack = null;
		this.starpower = null;

		try {
			this.hack = await HacksApi.getHackAsync( this.hackSlug );
		} catch{
			this.redirect404();
			return;
		}

		if( this.currentUser ) {
			this.starpower = await StarpowerApi.getStarpowerAsync( this.currentUser.userId, this.hack.hackId );
		}
	}

	async #editMedia() : Promise<void> {
		this.editingMedia = true;
		await this.updateComplete;
		this.shadowRoot?.querySelector( 'rhdc-hack-media-edit' )?.scrollIntoView({
			behavior: 'smooth',
			block: 'center'
		});
	}

	#updateHack( event: RhdcHackUpdatedEvent ) : void {
		this.hack = event.detail.hack;
	}

}
