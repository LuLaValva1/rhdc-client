import { css, CSSResultGroup, html } from 'lit';
import { customElement, state, query } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { HacksApi, Hack, HackSortField, MatureFilter, Category, ConsoleCompatibility } from '../apis/hacks-api';
import { Optional } from '../util/types';
import { Page } from '../apis/pagination';
import { RhdcHackSortBar } from '../components/hacks/rhdc-hack-sort-bar';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import SlSelect from '@shoelace-style/shoelace/dist/components/select/select';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/select/select';
import '@shoelace-style/shoelace/dist/components/option/option';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '../components/common/rhdc-infinite-scroll';
import '../components/hacks/rhdc-hack-sort-bar';
import '../components/hacks/rhdc-hack-card';

@customElement( 'rhdc-hacks-list-page' )
export class RhdcHacksListPage extends RhdcElement {

	@state()
	loading = true;

	@state()
	hacksPromise: Optional<Promise<Page<Hack>>>;

	@query( '.header > sl-input' )
	searchBar!: SlInput;

	@query( '#category-filter' )
	categoryFilter!: SlSelect;

	@query( '#console-filter' )
	consoleFilter!: SlSelect;

	@query( '#mature-filter' )
	matureFilter!: SlSelect;

	@query( 'rhdc-hack-sort-bar' )
	sortBar!: RhdcHackSortBar;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				padding: var(--sl-spacing-small);
				max-width: 1200px;
				margin: auto;

				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-2x-small);
			}

			.header, .filters, .sorting {
				margin-bottom: var(--sl-spacing-small);
			}

			.header {
				display: flex;
				gap: var(--sl-spacing-small);
				justify-content: space-between;
			}

			.header > sl-input {
				flex-grow: 1;
				max-width: 900px;
			}

			.filters {
				display: flex;
				flex-wrap: wrap;
				gap: var(--sl-spacing-x-small);
			}

			sl-select {
				flex-grow: 1;
				max-width: 300px;
			}

			sl-select::part(form-control-label) {
				font-size: var(--sl-font-size-x-small);
			}
		`;
	}

	override render() : unknown {
		return html`
			<div class="header">
				<sl-input
					type="search"
					placeholder="Search for a hack"
					autocapitalize="off"
					enterkeyhint="search"
					inputmode="search"
					autofocus
					?disabled=${this.loading}
					clearable
					@sl-change=${this.#search}
					@sl-clear=${this.#search}
				>
					<sl-icon name="search" slot="suffix"></sl-icon>
				</sl-input>
				<sl-button variant="primary" @click=${this.navAction( '/submit' )}>Submit a New Hack</sl-button>
			</div>
			<div class="filters">
				<sl-select
					id="category-filter"
					label="Category"
					value=""
					@sl-change=${this.#search}
					?disabled=${this.loading}
				>
					<sl-option value="">Any</sl-option>
					<sl-option value="${Category.Original}">${Category.Original}</sl-option>
					<sl-option value="${Category.Concept}">${Category.Concept}</sl-option>
					<sl-option value="${Category.Kaizo}">${Category.Kaizo}</sl-option>
					<sl-option value="${Category.Retexture}">${Category.Retexture}</sl-option>
				</sl-select>
				<sl-select
					id="console-filter"
					label="Console Compatibility"
					value=""
					@sl-change=${this.#search}
					?disabled=${this.loading}
				>
					<sl-option value="">Not Required</sl-option>
					<sl-option value="${ConsoleCompatibility.Unoptimized}">Unoptimized (or better)</sl-option>
					<sl-option value="${ConsoleCompatibility.Playable}">Playable (or better)</sl-option>
					<sl-option value="${ConsoleCompatibility.Good}">Good (or better)</sl-option>
					<sl-option value="${ConsoleCompatibility.Excellent}">Excellent</sl-option>
				</sl-select>
				<sl-select
					id="mature-filter"
					label="Mature Filter"
					value="${MatureFilter.Ignore}"
					@sl-change=${this.#search}
					?disabled=${this.loading}
				>
					<sl-option value="${MatureFilter.Ignore}">Hide Mature Hacks</sl-option>
					<sl-option value="${MatureFilter.Include}">Show Mature Hacks</sl-option>
					<sl-option value="${MatureFilter.Exclusive}">Mature Hacks Only</sl-option>
				</sl-select>
			</div>
			<rhdc-hack-sort-bar @rhdc-change=${this.#search}></rhdc-hack-sort-bar>
			<rhdc-infinite-scroll
				.pageGetter=${this.hacksPromise}
				.renderer=${this.#renderHackCard}
				.noResultsMessage=${'No hacks found.'}
				.errorTitle=${'Failed to load hacks.'}
				@rhdc-load-start=${() => this.loading = true}
				@rhdc-load-end=${() => this.loading = false}
			></rhdc-infinite-scroll>
		`;
	}
	
	override connectedCallback() : void {
		super.connectedCallback();
		this.hacksPromise = HacksApi.searchHacksAsync( null, HackSortField.DateUploaded, true, MatureFilter.Ignore );
	}

	#renderHackCard( hack: Hack ) : unknown {
		return html`<rhdc-hack-card .hack=${hack}></rhdc-hack-card>`;
	}

	#search() : void {
		this.hacksPromise = HacksApi.searchHacksAsync(
			this.searchBar.value || null,
			this.sortBar.sortField,
			this.sortBar.sortDescending,
			this.matureFilter.value as MatureFilter,
			this.categoryFilter.value as Category || null,
			this.consoleFilter.value as ConsoleCompatibility || null
		);
	}

}
