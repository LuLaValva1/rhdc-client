import { css, CSSResultGroup, html } from 'lit';
import { customElement } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import '@shoelace-style/shoelace/dist/components/tab-group/tab-group';
import '@shoelace-style/shoelace/dist/components/tab/tab';
import '@shoelace-style/shoelace/dist/components/tab-panel/tab-panel';
import '../components/leaderboard/rhdc-leaderboard';

@customElement( 'rhdc-leaderboard-page' )
export class RhdcLeaderboardPage extends RhdcElement {

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				max-width: min( 100vw, calc( 1200px + 2 * var(--sl-spacing-x-small) ) );
				margin: auto;
				padding: var(--sl-spacing-x-small);
				box-sizing: border-box;
			}
		`;
	}

	override render() : unknown {
		return html`
			<sl-tab-group>
				<sl-tab slot="nav" panel="main">General Leaderboard</sl-tab>
				<sl-tab slot="nav" panel="kaizo">Kaizo Leaderboard</sl-tab>

				<sl-tab-panel name="main">
					<rhdc-leaderboard></rhdc-leaderboard>
				</sl-tab-panel>
				<sl-tab-panel name="kaizo">
					<rhdc-leaderboard kaizo></rhdc-leaderboard>
				</sl-tab-panel>
			</sl-tab-group>
		`;
	}

}
