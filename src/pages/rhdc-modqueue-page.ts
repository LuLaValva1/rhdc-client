import { css, CSSResultGroup, html, HTMLTemplateResult, PropertyValues } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { until } from 'lit/directives/until.js';
import { RhdcElement } from '../rhdc-element';
import { ModqueueApi, ModqueueStatus, ModqueueComponentStatus } from '../apis/modqueue-api';
import { ConstRef, Nullable, Weak } from '../util/types';
import { RhdcModqueueStatusChangeEvent } from '../events/rhdc-modqueue-status-change-event';
import { RhdcModqueueListBase } from '../components/modqueue/rhdc-modqueue-list-base';
import { getErrorMessage } from '../util/http-error-parser';
import { HttpError, HttpErrorType } from '../apis/http-client';
import SlTabShowEvent from '@shoelace-style/shoelace/dist/events/sl-tab-show';
import '@shoelace-style/shoelace/dist/components/tab-group/tab-group';
import '@shoelace-style/shoelace/dist/components/tab/tab';
import '@shoelace-style/shoelace/dist/components/tab-panel/tab-panel';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '../components/modqueue/rhdc-modqueue-hack-list';
import '../components/modqueue/rhdc-modqueue-version-list';
import '../components/modqueue/rhdc-modqueue-starpower-list';
import '../components/modqueue/rhdc-modqueue-pending-claim-list';
import '../components/modqueue/rhdc-modqueue-rejected-claim-list';
import '../components/modqueue/rhdc-modqueue-bonus-list';

@customElement( 'rhdc-modqueue-page' )
export class RhdcModqueuePage extends RhdcElement {

	@state()
	status: Nullable<ModqueueStatus> = null;

	#bonusCount = new Promise<HTMLTemplateResult>( () => null );

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				margin: auto;
				max-width: min( 900px, 100vw );
				box-sizing: border-box;
			}

			sl-tab > sl-badge, sl-tab > sl-spinner {
				margin-inline-start: 1ch;
			}

			#loading {
				align-self: center;
				font-size: 2rem;
				margin-top: 2rem;
			}

			sl-badge > sl-icon {
				margin: 0 -0.1875rem;
			}

			sl-badge[hidden] {
				max-width: 0;
				margin-inline-start: 0 !important;
				overflow: hidden;
				visibility: hidden;
			}
		`;
	}

	override render() : unknown {
		if( !this.status ) {
			return html`<sl-spinner id="loading"></sl-spinner>`;
		}

		const bonusBadge = until( this.#bonusCount, html`<sl-spinner></sl-spinner>` );
		return html`
			<sl-tab-group @sl-tab-show=${this.#tabShown} @rhdc-modqueue-status-change-event=${this.#itemRemoved}>
				<sl-tab slot="nav" panel="hacks">Hacks ${this.#renderBadge( this.status.hacks )}</sl-tab>
				<sl-tab slot="nav" panel="versions">Versions ${this.#renderBadge( this.status.versions )}</sl-tab>
				<sl-tab slot="nav" panel="starpower">Starpower ${this.#renderBadge( this.status.starpower )}</sl-tab>
				<sl-tab slot="nav" panel="claims">Author Claims ${this.#renderBadge( this.status.claims )}</sl-tab>
				<sl-tab slot="nav" panel="rejected-claims">Rejected Claims <sl-badge pill hidden>&nbsp;</sl-badge></sl-tab>
				<sl-tab slot="nav" panel="bonus">Completion Bonus ${bonusBadge}</sl-tab>

				<sl-tab-panel name="hacks"><rhdc-modqueue-hack-list id="hacks"></rhdc-modqueue-hack-list></sl-tab-panel>
				<sl-tab-panel name="versions"><rhdc-modqueue-version-list id="versions"></rhdc-modqueue-version-list></sl-tab-panel>
				<sl-tab-panel name="starpower"><rhdc-modqueue-starpower-list id="starpower"></rhdc-modqueue-starpower-list></sl-tab-panel>
				<sl-tab-panel name="claims"><rhdc-modqueue-pending-claim-list id="claims"></rhdc-modqueue-pending-claim-list></sl-tab-panel>
				<sl-tab-panel name="rejected-claims"><rhdc-modqueue-rejected-claim-list id="rejected-claims"></rhdc-modqueue-rejected-claim-list></sl-tab-panel>
				<sl-tab-panel name="bonus"><rhdc-modqueue-bonus-list id="bonus"></rhdc-modqueue-bonus-list></sl-tab-panel>
			</sl-tab-group>
		`;
	}

	override connectedCallback() : void {
		super.connectedCallback();
		this.#updateStatus();
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'status' ) && this.status ) {
			this.#bonusCount = ModqueueApi.getPendingCompletionBonusCountAsync().then( count => {
				if( count > 0 ) return html`<sl-badge variant="primary" pill>${count}</sl-badge>`;
				return html`<sl-badge variant="success" pill><sl-icon name="check-lg"></sl-icon></sl-badge>`;
			});
		}
	}

	protected override updated( changedProperties: PropertyValues ) : void {
		super.firstUpdated( changedProperties );
		if( changedProperties.has( 'status' ) && this.status ) {
			(this.shadowRoot?.getElementById( 'hacks' ) as RhdcModqueueListBase<unknown>)?.loadItems();
		}
	}

	#renderBadge( component: ConstRef<ModqueueComponentStatus> ) : HTMLTemplateResult {
		if( component.count <= 0 ) return html`<sl-badge variant="success" pill><sl-icon name="check-lg"></sl-icon></sl-badge>`;
		return html`<sl-badge variant=${component.priority} ?pulse=${component.important} pill>${component.count}</sl-badge>`;
	}

	#tabShown( event: SlTabShowEvent ) : void {
		const tab = this.shadowRoot?.getElementById( event.detail.name ) as Weak<RhdcModqueueListBase<unknown>>;
		tab?.loadItems();
	}

	async #itemRemoved( event: RhdcModqueueStatusChangeEvent ) : Promise<void> {
		this.status = await event.detail.status;
	}

	async #updateStatus() : Promise<void> {
		try {
			this.status = await ModqueueApi.getQueueStatusAsync();
			this.dispatchEvent( RhdcModqueueStatusChangeEvent.create( this.status ) );
		} catch( exception: unknown ) {
			if( exception instanceof HttpError && exception.type === HttpErrorType.Problem ) {
				if( exception.problem.status === 401 || exception.problem.status === 403 ) {
					this.navigate( '/' );
					return;
				}
			}
			this.toastError( 'Failed to load mod queue', getErrorMessage( exception ) );
		}
	}

}
