import { css, CSSResultGroup, html } from 'lit';
import { customElement } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';

@customElement( 'rhdc-rules-page' )
export class RhdcRulesPage extends RhdcElement {

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: block;
				margin: auto;
				min-width: min( 1000px, 95vw );
				max-width: 1000px;
			}

			h1 {
				font-size: var(--sl-font-size-2x-large);
				font-weight: bold;
				margin: var(--sl-spacing-large) 0;
			}

			h2 {
				font-size: var(--sl-font-size-x-large);
				font-weight: bold;
				margin-top: var(--sl-spacing-large);
				margin-bottom: var(--sl-spacing-x-small);
			}

			li {
				font-size: 1.125rem;
				margin: var(--sl-spacing-2x-small) 0;
			}
		`;
	}

	override render() : unknown {
		return html`
			<h1>Site Rules</h1>

			<h2>Code of Conduct</h2>
			<ul>
				<li>You must be at least 13 years of age or older, and have parent / guardian consent (if applicable), to join the site</li>
				<li>Do not post racism, homophobia, transphobia, sexism, ableism, or any other form of hate speech</li>
				<li>Do not spam or harass other users, and do not discourage new and upcoming creators</li>
				<li>Do not post false or misleading information in your profile</li>
				<li>Do not make references to self-harm or encourage any one to hurt themselves</li>
				<li>Do not make jokes about sexual assault or threaten it to other people</li>
				<li>Do not post porn, or images with nudity or alluding to nudity</li>
				<li>Do not post gorey or violent images</li>
				<li>Do not post links to any full rom files (not patches) or any other material that you are not authorized to distribute</li>
				<li>Do not post links to executable files or any type of malicious file</li>
			</ul>

			<h2>Hack Submissions</h2>
			<ul>
				<li>Hack submissions must abide by the above Code of Conduct</li>
				<li>If you are not the author of the hack you are submitting, you must have permission from the hack creator to submit it here</li>
			</ul>

			<h2>Star Progress Submissions</h2>
			<ul>
				<li>Progress submissions for Kaizo hacks require video proof of completion without the use of slowdown or savestates</li>
				<li>Progress submissions for non-Kazio hacks do not require proof and may be completed however you like with no restrictions</li>
			</ul>
		`;
	}

}
