import { css, CSSResultGroup, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import '@shoelace-style/shoelace/dist/components/divider/divider';
import '../components/search/rhdc-user-search';
import '../components/search/rhdc-hack-search';

@customElement( 'rhdc-search-page' )
export class RhdcSearchPage extends RhdcElement {

	@property({ attribute: false })
	searchTerm!: string;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				max-width: min( 100vw, calc( 1200px + 2 * var(--sl-spacing-x-small) ) );
				margin: auto;
				padding: var(--sl-spacing-x-small);
				box-sizing: border-box;
			}

			div {
				display: flex;
				flex-direction: column;
				align-items: stretch;
			}
		`;
	}

	override render() : unknown {
		return html`
			<h1>Search Results</h1>
			<div>
				<h2>Users</h2>
				<rhdc-user-search .searchTerm=${this.searchTerm}></rhdc-user-search>
			</div>
			<sl-divider></sl-divider>
			<div>
				<h2>Hacks</h2>
				<rhdc-hack-search .searchTerm=${this.searchTerm}></rhdc-hack-search>
			</div>
		`;
	}

}
