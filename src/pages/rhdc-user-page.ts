import { css, CSSResultGroup, html, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { Nullable, UserId } from '../util/types';
import { UsersApi, UserActivity } from '../apis/users-api';
import { rhdcUserContext, UserAuth } from '../decorators/user-context';
import '../components/users/rhdc-user-profile-card';
import '../components/users/rhdc-user-recent-activity';
import '../components/users/rhdc-user-hacks';
import '../components/users/rhdc-user-most-played';
import '../components/users/rhdc-user-playlists';

@customElement( 'rhdc-user-page' )
export class RhdcUserPage extends RhdcElement {

	@property({ attribute: false })
	userSlug!: string | UserId;

	@state()
	activity: Nullable<UserActivity> = null;

	@state()
	canChangeUsername: Nullable<boolean> = null;

	@state()
	loadingActivity = false;

	@rhdcUserContext()
	userContext!: Nullable<UserAuth>;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				max-width: min( 100vw, calc( 1200px + 2 * var(--sl-spacing-x-small) ) );
				margin: auto;
				padding: var(--sl-spacing-x-small);
				gap: var(--sl-spacing-small);
				box-sizing: border-box;
			}

			div {
				display: flex;
				justify-content: space-between;
				gap: var(--sl-spacing-medium);
			}

			@media only screen and (max-width: 781px) {
				div {
					flex-direction: column;
					align-items: stretch;
					justify-content: unset;
					gap: var(--sl-spacing-small);
				}
			}

		`;
	}

	override render() : unknown {
		return html`
			<rhdc-user-profile-card .userSlug=${this.userSlug}></rhdc-user-profile-card>
			<rhdc-user-hacks .userSlug=${this.userSlug}></rhdc-user-hacks>
			<rhdc-user-recent-activity .activity=${this.activity} ?loading=${this.loadingActivity}></rhdc-user-recent-activity>
			<div>
				<rhdc-user-most-played .activity=${this.activity?.mostPlayed} ?loading=${this.loadingActivity}></rhdc-user-most-played>
				<rhdc-user-playlists .userSlug=${this.userSlug}></rhdc-user-playlists>
			</div>
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'userSlug' ) ) {
			this.#loadActivity();
		}
	}

	async #loadActivity() : Promise<void> {
		this.loadingActivity = true;
		this.activity = null;
		try {
			this.activity = await UsersApi.getUserActivityAsync( this.userSlug );
		} finally {
			this.loadingActivity = false;
		}
	}

}
