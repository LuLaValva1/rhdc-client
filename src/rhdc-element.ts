import { LitElement } from 'lit';
import { RhdcNavEvent } from './events/rhdc-nav-event';
import { RhdcRequireLoginEvent } from './events/rhdc-login-event';
import { RhdcToastEvent } from './events/rhdc-toast-event';
import { RhdcPromptEvent } from './events/rhdc-prompt-event';
import { Nullable, Optional, Weak } from './util/types';
import { UserAuth } from './decorators/user-context';
import { Role } from './auth/roles';

export abstract class RhdcElement extends LitElement {

	protected navigate( route: string ) : void {
		this.dispatchEvent( RhdcNavEvent.create( route ) );
	}

	protected navAction( route: string ) : () => void {
		return this.navigate.bind( this, route );
	}

	protected redirect404() : void {
		this.dispatchEvent( new CustomEvent( 'rhdc-404-event', {
			bubbles: true,
			composed: true
		}));
	}

	protected toastInfo( summary: Nullable<string>, description: Weak<string> = undefined, icon: Optional<string> = undefined ) {
		this.dispatchEvent( RhdcToastEvent.info( summary, description, icon ) );
	}

	protected toastSuccess( summary: Nullable<string>, description: Weak<string> = undefined, icon: Optional<string> = undefined ) {
		this.dispatchEvent( RhdcToastEvent.success( summary, description, icon ) );
	}

	protected toastWarn( summary: Nullable<string>, description: Weak<string> = undefined, icon: Optional<string> = undefined ) {
		this.dispatchEvent( RhdcToastEvent.warn( summary, description, icon ) );
	}

	protected toastError( summary: Nullable<string>, description: Weak<string> = undefined, icon: Optional<string> = undefined ) {
		this.dispatchEvent( RhdcToastEvent.error( summary, description, icon ) );
	}

	protected requireLogin( userContext: Nullable<UserAuth>, route: Weak<string> = undefined, allowUnverified = false, allowRestricted = false ) : void {
		if( userContext ) {
			if( userContext.role === Role.Banned ) {
				this.toastError( 'Permission Denied', 'You are banned and cannot perform this action.' );
			} else if( !allowUnverified && userContext.role === Role.Unverified ) {
				this.toastError( 'Permission Denied', 'Your account is unverified and cannot perform this action until you verify your e-mail address.' );
			} else if( !allowRestricted && userContext.role === Role.Restricted ) {
				this.toastError( 'Permission Denied', 'Your account is restricted and cannot perform this action.' );
			} else if( route ) {
				this.navigate( route );
			}
		} else {
			this.dispatchEvent( RhdcRequireLoginEvent.create( route ) );
		}
	}

	protected requireLoginAction( userContext: Nullable<UserAuth>, route: Weak<string> = undefined, allowUnverified = false, allowRestricted = false ) : () => void {
		return this.requireLogin.bind( this, userContext, route, allowUnverified, allowRestricted );
	}

	protected confirmAsync( title: string, message: string, affirmText = 'Yes', rejectText = 'No' ) : Promise<boolean> {
		let resolver!: (_:boolean) => void;
		const promise = new Promise<boolean>( resolve => {
			resolver = resolve;
		});

		this.dispatchEvent( RhdcPromptEvent.create( title, message, {
			affirmText: affirmText,
			rejectText: rejectText,
			resolver: resolver
		}));

		return promise;
	}

}
