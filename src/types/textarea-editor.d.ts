declare module 'textarea-editor' {

	interface TAE_Prefix {
		value: string | ((text: string, line: number, ...args: any[] ) => string); // eslint-disable-line @typescript-eslint/no-explicit-any
		pattern: string;
		antipattern?: string | undefined;
	}

	interface TAE_Format {
		block?: boolean | undefined;
		multiline?: boolean | undefined;
		prefix: TAE_Prefix;
		suffix: TAE_Prefix | undefined;
	}

	class TextAreaEditor {

		constructor( textarea: HTMLTextAreaElement );

		range() : [start: number, end: number];
		range( range: number[] ) : TextAreaEditor;
		public range( range?: [start: number, end: number] | undefined ) : [start: number, end: number] | TextAreaEditor;

		public insert( text: string ) : TextAreaEditor;
		public focus() : TextAreaEditor;
		public toggle( format: string | TAE_Format, ...args: unknown[] ) : TextAreaEditor;
		public format( name: string | TAE_Format, ...args: unknown[] ) : TextAreaEditor;
		public unformat( name: string | TAE_Format ) : TextAreaEditor;
		public hasFormat( name: string | TAE_Format ) : boolean;

	}

	export default TextAreaEditor;

}
