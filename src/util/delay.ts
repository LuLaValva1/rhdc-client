export function delayAsync( milliseconds: number ) : Promise<void> {
	return new Promise( resolve => setTimeout( resolve, milliseconds ) );
}

export function nextCycle() : Promise<void> {
	return new Promise( resolve => setTimeout( resolve, 0 ) );
}

export function nextFrame() : Promise<void> {
	return new Promise( resolve => window.requestAnimationFrame( () => resolve() ) );
}
