import { ConstRef } from "./types";

export enum Side {
	Top,
	Bottom,
	Left,
	Right
}

export class DragUtil {

	public static getSideH( event: DragEvent, target: Element ) : Side {
		const bbox = target.getBoundingClientRect();
		const mid = (bbox.left + bbox.right) / 2;
		return (event.clientX <= mid) ? Side.Left : Side.Right;
	}
	
	public static getSideV( event: DragEvent, target: Element ) : Side {
		const bbox = target.getBoundingClientRect();
		const mid = (bbox.top + bbox.bottom) / 2;
		return (event.clientY <= mid) ? Side.Top : Side.Bottom;
	}
	
	public static showDropIndicator( target: HTMLElement, side: Side ) : void {
		switch( side ) {
			case Side.Top: target.style.boxShadow = 'inset 0 0.25rem 0 0 var(--sl-color-primary-500)'; break;
			case Side.Bottom: target.style.boxShadow = 'inset 0 -0.25rem 0 0 var(--sl-color-primary-500)'; break;
			case Side.Left: target.style.boxShadow = 'inset 0.25rem 0 0 0 var(--sl-color-primary-500)'; break;
			case Side.Right: target.style.boxShadow = 'inset -0.25rem 0 0 0 var(--sl-color-primary-500)'; break;
		}
	}

	public static clearDropIndicator( target: HTMLElement ) : void {
		target.style.boxShadow = 'none';
	}

	public static isBeforeH( event: DragEvent, target: Element ) : boolean {
		return (
			!!(DragUtil.getSideH( event, target ) === Side.Left) ===
			!!(window.getComputedStyle( target ).direction !== 'rtl')
		);
	}

	public static isBeforeV( event: DragEvent, target: Element ) : boolean {
		return DragUtil.getSideV( event, target ) === Side.Top;
	}

	public static withMovedElement<T>( array: ConstRef<T[]>, moveFrom: number, moveToBefore: number ) : T[] {
		const newArray: T[] = [];

		if( moveFrom === moveToBefore || moveFrom === moveToBefore - 1 ) {
			return [...array];
		}
		
		for( let i = 0; i < array.length; i++ ) {
			if( i === moveToBefore ) {
				newArray.push( array[moveFrom] );
			}

			if( i !== moveFrom ) {
				newArray.push( array[i] );
			}
		}

		if( moveToBefore === array.length ) {
			newArray.push( array[moveFrom] );
		}
		
		return newArray;
	}

}
