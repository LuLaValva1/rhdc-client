const KiB = 1024;
const MiB = KiB * 1024;
const GiB = MiB * 1024;
const TiB = GiB * 1024;

export function formatBytes( bytes: number ) {
	if( bytes < KiB ) {
		return `${bytes} bytes`;
	} else if( bytes < MiB ) {
		return `${(bytes / KiB).toFixed( 1 )} kiB`;
	} else if( bytes < GiB ) {
		return `${(bytes / MiB).toFixed( 1 )} MiB`;
	} else if( bytes < TiB ) {
		return `${(bytes / GiB).toFixed( 1 )} GiB`;
	} else {
		return `${(bytes / TiB).toFixed( 1 )} TiB`;
	}
}

export function formatUtcDate( date: Date ) : string {
	return date.toISOString().substring( 0, 10 );
}

export function formatLocalDate( date: Date ) : string {
	const year = date.getFullYear().toString();
	const month = ('0' + (date.getMonth() + 1)).slice( -2 );
	const day = ('0' + (date.getDate())).slice( -2 );
	return `${year}-${month}-${day}`;
}

export function getOrdinalSuffix( number: number ) : string {
	if( number % 100 >= 11 && number % 100 <= 13 ) {
		return 'th';
	}

	switch( number % 10 ) {
		case 1: return 'st';
		case 2: return 'nd';
		case 3: return 'rd';
		default: return 'th';
	}
}
