export default function generatePassword() : string {
	// generate a base64 password with 12 bytes (96 bits) of entropy
	const bytes = new Uint8Array( 16 );
	window.crypto.getRandomValues( bytes );

	let password = '';
	for( const byte of bytes ) {
		const c = byte & 63;
		if( c < 26 ) {
			password += String.fromCharCode( 65 + c );
		} else if( c < 52 ) {
			password += String.fromCharCode( 71 + c );
		} else if( c < 62 ) {
			password += String.fromCharCode( c - 4 );
		} else if( c === 62 ) {
			password += '+';
		} else if( c === 63 ) {
			password += '/';
		} else {
			throw new Error( 'Password generation error' );
		}
	}

	return password;
}
