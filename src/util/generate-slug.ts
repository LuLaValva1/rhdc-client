const s_replacedChars = new Set<string>([
	' ', ':', '/', '?', '#', '[', ']', '@', '!', '$',
	'&', '\'', '(', ')', '*', '+', ',', ';', '='
]);

export default function generateSlug( name: string ) : string {
	name = name.normalize( 'NFKD' ).toLowerCase().replace(
		/\p{Pc}|\p{Cc}|\p{Me}|\p{Cf}|\p{Lm}|\p{Sk}|\p{Mn}|\p{Mc}/gu, ''
	);

	let slug = '';
	for( let i = 0; i < name.length; i++ ) {
		if( s_replacedChars.has( name[i] ) ) {
			slug += '-';
			continue;
		}

		const code = name.codePointAt( i )!;
		if( code > 32 && code < 127 ) {
			slug += name[i];
		} else if( code >= 0xA0 ) {
			slug += '-';
		}
	}

	switch( slug.length ) {
		case 0:
			return '-';
		case 12:
		case 32:
			return /^[a-fA-F0-9]+$/.test( slug ) ? `x${slug}` : slug;
		case 36:
			return /^[a-fA-F0-9]{8}(?:-[a-fA-F0-9]{4}){3}-[a-fA-F0-9]{12}$/.test( slug ) ? `x${slug}` : slug;
		case 38:
			return /^\{[a-fA-F0-9]{8}(?:-[a-fA-F0-9]{4}){3}-[a-fA-F0-9]{12}\}$/.test( slug ) ? `x${slug}` : slug;
		default:
			return slug;
	}
}
