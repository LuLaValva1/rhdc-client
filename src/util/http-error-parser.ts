import { HttpError, HttpErrorType } from "../apis/http-client";
import { DEV_MODE } from "../config";

export function getErrorMessage( exception: unknown ) : string {
	if( !(exception instanceof HttpError) ) {
		console.error( exception );
		return 'Unexpected javascript error';
	}

	const httpError = exception as HttpError;
	if(
		httpError.type === HttpErrorType.Problem &&
		httpError.problem.status >= 400 &&
		httpError.problem.status < 500
	) {
		if( httpError.problem.detail ) {
			return httpError.problem.detail;
		} else if( httpError.problem.status === 401 ) {
			return 'Not Authorized';
		} else if( httpError.problem.status === 403 ) {
			return 'Insufficient permissions.';
		}
	}

	if( httpError.type === HttpErrorType.Basic ) {
		if( httpError.status.code === 0 ) {
			return 'Could not reach the romhacking.com server.';
		} else if( httpError.status.code === 403 ) {
			return 'Insufficient permissions.';
		} else if( httpError.status.code === 418 && DEV_MODE ) {
			return 'You are not authorized to access the development site. Did you mean to go to https://romhacking.com?';
		} else if( httpError.status.text ) {
			return httpError.status.text;
		}
	}

	return 'Unexpected server error.';
}
