import { Hack } from '../apis/hacks-api';
import { ConstArray, Weak } from './types';
import { toApiUrl } from '../util/url';
import { Gender, User } from '../apis/users-api';

export interface OgProperty {
	property: string;
	content: string;
}

export class OgThing {

	protected readonly _props: Readonly<OgProperty>[] = [];

	public constructor(
		title: string,
		type: string,
		images: string[],
		url: string,
		description: Weak<string> = undefined
	) {
		this._add( 'og:title', title );
		this._add( 'og:type', type );
		for( const img of images ) {
			this._add( 'og:image', img );
		}
		this._add( 'og:url', url );
		this._add( 'og:locale', 'en_US' );
		this._add( 'og:site_name', 'Romhacking.com' );
		if( description ) {
			this._add( 'og:description', description.length > 140 ? `${description.substring( 0, 137 )}...` : description );
		}
	}

	protected _add( property: string, content: string ) {
		this._props.push( Object.freeze({ property: property, content: content }) );
	}

	public get properties() : ConstArray<OgProperty> {
		return Object.freeze( this._props );
	}

}

export class OgHack extends OgThing {

	public constructor( hack: Hack ) {
		super(
			hack.title,
			'rhdc:hack',
			hack.screenshots.map( dl => toApiUrl( dl.directHref ) ),
			`${window.location.origin}/hack/${hack.urlTitle}`,
			hack.description
		);

		for( const video of hack.videos ) {
			this._add( 'og:video', video );
		}
	}

}

export class OgUser extends OgThing {

	public constructor( user: User ) {
		super(
			user.username,
			'profile',
			[ toApiUrl( `/v3/user/${user.username}/avatar` ) ],
			`${window.location.origin}/user/${user.username}`,
			user.bio
		);

		this._add( 'profile:username', user.username );
		if( user.gender === Gender.Masculine ) {
			this._add( 'profile:gender', 'male' );
		} else if( user.gender === Gender.Feminine ) {
			this._add( 'profile:gender', 'female' );
		}
	}

}

export class OpenGraph {

	static #managedMetadata : HTMLMetaElement[] = [];

	static clearData() : void {
		for( const meta of OpenGraph.#managedMetadata ) {
			meta.remove();
		}
		OpenGraph.#managedMetadata = [];
	}

	static setData( data: OgThing ) : void {
		OpenGraph.clearData();

		for( const prop of data.properties ) {
			const meta = document.createElement( 'meta' );
			meta.setAttribute( 'property', prop.property );
			meta.setAttribute( 'content', prop.content );
			OpenGraph.#managedMetadata.push( meta );
			document.head.appendChild( meta );
		}
	}

	static {
		for( const metaTag of document.querySelectorAll( 'head > meta' ) ) {
			const prop = metaTag.getAttribute( 'property' );
			if( prop && (prop.startsWith( 'og:' ) || prop.startsWith( 'profile:' )) ) {
				this.#managedMetadata.push( metaTag as HTMLMetaElement );
			}
		}
	}

}
