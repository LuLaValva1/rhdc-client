import MarkdownIt from 'markdown-it';
import StateInline from 'markdown-it/lib/rules_inline/state_inline';

function userRefParser( state: StateInline, silent: boolean ) : boolean {
	if( silent ) return false;

	const start = state.pos;
	if( state.src.substring( start, start + 2 ) !== '{{' ) return false;

	// Don't bother supporting nested user links. Just assume the next }} with a valid userId prefix is the closing tag
	
	const max = state.posMax;
	for( let i = start + 2; i < max - 26; i++ ) {
		if( state.src.charAt( i ) === '\n' ) return false;
		if( state.src.charAt( i ) !== '|' ) continue;
		if( state.src.substring( i + 25, i + 27 ) !== '}}' ) continue;

		const userId = state.src.substring( i + 1, i + 25 );
		if( !/^[a-fA-F0-9]{24}$/g.test( userId ) ) continue;

		const token = state.push( 'rhdc_user_open', 'rhdc-lazy-username', 1 );
		token.attrs = [[ 'user-id', userId ]];
		
		state.pos = start + 2;
		state.posMax = i;
		state.md.inline.tokenize( state );

		state.push( 'rhdc_user_close', 'rhdc-lazy-username', -1 );

		state.pos = i + 27;
		state.posMax = max;
		return true;
	}
	
	return false;
}

export default function( md: MarkdownIt ) {
	md.inline.ruler.push( 'rhdc-user', userRefParser );
}
