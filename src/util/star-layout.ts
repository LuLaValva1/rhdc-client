import { Nullable, Optional, Weak } from "./types";

export interface BasicCourse {
	name?: Optional<string>;
	courseId: number;
	starMask: number;
}

export interface BasicGroup {
	name: string;
	side: 'left' | 'right';
	courses: BasicCourse[];
}

export interface BasicStarLayout {
	$schema: 'https://parallel-launcher.ca/layout/basic-01/schema.json';
	skipFileSelect: boolean;
	groups: BasicGroup[];
	collectedStarIcon?: Weak<string>;
	missingStarIcon?: Weak<string>;
}

export interface AdvancedFormat {
	save_type?: Optional<'EEPROM' | 'SRAM' | 'FlashRAM' | 'MemPak' | 'Multi'>;
	num_slots: number;
	slots_start: number;
	slot_size: number;
	active_bit: number;
	checksum_offset: Nullable<number>;
}

export interface AdvancedData {
	offset: number;
	mask: number;
}

export interface AdvancedCourse {
	name?: Optional<string>;
	data: AdvancedData[];
}

export interface AdvancedGroup {
	name: string;
	side: 'left' | 'right' | 'top' | 'bottom';
	courses: AdvancedCourse[];
}

export interface AdvancedStarLayout {
	$schema: 'https://parallel-launcher.ca/layout/advanced-01/schema.json';
	format: AdvancedFormat;
	groups: AdvancedGroup[];
	collectedStarIcon?: Weak<string>;
	missingStarIcon?: Weak<string>;
}

export type StarLayout = BasicStarLayout | AdvancedStarLayout;

function countBits( starMask: number ) : number {
	let bitCount;
	for( bitCount = 0; starMask > 0; bitCount++ ) {
		starMask &= starMask - 1;
	}
	return bitCount;
}

export function countLayoutStars( layout: StarLayout ) : number {
	let starCount = 0;
	if( layout.$schema === 'https://parallel-launcher.ca/layout/advanced-01/schema.json' ) {
		for( const group of layout.groups ) {
			for( const course of group.courses ) {
				for( const span of course.data ) {
					starCount += countBits( span.mask );
				}
			}
		}
	} else {
		for( const group of layout.groups ) {
			for( const course of group.courses ) {
				starCount += countBits( course.starMask );
			}
		}
	}
	return starCount;
}

export function getMimeType( base64: string ) : Nullable<string> {
	const header = window.atob( base64.substring( 0, 12 ) );
	if( header.startsWith( 'BM' ) ) return 'image/bmp';
	if( header.startsWith( 'GIF89a' ) ) return 'image/gif';
	if( header.startsWith( '\u00FF\u00D8\u00FF' ) ) return 'image/gif';
	if( header.startsWith( '\u0089PNG\r\n\u001A\n' ) ) return 'image/png';
	if( header.startsWith( '<' ) ) return 'image/svg+xml';
	if( header.startsWith( 'P1' ) || header.startsWith( 'P4' ) ) return 'image/x-portable-bitmap';
	if( header.startsWith( 'P2' ) || header.startsWith( 'P5' ) ) return 'image/x-portable-graymap';
	if( header.startsWith( 'P3' ) || header.startsWith( 'P6' ) ) return 'image/x-portable-pixmap';
	if( header.startsWith( '! XPM2' ) || header.startsWith( '/* XPM */') ) return 'image/x-xpixmap';
	if( header.startsWith( '#define' ) ) return 'image/x-xbitmap';
	return null;
}
