export type Nullable<T> = ( T | null );
export type Optional<T> = ( T | undefined );
export type Weak<T> = ( T | null | undefined );
export type ConstRef<T> = ( T | Readonly<T> );
export type ConstArray<T> = Readonly<Readonly<T>[]>;
export type SparseArray<T> = Optional<T>[];

export type Brand<T, B> = T & { __brand: B };
export type HexId = Brand<string, 'HexId'>;
export type Uuid = Brand<string, 'Uuid'>;
export type UserId = Brand<string, 'UserId'>;

export type FileLike = Blob & { name: string };
