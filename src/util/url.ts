import { API_HOST, DEV_MODE } from '../config';
import { RHDC_DEV_KEY } from '../auth/dev-key';

export const getUrlPath = function( href: string ) : string {
	const url = new URL( href, API_HOST );
	return url.pathname + url.search + url.hash;
};

export const toApiUrl = function( href: string ) : string {
	if( DEV_MODE && RHDC_DEV_KEY ) {
		const url = new URL( getUrlPath( href ), API_HOST );
		if( url.search ) {
			url.search += '&rhdcDevKey=' + encodeURIComponent( RHDC_DEV_KEY );
		} else {
			url.search = '?rhdcDevKey=' + encodeURIComponent( RHDC_DEV_KEY );
		}
		return url.href;
	}

	return API_HOST + getUrlPath( href );
}
