import { html, css, CSSResultGroup } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { ImmutableDate, BasicTime } from '../util/time';
import { RhdcDateInput } from './rhdc-date';
import { RhdcTimeInput } from './rhdc-time';
import './rhdc-date';
import './rhdc-time';

@customElement( 'rhdc-date-time' )
export class RhdcDateTime extends RhdcElement {

	@property({ attribute: false })
	value!: Readonly<Date>;

	@property({ attribute: 'label', type: String })
	label = '';

	@property({ attribute: 'disabled', type: Boolean })
	disabled = false;

	@query( 'rhdc-date' )
	dateInput!: RhdcDateInput;

	@query( 'rhdc-time' )
	timeInput!: RhdcTimeInput;

	constructor() {
		super();
		this.value = ImmutableDate.today( false ).toDate();
	}

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: inline-flex;
				align-items: flex-end;
				gap: var(--sl-spacing-x-small);
			}

			rhdc-date {
				max-width: unset;
			}

			rhdc-time {
				margin: var(--sl-spacing-2x-small) 0;
			}
		`;
	}

	override render() : unknown {
		return html`
			<rhdc-date
				label=${this.label}
				.value=${new ImmutableDate( this.value, false )}
				?disabled=${this.disabled}
				@rhdc-change=${this.#onChange}
			></rhdc-date>
			<rhdc-time
				.value=${BasicTime.fromLocalDate( this.value )}
				?disabled=${this.disabled}
				required
				@rhdc-change=${this.#onChange}
			></rhdc-time>
		`;
	}

	#onChange( event: Event ) : void {
		this.value = this.dateInput.value.toDate( this.timeInput.value || new BasicTime( 0, 0 ) );
		event.stopPropagation();
		this.dispatchEvent( new CustomEvent( 'rhdc-change' ) );
	}

}
