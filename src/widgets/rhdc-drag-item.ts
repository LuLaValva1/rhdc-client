import { html, css, CSSResultGroup } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { RhdcElement } from '../rhdc-element';
import { Nullable, Optional } from '../util/types';
import { DragUtil, Side } from '../util/drag-util';

class DragInfo {
	
	public readonly source: RhdcDragItem;
	public readonly target: RhdcDragItem;
	public readonly before: boolean;
	public readonly innerEvent: DragEvent;

	public getData<T>() : Readonly<T> {
		return this.source.data as T;
	}

	public getTargetData<T>() : Readonly<T> {
		return this.target.data as T;
	}

	constructor( source: RhdcDragItem, target: RhdcDragItem, before: boolean, innerEvent: DragEvent ) {
		this.source = source;
		this.target = target;
		this.before = before;
		this.innerEvent = innerEvent;
	}

}

export class RhdcDragEvent extends CustomEvent<DragInfo> {}

let g_dragItem: Nullable<RhdcDragItem> = null;

@customElement( 'rhdc-drag-item' )
export class RhdcDragItem extends RhdcElement {

	@property({ attribute: 'horizontal', type: Boolean })
	horizontal = false;

	@property({ attribute: 'disabled', type: Boolean, reflect: true })
	disabled = false;

	@property({ attribute: 'before-only', type: Boolean })
	beforeOnly = false;

	@property({ attribute: false })
	data: unknown = undefined;

	@property({ attribute: false })
	groupId: Optional<string> = undefined;

	get #container() : HTMLElement {
		return this.shadowRoot!.getElementById( 'container' )!;
	}

	public get content() : Element[] {
		return this.shadowRoot!.querySelector( 'slot' )!.assignedElements()!;
	}

	public getInnerElement<T extends Element>() : T {
		return this.content[0] as T;
	}

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: contents;
			}

			div {
				padding: var(--sl-spacing-x-small);
				cursor: grab;
			}

			div[data-disabled] {
				cursor: no-drop;
			}
		`;
	}

	override render() : unknown {
		return html`
			<div
				id="container"
				part="base"
				?data-disabled=${this.disabled}
				draggable=${ifDefined( this.disabled ? undefined : 'true' )}
				@dragstart=${this.#onDragStart}
				@dragend=${this.#onDragEnd}
				@dragover=${this.#onDragOver}
				@dragleave=${this.#onDragLeave}
				@drop=${this.#onDrop}
			><slot></slot></div>
		`;
	}

	#onDragStart( event: DragEvent ) : void {
		if( this.disabled ) {
			event.stopPropagation();
			event.preventDefault();
			return;
		}

		g_dragItem = this;
		this.#container.style.opacity = '0.25';

		if( this.content[0] ) {
			const bbox = this.content[0].getBoundingClientRect();
			event.dataTransfer?.setDragImage( this.content[0], event.clientX - bbox.left, event.clientY - bbox.top );
		}
	}

	#onDragEnd() : void {
		g_dragItem = null;
		this.#container.style.opacity = '1.0';
	}

	#onDragOver( event: DragEvent ) : void {
		if( this.disabled || !g_dragItem || g_dragItem === this ) return;
		if( this.groupId && this.groupId !== g_dragItem.groupId ) return;

		DragUtil.showDropIndicator(
			this.#container,
			this.horizontal ?
				(this.beforeOnly ? Side.Left : DragUtil.getSideH( event, this.#container )) :
				(this.beforeOnly ? Side.Top : DragUtil.getSideV( event, this.#container ))
		);

		event.preventDefault();
	}

	#onDragLeave() : void {
		DragUtil.clearDropIndicator( this.#container );
	}

	#onDrop( event: DragEvent ) : void {
		if( g_dragItem ) {
			g_dragItem.#container.style.opacity = '1.0';
		}

		if( this.disabled || !g_dragItem || g_dragItem === this ) return;
		if( this.groupId && this.groupId !== g_dragItem.groupId ) return;
		DragUtil.clearDropIndicator( this.#container );

		const before = this.beforeOnly || (this.horizontal ? DragUtil.isBeforeH( event, this.#container ) : DragUtil.isBeforeV( event, this.#container ));
		const moveEvent = new RhdcDragEvent( 'rhdc-drag-event', {
			bubbles: true,
			composed: true,
			detail: new DragInfo( g_dragItem, this, before, event )
		});

		this.dispatchEvent( moveEvent );
		event.preventDefault();
	}

}
