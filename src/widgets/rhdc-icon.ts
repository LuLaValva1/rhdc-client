import { customElement } from 'lit/decorators.js';
import { property, state } from 'lit/decorators.js';
import { css, CSSResultGroup, LitElement, PropertyValues } from 'lit';
import { Nullable, Optional } from '../util/types';
import { getIconLibrary, watchIcon, unwatchIcon } from '@shoelace-style/shoelace/dist/components/icon/library';
import { SlIcon } from '@shoelace-style/shoelace';

const CACHEABLE_ERROR = Symbol();
const RETRYABLE_ERROR = Symbol();
type SVGResult = SVGSVGElement | typeof RETRYABLE_ERROR | typeof CACHEABLE_ERROR;

const s_iconCache = new Map<string,Promise<SVGResult>>;
const s_parser = new DOMParser();

async function resolveIcon( url: string ) : Promise<SVGResult> {
	let fileData : Response;
	try {
		fileData = await fetch( url, { mode: 'cors' } );
		if( !fileData.ok ) return fileData.status === 410 ? CACHEABLE_ERROR : RETRYABLE_ERROR;
	} catch{
		return RETRYABLE_ERROR;
	}

	try {
		const div = document.createElement( 'div' );
		div.innerHTML = await fileData.text();

		const svg = div.firstElementChild;
		if( svg?.tagName?.toLowerCase() !== 'svg' ) return CACHEABLE_ERROR;

		const doc = s_parser.parseFromString( svg.outerHTML, 'text/html' );

		const svgEl = doc.body.querySelector( 'svg' );
		if( !svgEl ) return CACHEABLE_ERROR;

		svgEl.part.add( 'svg' );
		return document.adoptNode( svgEl );
	} catch{
		return CACHEABLE_ERROR;
	}
}

@customElement( 'rhdc-icon' )
export class RhdcIcon extends LitElement {

	@property({ reflect: true })
	name?: string;

	@property({ reflect: true })
	library = 'default';

	@property()
	src?: string;

	@property()
	label = '';

	@state()
	private svg: Nullable<SVGElement> = null;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: inline-block;
				width: 1em;
				height: 1em;
				box-sizing: content-box;
			}

			svg {
				display: block;
				height: 100%;
				width: 100%;
			}
		`;
	}

	override connectedCallback() : void {
		super.connectedCallback();
		watchIcon( this as unknown as SlIcon );
	}

	override disconnectedCallback() : void {
		super.disconnectedCallback();
		unwatchIcon( this as unknown as SlIcon );
	}

	protected override firstUpdated( changedProperties: PropertyValues ) : void {
		super.firstUpdated( changedProperties );
		this.setIcon();
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );

		if( changedProperties.has( 'name' ) || changedProperties.has( 'src' ) || changedProperties.has( 'library' ) ) {
			this.setIcon();
		}

		if( changedProperties.has( 'label' ) ) {
			const hasLabel = typeof this.label === 'string' && this.label.length > 0;
			if( hasLabel ) {
				this.setAttribute( 'role', 'img' );
				this.setAttribute( 'aria-label', this.label );
				this.removeAttribute( 'aria-hidden' );
			} else {
				this.removeAttribute( 'role' );
				this.removeAttribute( 'aria-label' );
				this.setAttribute( 'aria-hidden', 'true' );
			}
		}
	}

	public async setIcon() : Promise<void> {
		const library = getIconLibrary( this.library );
		const url = this.#getUrl();

		if( !url ) {
			this.svg = null;
			this.#emit( 'sl-error' );
			return;
		}

		let iconResolver = s_iconCache.get( url );
		if( !iconResolver ) {
			iconResolver = resolveIcon( url );
			s_iconCache.set( url, iconResolver );
		}

		const svg = await iconResolver;
		if( svg === RETRYABLE_ERROR ) {
			s_iconCache.delete( url );
		}

		if( url !== this.#getUrl() ) return;

		if( svg === CACHEABLE_ERROR || svg === RETRYABLE_ERROR ) {
			this.svg = null;
			this.#emit( 'sl-error' );
		} else {
			this.svg = svg.cloneNode( true ) as SVGElement;
			library?.mutator?.( this.svg );
			this.#emit( 'sl-load' );
		}
	}

	protected override render() : unknown {
		return this.svg;
	}

	#getUrl() : Optional<string> {
		const library = getIconLibrary( this.library );
		if( this.name && library ) {
			return library.resolver( this.name );
		}
		return this.src;
	}

	#emit( event: string ) : void {
		this.dispatchEvent( new CustomEvent( event, { bubbles: true, cancelable: false, composed: true } ) );
	}

}
