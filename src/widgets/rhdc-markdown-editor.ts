import { css, CSSResultGroup, html, PropertyValues } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';
import { live } from 'lit/directives/live.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { RhdcElement } from '../rhdc-element';
import { RhdcDialog } from '../components/common/rhdc-dialog';
import { RhdcMarkdown } from './rhdc-markdown';
import { RhdcUserSelect } from '../components/common/rhdc-user-select';
import { User, UsersApi } from '../apis/users-api';
import { getErrorMessage } from '../util/http-error-parser';
import { Optional } from '../util/types';
import { nextCycle } from '../util/delay';
import SlTextarea from '@shoelace-style/shoelace/dist/components/textarea/textarea';
import SlDialog from '@shoelace-style/shoelace/dist/components/dialog/dialog';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import TextAreaEditor from 'textarea-editor';
import '@shoelace-style/shoelace/dist/components/textarea/textarea';
import '@shoelace-style/shoelace/dist/components/dialog/dialog';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '../components/common/rhdc-user-select';
import '../components/common/rhdc-dialog';
import './rhdc-markdown';

const INLINE_CODE_FORMAT = {
	block: false,
	multiline: false,
	prefix: {
		value: '`',
		pattern: '`',
		antipattern: '```'
	},
	suffix: {
		value: '`',
		pattern: '`',
		antipattern: '```'
	}
};

const RHDC_USER_FORMAT = {
	block: false,
	multiline: false,
	prefix: {
		value: '{{',
		pattern: '\\{\\{'
	},
	suffix: {
		value: (text: string, line: number, userId: string ) => `|${userId}}}`,
		pattern: '\\|[a-fA-F0-9]{24}\\}\\}'
	}
};

@customElement( 'rhdc-markdown-editor' )
export class RhdcMarkdownEditor extends RhdcElement {

	@property({ attribute: 'trusted', type: Boolean })
	trusted = false;

	@property({ attribute: 'label', type: String })
	label = '';

	@property({ attribute: 'placeholder', type: String })
	placeholder = '';

	@property({ attribute: 'rows', type: Number })
	rows = 4;

	@property({ attribute: 'disabled', type: Boolean, reflect: true })
	disabled = false;

	@property({ attribute: 'required', type: Boolean })
	required = false;

	@property({ attribute: 'spellcheck', type: Boolean })
	checkSpelling = false;

	@property({ attribute: 'enterkeyhint', type: String })
	enterkeyhint: 'enter' | 'done' | 'go' | 'next' | 'previous' | 'search' | 'send' | undefined

	@property({ attribute: 'value', type: String })
	value = '';

	@query( 'sl-textarea' )
	input!: SlTextarea;

	@query( '#link-dialog' )
	linkDialog!: RhdcDialog;

	@query( '#image-dialog' )
	imageDialog!: RhdcDialog;

	@query( '#user-dialog' )
	userDialog!: RhdcDialog;

	@query( 'sl-dialog' )
	previewDialog!: SlDialog;

	@query( 'rhdc-markdown' )
	preview!: RhdcMarkdown;

	@query( 'rhdc-user-select' )
	userSelector!: RhdcUserSelect;

	#editor: Optional<TextAreaEditor>;
	#textarea: Optional<SlTextarea>;

	public checkValidity() : boolean {
		return this.input.checkValidity();
	}

	public reportValidity() : boolean {
		return this.input.reportValidity();
	}

	public setCustomValidity( message: string ) : void {
		return this.input.setCustomValidity( message );
	}

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-2x-small);
				--icon-color: var(--rhdc-text-color);
				--preview-width: 800px;
			}

			.buttons {
				display: flex;
				flex-direction: row;
				flex-wrap: wrap;
				align-items: baseline;
				gap: var(--sl-spacing-2x-small);
				color: var(--icon-color);
			}

			.buttons > span {
				flex-grow: 1;
			}

			sl-button[hidden] {
				display: none;
			}

			rhdc-dialog {
				--width: 600px;
			}

			rhdc-dialog > div {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-small);
				width: 100%;
			}

			sl-dialog {
				min-width: min( 90vw, var(--preview-width) );
				max-width: min( 90vw, var(--preview-width) );
			}

			rhdc-markdown {
				width: 100%;
			}

			sl-button::part(label) {
				padding: 0 var(--sl-spacing-small);
			}

			sl-icon {
				font-size: 1.5em;
				vertical-align: middle;
			}

			sl-dialog {
				--width: var(--preview-width);
			}
		`;
	}

	override render() : unknown {
		return html`
			<label part="label" for="input" aria-hidden="${this.label ? 'false' : 'true'}">${this.label}</label>
			<div class="buttons">
				<sl-button @click=${this.#format.bind( this, 'bold' )}><sl-icon name="type-bold"></sl-icon></sl-button>
				<sl-button @click=${this.#format.bind( this, 'italic' )}><sl-icon name="type-italic"></sl-icon></sl-button>
				<sl-button @click=${this.#format.bind( this, 'strikethrough' )}><sl-icon name="type-strikethrough"></sl-icon></sl-button>
				<sl-button @click=${this.#format.bind( this, 'blockquote' )}><sl-icon name="blockquote-left"></sl-icon></sl-button>
				<sl-button @click=${this.#format.bind( this, 'orderedList' )}><sl-icon name="list-ol"></sl-icon></sl-button>
				<sl-button @click=${this.#format.bind( this, 'unorderedList' )}><sl-icon name="list-ul"></sl-icon></sl-button>
				<sl-button @click=${this.#inlineCode}><sl-icon name="code-slash"></sl-icon></sl-button>
				<sl-button @click=${this.#format.bind( this, 'code' )}><sl-icon name="code-square"></sl-icon></sl-button>
				<sl-button @click=${this.#addLink} ?hidden=${!this.trusted}><sl-icon name="link-45deg"></sl-icon></sl-button>
				<sl-button @click=${this.#addImage} ?hidden=${!this.trusted}><sl-icon name="card-image"></sl-icon></sl-button>
				<sl-button @click=${this.#addUserLink}><sl-icon name="person"></sl-icon></sl-button>
				<span></span>
				<sl-button @click=${this.#preview}><sl-icon name="eye"></sl-icon></sl-button>
			</div>
			<sl-textarea
				id="input"
				part="input"
				title=""
				.value=${live( this.value )}
				?disabled=${this.disabled}
				?required=${this.required}
				placeholder=${this.placeholder}
				rows=${this.rows}
				autocapitalize="off"
				autocorrect="todo"
				spellcheck="${this.checkSpelling ? 'true' : 'false'}"
				enterkeyhint=${ifDefined( this.enterkeyhint )}
				inputmode="text"
				@change=${this.#onChange}
				@input=${this.#onInput}
			></sl-textarea>
			<rhdc-dialog id="link-dialog" label="Add Link" persistence="weak" .confirmText=${'Apply'}>
				<div>
					<sl-input type="text"
						id="link-text"
						label="Link Text"
						placeholder="Link Text"
						autocapitalize="off"
						autocorrect="off"
						autocomplete="off"
						enterkeyhint="next"
						?spellcheck=${false}
						inputmode="text"
					></sl-input>
					<sl-input type="text"
						id="link-href"
						label="Link URL"
						placeholder="Link URL"
						autocapitalize="off"
						autocorrect="off"
						autocomplete="off"
						enterkeyhint="next"
						?spellcheck=${false}
						inputmode="url"
					></sl-input>
				</div>
			</rhdc-dialog>
			<rhdc-dialog id="image-dialog" label="Add Image" persistence="weak" .confirmText=${'Apply'}>
				<div>
					<sl-input type="text"
						id="image-alt"
						label="Alt Text"
						placeholder="Alt Text (Optional)"
						autocapitalize="off"
						autocorrect="off"
						autocomplete="off"
						enterkeyhint="next"
						?spellcheck=${false}
						inputmode="text"
						clearable
					></sl-input>
					<sl-input type="text"
						id="image-href"
						label="Image URL"
						placeholder="Image URL"
						autocapitalize="off"
						autocorrect="off"
						autocomplete="off"
						enterkeyhint="next"
						?spellcheck=${false}
						inputmode="url"
					></sl-input>
				</div>
			</rhdc-dialog>
			<rhdc-dialog id="user-dialog" label="Add User Link" persistence="weak" .confirmText=${'Apply'}>
				<div>
					<rhdc-user-select
						label="User"
						placeholder="Search for a user..."
						disabled
						@rhdc-change=${this.#userSelected}
					></rhdc-user-select>
					<sl-input type="text"
						id="user-placeholder"
						label="Username Placeholder"
						help-text="The text to display while the user information is being loaded"
						autocapitalize="off"
						autocorrect="off"
						autocomplete="off"
						enterkeyhint="next"
						?spellcheck=${false}
						inputmode="text"
					></sl-input>
				</div>
			</rhdc-dialog>
			<sl-dialog label="Preview">
				<rhdc-markdown ?trusted=${this.trusted}></rhdc-markdown>
			</sl-dialog>
		`;
	}

	protected override updated( changedProperties: PropertyValues ) : void {
		super.updated( changedProperties );
		
		const textarea = this.shadowRoot?.getElementById( 'input' ) as Optional<SlTextarea>;
		if( textarea === this.#textarea ) return;

		this.#textarea = textarea;
		this.#initEditor();
	}

	async #initEditor() : Promise<void> {
		if( !this.#textarea ) {
			this.#editor = undefined;
			return;
		}

		if( !this.#textarea.hasUpdated ) {
			await this.#textarea!.updateComplete;
		}

		const nativeInput = this.#textarea.shadowRoot?.querySelector( 'textarea' );
		this.#editor = nativeInput ? new TextAreaEditor( nativeInput ) : undefined;
	}

	#sync() : void {
		const nativeInput = this.input?.shadowRoot?.querySelector( 'textarea' );
		if( nativeInput ) this.input.value = nativeInput.value;
	}

	#onChange() : void {
		this.value = this.input.value;
		this.dispatchEvent( new CustomEvent( 'rhdc-change' ) );
	}

	#onInput() : void {
		this.value = this.input.value;
		this.dispatchEvent( new CustomEvent( 'rhdc-input' ) );
	}

	#format( format: string ) : void {
		this.#editor?.toggle( format );
		this.#sync();
		this.#onChange();
	}

	#inlineCode() : void {
		this.#editor?.toggle( INLINE_CODE_FORMAT );
		this.#sync();
		this.#onChange();
	}

	#addLink() : void {
		if( !this.#editor ) return;

		const nativeInput = this.input?.shadowRoot?.querySelector( 'textarea' );
		if( !nativeInput ) return;

		const replace = this.#editor.hasFormat( 'link' );
		if( replace ) {
			const scratch = document.createElement( 'textarea' );
			const tempEditor = new TextAreaEditor( scratch );
			tempEditor.range( this.#editor.range() );
			tempEditor.unformat( 'link' );
			this.#getInput( 'link-text' ).value = this.input.value.substring( ...tempEditor.range() );
		} else {
			this.#getInput( 'link-text' ).value = this.input.value.substring( ...this.#editor.range() );
		}

		this.#getInput( 'link-href' ).value = '';
		this.linkDialog.runAsync( async () => {
			if( replace ) this.#editor!.unformat( 'link' );
			this.input.setRangeText( this.#getInput( 'link-text' ).value || '', nativeInput.selectionStart, nativeInput.selectionEnd, 'select' );
			this.#editor!.format( 'link', this.#getInput( 'link-href' ).value || '' );
			this.#sync();
			this.#onChange();
		});
	}

	#addImage() : void {
		if( !this.#editor ) return;

		const nativeInput = this.input?.shadowRoot?.querySelector( 'textarea' );
		if( !nativeInput ) return;

		const replace = this.#editor.hasFormat( 'image' );
		if( replace ) {
			const scratch = document.createElement( 'textarea' );
			const tempEditor = new TextAreaEditor( scratch );
			tempEditor.range( this.#editor.range() );
			tempEditor.unformat( 'image' );
			this.#getInput( 'image-alt' ).value = this.input.value.substring( ...tempEditor.range() );
		} else {
			this.#getInput( 'image-alt' ).value = this.input.value.substring( ...this.#editor.range() );
		}

		this.#getInput( 'image-href' ).value = '';
		this.imageDialog.runAsync( async () => {
			if( replace ) this.#editor!.unformat( 'image' );
			this.input.setRangeText( this.#getInput( 'image-alt' ).value || '', nativeInput.selectionStart, nativeInput.selectionEnd, 'select' );
			this.#editor!.format( 'image', this.#getInput( 'image-href' ).value || '' );
			this.#sync();
			this.#onChange();
		});
	}

	#addUserLink() : void {
		if( !this.#editor ) return;

		const nativeInput = this.input?.shadowRoot?.querySelector( 'textarea' );
		if( !nativeInput ) return;

		const replace = this.#editor.hasFormat( RHDC_USER_FORMAT );
		if( replace ) {
			const scratch = document.createElement( 'textarea' );
			const tempEditor = new TextAreaEditor( scratch );
			tempEditor.range( this.#editor.range() );
			tempEditor.unformat( RHDC_USER_FORMAT );
			this.#getInput( 'user-placeholder' ).value = this.input.value.substring( ...tempEditor.range() );
		} else {
			this.#getInput( 'user-placeholder' ).value = this.input.value.substring( ...this.#editor.range() );
		}

		this.userSelector.value = '';
		this.userSelector.disabled = false;
		this.userDialog.runAsync( async () => {
			if( !this.userSelector.value ) return;
			
			let user: User;
			try {
				user = await UsersApi.getUserAsync( this.userSelector.value );
			} catch( exception: unknown ) {
				this.toastError( 'User not found', getErrorMessage( exception ) );
				throw exception;
			}

			if( replace ) this.#editor!.unformat( RHDC_USER_FORMAT );
			this.input.setRangeText( this.#getInput( 'user-placeholder' ).value || this.userSelector.value, nativeInput.selectionStart, nativeInput.selectionEnd, 'select' );
			this.#editor!.format( RHDC_USER_FORMAT, user.userId );
			this.#sync();
			this.#onChange();
		});
	}

	#preview() : void {
		this.preview.markdown = this.value;
		this.previewDialog.show();
	}

	async #userSelected() : Promise<void> {
		if( !this.userSelector.value ) return;
		this.#getInput( 'user-placeholder' ).value = this.userSelector.value;
		await nextCycle();
		this.#getInput( 'user-placeholder' )?.focus();
	}

	#getInput( id: string ) : SlInput {
		return this.shadowRoot!.getElementById( id ) as SlInput;
	}

}
