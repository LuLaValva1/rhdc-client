import { css, CSSResultGroup, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';
import { RhdcElement } from '../rhdc-element';
import RhdcUserMarkdownPlugin from '../util/rhdc-user-markdown-plugin';
import MarkdownIt from 'markdown-it';
import Token from 'markdown-it/lib/token';
import Renderer from 'markdown-it/lib/renderer';
import '../components/common/rhdc-lazy-username';

const safeParser = MarkdownIt({
	html: false,
	xhtmlOut: false,
	linkify: false,
	breaks: true
}).disable([ 'link', 'image' ]).use( RhdcUserMarkdownPlugin );


const trustedParser = MarkdownIt({
	html: false,
	xhtmlOut: false,
	linkify: true,
	breaks: true
}).use( RhdcUserMarkdownPlugin );

@customElement( 'rhdc-markdown' )
export class RhdcMarkdown extends RhdcElement {

	@property({ attribute: 'trusted', type: Boolean })
	trusted = false;

	@property({ attribute: false })
	markdown = '';

	static {
		const imageRenderer = trustedParser.renderer.rules.image!;
		trustedParser.renderer.rules.image = function( tokens: Token[], idx: number, options: MarkdownIt.Options, env: unknown, self: Renderer ) {
			tokens[idx].attrSet( 'referrerpolicy', 'origin' );
			tokens[idx].attrSet( 'decoding', 'async' );
			return imageRenderer( tokens, idx, options, env, self );
		};
	}

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				font-family: var(--sl-font-sans);
				color: var(--rhdc-text-color);
			}

			a {
				color: var(--sl-color-primary-600) !important;
			}

			blockquote {
				margin-left: 3px;
				border-left: 2px solid var(--sl-color-primary-800);
				padding-left: 4px;
			}

			hr {
				border: none;
				border-top: var(--sl-panel-border-width) solid var(--sl-panel-border-color);
				margin: 0 var(--sl-spacing-medium);
			}

			code, pre {
				font-family: var(--sl-font-mono);
				background-color: var(--sl-color-neutral-50);
				border: 1px solid var(--sl-color-neutral-200);
				border-radius: var(--sl-border-radius-medium);
				padding: 4px;
			}

			pre > code {
				border: none;
				padding: 0;
			}

		`;

	}

	override render() : unknown {
		const parsedHtml = (this.trusted ? trustedParser : safeParser).render( this.markdown );
		return html`${unsafeHTML( parsedHtml )}`;
	}

}
