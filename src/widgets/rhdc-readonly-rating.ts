import { css, CSSResultGroup, html, HTMLTemplateResult } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { map } from 'lit/directives/map.js';
import { range } from 'lit/directives/range.js';
import { styleMap } from 'lit/directives/style-map.js';
import { RhdcElement } from '../rhdc-element';
import './rhdc-icon';

/* The Shoelace rating component sl-rating has some performance problems.
 * For readonly ratings, use a more performant solution
 */

@customElement( 'rhdc-readonly-rating' )
export class RhdcHackCard extends RhdcElement {

	@property({ attribute: 'icon' })
	icon = 'star-fill';

	@property({ attribute: 'max' })
	max = 5;

	@property({ attribute: 'value' })
	value = 0;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: inline-flex;
				font-size: 1.2rem;
				gap: var(--sl-spacing-2x-small);
			}

			rhdc-icon {
				width: 1em;
				height: 1em;
			}

			rhdc-icon.empty {
				color: var(--sl-color-neutral-300);
			}

			div {
				position: relative;
			}

			div > rhdc-icon.empty {
				position: absolute;
				top: 0;
				left: 0;
			}
		`;
	}

	override render() : unknown {
		const iconRenderer = this.#renderIcon.bind( this );
		return html`${map( range( this.max ), iconRenderer )}`;
	}

	#renderIcon( index: number ) : HTMLTemplateResult {
		if( this.value >= index + 1 ) {
			return html`<rhdc-icon name="${this.icon}"></rhdc-icon>`;
		} else if( this.value <= index ) {
			return html`<rhdc-icon name="${this.icon}" class="empty"></rhdc-icon>`;
		}

		const fraction = index + 1 - this.value;
		const startInset = `${100 * (1 - fraction)}%`;
		const endInset = `${100 * fraction}%`;

		const leftStyle = styleMap({ clipPath: `inset( 0 ${endInset} 0 0)` });
		const rightStyle = styleMap({ clipPath: `inset( 0 0 0 ${startInset})` });
		return html`
			<div>
				<rhdc-icon style=${leftStyle} name="${this.icon}"></rhdc-icon>
				<rhdc-icon style=${rightStyle} name="${this.icon}" class="empty"></rhdc-icon>
			</div>
		`;
	}

}
