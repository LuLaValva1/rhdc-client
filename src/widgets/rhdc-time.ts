import { html, css, CSSResultGroup } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { BasicTime } from '../util/time';
import { Optional } from '../util/types';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/icon/icon';

function parseTime( time: string ) : Optional<BasicTime> {
	if( !time ) return undefined;
	const parts = time.split( ':' );
	if( parts.length < 2 ) return undefined;
	const hours = +parts[0].trim();
	const minutes = +parts[1].trim();
	if( Number.isNaN( hours ) || hours < 0 || hours >= 24 ) return undefined;
	if( Number.isNaN( minutes ) || hours < 0 || hours >= 60 ) return undefined;
	return new BasicTime( hours, minutes );
}

@customElement( 'rhdc-time' )
export class RhdcTimeInput extends RhdcElement {

	@property({ attribute: false })
	value: Optional<BasicTime>;

	@property({ attribute: false })
	defaultValue: Optional<BasicTime>;

	@property({ attribute: 'label', type: String })
	label = '';

	@property({ attribute: 'disabled', type: Boolean })
	disabled = false;

	@property({ attribute: 'readonly', type: Boolean })
	readonly = false;

	@property({ attribute: 'required', type: Boolean })
	required = false;

	@query( 'sl-input' )
	input!: SlInput;

	static override get styles() : CSSResultGroup {
		return css`:host { display: inline-block; }`;
	}

	override render() : unknown {
		return html`
			<sl-input type="time"
				part="input"
				.value=${this.value?.toString() || ''}
				.defaultValue=${this.defaultValue?.toString() || ''}
				label=${this.label}
				?disabled=${this.disabled}
				?readonly=${this.readonly}
				?required=${this.required}
				step="60"
				@sl-change=${this.#onChange}
			>
				<sl-icon slot="suffix" name="clock"></sl-icon>
			</sl-input>
		`;
	}

	public get stringValue() : string {
		return this.input.value;
	}

	public set stringValue( value: string ) {
		this.input.value = value;
		this.value = parseTime( this.input.value );
	}

	public get uncommittedValue() : Optional<BasicTime> {
		return parseTime( this.input.value );
	}

	public checkValidity() : boolean {
		return this.input.checkValidity();
	}

	public reportValidity() : boolean {
		return this.input.reportValidity();
	}

	public setCustomValidity( message: string ) : void {
		this.input.setCustomValidity( message );
	}

	public override focus( options?: Optional<FocusOptions> ) : void {
		super.focus( options );
		this.input.focus( options );
	}

	#onChange( event: Event ) : void {
		this.value = parseTime( this.input.value );
		this.dispatchEvent( new CustomEvent( 'rhdc-change' ) );
		event.stopPropagation();
	}

}
