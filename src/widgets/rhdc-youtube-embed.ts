import { css, CSSResultGroup, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';

const FEATURE_POLICY: Record<string,boolean> = {
	['accelerometer']: false,
	['ambient-light-sensor']: false,
	['autoplay']: false,
	['battery']: false,
	['camera']: false,
	['display-capture']: false,
	['document-domain']: false,
	['encrypted-media']: false,
	['execution-while-not-rendered']: false,
	['execution-while-out-of-viewport']: true,
	['fullscreen']: true,
	['gamepad']: false,
	['geolocation']: false,
	['gyroscope']: false,
	['layout-animations']: true,
	['legacy-image-formats']: true,
	['magnetometer']: false,
	['microphone']: false,
	['midi']: false,
	['navigation-override']: false,
	['oversized-images']: true,
	['payment']: false,
	['picture-in-picture']: true,
	['publickey-credentials-get']: true,
	['screen-wake-lock']: true,
	['speaker-selection']: false,
	['sync-xhr']: false,
	['unoptimized-images']: true,
	['unsized-media']: true,
	['usb']: false,
	['web-share']: true,
	['xr-spatial-tracking']: false
};

const FEATURE_POLICY_STRING = Object.keys( FEATURE_POLICY ).map(
	feature => `${feature} ${FEATURE_POLICY[feature] ? '\'src\'' : '\'none\'' }`
).join( '; ' );

@customElement( 'rhdc-youtube-embed' )
export class RhdcYoutubeEmbed extends RhdcElement {

	@property({ attribute: 'href', type: String })
	href = 'about:blank';

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				width: 432px;
				height: 243px;
			}

			iframe {
				width: 100%;
				height: 100%;
				border: none;
				object-position: center;
				object-fit: contain;
			}
		`;
	}

	override render() : unknown {
		return html`
			<iframe
				title="YouTube video"
				allow=${FEATURE_POLICY_STRING}
				allowfullscreen
				referrerpolicy="no-referrer"
				sandbox="allow-scripts allow-orientation-lock allow-same-origin"
				src=${this.href}
			></iframe>
		`;
	}

}
